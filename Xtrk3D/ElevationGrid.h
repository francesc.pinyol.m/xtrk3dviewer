#pragma once
#include "Structure.h"

namespace Xtrk3D {

	class ElevationGrid : public LineSegment
	{
	public:
		ElevationGrid(json& j, const glm::mat4& M=glm::mat4(), float scaleFactor=1.0);
		~ElevationGrid();
		static void CreateElevationGrid(json& j, const glm::vec3 translation, const float rotation, const float scalefaktor=1.0);
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkElevationGrid; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout,int count=0);
		virtual void add(const glm::vec3& point) {};
		virtual bool OnSelect(bool cm, std::string layer){return false;};
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) +" ElevationGrid "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void setEndpointZ(int pointNr, float value) { };
		virtual void dump(json& j) {};
	protected:
		int dimension[2]{ 0,0 };
		float spacing[2]{ 0,0 };
		std::vector<float>heights;
		float color[3]{ 0,0,0 };
		float transparancy;
		std::vector <vertex>V2;
		std::vector<GLuint>I2;
	};

	class IndexedFaceSet: public LineSegment
	{
	public:
		IndexedFaceSet(json& js, const glm::mat4& M = glm::mat4(), float scaleFactor = 1.0);
		~IndexedFaceSet() {};
		static void CreateIndexedFaceSet(json& jp, const glm::vec3 translation, const float rotation, const float scalefaktor = 1.0);
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkElevationGrid; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual void add(const glm::vec3& point) {};
		virtual bool OnSelect(bool cm, std::string layer) { return false; };
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " IndexedFaceSet "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void setEndpointZ(int pointNr, float value) { };
		virtual void dump(json& j) {};
	protected:
		float color[3]{ 0,0,0 };
		glm::vec3 EdgeColor;

		float transparancy;
		std::vector<GLuint>edgeIndexes;
		bool DrawEdges;
	};

	class Extrusion : public FilledSegment
	{
	public:
		Extrusion(json& js, const glm::mat4& M = glm::mat4(), float scaleFactor = 1.0);
		~Extrusion() {};
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkExtrusion; };
		virtual bool OnSelect(bool cm, std::string layer) { return false; };
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Extrusion "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void setEndpointZ(int pointNr, float value) { };
		virtual void dump(json& j) {};
	protected:
		float color[3]{ 0,0,0 };
		glm::vec3 EdgeColor;
		double Height;
	};

	class Cylinder: public LineSegment
	{
	public:
		Cylinder(json& js, const glm::mat4& M = glm::mat4(), float scaleFactor = 1.0);
		~Cylinder() {};
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkCylinder; };
		virtual bool OnSelect(bool cm, std::string layer) { return false; };
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Cylinder "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count);
		virtual void setEndpointZ(int pointNr, float value) { };
		virtual void dump(json& j) {};
	protected:
		float color[3]{ 0,0,0 };
		double Height;
		double Radius;
	};

	class ConeSegment : public LineSegment
	{
	public:
		ConeSegment(json& js, const glm::mat4& M = glm::mat4(), float scaleFactor = 1.0);
		~ConeSegment() {};
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkCylinder; };
		virtual bool OnSelect(bool cm, std::string layer) { return false; };
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Cylinder "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count);
		virtual void setEndpointZ(int pointNr, float value) { };
		virtual void dump(json& j) {};
	protected:
		float color[3]{ 0,0,0 };
		double Height;
		double Radius;
	};


	class IndexedLineSet : public PolyLineSegment
	{
	public:
		IndexedLineSet(json& js, const glm::mat4& M = glm::mat4(), float scaleFactor = 1.0);
		~IndexedLineSet() {};	
		virtual void Init(Track* parent);
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j) {};
	protected:
		float color[3]{ 0,0,0 };
		double height;
		bool hasHeight;
	};

	class BSpline : public PolyLineSegment
	{
	public:
		BSpline(json& js, const glm::mat4& M = glm::mat4(), float scaleFactor = 1.0);
		~BSpline() {};
		virtual void Init(Track* parent);
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j) {};
	protected:
		float color[3]{ 0,0,0 };
		float Bsplineparameter(double t);
		double height;
		bool hasHeight;
	};

	class Spline : public PolyLineSegment
	{
	public:
		Spline(json& js, const glm::mat4& M = glm::mat4(), float scaleFactor = 1.0);
		~Spline() {};
		virtual void Init(Track* parent);
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j) {};
	protected:
		float color[3]{ 0,0,0 };
		bool hasHeight;
		double height;
		inline float tj(float ti, glm::vec3 Pi, glm::vec3 Pj, float alfa) { return powf(glm::distance2(Pi, Pj), (alfa / 2)) + ti; };
		void CatmullRomSpline(glm::vec3 P0, glm::vec3 P1, glm::vec3 P2, glm::vec3 P3, float delta, std::vector<glm::vec3>& Drawpoints, float alfa);
	};

	class Use : public LineSegment
	{
	public:
		Use(json& js, const glm::mat4& M, std::map<std::string, Xtrk3D::XTrkObj*>& Defines, float scaleFactor = 1.0);
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual bool OnSelect(bool cm, std::string layer) { return false; };
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Use "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j) {};
		virtual void Init(Track* parent)
		{
			Defptr->initSurface(Xtrk3D::getTrackplan());
		};

	protected:
		XTrkObj* Defptr;
		std::string defineName;
		char SelectText[100];
	};


	class Group:public Segment
	{
	public:
		Group(json& js, const glm::mat4& M, float scalFactor = 1.0);
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count=0);
		~Group() { clearChildren(); clearSegments(); };
		virtual bool OnSelect(bool cm, std::string layer) { return false; };
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index); };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		void addChild(Xtrk3D::XTrkObj* Child);
		virtual void dump(json& j) {};
		virtual void Init(Track* parent)
		{
			if (!Children.empty()) {
				CG = glm::vec3();
				for (unsigned int i = 0; i < Children.size(); ++i) {
					Children[i]->initSurface(Xtrk3D::getTrackplan());
					CG += Children[i]->getCGposition();
				}
				CG /= Children.size();
			}
		};

	protected:
		std::vector< Xtrk3D::XTrkObj*>Children;
		void clearChildren() {
			for (auto& ii : Children)
				delete ii;
			Children.clear();
		};
	};

	class Overlay
	{
	public:
		Overlay(float scalefactor, TrackPlan* myPlan);
		void setScalefactor(float scalefactor) { _scalefactor = scalefactor; };
		void parseOverlay(json& jp);
		void ParseDefine(json& jp);
		void setDefaultDir(std::string path) {
			_defaultDir = path;
		};

	protected:
		std::string _defaultDir;
		Xtrk3D::XTrkObj* ParseDraw(json& jp, int index);
		Xtrk3D::Segment* ParseSegment(json& jp, const glm::mat4& M);
		void ParseSegment(json& jp, Xtrk3D::XTrkObj* Obj, int index);
		Xtrk3D::Segment* ParseGroup(json& js, const glm::mat4& M, float scaleFactor);
		float _scalefactor;
		TrackPlan* trackplan;
		void ParseId(json& jp);
	};


}

