#define _USE_MATH_DEFINES

#include <sstream>
#include <iomanip>
#include "misc.h"
#include "TrackPlan.h"
#include "turnout.h"
#include "Structure.h"

#include "Interface.h" //2020-09-24

#include <ctime>
#include <ratio>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;
using namespace std::chrono;

//using namespace tinyxml2;
namespace Xtrk3D {
	//							#Name		Gauge	ratio	RailH	RailW	TieL	TieH	TieW	TieCC	embTop	embBot	embH	B		H		E	L
	const std::vector<float>N_param = { 9, 160, 1.4f, 0.7f, 15.9f, 0.6f, 1.9f, 4.4f, 20.3f, 27.8f, 1.9f, 27, 37, 6, 126 };
	const std::vector<float>Nn3_param = { 6.5f, 160, 1.0, 0.5f, 13.4f, 0.6f, 1.9f, 4.4f, 17.8f, 25.3f, 1.9f, 22, 26, 6, 126 };
	const std::vector<float>HO_param = { 16.5f, 87, 2.0f, 0.9f, 29.1f, 1.1f, 3.4f, 8, 37.2f, 50.9f, 3.4f, 48, 65, 11, 231 };
	const std::vector<float>HOn3_param = { 10.5f, 87, 1.8f, 0.8f, 23.1f, 1.1f, 3.4f, 8, 31.2f, 44.9f, 3.4f, 38, 48, 11, 231 };
	const std::vector<float>HOn30_param = { 9, 87, 1.4f, 0.7f, 21.6f, 1.1f, 3.4f, 8, 29.7f, 43.4f, 3.4f, 38, 48, 11, 231 };
	const std::vector<float>HOn2_param = { 7.0f, 87, 1.4f, 0.7f, 19.6f, 1.1f, 3.4f, 8, 27.7f, 41.5f, 3.4f, 38, 48, 11, 231 };
	const std::vector<float>HOm_param = { 12, 87, 1.8f, 0.8f, 24.6f, 1.1f, 3.4f, 8, 32.7f, 46.4f, 3.4f, 38, 48, 11, 231 };
	const std::vector<float>HOe_param = { 9, 87, 1.4f, 0.7f, 21.6f, 1.1f, 3.4f, 8, 29.7f, 43.4f, 3.4f, 38, 48, 11, 231 };
	const std::vector<float>HOf_param = { 6.5f, 87, 1.0f, 0.5f, 19.1f, 1.1f, 3.4f, 8, 27.2f, 40.9f, 3.4f, 38, 48, 11, 231 };
	const std::vector<float>P4_param = { 18.83f, 87, 2.0f, 0.9f, 33.3f, 1.3f, 3.9f, 9.2f, 42.5f, 58.2f, 3.9f, 48, 65, 11, 231 };
	const std::vector<float>EM_param = { 18.2f, 76, 2.0f, 0.9f, 32.6f, 1.3f, 3.9f, 9.2f, 41.8f, 57.6f, 3.9f, 48, 65, 11, 231 };
	const std::vector<float>OO_param = { 16.5f, 76, 2.0f, 0.9f, 30.9f, 1.3f, 3.9f, 9.2f, 40.1f, 55.9f, 3.9f, 48, 65, 11, 231 };
	const std::vector<float>OO9_param = { 9, 76, 1.8f, 0.8f, 23.4f, 1.3f, 3.9f, 9.2f, 32.6f, 48.4f, 3.9f, 38, 48, 11, 231 };
	const std::vector<float>O_param = { 31.75f, 48, 3.0f, 1.3f, 54.7f, 2.1f, 6.3f, 14.6f, 69.3f, 94.3f, 6.3f, 94, 120, 22, 448 };
	const std::vector<float>On3_param = { 19, 48, 2.5f, 1.1f, 42, 2.1f, 6.3f, 14.6f, 56.6f, 81.6f, 6.3f, 74, 90, 22, 448 };
	const std::vector<float>On30_param = { 16.5f, 48, 2.5f, 1.1f, 39.4f, 2.1f, 6.3f, 14.6f, 54, 79, 6.3f, 74, 90, 22, 448 };
	const std::vector<float>On2_param = { 12.7f, 48, 2.0f, 0.9f, 35.6f, 2.1f, 6.3f, 14.6f, 50.2f, 75.2f, 6.3f, 74, 90, 22, 448 };
	const std::vector<float>On18_param = { 9, 48, 1.8f, 0.8f, 31.9f, 2.1f, 6.3f, 14.6f, 46.5f, 71.5f, 6.3f, 74, 90, 22, 448 };
	const std::vector<float>P48_param = { 29.9f, 48, 3.0f, 1.3f, 52.8f, 2.1f, 6.3f, 14.6f, 67.4f, 92.4f, 6.3f, 74, 90, 22, 448 };
	const std::vector<float>On14mm_param = { 14, 48, 2.0f, 0.9f, 39.3f, 2.3f, 6.9f, 16.1f, 55.4f, 83, 6.9f, 74, 90, 22, 448 };
	const std::vector<float>S_param = { 22.2f, 64, 2.5f, 1.1f, 39.4f, 1.6f, 4.7f, 10.9f, 50.3f, 69.1f, 4.7f, 66, 87, 15, 315 };
	const std::vector<float>Sn3_param = { 14.3f, 64, 2.5f, 1.1f, 31.5f, 1.6f, 4.7f, 10.9f, 42.4f, 61.2f, 4.7f, 53, 64, 15, 315 };
	const std::vector<float>Sn30_param = { 11.91f, 64, 2.5f, 1.1f, 29.1f, 1.6f, 4.7f, 10.9f, 40, 58.8f, 4.7f, 53, 64, 15, 315 };
	const std::vector<float>Sn2_param = { 9, 64, 1.8f, 0.8f, 26.2f, 1.6f, 4.7f, 10.9f, 37.1f, 55.9f, 4.7f, 53, 64, 15, 315 };
	const std::vector<float>TT_param = { 12, 120, 1.8f, 0.8f, 21.2f, 0.8f, 2.5f, 5.8f, 27, 37, 2.5f, 36, 48, 7, 168 };
	const std::vector<float>TTn3_param = { 9, 120, 1.8f, 0.8f, 18.2f, 0.8f, 2.5f, 5.8f, 24, 34, 2.5f, 28, 34, 7, 168 };
	const std::vector<float>TTi_param = { 4.5f, 120, 1.0f, 0.5f, 13.7f, 0.8f, 2.5f, 5.8f, 19.5f, 29.5f, 2.5f, 28, 34, 7, 168 };
	const std::vector<float>Z_param = { 6.5f, 220, 1.0f, 0.5f, 11.5f, 0.5f, 1.4f, 3.2f, 14.7f, 20.1f, 1.4f, 20, 27, 4, 91 };
	const std::vector <float>One_param = { 45, 32, 3.8f, 1.7f, 79.3f, 3.1f, 9.4f, 21.9f, 101.2f, 138.7f, 9.4f, 130, 165, 28, 550 };
	const std::vector<float>G_param = { 45, 22.5f, 3.8f, 1.7f, 93.8f, 4.4f, 13.3f, 31.1f, 125, 178.3f, 13.3f, 184, 235, 32, 600 };
	const std::vector<float>Gn15_param = { 16.5f, 22.5f, 2.5f, 1.1f, 65.4f, 4.4f, 13.3f, 31.1f, 96.5f, 149.8f, 13.3f, 146, 178, 32, 600 };
	const std::vector<float>Gn3_param = { 45, 22.5f, 3.8f, 1.7f, 100, 5, 15, 35, 135, 195, 15, 146, 178, 32, 600 };
	const std::vector<float>F_param = { 70.61f, 20, 3.8f, 1.7f, 124.7f, 4.9f, 14.8f, 34.4f, 159.2f, 218.2f, 14.8f, 184, 235, 32, 600 };
	const std::vector<float>LEGO_param = { 37.6f, 24, 3.8f, 1.7f, 83.4f, 4.2f, 12.5f, 29.2f, 112.6f, 162.6f, 12.5f, 184, 235, 32, 600 };
	const std::vector <float>Eight_param = { 190.5f, 8, 6.3f, 2.5f, 328, 12.5f, 37.5f, 87.5f, 415.5f, 565.5f, 37.5, 517, 660, 40, 700 };
	const std::vector<float>T_param = { 3.2f, 450, 1.0f, 0.5f, 5.6f, 0.2f, 0.7f, 1.6f, 7.2f, 9.9f, 0.7f, 10, 12, 3, 48 };
	const std::vector<float>RMC_param = { 140, 10, 6.3f, 2.5f, 249.7f, 10, 30, 70, 319.7f, 439.7f, 30, 517, 660, 40, 700 };
	const std::vector<float>DEMO_param = { 9, 160, 1.4f, 0.7f, 15.8f, 0.6f, 1.9f, 4.4f, 20.2f, 27.7f, 1.9f, 27, 37, 6, 126 };

	void Scale::Init() {
		Param = HO_param;
	}

	TrackPlan::TrackPlan()
		: metric(true)
		, ColorByLayer(false)
		, DrawStruct(false)
		, TunnelMode(1)
		, DrawElevations(true)
		//	, TE_version(0)
		, benchworkZeroLevel(-2.0f)
	{
		Dimensions["N"] = N_param;
		Dimensions["Nn3"] = Nn3_param;
		Dimensions["Nm"] = Nn3_param;
		Dimensions["N(UK)"] = N_param;
		Dimensions["N(JP)"] = N_param;
		Dimensions["2mm(FS)"] = N_param;
		Dimensions["HO"] = HO_param;
		Dimensions["HOn3.5"] = HOn3_param;
		Dimensions["HOn3"] = HOn3_param;
		Dimensions["HOn30"] = HOn30_param;
		Dimensions["HOn2"] = HOn2_param;
		Dimensions["HOm"] = HOm_param;
		Dimensions["HOe"] = HOe_param;
		Dimensions["HOf"] = HOf_param;
		Dimensions["P4"] = P4_param;
		Dimensions["EM"] = EM_param;
		Dimensions["OO"] = OO_param;
		Dimensions["OO9"] = OO9_param;
		Dimensions["O"] = O_param;
		Dimensions["On3.5"] = On3_param;
		Dimensions["On3"] = On3_param;
		Dimensions["On30"] = On30_param;
		Dimensions["On2"] = On2_param;
		Dimensions["On18"] = On18_param;
		Dimensions["P48"] = P48_param;
		Dimensions["O(Fine)"] = O_param;
		Dimensions["O(SpecialFine)"] = O_param;
		Dimensions["ScaleSeven"] = O_param;
		Dimensions["On14mm"] = On14mm_param;
		Dimensions["S"] = S_param;
		Dimensions["Sn3.5"] = Sn3_param;
		Dimensions["Sn3"] = Sn3_param;
		Dimensions["Sn30"] = Sn30_param;
		Dimensions["Sn2"] = Sn2_param;
		Dimensions["TT"] = TT_param;
		Dimensions["TTn3.5"] = TTn3_param;
		Dimensions["TTi"] = TTi_param;
		Dimensions["Z"] = Z_param;
		Dimensions["1"] = One_param;
		Dimensions["1/32"] = One_param;
		Dimensions["G"] = G_param;
		Dimensions["Gn15"] = Gn15_param;
		Dimensions["Gn3"] = Gn3_param;
		Dimensions["F"] = F_param;
		Dimensions["LEGO"] = LEGO_param;
		Dimensions["8"] = Eight_param;
		Dimensions["T"] = T_param;
		Dimensions["RMC"] = RMC_param;
		Dimensions["DEMO"] = DEMO_param;
	}

	TrackPlan::~TrackPlan()
	{
		clear();
	}

	void TrackPlan::clear()
	{
		for (auto &ii : Tracks)
			delete ii.second;
		Tracks.clear();
		PointNodeList.clear();
		Layers.clear();
		for (auto &ii : Objects)
			delete ii;
		Objects.clear();
		for (auto &ii : DefinesMap)
			delete ii.second;
		DefinesMap.clear();
		for (auto &ii :OverlayObjects)
		delete ii;
		OverlayObjects.clear();

		//	TE_version = 0;
	}

	void TrackPlan::prepareLayout()
	{
		createPointList();
		calculateZValues();
		for (auto &ii : Tracks) {
			ii.second->setSegmentZ();
			ii.second->param = (Dimensions.count(ii.second->scale) > 0) ? Dimensions[ii.second->scale] : Dimensions["HO"];
			ii.second->initSurface(nullptr);
		}
	}

	void TrackPlan::dump(nlohmann::json& j)
	{

		for (auto &ii : Tracks)
		{
			int i = ii.first;
			int j = 0;
		}
		
		std::map<int, Xtrk3D::XTrkObj*>sortedList;
		for (auto &ii : Tracks)
			sortedList[(int)ii.first]=ii.second;
		for (auto &ii : Objects)
			sortedList[(int)ii->index] = ii;
		for (auto &ii : sortedList)
		{
			json jj;
			ii.second->dump(jj);
			j["TrackPlan"].push_back(jj);
		}

	}

	void TrackPlan::reCalculateTrackObjects()
	{
		for (auto &ii : Tracks) {
			ii.second->param = (Dimensions.count(ii.second->scale) > 0) ? Dimensions[ii.second->scale] : Dimensions["HO"];
			ii.second->initSurface(nullptr);
		}
	}


	//////////////////
	void TrackPlan::createPointList()
		//create list of pointNode from Endpoints in std::map<int,Track*>Tracks
		//endp[n] set to pointNode index in PointNodeList
	{
		for (auto &Node1 : Tracks)
		{
			for (unsigned int index_Node1 = 0; index_Node1 < Node1.second->endp.size(); ++index_Node1)
			{
				endPoint* T_elem = &(Node1.second->endp[index_Node1]);
				//			if (T_elem->version > TE_version)
				//				TE_version = T_elem->version;
				if (T_elem->Node2 < 0)
				{
					T_elem->PointListIndex = PointNodeList.size();
					PointNodeList.push_back(PointNode(Node1.first, index_Node1, -1, -1, T_elem->pos, T_elem->Z_def));
				}
				else {
					Track* Node2 = Tracks[T_elem->Node2];
					for (unsigned int index_Node2 = 0; index_Node2 < Node2->endp.size(); ++index_Node2)
					{
						if (Node2->endp[index_Node2].Node2 == Node1.first)
							if (glm::length(Node2->endp[index_Node2].pos - T_elem->pos) < scale.TieL())
							{
								T_elem->Node2index = index_Node2;
								if (Node2->getObjType() == XtrkTurntable) {
									T_elem->isOnTurnTable = true;
								}
								if (Node1.second->visibility != 10 && Node2->visibility == 10) {
									T_elem->isBridgeEdge = true;
								}
								auto testvar = glm::length(Node2->endp[index_Node2].pos - T_elem->pos);
								if (fabs(testvar) > 0.0001)
									Node2->endp[index_Node2].pos = T_elem->pos;  // there is a gap between tracks, let endp be same.
								if (Node1.first < (int)Node2->index) {
									T_elem->PointListIndex = PointNodeList.size();
									Node2->endp[index_Node2].PointListIndex = PointNodeList.size();
									PointNodeList.push_back(PointNode(Node1.first, index_Node1, Node2->index, index_Node2, T_elem->pos, T_elem->Z_def));
								break;

								}
							}
					}
				}
			}
		}
		for (unsigned int i = 0; i < PointNodeList.size(); ++i)
		{
			std::pair<int, int> N1 = PointNodeList[i].Nodes[0];
			makeNeighbours(N1, i);
			N1 = PointNodeList[i].Nodes[1];
			if (N1.first < 0)
				continue;
			makeNeighbours(N1, i);
		}
	}

	void TrackPlan::makeNeighbours(std::pair<int, int>Node, int i)
	{
		Track* Node1 = Tracks[Node.first];
		int T = Node.second;

		for (unsigned int j = 0; j < Node1->endp.size(); ++j)
		{
			if (j == T)
				continue;
			float L = (float)Node1->getDistance(T, j);
			int S = Node1->endp[j].PointListIndex;
			PointNodeList[i].neighbours.push_back(std::pair<int, float>(S, L));
		}
	}

	std::pair<double, std::list<std::pair<int, double>>> TrackPlan::Dijkstra(int start)
	{
		std::priority_queue<std::pair<double, int>, std::vector<std::pair<double, int>>, std::greater<std::pair<double, int>>> Q;
		std::priority_queue<std::pair<double, int>>Dest; //<slope,index>
		std::map<int, std::pair<double, int>>tentative; //<cost, back_pointer>

		tentative[start] = std::make_pair(0, -1);
		Q.push(std::make_pair(0, start));

		while (!Q.empty()) {
			int Current = Q.top().second;
			double dist = Q.top().first;
			Q.pop();
			if (tentative.count(Current) > 0) //occurances on the queue added eartlier will have greater distances
				if (dist > tentative[Current].first)
					continue;
			if (PointNodeList[Current].Z_def && Current != start) {
				Dest.push(std::make_pair(fabs(PointNodeList[Current].pos.z - PointNodeList[start].pos.z) / tentative[Current].first, Current));
				//Don't proceed past a defined z value
				continue;
			}
			for (std::vector<std::pair<int, float>>::iterator iN = PointNodeList[Current].neighbours.begin(); iN != PointNodeList[Current].neighbours.end(); ++iN) {
				if (iN->first < 0)
					continue;
				if (tentative.count(iN->first) > 0)
					if (tentative[iN->first].first <= dist + iN->second) //2023-03-23
						continue;
				if (PointNodeList[iN->first].Z_def && PointNodeList[Current].Z_def)
					continue;
				tentative[iN->first] = std::make_pair(dist + iN->second, Current);
				Q.push(std::make_pair(tentative[iN->first].first, iN->first));
			}
		}
		std::list<std::pair<int, double>> path;
		if (Dest.empty()) {
			return std::make_pair(-1, path);
		}
		else {
			int Current = Dest.top().second;
			float slope = 0.0;
			if (PointNodeList[start].Z_def)
				slope = (PointNodeList[Current].pos.z - PointNodeList[start].pos.z) / (float)tentative[Current].first;
			while (Current >= 0) {
				path.push_front(std::make_pair(Current, PointNodeList[start].pos.z + slope * tentative[Current].first));
				Current = tentative[Current].second;
			}
			return std::make_pair(Dest.top().first, path);
		}
	}

	double TrackPlan::calculateDistToCurve(int pointId, int TrackId)
	{
		if (!Tracks[TrackId]->isStraightTrack())
			return 0;
		PointNode *Node1 = &PointNodeList[pointId];
		int obj1 = Node1->Nodes[0].first;
		int NextTrack;
		int NextIndex;
		if (TrackId == obj1) {
			NextTrack = Node1->Nodes[1].first;
			NextIndex = Node1->Nodes[1].second;
		}
		else {
			NextTrack = obj1;
			NextIndex = Node1->Nodes[0].second;
		}
		if (NextTrack < 0)
			return Tracks[obj1]->param.ExtendL();
		if (!Tracks[NextTrack]->isStraightTrack())
			return 0;
		double  dist = Tracks[NextTrack]->len;
		NextIndex = (NextIndex == 0 ? 1 : 0);
		if (Tracks[NextTrack]->endp.size() > 1) {
			dist += calculateDistToCurve(Tracks[NextTrack]->endp[NextIndex].PointListIndex, NextTrack);
		}
		return dist;
	}


	std::vector<endPoint*> TrackPlan::PointNodeToEndPoint(PointNode* Node)
	{
		int obj1 = Node->Nodes[0].first;
		int ind1 = Node->Nodes[0].second;
		int obj2 = Node->Nodes[1].first;
		int ind2 = Node->Nodes[1].second;
		std::vector<endPoint*> E;
		if (obj1 >= 0 && ind1 >= 0) {
			E.push_back(&Tracks[obj1]->endp[ind1]);
		}
		if (obj2 >= 0 && ind2 >= 0) {
			E.push_back(&Tracks[obj2]->endp[ind2]);
		}
		return E;
	}

	bool TrackPlan::getCGPosition(int index, glm::vec3& pos)
	{
		if (index >= 0)
			if (Tracks.count(index) > 0)
			{
				pos = Tracks[index]->getCGposition();
				return true;
			}
		return false;
	}


	glm::vec3 TrackPlan::averagePos(std::vector<endPoint>& endPoints, std::vector<int>& index)
	{
		if (index.empty())
			return glm::vec3();
		glm::vec3 m(endPoints.at(index[0]).pos);
		if (index.size() > 1) {
			for (unsigned int i = 1; i < index.size(); ++i) {
				m += endPoints.at(index[i]).pos;
			}
			m /= index.size();
		}
		return m;
	};

	void TrackPlan::setPointNodeZValue(PointNode *Node, double t_z)
	{
		Node->Z_def = 2;
		Node->pos.z = (float)t_z;
		for (auto &pE : PointNodeToEndPoint(Node))
		{
			if (pE != nullptr) {
				pE->pos.z = Node->pos.z;
				pE->Z_def = 2;
			}
		}
	}

	void TrackPlan::setNodeZ(int nodeID, double t_z, std::list<int>& z_list)
	{
		PointNode *Node1 = &PointNodeList[nodeID];
		if (Node1->Z_def && 0x7 != 0)
			return;
		setPointNodeZValue(Node1, t_z);

		for (unsigned int i = 0; i < 2; ++i) {
			if (Node1->Nodes[i].first < 0) //end node
				continue;
			std::vector<endPoint> endPoints = Tracks[Node1->Nodes[i].first]->endp;
			if (endPoints.size() > 2) {
				std::vector<int>Zdef[2];  //Nodes with z value set
				std::vector<int>Zundef[2];  //Nodes with z value set

				double angle1 = endPoints.begin()->Normal;
				for (unsigned int i = 0; i < endPoints.size(); ++i) {
					double diff = fabs(endPoints[i].Normal - angle1);
					bool side = (diff - ((int)(diff / 360)) * 360) < 90;
					endPoints[i].Z_def > 0 ?
						Zdef[side ? 0 : 1].push_back(i) :
						Zundef[side ? 0 : 1].push_back(i);
				}
				if (Zdef[0].size() > 0 && Zdef[1].size() > 0) {
					glm::vec3 m1(averagePos(endPoints, Zdef[0]));
					glm::vec3 m2(averagePos(endPoints, Zdef[1]));
					float deltaZ = m2.z - m1.z;
					float len = glm::length(m2.xy - m1.xy);
					//modify(Zundef[0],m2,-deltaZ,len);
					for (auto &ii : Zundef[0])
					{
						auto index = endPoints[ii].PointListIndex;
						auto p = endPoints[ii].pos;
						float d = glm::length(p.xy - m2.xy) / len;
						float z2 = m2.z - d * deltaZ;
						z_list.push_back(index);
						setPointNodeZValue(&PointNodeList[index], z2);
					}
					//modify(Zundef[1],m1,deltaZ,len);
					for (auto &ii : Zundef[1])
					{
						auto index = endPoints[ii].PointListIndex;
						auto p = endPoints[ii].pos;
						float d = glm::length(p.xy - m1.xy) / len;
						float z2 = d * deltaZ + m1.z;
						z_list.push_back(index);
						setPointNodeZValue(&PointNodeList[index], z2);
					}
				}
			}
		}
	}

	void TrackPlan::calculateZValues()
	{
		// Endpoint (T,E) Calculate all z-values between defined values.
		// find shortest distance using Dijkstra algoritm to get steepest slope

		// turnouts didn�t work out well
		// some issue with curved turnouts since legs are of so different length
		// temporary fix: set distance of turnouts to 0.01 in TrackPlan::makeNeighbours
		// permanent fix 2020-08-23: modified function setNodeZ to set all other end nodes when opposide nodes are set and update z_list accordingly
		// Turnout::getDistance also modified to get better distance between legs in turnouts

		std::list<int>z_list;
		for (unsigned int i = 0; i < PointNodeList.size(); ++i)
		{
			if (((PointNodeList[i].Z_def & 0x7) == 1) || ((PointNodeList[i].Z_def & 0x7) == 2)) {
				z_list.push_back(i);
			}
			auto d1 = calculateDistToCurve(i, PointNodeList[i].Nodes[0].first);
			if (PointNodeList[i].Nodes[1].first >= 0)
				d1 = fmin(d1, calculateDistToCurve(i, PointNodeList[i].Nodes[1].first));
			PointNodeList[i].distToCurve = d1;

			PointNode *Node1 = &PointNodeList[i];
			for (auto &pE : PointNodeToEndPoint(Node1))
			{
				if (pE != nullptr)
					pE->distToCurve = d1;
			}
		}
		//Dijkstra algoritm to calculate z-values
		if (z_list.size() > 0) {
			for (;;) {
				std::priority_queue<std::pair<double, std::list<std::pair<int, double>>>> Q;
				for (auto iZ = z_list.begin(); iZ != z_list.end(); ++iZ) {
					Q.push(Dijkstra(*iZ));
				}
				if (Q.top().first >= 0) {
					for (auto iL = Q.top().second.begin(); iL != Q.top().second.end(); ++iL) {
						PointNode *Node1 = &PointNodeList[iL->first];
						if ((Node1->Z_def & 0x7) != 1) {
							z_list.push_back(iL->first);
							setNodeZ(iL->first, iL->second, z_list);
						}
					}
				}
				else
					break;
				z_list.sort();
				z_list.unique();
			}

			for (unsigned int i = 0; i < PointNodeList.size(); ++i)
				if (PointNodeList[i].Z_def == 0) {
					std::priority_queue<std::pair<double, std::list<std::pair<int, double>>>> Q;
					Q.push(Dijkstra(i));
					if (Q.top().first >= 0)
						for (auto iL = Q.top().second.begin(); iL != Q.top().second.end(); ++iL) {
							setNodeZ(iL->first, PointNodeList[Q.top().second.back().first].pos.z, z_list);
						}
				}
			//Dijkstra done
			for (auto &Node : Tracks)
			{
				if (Node.second->endp.empty())
					continue;
				float sum(0.0);
				for (auto &p : Node.second->endp)
					sum += p.pos.z;
				Node.second->setCG_Z(sum / (float)Node.second->endp.size());
			}
		}
	}
/*
void TrackPlan::parseLayout(std::istream& in)
{
	Parser CS(&in);
	clear();
	do {
		switch (CS->getNextToken(XtrkToken)) {
		case xtrkUndefined:
			continue;
		case XtrkScale:
			if (Dimensions.count(CS->GetToken(1)) > 0) {
				scaleAsText = CS->GetToken(1);
				scale = Dimensions[CS->GetToken(1)];
			}
			else {
				scaleAsText = "Undefined - "+ CS->GetToken(1);
				scale = Dimensions["HO"];
			}
			break;
		case XtrkEnd:
			break;
		case XtrkLayers: {
			if (CS->getToken(1, XtrkToken) == XtrkCurrentLayer)
				break;
			if (CS->GetToken(1)[0] == 'C')
				break;
			int pos = std::stoi(CS->GetToken(1));
			bool visible = std::stoi(CS->GetToken(2)) == 1;
			std::string name = std::to_string(pos + 1) + " - " + CS->GetToken(10);
			int color = std::stoi(CS->GetToken(5));
			Layers[pos] = Layer(name, color, visible);
			break;
		}

		case XtrkVersion:
		case XtrkTitle1:
		case XtrkTitle2:
		case XtrkMapScale:
			break;
		case XtrkRoomSize:
			if (CS->GetToken(2)[0] == 'x')
				UR = glm::vec3(std::stof(CS->GetToken(1)), stof(CS->GetToken(3)), 0);
			break;
		case XtrkCurrentLayer:
		case XtrkBlock:
		case XtrkControl:
		case XtrkSensor:
		case XtrkSignal:
			break;

		case XtrkDraw:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			glm::vec3 pos = glm::vec3(stod(CS->GetToken(6)), stod(CS->GetToken(7)), 0);
			float rotation = stof(CS->GetToken(9));

			Draw* T = new Draw(index, layer, pos, rotation);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				Segment* pS = parseSegment(CS, T->getMatrix());
				if (pS != nullptr) {
					pS->index = T->index;
					T->addSegment(pS);
				}
			}
			Objects.push_back(T);
			break;
		}

		case XtrkStructure:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			std::string scale = CS->GetToken(6);
			glm::vec3 pos = glm::vec3(stod(CS->GetToken(8)), stod(CS->GetToken(9)), 0);
			float rotation = stof(CS->GetToken(11));
			std::string label = CS->GetToken(12);
			Structure* T = new Structure(index, layer, scale, pos, rotation, label);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				Segment* pS = parseSegment(CS, T->getMatrix());
				if (pS != nullptr){
					pS->index = T->index;
					T->addSegment(pS);
				}
			}
			Objects.push_back(T);
			break;
		}

		case XtrkBzrlin:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			Bzrln* T = new Bzrln(index, layer);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				Segment* pS = parseSegment(CS, T->getMatrix());
				if (pS != nullptr) {
					pS->index = T->index;
					T->addSegment(pS);
				}
			}
			Objects.push_back(T);
			break;
		}

		case XtrkTurnout:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			int lineWidth = stoi(CS->GetToken(3));
			std::string scale = CS->GetToken(6);
			int visibility = stoi(CS->GetToken(7));
			glm::vec3 pos = glm::vec3(stod(CS->GetToken(8)), stod(CS->GetToken(9)), 0);
			float rotation = stof(CS->GetToken(11));
			std::string Label = CS->GetToken(12);
			Turnout* T = new Turnout(index,layer,lineWidth,scale,visibility,pos,rotation,Label);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				if (CS->GetToken(0)[0] == 'E' || CS->GetToken(0)[0] == 'T') {
					endPoint eP(CS); //(Node2Index, pos, Normal, zDefined)
					T->endp.push_back(eP);
				}
				else
					if (CS->GetToken(0)[0] == 'C' || CS->GetToken(0)[0] == 'S')
					{
						Segment* pS = parseSegment(CS, T->getMatrix());
						if (pS != nullptr) {
							pS->index = T->index;
							T->addSegment(pS);
						}
					}
			}
			T->CG = T->SetMidPoint();
			Tracks[index] = T;
			break;
		}

		case XtrkStraight:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			int lineWidth = stoi(CS->GetToken(3));
			std::string scale = CS->GetToken(6);
			int visibility = stoi(CS->GetToken(7));
			Track* T = new StraightTrack(index, layer, lineWidth, scale, visibility);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				if (CS->GetToken(0)[0] == 'E' || CS->GetToken(0)[0] == 'T') {
					endPoint eP(CS);
					T->endp.push_back(eP);
				}
			}
			if (T->endp.size() == 2) {
				StraightSegment* pS = new StraightSegment(T->endp[0].pos, T->endp[1].pos, glm::mat4());
				T->len = pS->len;
				T->CG = pS->getCGposition();
				T->addSegment(pS);
			}
			Tracks[index] = T;
			break;
		}

		case XtrkCurve:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			int lineWidth = stoi(CS->GetToken(3));
			std::string scale = CS->GetToken(6);
			int visibility = stoi(CS->GetToken(7));
			float CX = stof(CS->GetToken(8));
			float CY = stof(CS->GetToken(9));
			float Z(0);
			glm::vec3 center = glm::vec3(CX, CY, Z);
			float radius = stof(CS->GetToken(11));
			int turns = stoi(CS->GetToken(12));
			Track* T = new CurveTrack(index,layer,lineWidth,scale,visibility,center,radius,turns);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				if (CS->GetToken(0)[0] == 'E' || CS->GetToken(0)[0] == 'T') {
					endPoint eP(CS);
					T->endp.push_back(eP);
				}
			}
			if (T->endp.size() == 2) {
				glm::vec3 point1 = glm::vec3(T->endp[0].pos - center);
				glm::vec3 point2 = glm::vec3(T->endp[1].pos - center);
				float angle1 = wrap0_360(glm::atan(point1.x, point1.y) * (float)(180 / M_PI));
				float angle2 = wrap0_360(glm::atan(point2.x, point2.y) * (float)(180 / M_PI));
				float swing = wrap0_360(angle2 - angle1);

				CurveSegment* pS = new CurveSegment(center, radius, angle1, swing, turns, glm::mat4());
				T->len = pS->len;
				pS->setCG_Z((T->endp[0].pos.z + T->endp[1].pos.z) / 2);
				T->CG = pS->getCGposition();
				T->addSegment(pS);
			}
			Tracks[index] = T;
			break;
		}
		case XtrkJoint:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			int lineWidth = stoi(CS->GetToken(3));
			std::string scale = CS->GetToken(6);
			int visibility = stoi(CS->GetToken(7));
			Track* T = new Joint(index, layer, lineWidth, scale, visibility);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				if (CS->GetToken(0)[0] == 'E' || CS->GetToken(0)[0] == 'T') {
					endPoint eP(CS);
					T->endp.push_back(eP);
				}
			}
			if (T->endp.size() == 2) {
				//joint Segment between curve and straight, according to MOROP NEM113 standard (https://www.morop.eu/downloads/nem/de/nem113_d.pdf)
				JointSegment* pS = new JointSegment(T->endp[0].pos, T->endp[1].pos,glm::mat4(), &Tracks);
				T->len = pS->len;
				T->CG = pS->getCGposition();
				T->addSegment(pS);
			}
			Tracks[index] = T;
			break;
		}

		case XtrkBezier:
		case XtrkCornu:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			int lineWidth = stoi(CS->GetToken(3));
			std::string scale = CS->GetToken(6);
			int visibility = stoi(CS->GetToken(7));
			glm::vec3 pos = glm::vec3(stod(CS->GetToken(8)), stod(CS->GetToken(9)), 0);
			float rotation = stof(CS->GetToken(11));
			Cornu* pT = new Cornu(index, layer, lineWidth, scale, visibility, pos, rotation);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				if (CS->GetToken(0)[0] == 'E' || CS->GetToken(0)[0] == 'T') {
					endPoint eP(CS);
					pT->endp.push_back(eP);
				}
				else
					//skip W3 and SUBSEGS/SUBSEND- just read C & S
					if (CS->GetToken(0)[0] == 'C' || CS->GetToken(0)[0] == 'S')
					{
						Segment* pS = parseSegment(CS, pT->getMatrix());
						if (pS != nullptr) {
							pS->index = pT->index;
							pT->addSegment(pS);
						}
					}
			}
			pT->CG = pT->setMidPoint();
			Tracks[index] = pT;
			break;
		}

		case XtrkTurntable:
		{
			int index = stoi(CS->GetToken(1));
			int layer = stoi(CS->GetToken(2));
			int lineWidth = stoi(CS->GetToken(3));
			std::string scale = CS->GetToken(6);
			int visibility = stoi(CS->GetToken(7));
			glm::vec3 pos = glm::vec3(stod(CS->GetToken(8)), stod(CS->GetToken(9)), 0);
			float radius = stof(CS->GetToken(11));
			Track* T = new Turntable(index, layer, lineWidth, scale, visibility, pos, 0,radius);
			while (CS->parseNextRow()) {
				if (CS->getNextToken(TStructTokens) == XtrkEnd)
					break;
				if (CS->GetToken(0)[0] == 'E' || CS->GetToken(0)[0] == 'T') {
					endPoint eP(CS);
					T->endp.push_back(eP);
				}
			}
			T->CG = T->SetMidPoint();
			Tracks[index] = T;
			break;
		}
		case XtrkNote:
			break;

		}

	}
	while(CS->parseNextRow());
	createPointList();
	calculateZValues();
	for (auto &ii : Tracks) {
		ii.second->setSegmentZ();
		ii.second->param = (Dimensions.count(ii.second->scale) > 0) ? Dimensions[ii.second->scale] : Dimensions["HO"];
		ii.second->initSurface(nullptr);
	}

}

Segment* TrackPlan::TrackPlan::parseSegment(Parser* CS, const glm::mat4& t_Matrix) //Exprimental
{
	Segment* pS = nullptr;
	switch (CS->GetToken(0)[0]) {
	case 'S': {
		if (CS->GetToken(0)[1] == 'U') // SUBSEGS or SUBSEND
		{
			break;
		}
		glm::vec3 p1(std::stof(CS->GetToken(3)), std::stof(CS->GetToken(4)), 0);
		glm::vec3 p2(std::stof(CS->GetToken(5)), std::stof(CS->GetToken(6)), 0);
		pS = new StraightSegment(p1, p2, t_Matrix);
		break;
	}
	case'C': {
		float R = std::stof(CS->GetToken(3));
		glm::vec3 p1(std::stof(CS->GetToken(4)), std::stof(CS->GetToken(5)), 0);
		float alfa = std::stof(CS->GetToken(6));
		float sw = std::stof(CS->GetToken(7));
		pS = new CurveSegment(p1, R, alfa, sw, 0, t_Matrix);
		break;
	}

	case 'L': //Straight line segment L3
	{
		//L3<sp>color<sp>width<sp>x1<sp>y1<sp>0<sp>x2<sp>y2<sp>0
		int Color = std::stoi(CS->GetToken(1));
		float Width = std::stof(CS->GetToken(2));
		glm::vec3 p1(std::stof(CS->GetToken(3)), std::stof(CS->GetToken(4)), 0);
		glm::vec3 p2(std::stof(CS->GetToken(6)), std::stof(CS->GetToken(7)), 0);
		pS = new LineSegment(p1, p2, Color, Width, t_Matrix);
		break;
	}
	case 'A': //Curved line segment A3
	{
		//documentation incorrect, extra arg 6 is 0
		//A3<sp>color<sp>line-width<sp>radius<sp>center-X<sp>center-Y<sp>0<sp>angle<sp>degrees of swing.
		//pS = new CurvedLine3(Line);
		int Color = std::stoi(CS->GetToken(1));
		float Width = std::stof(CS->GetToken(2));
		float radius = std::stof(CS->GetToken(3));
		glm::vec3 Center(std::stof(CS->GetToken(4)), std::stof(CS->GetToken(5)), 0);
		float angle = std::stof(CS->GetToken(7));
		float swing = std::stof(CS->GetToken(8));
		pS = new ArcSegment(Color, Width, radius, Center, angle, swing, t_Matrix);
		break;
	}
	case 'G': //Filled circle segment G3
	{
		//G3<sp>color<sp>width<sp>radius<sp>center-X<sp>center-Y<sp>0
		//pS = new Circle3(Line);
		int Color = std::stoi(CS->GetToken(1));
		float Width = std::stof(CS->GetToken(2));
		float radius = std::stof(CS->GetToken(3));
		glm::vec3 Center(std::stof(CS->GetToken(4)), std::stof(CS->GetToken(5)), 0);
		pS = new CircleSegment(Color, Width, radius, Center, t_Matrix);
		break;
	}
	case 'Y': //Poly segment Y/Y3/Y4
	{
		//Y3<sp>color<sp>width<sp>end-points
		int Color = std::stoi(CS->GetToken(1));
		float Width = std::stof(CS->GetToken(2));
		int count = std::stoi(CS->GetToken(3));
		pS = new PolyLineSegment(Color, Width, t_Matrix);
		std::string str;
		while (count-- > 0)
			if (CS->parseNextRow()) {
				glm::vec3 point(std::stof(CS->GetToken(0)), std::stof(CS->GetToken(1)), 0);
				pS->add(point);
			}
		break;
	}
	case 'F': //Filled poly segment F/F3/F4
	{
		//F3<sp>color<sp>width<sp>#end - points
		int Color = std::stoi(CS->GetToken(1));
		float Width = std::stof(CS->GetToken(2));
		int count = std::stoi(CS->GetToken(3));
		pS = new FilledSegment(Color, Width, t_Matrix);
		std::string str;
		while (count-- > 0)
			if (CS->parseNextRow()) {
				glm::vec3 point(std::stof(CS->GetToken(0)), std::stof(CS->GetToken(1)), 0);
				pS->add(point);
			}
		break;
	}
	case 'Z': //Text segment Z
	{
		int Color = std::stoi(CS->GetToken(1));
		glm::vec3 p1(std::stof(CS->GetToken(2)), std::stof(CS->GetToken(3)), 0);
		pS = new TextSegment(Color, p1, std::stof(CS->GetToken(4)), std::stof(CS->GetToken(6)), CS->GetToken(7), t_Matrix);
		break;
	}
	case 'X': //special segment
		return nullptr;
	case 'Q': //table-edge line Q3
	{
		//Q3<sp>0<sp>0.187500<sp>startX<sp>startY<sp>0<sp>endX<sp>endY<sp>0
		glm::vec3 p1(std::stof(CS->GetToken(3)), std::stof(CS->GetToken(4)), 0);
		glm::vec3 p2(std::stof(CS->GetToken(6)), std::stof(CS->GetToken(7)), 0);
		pS = new TableEdgeSegment(p1, p2, t_Matrix);
		break;
	}
	case 'B': //benchwork line B3
	{
		// read Benchwork
		//    B3 16760832 0.000000 12.000000 34.000000 0 97.000000 34.000000 0 33818624
		//      color            pt1 x      pt1 y        pt2 x     pt2 y     (hex 2 04 08 00= L-shape 2" 4" orientation=0 )
		glm::vec3 p1(std::stof(CS->GetToken(3)), std::stof(CS->GetToken(4)), 0);
		glm::vec3 p2(std::stof(CS->GetToken(6)), std::stof(CS->GetToken(7)), 0);
		pS = new BenchworkSegment(p1, p2, t_Matrix, std::stoi(CS->GetToken(9)));
		break;
	}
	case 'M': //dimensions line M3
	{
		// read Dimension Line
		//    M3 0 0.000000 28.000000 36.000000 0 67.000000 36.000000 0 -1
		//                  pt1 x      pt1 y        pt2 x     pt2 y   text_size
		return nullptr;
	}
	case 'H': //Bezier line segment H3
		return nullptr;
	}
//	if (pS != nullptr)
//		pS->index = this->index;
	return pS;

}

*/

}