#pragma once

#define _USE_MATH_DEFINES
#define GLM_SWIZZLE

#include <vector>
#include <list>
#include <string>
#include <map>
#include <fstream>
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#include <niohman_json/json.hpp>

#include "BasePlane.h"

namespace Xtrk3D {

enum xtrkObjType
{
	XtrkVersion,
	XtrkTitle1,
	XtrkTitle2,
	XtrkMapScale,
	XtrkRoomSize,
	XtrkScale,
	XtrkLayers,
	XtrkTurnout,
	XtrkJoint,
	XtrkCurve,
	XtrkStraight,
	XtrkTurntable,
	XtrkBezier,
	XtrkBzrlin,
	XtrkCornu,
	XtrkNote,
	XtrkBlock,
	XtrkControl,
	XtrkSensor,
	XtrkSignal,
	XtrkStructure,
	XtrkDraw,
	XtrkEnd,
	XtrkCurrentLayer,
	xtrkSegment,
	xtrkLineSegment,
	xtrkArcSegment,
	xtrkPolyLineSegment,
	xtrkFilledSegment,
	xtrkCircleSegment,
	xtrkTableEdgeSegment,
	xtrkBenchWorkSegment,
	xtrkTextSegment,
	xtrkElevationGrid,
	xtrkExtrusion,
	xtrkCylinder,
	xtrkUndefined
};

	class Segment;
	class Track;
	class XTrkObj;
	struct endPoint;

	//#Name		Gauge	ratio	RailH	RailW	TieL	TieH	TieW	TieCC	embTop	embBot	embH	MaxW	MaxH	ExtendW	ExtendL
	//			0		1		2		3		4		5		6		7		8		9		10		11		12		13		14
	class Scale
	{
	public:
		std::vector<float>Param;

		void Init();
		Scale() { Init(); };
		Scale(std::vector<float>values) : Param(values) { };
		Scale(Scale& other) { Param = other.Param; }
		float gauge() { return Param[0] / 25.4f; };
		float ratio() { return Param[1]; };
		float RailH() { return Param[2] / 25.4f; };
		float RailW() { return Param[3] / 25.4f; };
		float TieL() { return Param[4] / 25.4f; };
		float TieH() { return Param[5] / 25.4f; };
		float TieW() { return Param[6] / 25.4f; };
		float TieCC() { return Param[7] / 25.4f; };
		float embTopW() { return Param[8] / 25.4f; };
		float embBotW() { return Param[9] / 25.4f; };
		float embH() { return Param[10] / 25.4f; };
		float MaxW() { return Param[11] / 25.4f; };
		float MaxH() { return Param[12] / 25.4f; };
		float ExtendW() { return Param[13] / 25.4f; };
		float ExtendL() { return Param[14] / 25.4f; };
	};


	typedef struct Layer {
		std::string Name;
		int Color;
		bool Visible;
		Layer() {
			Name = std::string("undefined");
			Color = 255; Visible = true;
		};
		Layer(std::string name, int color, bool visible) :Name(name), Color(color), Visible(visible) {};
	} Layer;

	struct PointNode
	{
		PointNode(int Node1, int NodeIndex1, int Node2, int NodeIndex2, glm::vec3 Pos, int z_def) :pos(Pos), Z_def(z_def)
		{
			distToCurve = -1;
			Nodes[0] = std::make_pair(Node1, NodeIndex1);
			Nodes[1] = std::make_pair(Node2, NodeIndex2);
		}
		std::pair<int, int> Nodes[2];
		glm::vec3 pos;
		int Z_def;
		double distToCurve;
		std::vector<std::pair<int, float>>neighbours;
	};

	typedef std::list<XTrkObj*> ObjList;

	class TrackPlan
	{
	public:
		TrackPlan();
		~TrackPlan();

		std::map<std::string, Scale>Dimensions;
		bool metric;
		bool ColorByLayer;
		bool DrawStruct;
		int TunnelMode;
		bool DrawElevations;

		std::map<int, Layer>Layers;
		void clear();

		void prepareLayout();
		void reCalculateTrackObjects();

		bool getCGPosition(int index, glm::vec3& pos);
		glm::vec3 LL, UR;
		Scale scale;
		std::string scaleAsText;
		Scale* getScale(std::string str) { return &Dimensions[str]; };

		std::map<int, Xtrk3D::Track*>Tracks;
		ObjList Objects;	//non-track objects
		ObjList OverlayObjects;  // json overlay draw objects
		std::map<std::string, Xtrk3D::XTrkObj*>DefinesMap; // json overlay define objects


		std::vector<PointNode>PointNodeList;
		std::vector<endPoint*>PointNodeToEndPoint(PointNode* Node);

		float benchworkZeroLevel;
		static glm::vec3 averagePos(std::vector<endPoint>& endPoints, std::vector<int>& index);
		void dump(nlohmann::json& j);

	protected:
		void createPointList();
		void makeNeighbours(std::pair<int, int>Node, int i);
		void calculateZValues();
		void setNodeZ(int nodeID, double t_z, std::list<int>& z_list);
		double calculateDistToCurve(int pointId, int TrackId);
		//	std::string convert(float val, int precision = 2);
		std::pair<double, std::list<std::pair<int, double>>> Dijkstra(int start);
		void setPointNodeZValue(PointNode *Node, double t_z);
	};

}