#pragma once

#include "clipper.hpp"


namespace Xtrk3D
{
	class TrackPlan;
	class SceneNode;
	class OpenGLShaderPrograms;

	class BasePlane
	{
	public:
		BasePlane();
		~BasePlane(); 
		void initSurface(TrackPlan* myLayout);
		SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout);
		bool visible;
		glm::vec3 color;
		float transparancy;
		float z;
	protected:
		ClipperLib::Paths m_plane;
	};
}