#pragma once
#include "track.h"
namespace Xtrk3D {
	class Turnout :public Track
	{
	public:
		~Turnout();
		Turnout(Xtrk3D::OBJ_TYPE* obj);
		Turnout(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3& t_pos, const float t_rotation, std::string& t_label);
		virtual double getDistance(int index1, int index2);
		virtual enum xtrkObjType getObjType() { return XtrkTurnout; };
		virtual std::string OnSelect();// { return Xtrk3D::XTrkObj::OnSelect() + (Segments.size() == 1 ? " Sectional Track " : " Turnout "); };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual bool isStraightTrack();
		virtual void dump(json& j);
		std::string Label;

	protected:
		Turnout() :Track() {};
	};

	class StraightTrack :public Track
	{
	public:
		StraightTrack(Xtrk3D::OBJ_TYPE* obj);
		StraightTrack(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility);
		~StraightTrack() {};
		virtual double getDistance(int p1, int p2) { return len; };
		virtual enum xtrkObjType getObjType() { return XtrkStraight; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return Xtrk3D::XTrkObj::OnSelect() + " Straight Track "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual bool isStraightTrack() { return true; };
		virtual void dump(json& j);

	protected:
		StraightTrack() :Track() {};
	};

	class CurveTrack :public Track
	{
	public:
		CurveTrack(Xtrk3D::OBJ_TYPE* obj);
		CurveTrack(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3 t_center, const double t_radius, const int t_turns);
		virtual double getDistance(int p1, int p2) { return len; };
		virtual enum xtrkObjType getObjType() { return XtrkCurve; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return Xtrk3D::XTrkObj::OnSelect() + " Curve Track "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual bool isHelix() { return m_turns > 0; };
		virtual void dump(json& j);

	protected:
		CurveTrack() :Track(), m_radius(0), m_angle1(0), m_angle2(0), m_turns(0) {};
		glm::vec3 m_center;
		double m_radius;
		double m_angle1;
		double m_angle2;
		int m_turns;
	};

	class Joint :public Track
	{
	public:
		Joint(Xtrk3D::OBJ_TYPE* obj, std::map<int, Track*>&Tracks);
		Joint(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility);
		virtual double getDistance(int p1, int p2);
		virtual enum xtrkObjType getObjType() { return XtrkJoint; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return Xtrk3D::XTrkObj::OnSelect() + " Joint Track"; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j);

	protected:
		Joint() :Track() {};
	};

	class Turntable :public Track
	{
	public:
		Turntable(Xtrk3D::OBJ_TYPE* obj);
		Turntable(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3& t_pos, const double t_rotation, const double t_radius);

		virtual void setSegmentZ() {};
		virtual void initSurface(TrackPlan* myLayout) {};
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual enum xtrkObjType getObjType() { return XtrkTurntable; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return Xtrk3D::XTrkObj::OnSelect() + " Turntable "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j);

	protected:
		Turntable() :Track(), m_radius(0) {};
		double m_radius;
	};

	class Cornu :public Track
	{
	public:
		Cornu(Xtrk3D::OBJ_TYPE* obj);
		Cornu(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3 t_pos, const double t_rotation);

		virtual void setSegmentZ();
		virtual double getDistance(int p1, int p2) { return len; };
		virtual enum xtrkObjType getObjType() { return XtrkCornu; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Cornu Track"; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j);
		virtual glm::vec3 setMidPoint();
		virtual void initSurface(TrackPlan* myLayout);

	protected:
		struct subpoint {
			Segment* parent;
			int index;
			subpoint(Segment* Parent, int Index) :parent(Parent), index(Index) {};
			subpoint() :parent(nullptr), index(-1) {};
		};
		Cornu() :Track() {};
		std::list<subpoint>subPointList;
		Cornu::subpoint NextSub(std::vector<Segment*>& SegmentList, const glm::vec3 &point);
	};

}