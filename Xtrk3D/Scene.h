#pragma once

#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS
// third-party libraries
#include <glew/include/GL/glew.h>
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>

#include "Vertex.h"
#include "OpenGLText.h"
#include "TrackPlan.h"
#include "Axes.h"
#include "BasePlane.h"

#include <vector>
#include <list>
#include <map>
#include <string>

namespace Xtrk3D {
	typedef enum class shapeType
	{
		OtherType = 0,
		TrackType = 1,
		StructType = 2,
		TunnelSideType = 4,
		TunnelSideTopType = 0xC,
		TunnelRoofType = 0x14,
		EndPtType = 0x20,
		ElevationType = 0x40,
		EmbankmentType = 0x80
	} ShapeType;


	class BoundSphere {
	public:
		BoundSphere() :radius(-1), start(0), farDistance(-1.0f) {};
		glm::vec3 center;
		GLfloat radius;
		BoundSphere(std::vector<glm::vec3>& points);
		BoundSphere(std::vector<vertex> vertices);
		bool isVisible(const Camera &camera, const glm::mat4 &transform);
		void setFarDistance(float t_farDistance) { farDistance = t_farDistance; };
	protected:
		int start; //init to 0
		int order[7] = { 0, 1, 2, 3, 0, 1, 2 };
		float farDistance;
	};


	class Shape {
	public:
		ShapeType Type;

		Shape(
			ShapeType ObjType
			, std::vector<vertex>& vertices
			, std::vector<GLuint>& indices
			, OpenGLShaderPrograms* Shaders
			, Appearance material
			, Appearance LayerMaterial
			, GLenum type = GL_TRIANGLES
			, GLuint cullface = GL_BACK
			, glm::vec2 texScale=glm::vec2(1.0f)
		);

		void Select(const glm::mat4& transform, const OpenGLProgram* pickShader, const unsigned int uuid);
		int Render(const glm::mat4& transform, const bool LayerColorMode = true);

	protected:
		GLuint vbo;			//vertex buffer object
		GLuint ebo;			//element buffer object
		GLuint vao;			//vertex array object
		GLenum drawType;	//GL_TRIANGLES, ...
		glm::vec2 textureScale;
		GLint drawCount;
		Appearance appearance;
		Appearance LayerAppearance;
		OpenGLShaderPrograms* shaders;
		GLuint cullFace;
		unsigned* refCount;

		Shape() :
			Type(ShapeType::OtherType),
			refCount(nullptr),
			shaders(nullptr),
			vbo(-1),
			ebo(-1),
			vao(-1),
			drawType(GL_TRIANGLES),
			cullFace(GL_BACK),
			drawCount(0),
			LayerAppearance(Appearance::solidColor),
			appearance(Appearance::solidColor),
			textureScale(glm::vec2(1.0f))
		{};

		void retain() {
			assert(refCount);
			*refCount += 1;
		};

		void release() {
			if (!refCount) return;
			assert(refCount && *refCount > 0);
			*refCount -= 1;
			if (*refCount == 0) {
				glDeleteBuffers(1, &ebo);
				glDeleteBuffers(1, &vbo);
				glDeleteVertexArrays(1, &vao);
				delete refCount;
				refCount = nullptr;
			}
		};
		virtual void Set(std::vector<vertex>& vertices, std::vector<GLuint>& elementData);

	public:
		~Shape() {
			if (refCount) release();
		};

		Shape(const Shape& other) :
			refCount(other.refCount)
			, shaders(other.shaders)
			, vbo(other.vbo)
			, ebo(other.ebo)
			, vao(other.vao)
			, drawType(other.drawType)
			, drawCount(other.drawCount)
			, appearance(other.appearance)
			, LayerAppearance(other.LayerAppearance)
			, cullFace(other.cullFace)
			, Type(other.Type)
			, textureScale(other.textureScale)
		{
			retain();
		}

		Shape& operator = (const Shape& other)
		{
			release();
			refCount = other.refCount;
			shaders = other.shaders;
			vbo = other.vbo;
			ebo = other.ebo;
			vao = other.vao;
			drawType = other.drawType;
			drawCount = other.drawCount;
			appearance = other.appearance;
			LayerAppearance = other.LayerAppearance;
			Type = other.Type;
			textureScale = other.textureScale;
			retain();
			return *this;
		}
	};

	class Ring :public Shape
	{
	public:
		Ring(ShapeType objType, glm::vec3 center, double Ri, double Ry, double height, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material);
		Ring(ShapeType objType, glm::vec3 center, double Ri, double Ry, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material = Appearance::solidColor, GLuint cullface = GL_BACK);
	protected:
		std::vector<GLuint>indices;
		std::vector<vertex>vertices;
	};
	class Cone :public Shape
	{
	public:
		Cone(ShapeType objType, glm::vec3 center, double Radius, double Height, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material, GLuint cullface = GL_BACK);
	protected:
		std::vector<GLuint>indices;
		std::vector<vertex>vertices;
	};

	class Disc :public Shape
	{
	public:
		Disc(ShapeType objType, glm::vec3 center, double Radius, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material = Appearance::solidColor, GLuint cullface = GL_BACK,float TexScale=1.0);
		Disc(ShapeType objType, glm::vec3 center, double Radius, double Height, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material = Appearance::solidColor, GLuint cullface = GL_BACK);
	protected:
		std::vector<GLuint>indices;
		std::vector<vertex>vertices;
	};

	class Arrow :public Shape
	{
	public:
		Arrow(ShapeType objType, glm::vec3 start, glm::vec3 end, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material = Appearance::solidColor);
	};

	class SceneNode;
	class XTrkObj;

	class TextNode
	{
	public:
		TextNode() :parent(nullptr) {};
		TextNode(std::string Str, glm::vec3 Color) : parent(nullptr), str(Str), color(Color) {};
		TextNode(float val, glm::vec3 Color, int precision = 2);
		void setTransform(const glm::mat4 &Mat) { transform = Mat; Update(); };
		void Update();
		~TextNode() {};

		std::string str;
		glm::vec3 color;
		glm::mat4 WorldTransform;
		glm::mat4 transform;
		SceneNode* parent;
	};

	typedef std::vector<SceneNode*> SceneNodeList;
	typedef std::vector<Shape> ShapeNodeList;
	typedef std::vector<TextNode> TextNodeList;

	class SceneNode {
	public:
		SceneNode() :parent(nullptr), layer(0), layer2(-1), isTrack(false), TrackObj(nullptr), isTie(false) {};
		SceneNode(const SceneNode& other);
		virtual ~SceneNode();
		SceneNode* parent;
		int layer;
		int layer2;
		bool isTrack;
		BoundSphere boundingSphere;
		XTrkObj* TrackObj;
		bool isTie;
//2019-10-02 to calculate sleepers LOD when rendering

		bool hasChildren() const { return !children.empty() || !shapeList.empty(); };
		void addNode(SceneNode* childNode);
		void addShape(Shape &s);
		void addText(TextNode  &Obj);
		int Render(const Camera& myCamera, const bool LayerColorMode);
		int RenderType(const Camera& myCamera, const ShapeType mode);
		int RenderText(OpenGLText& Renderer);
		void Select(const Camera& myCamera, OpenGLProgram* pickShader, const unsigned int uuid);
		void setTransform(const glm::mat4 &Mat) { transform = Mat; Update(); };
		const glm::mat4 getMatrix() const { return WorldTransform; };
		void Update();
		void clear();

	protected:
		SceneNodeList children;
		ShapeNodeList shapeList;
		TextNodeList textList;
		glm::mat4 WorldTransform;
		glm::mat4 transform;

		SceneNode& operator = (const SceneNode& other);
	};

	class TrackPlan;
	struct PointNode;

		class Scene
	{
	public:
		Scene();
		~Scene();

		SceneNodeList trackNodes;
		SceneNodeList tunnels;
		SceneNodeList nonTrackNodes;
		SceneNodeList overlayNodes;
		SceneNodeList endPoints;
		SceneNodeList dynamicObjects;

		OpenGLText* myText;
		void Clear();
		void Init(TrackPlan* layout);
		void InitNonTracks();
		void Reset(Camera& myCamera);
		int Render(const Camera& myCamera);
		SceneNode* Select(const Camera& myCamera, int x, int y);
		void ObjProperties(SceneNode* Obj);
		void updateObj(SceneNode* Obj);
		OpenGLShaderPrograms MyShaders;
		Axes Origo;
		BasePlane Table;
		SceneNode* backGround;
		void updateTable();

	protected:
		void InitTracks();
		void InitPointNodes();
		void addEndPoint(PointNode* Item, int layer1, int layer2, TrackPlan*  layout, OpenGLShaderPrograms& ShaderPrograms);
		TrackPlan* myPlan;
		GLint uboCamera;			//Camera transformation uniform buffer object

	};
}