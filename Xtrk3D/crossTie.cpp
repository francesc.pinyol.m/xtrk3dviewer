#include "crossTie.h"
#include "track.h"
namespace Xtrk3D {

	CrossTie::CrossTie()
	{
	}


	CrossTie::~CrossTie()
	{
	}

	CrossTie::CrossTie(OpenGLShaderPrograms* Shaders, Scale& param, const int LayerColor)
	{
		glm::vec2 size(param.TieW() / 2, param.TieL() / 2);
		float top(-param.RailH());
		float bot(-param.RailH() - param.TieH());
		glm::vec3 TieColor(glm::vec3((LayerColor & 0xff0000) / 65536 / 255.0f, (LayerColor & 0xff00) / 256 / 255.0f, (LayerColor & 0xff) / 255.0f));
		appearance = Appearance::tieSurface;
		LayerAppearance = Appearance::solidColor;

		Set(size, top, bot, TieColor);
		shaders = Shaders;
	}

	CrossTie::CrossTie(OpenGLShaderPrograms* Shaders, glm::vec3 size, const glm::vec3 Color)
	{
		appearance = Appearance::bufferstop;
		LayerAppearance = Appearance::solidColor;
		glm::vec2 S(size.x, size.y);
		Set(S, size.z / 2.0f, -size.z / 2.0f, Color);
		shaders = Shaders;
	}

	void CrossTie::Set(glm::vec2 size, float top, float bot, const glm::vec3 TieColor)
	{
		std::vector<vertex>Vertices = {
			vertex(glm::vec3(-size.x,-size.y,top), glm::vec3(), TieColor, glm::vec2(0, 0))		//0
			,vertex(glm::vec3(size.x,-size.y,top), glm::vec3(), TieColor, glm::vec2(0, 0.25))	//1
			,vertex(glm::vec3(size.x, size.y,top),  glm::vec3(), TieColor, glm::vec2(0.9, 0.25))	//2
			,vertex(glm::vec3(-size.x,size.y,top), glm::vec3(), TieColor, glm::vec2(0.9, 0))		//3

			,vertex(glm::vec3(-size.x,-size.y,bot), glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(0, 0.75))	//4
			,vertex(glm::vec3(size.x,-size.y,bot), glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(0, 0.5))		//5
			,vertex(glm::vec3(size.x, size.y,bot),  glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(0.9, 0.5))	//6
			,vertex(glm::vec3(-size.x,size.y,bot), glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(0.9, 0.75))	//7

			,vertex(glm::vec3(-size.x,-size.y,top), glm::vec3(), TieColor, glm::vec2(0, 1))		//8(0)
			,vertex(glm::vec3(-size.x,size.y,top), glm::vec3(), TieColor, glm::vec2(0.9, 1))		//9(3)

			,vertex(glm::vec3(-size.x,-size.y,top), glm::vec3(), TieColor, glm::vec2(0.91, 1))	//10
			,vertex(glm::vec3(size.x,-size.y,top), glm::vec3(), TieColor, glm::vec2(1, 1))		//11
			,vertex(glm::vec3(-size.x,-size.y,bot), glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(0.91, 0.76))	//12
			,vertex(glm::vec3(size.x,-size.y,bot), glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(1, 0.76))	//13


			,vertex(glm::vec3(-size.x,size.y,bot), glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(0.91, 1))	//11-14
			,vertex(glm::vec3(-size.x,size.y,top), glm::vec3(), TieColor, glm::vec2(1, 0.76))	//3-15
			,vertex(glm::vec3(size.x, size.y,top),  glm::vec3(), TieColor, glm::vec2(0.91, 0.76))	//2-16
			,vertex(glm::vec3(size.x,size.y,bot), glm::vec3(), glm::vec3(0.72, 0.48, 0.34), glm::vec2(1, 1))	//10-17
		};
		std::vector<GLuint> Indices = {
			0,1,2,0,2,3 //top side
			,1,5,2,5,6,2 //side
	//		,4,6,5,4,7,6 //bottom
			,8,9,4,4,9,7 //side
			,10,12,11,12,13,11 //end
			,14,15,16,14,16,17 //end
		};
		if (appearance == Appearance::bufferstop) {
			std::vector<GLuint> bottom = { 4,6,5,4,7,6 };
			Indices.insert(Indices.end(), bottom.begin(), bottom.end());
		}
		drawType = GL_TRIANGLES;
		drawCount = Indices.size();
		Type = ShapeType::TrackType;
		Shape::Set(Vertices, Indices);
		BoundingSphere = BoundSphere(Vertices);
	}

}