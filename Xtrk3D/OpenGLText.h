#pragma once
#define GLM_FORCE_RADIANS
#include <glew/include/GL/glew.h>
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>

#include "Camera.h"
#include "OpenGLShader.h"
#include <string>
#include <map>

namespace Xtrk3D {

	class OpenGLText
	{
	public:
		OpenGLText(OpenGLShaderPrograms* programs);
		int Render(const glm::mat4& transform, const std::string& str, const glm::vec3& color);
		~OpenGLText();
	protected:
		OpenGLText();
		OpenGLShaderPrograms* shaders;
		//	std::map<char, glm::vec2>glyphs;
		GLuint vbo;			//vertex buffer object
		GLuint ebo;			//element buffer object
		GLuint vao;			//vertex array object
		GLint drawCount;
	};

}