#include "Axes.h"
#include "OpenGLShader.h"

#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/type_ptr.hpp>

namespace Xtrk3D {

	void Axes::InitDraw(OpenGLProgram* TriangleShaders, OpenGLProgram* LineShaders)
	{
		if (drawTrianglesProgram != nullptr)
			return;

		drawTrianglesProgram = TriangleShaders;
		drawLineProgram = LineShaders;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		std::vector<vertex> vertices;

		const float L(20);
		const float R(0.4f);
		const int divs(32);
		const float arrowLen(L - 2 * R);

		vertices.push_back(vertex(glm::vec3(0, 0, 0),glm::vec3(0.8f, 0, 0)));
		vertices.push_back(vertex(glm::vec3(arrowLen, 0, 0), glm::vec3(0.8f, 0, 0)));
		vertices.push_back(vertex(glm::vec3(L, 0, 0), glm::vec3(0.8f, 0, 0)));

		vertices.push_back(vertex(glm::vec3(0, 0, 0), glm::vec3(0, 0.6f, 0)));
		vertices.push_back(vertex(glm::vec3(0, arrowLen, 0), glm::vec3(0, 0.6f, 0)));
		vertices.push_back(vertex(glm::vec3(0, L, 0), glm::vec3(0, 0.6f, 0)));

		vertices.push_back(vertex(glm::vec3(0, 0, 0), glm::vec3(0, 0, 0.8f)));
		vertices.push_back(vertex(glm::vec3(0, 0, arrowLen), glm::vec3(0, 0, 0.8f)));
		vertices.push_back(vertex(glm::vec3(0, 0, L), glm::vec3(0, 0, 0.8f)));

		std::vector <glm::vec2> uv;
		double delta = 2.0*M_PI / double(divs);
		double sin_delta = sin(delta);
		double cos_delta = cos(delta);
		double u1;
		double u = R;
		double v = 0.0;
		uv.push_back(glm::vec2(u, v));

		for (int i = 0; i < divs; ++i) {
			u1 = u * cos_delta - v * sin_delta;
			v = u * sin_delta + v * cos_delta;
			u = u1;
			uv.push_back(glm::vec2(u, v));
		}

		int e(9);

		elementsX[0].push_back(1);
		elementsX[1].push_back(2);
		for (unsigned int i = 0; i < uv.size(); ++i)
		{
			vertices.push_back(vertex(glm::vec3(arrowLen, uv[i].x, uv[i].y),glm::vec3(1, 0, 0)));
			elementsX[0].push_back(e);
			elementsX[1].push_back(e++);
		}
		elementsY[0].push_back(4);
		elementsY[1].push_back(5);
		for (unsigned int i = 0; i < uv.size(); ++i)
		{
			vertices.push_back(vertex(glm::vec3(uv[i].x, arrowLen, uv[i].y),glm::vec3(0, 0.6f, 0)));
			elementsY[0].push_back(e);
			elementsY[1].push_back(e++);
		}
		elementsZ[0].push_back(7);
		elementsZ[1].push_back(8);
		for (unsigned int i = 0; i < uv.size(); ++i)
		{
			vertices.push_back(vertex(glm::vec3(uv[i].x, uv[i].y, arrowLen),glm::vec3(0, 0, 1)));
			elementsZ[0].push_back(e);
			elementsZ[1].push_back(e++);
		}

			//vertex data
		vbo = make_buffer(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertex), vertices.data());

		glEnableVertexAttribArray(0);//position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertex_size, 0);

		glEnableVertexAttribArray(1);//color
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, vertex_size, (const GLvoid*)(vertex_colorIndex));

		glEnableVertexAttribArray(4);//normal
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, vertex_size, (const GLvoid*)(vertex_normalIndex));

		glEnableVertexAttribArray(3); //alpha channel
		glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, vertex_size, (const GLvoid*)(vertex_alphaIndex));

		glEnableVertexAttribArray(2);//vertTexCoord
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_TRUE, vertex_size, (const GLvoid*)(vertex_texCoordIndex));

		// Create an element array
		ebo = make_buffer(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(GLuint), elements.data());

		// unbind the VAO
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		drawCount = elements.size();
	}

	Axes::~Axes()
	{
	}

	void Axes::Render(const Camera& myCamera)
	{
		drawTrianglesProgram->Use();
		glUniformMatrix4fv(drawTrianglesProgram->GetUniform("modelMatrix"), 1, GL_FALSE, glm::value_ptr(glm::mat4()));
		glDisable(GL_CULL_FACE);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

		//	std::vector<GLuint> elements = { 0,1,2,3,4,5,3,8,3,9 };// , 2, 3, 4, 5, 3, 8, 3, 9};
		for (unsigned int i = 0; i < 2; ++i)
		{
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementsX[i].size() * sizeof(GLuint), elementsX[i].data(), GL_DYNAMIC_DRAW);
			glDrawElements(GL_TRIANGLE_FAN, elementsX[i].size(), GL_UNSIGNED_INT, 0);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementsY[i].size() * sizeof(GLuint), elementsY[i].data(), GL_DYNAMIC_DRAW);
			glDrawElements(GL_TRIANGLE_FAN, elementsY[i].size(), GL_UNSIGNED_INT, 0);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementsZ[i].size() * sizeof(GLuint), elementsZ[i].data(), GL_DYNAMIC_DRAW);
			glDrawElements(GL_TRIANGLE_FAN, elementsZ[i].size(), GL_UNSIGNED_INT, 0);
		}
		drawLineProgram->Use();
		glUniformMatrix4fv(drawLineProgram->GetUniform("modelMatrix"), 1, GL_FALSE, glm::value_ptr(glm::mat4()));
		glDisable(GL_CULL_FACE);
		//glBindVertexArray(vao);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(GLuint), elements.data(), GL_DYNAMIC_DRAW);
		glDrawElements(GL_LINES, elements.size(), GL_UNSIGNED_INT, 0);

		//glBindBuffer(GL_ARRAY_BUFFER, vbo);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		//glBindVertexArray(0);
		drawLineProgram->StopUsing();
	}
}