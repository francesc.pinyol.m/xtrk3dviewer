#include "ElevationGrid.h"
#include "EarcutWrapper.h"
#include "PolygonSet.h"

namespace Xtrk3D {
	Overlay::Overlay(float scalefactor, TrackPlan* myPlan) : _scalefactor(scalefactor), trackplan(myPlan) {}

	void Overlay::parseOverlay(json& jp)
	{
		int index(trackplan->OverlayObjects.size());
		if (jp.is_array()) {
			auto xx = jp.size();
			for (auto& element : jp.items()) {
				//				auto xy = element.key();
				for (auto& z : element.value().items()) {
					if (z.key() == "Draw") {
						json DrawNode(z);
						XTrkObj* Obj = ParseDraw(DrawNode["Draw"], ++index);
						if (Obj != nullptr)
							trackplan->OverlayObjects.push_back(Obj);

					}
				}
			}
		}
		else if (jp.is_object()) {
			XTrkObj* Obj = ParseDraw(jp["Draw"], ++index);
			if (Obj != nullptr)
				trackplan->OverlayObjects.push_back(Obj);
		}
	}

	void Overlay::ParseId(json& jp)
	{
		if (jp.contains("Id")) {
			if (jp.contains("Segments")) {
				std::string Id = jp["Id"].get<std::string>();
				if (trackplan->DefinesMap.count(Id) == 0)
				{
					trackplan->DefinesMap[Id] = new  Xtrk3D::Draw(0, 0, glm::vec3(), 0.0f);
				}
				else {
					trackplan->DefinesMap[Id]->clearSegments();
				}
				Xtrk3D::XTrkObj* Shape = trackplan->DefinesMap[Id];
				ParseSegment(jp, Shape, -1);
			}
		}
	}

	void Overlay::ParseDefine(json& jp)
	{
		if (jp.is_object()) {
			ParseId(jp);
			return;
		}
		if (jp.is_array()) {
			for (auto& pp : jp) {
				ParseId(pp);
			}
			return;
		}
	}

	Xtrk3D::XTrkObj* Overlay::ParseDraw(json& jp, int index)
	{
		int Layer(0);
		if (jp.contains("Index"))
			index = jp["Index"].get<int>();
		if (jp.contains("Layer"))
			Layer = jp["Layer"].get<int>();

		unsigned int i(0);
		double Translate[3] = { 0,0,0 };
		if (jp.contains("Translation")) {
			for (auto& p : jp["Translation"]) {
				Translate[i++] = p.get<double>() * _scalefactor;
			}
		}
		glm::vec3 translation(Translate[0], Translate[1], Translate[2]);

		float rotation(0.0f);
		if (jp.contains("Rotation"))
			rotation = -jp["Rotation"].get<float>();

		if (jp.contains("Segments")) {
			Xtrk3D::XTrkObj* Shape = new  Xtrk3D::Draw(0, 0, translation, rotation);
			ParseSegment(jp, Shape, -index); //use negative numbers for overlay
			return Shape;
		}
		return nullptr;
	}

	Xtrk3D::Segment* Overlay::ParseGroup(json& js, const glm::mat4& M, float scaleFactor)
	{
		auto xx = js.is_object();
		Xtrk3D::Group* myGroup = new Group(js, M, scaleFactor);
		if (js.is_object()) {
			auto Shape = ParseDraw(js, 0);
			myGroup->addChild(Shape);
			myGroup->CG = (M * glm::vec4(Shape->CG, 1)).xyz;
		}
		if (js.is_array()) {
			int i(0);
			glm::vec3 cc;
			for (auto& pp : js) {
				auto Shape = ParseDraw(pp, 0);
				myGroup->addChild(Shape);
				cc += Shape->CG;
				i++;
			}
			myGroup->CG = (M * glm::vec4(cc / (float)i, 1)).xyz;
		}
		return myGroup;
	}


	void Overlay::ParseSegment(json& jp, Xtrk3D::XTrkObj* Obj, int index)
	{
		Obj->index = index;
		if (jp["Segments"].is_object()) {
			auto Segment = ParseSegment(jp["Segments"], Obj->getMatrix());
			if (Segment != nullptr) {
				Segment->index = index;
				Obj->addSegment(Segment);
				Obj->CG = Segment->CG;
			}
		}
		else if (jp["Segments"].is_array()) {
			int i = 0;
			glm::vec3 CC;
			for (auto& pp : jp["Segments"]) {
				auto Segment = ParseSegment(pp, Obj->getMatrix());
				if (Segment != nullptr) {
					Segment->index = index;
					Obj->addSegment(Segment);
					i++;
					CC += Segment->CG;
				}
			}
			if (i > 0) {
				Obj->CG = CC / (float)i;
			}
		}
	}

	Xtrk3D::Segment* Overlay::ParseSegment(json& jp, const glm::mat4& M)
	{
		if (jp.contains("Elevationgrid")) {
			return new Xtrk3D::ElevationGrid(jp, M, _scalefactor);
		}
		if (jp.contains("IndexedFaceSet")) {
			return new Xtrk3D::IndexedFaceSet(jp, M, _scalefactor);
		}
		if (jp.contains("IndexedLineSet")) {
			return new Xtrk3D::IndexedLineSet(jp, M, _scalefactor);
		}
		if (jp.contains("BSpline")) {
			return new Xtrk3D::BSpline(jp, M, _scalefactor);
		}
		if (jp.contains("Spline")) {
			return new Xtrk3D::Spline(jp, M, _scalefactor);
		}
		if (jp.contains("Use")) {
			return new Xtrk3D::Use(jp, M, trackplan->DefinesMap, _scalefactor);
		}

		if (jp.contains("Group")) {
			return ParseGroup(jp["Group"], M, _scalefactor);
		}

		if (jp.contains("Draw")) {
			return ParseGroup(jp["Draw"], M, _scalefactor);
		}

		if (jp.contains("Extrusion")) {
			return new Xtrk3D::Extrusion(jp, M, _scalefactor);
		}

		if (jp.contains("Cylinder")) {
			return new Xtrk3D::Cylinder(jp, M, _scalefactor);
		}

		if (jp.contains("Cone")) {
			return new Xtrk3D::ConeSegment(jp, M, _scalefactor);
		}

		if (jp.contains("Model")) {
			return new Xtrk3D::PolygonSet(jp, M, _scalefactor, _defaultDir);
		}

		//if (jp.contains("Group")) {}
		//if (jp.contains("Group")) {}
		//if (jp.contains("Group")) {}

		return nullptr;
	}

	void ElevationGrid::CreateElevationGrid(json& j, const glm::vec3 translation, const float rotation, const float scalefactor)
	{
		Xtrk3D::XTrkObj* Shape = new  Xtrk3D::Draw(0, 0, translation, rotation);
		Xtrk3D::ElevationGrid* E = new Xtrk3D::ElevationGrid(j, Shape->getMatrix(), scalefactor);
		Shape->addSegment(E);
		Xtrk3D::getTrackplan()->Objects.push_back(Shape);
	}

	ElevationGrid::ElevationGrid(json& js, const glm::mat4& M, float scaleFactor) :LineSegment(0, 0, M), transparancy(1.0)
	{

		float random_factor(5.0f);

		int i(0);
		for (auto& p : js["Elevationgrid"]["Dimension"]) {
			dimension[i++] = p.get<int>();
		}
		i = 0;
		for (auto& p : js["Elevationgrid"]["Spacing"]) {
			spacing[i++] = p.get<float>() * scaleFactor;
		}
		i = 0;
		for (auto& p : js["Elevationgrid"]["Color"]) {
			color[i++] = p.get<float>();
		}
		if (i > 0) {
			ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);
		}
		else {
			ObjColor = glm::vec3(0.2, 0.8, 0.2);
		}

		if (js["Elevationgrid"].contains("Transparancy")) {
			transparancy = 1.0f - js["Elevationgrid"]["Transparancy"].get<float>();
		}
		glm::vec3 EdgeColor = glm::vec3(1, 1, 1) - ObjColor;// *0.8f;
		auto intensity = glm::length(ObjColor);
		if (intensity < 0.7) EdgeColor = ObjColor * 1.4f;
		i = 0;
		int j(0);
		for (auto& p : js["Elevationgrid"]["Height"]) {
			//heights.push_back(p.get<float>());
			glm::vec4 point(glm::vec3(spacing[0] * i, spacing[1] * j, p.get<float>() * scaleFactor), 1);

			float r = fmin(1.0f, fmax(ObjColor.x + (((float)rand() / (RAND_MAX)) - 0.5f) / random_factor, 0.0f));
			float g = fmin(1.0f, fmax(ObjColor.y + (((float)rand() / (RAND_MAX)) - 0.5f) / random_factor, 0.0f));
			float b = fmin(1.0f, fmax(ObjColor.z + (((float)rand() / (RAND_MAX)) - 0.5f) / random_factor, 0.0f));
			Verts.push_back(vertex((M * point).xyz, glm::vec3(), glm::vec3(r, g, b), point.xy, transparancy));
			V2.push_back(vertex((M * point).xyz, glm::vec3(), EdgeColor, point.xy));
			i++;
			if (i >= dimension[0]) {
				i = 0;
				j++;
			}
			if (j >= dimension[1]) break;
		}
		if ((int)Verts.size() < (dimension[0] * dimension[1])) return;//fail

		for (int jj = 0; jj < dimension[1] - 1; ++jj)
		{
			bool even = (jj % 2 == 0);
			for (int ii = 0; ii < dimension[0] - 1; ++ii)
			{
				I2.push_back(ii + jj * dimension[0]);
				I2.push_back(ii + 1 + jj * dimension[0]);
				I2.push_back(ii + jj * dimension[0]);
				I2.push_back(ii + (jj + 1) * dimension[0]);

				if (even) {
					Indices.push_back(ii + jj * dimension[0]);
					Indices.push_back(ii + 1 + jj * dimension[0]);
					Indices.push_back(ii + (jj + 1) * dimension[0]);

					Indices.push_back(ii + 1 + jj * dimension[0]);
					Indices.push_back(ii + 1 + (jj + 1) * dimension[0]);
					Indices.push_back(ii + (jj + 1) * dimension[0]);

					I2.push_back(ii + 1 + jj * dimension[0]);
					I2.push_back(ii + (jj + 1) * dimension[0]);
				}
				else {
					Indices.push_back(ii + jj * dimension[0]);
					Indices.push_back(ii + 1 + (jj + 1) * dimension[0]);
					Indices.push_back(ii + (jj + 1) * dimension[0]);

					Indices.push_back(ii + jj * dimension[0]);
					Indices.push_back(ii + 1 + jj * dimension[0]);
					Indices.push_back(ii + 1 + (jj + 1) * dimension[0]);

					I2.push_back(ii + jj * dimension[0]);
					I2.push_back(ii + 1 + (jj + 1) * dimension[0]);
				}
				I2.push_back((jj + 1) * dimension[0] - 1);
				I2.push_back((jj + 2) * (dimension[0]) - 1);
				even = !even;
			}
		}
		for (int ii = 0; ii < dimension[0] - 1; ++ii)
		{
			I2.push_back(ii + dimension[0] * (dimension[1] - 1));
			I2.push_back(ii + dimension[0] * (dimension[1] - 1) + 1);
		}
		CG = (M * glm::vec4(dimension[0] * spacing[0] / 2.0f, dimension[1] * spacing[1] / 2.0f, 0, 1)).xyz;
	}


	ElevationGrid::~ElevationGrid()
	{
	}


	SceneNode* ElevationGrid::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count)
	{
		SceneNode* Node = new SceneNode();
		Node->boundingSphere = BoundSphere(Verts);
		Shape Surface(ShapeType::OtherType, Verts, Indices, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE, glm::vec2(1.0));
		Node->addShape(Surface);

		Shape Edge(ShapeType::OtherType, V2, I2, Shaders, Appearance::solidColor, Appearance::solidColor, GL_LINES, GL_NONE, glm::vec2(1.0));
		Node->addShape(Edge);

		Node->layer = this->layer;
		Node->isTrack = false;
		return Node;
	}


	XTrkObj* ElevationGrid::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Elevationgrid";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "ElevationGrid";
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		item2.label = "Center";
		item2.point = &CG;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);
		item2.label = "# points:";
		item2.i = Verts.size();
		item2.type = VariantType::isInt;
		properties.push_back(item2);

		return this;
	}


	void IndexedFaceSet::CreateIndexedFaceSet(json& j, const glm::vec3 translation, const float rotation, const float scalefactor)
	{
		Xtrk3D::XTrkObj* Shape = new  Xtrk3D::Draw(0, 0, translation, rotation);
		Xtrk3D::IndexedFaceSet* E = new Xtrk3D::IndexedFaceSet(j, Shape->getMatrix(), scalefactor);
		Shape->addSegment(E);
		Xtrk3D::getTrackplan()->Objects.push_back(Shape);
	}

	IndexedFaceSet::IndexedFaceSet(json& js, const glm::mat4& M, float scaleFactor) :LineSegment(0, 0, M), transparancy(1.0f), DrawEdges(true)
	{

		int i(0);
		for (auto& p : js["IndexedFaceSet"]["Color"]) {
			if (i > 3) {
				transparancy = p.get<float>();
			}
			else {
				color[i++] = p.get<float>();
			}
		}
		ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);
		EdgeColor = glm::vec3(1, 1, 1) - ObjColor;// *0.8f;
		auto intensity = glm::length(ObjColor);
		if (intensity < 0.7) EdgeColor = ObjColor * 1.4f;

		if (js["IndexedFaceSet"]["Edges"].is_boolean())
		{
			DrawEdges = js["IndexedFaceSet"]["Edges"].get<bool>();
		}

		i = 0;
		float point[3];
		for (auto& p : js["IndexedFaceSet"]["Coord"]) {
			point[i++] = p.get<float>() * scaleFactor;
			if (i >= 3) {
				i = 0;
				Verts.push_back(vertex(glm::vec3(point[0], point[1], point[2]), ObjColor, transparancy));
			}
		}
		int maxVertex = Verts.size();
		std::vector<GLuint> I1;
		std::vector<std::vector<GLuint>>I2; // i1,i2,i3,-1,i4,i5,...
		GLuint firstI;
		for (auto& p : js["IndexedFaceSet"]["CoordIndex"]) {
			int index = p.get<int>();
			if (index >= maxVertex) {
				throw std::runtime_error("IndexedFaceSet: CoordIndex " + index + std::string(" is larger then number of Coord !"));
			}
			if (index < 0) {
				I2.push_back(I1);
				I1.clear();
				edgeIndexes.push_back(firstI);
			}
			else {
				if (I1.empty()) {
					firstI = index;
				}
				else {
					edgeIndexes.push_back(index);
				}
				I1.push_back((GLuint)index);
				edgeIndexes.push_back(index);
			}
		}
		if (!I1.empty()) {
			I2.push_back(I1);
			edgeIndexes.push_back(firstI);
		}
		//triangulate
		glm::vec3 cc;
		for (auto& i : I2) {
			if (i.size() == 3)
				for (auto& j : i)
					Indices.push_back(j);
			if (i.size() > 3) {
				std::vector<vertex>V2;
				for (auto& j : i) {
					V2.push_back(Verts[j]);
				}
				I1.clear();
				EarcutWrapper E(V2, I1);
				for (auto& j : I1) {
					Indices.push_back(i[j]);
					cc += Verts[i[j]].position;
				}
			}
		}
		cc = cc / (float)Indices.size();
		glm::vec3 dc;
		int c(0);
		for (auto i = Indices.begin(); i != Indices.end();) {
			glm::vec3 g(Verts[*i++].position);
			g += Verts[*i++].position;
			g += Verts[*i++].position;
			dc += g / 3.0f;
			c++;
		}
		dc = dc / (float)c;
		CG = (M * glm::vec4(dc, 1)).xyz;
	}

	SceneNode* IndexedFaceSet::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count)
	{
		SceneNode* Node = new SceneNode();
		//Node->boundingSphere = BoundSphere(Verts);
		Shape Surface(ShapeType::OtherType, Verts, Indices, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE, glm::vec2(1.0));
		Node->addShape(Surface);

		if (DrawEdges) {
			std::vector<vertex>V2;
			for (auto& i : Verts) {
				V2.push_back(vertex(i.position, EdgeColor));
			}
			Shape Surface2(ShapeType::OtherType, V2, edgeIndexes, Shaders, Appearance::solidColor, Appearance::solidColor, GL_LINES, GL_NONE, glm::vec2(1.0));
			Node->addShape(Surface2);
		}
		Node->layer = this->layer;
		Node->isTrack = false;
		Node->setTransform(getMatrix());
		return Node;
	}


	XTrkObj* IndexedFaceSet::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "IndexedFaceSet";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "IndexedFaceSet";
		item2.type = VariantType::isStr;
		properties.push_back(item2);

		item2.label = "Center";
		item2.point = &CG;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);
		item2.label = "# points:";
		item2.i = Verts.size();
		item2.type = VariantType::isInt;
		properties.push_back(item2);
		return this;
	}

	Extrusion::Extrusion(json& js, const glm::mat4& M, float scaleFactor) :FilledSegment(0, 0, M) //, transparancy(1.0f), DrawEdges(true)
	{
		int i(0);
		for (auto& p : js["Extrusion"]["Color"]) {
			//			if (i > 3) {
			//				transparancy = p.get<float>();
			//			}
			//			else {
			color[i++] = p.get<float>();
			//			}
		}
		ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);
		EdgeColor = glm::vec3(1, 1, 1) - ObjColor;// *0.8f;
		auto intensity = glm::length(ObjColor);
		if (intensity < 0.7) EdgeColor = ObjColor * 1.4f;

		if (js["Extrusion"].contains("Height")) {
			Height = js["Extrusion"]["Height"].get<double>() * scaleFactor;
		}
		i = 0;
		float point[3];
		for (auto& p : js["Extrusion"]["Bottom"]) {
			point[i++] = p.get<float>() * scaleFactor;
			if (i >= 2) {
				i = 0;
				add(glm::vec3(point[0], point[1], Height));
			}
		}
		baseZset = true;
		glm::vec4 p1 = m_transformMatrix * glm::vec4(0, 0, 0, 1);
		baseZValue = p1.z;
	}


	XTrkObj* Extrusion::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Extrusion";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "Extrusion";
		item2.type = VariantType::isStr;
		properties.push_back(item2);

		item2.label = "Center";
		item2.point = &CG;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);
		int s(0);
		for (auto& ii : surface) {
			s += ii.size();
		};
		if (s > 0) {
			item2.label = "# points:";
			item2.i = s;
			item2.type = VariantType::isInt;
			properties.push_back(item2);
		}
		item2.label = "Height";
		item2.d = Height;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);
		return this;
	}

	Cylinder::Cylinder(json& js, const glm::mat4& M, float scaleFactor) : LineSegment(0, 0, M), Height(scaleFactor), Radius(scaleFactor) //, transparancy(1.0f), DrawEdges(true)
	{
		int i(0);
		for (auto& p : js["Cylinder"]["Color"]) {
			//			if (i > 3) {
			//				transparancy = p.get<float>();
			//			}
			//			else {
			color[i++] = p.get<float>();
			//			}
		}
		ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);

		if (js["Cylinder"].contains("Height")) {
			Height = js["Cylinder"]["Height"].get<double>() * scaleFactor;
		}
		if (js["Cylinder"].contains("Radius")) {
			Radius = js["Cylinder"]["Radius"].get<double>() * scaleFactor;
		}
	}

	SceneNode* Cylinder::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count)
	{
		SceneNode* Node = new SceneNode();
		glm::vec4 p1 = m_transformMatrix * glm::vec4(0, 0, 0, 1);
		CG = p1.xyz + glm::vec3((float)Height / 2.0f);
		Node->boundingSphere = BoundSphere();
		Node->boundingSphere.radius = (float)fmax(Radius, Height / 2);
		Node->boundingSphere.center = CG;
		Node->layer = this->layer;
		Node->addShape(Disc(ShapeType::OtherType, p1.xyz, Radius, Height, 48, ObjColor, Shaders, Appearance::solidColor, GL_BACK));
		return Node;
	}

	XTrkObj* Cylinder::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Cylinder";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "Cylinder";
		item2.type = VariantType::isStr;
		properties.push_back(item2);

		item2.label = "Radius";
		item2.d = Radius;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "Height:";
		item2.d = Height;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);
		return this;
	}

	ConeSegment::ConeSegment(json& js, const glm::mat4& M, float scaleFactor) : LineSegment(0, 0, M),Height(scaleFactor),Radius(scaleFactor) //, transparancy(1.0f), DrawEdges(true)
	{
		int i(0);
		for (auto& p : js["Cone"]["Color"]) {
			//			if (i > 3) {
			//				transparancy = p.get<float>();
			//			}
			//			else {
			color[i++] = p.get<float>();
			//			}
		}
		ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);

		if (js["Cone"].contains("Height")) {
			Height = js["Cone"]["Height"].get<double>() * scaleFactor;
		}
		if (js["Cone"].contains("Radius")) {
			Radius = js["Cone"]["Radius"].get<double>() * scaleFactor;
		}
	}

	SceneNode* ConeSegment::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count)
	{
		SceneNode* Node = new SceneNode();
		glm::vec4 p1 = m_transformMatrix * glm::vec4(0, 0, 0, 1);
		CG = p1.xyz + glm::vec3((float)Height / 2.0f);
		Node->boundingSphere = BoundSphere();
		Node->boundingSphere.radius = (float) fmax(Radius, Height / 2);
		Node->boundingSphere.center = CG;
		Node->layer = this->layer;
		Node->addShape(Cone(ShapeType::OtherType, p1.xyz, Radius, Height, 48, ObjColor, Shaders, Appearance::solidColor, GL_BACK));
		return Node;
	}

	XTrkObj* ConeSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Cone";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "Cone";
		item2.type = VariantType::isStr;
		properties.push_back(item2);

		item2.label = "Radius";
		item2.d = Radius;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "Height:";
		item2.d = Height;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);
		return this;
	}

	IndexedLineSet::IndexedLineSet(json& js, const glm::mat4& M, float scaleFactor) :PolyLineSegment(0,0,M,2)
	{
		int i(0);
		for (auto& p : js["IndexedLineSet"]["Color"]) {
			color[i++] = p.get<float>();
		}
		ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);
		surface.resize(1);

		i = 0;
		float point[3];
		for (auto& p : js["IndexedLineSet"]["Coord"]) {
			point[i++] = p.get<float>()*scaleFactor;
			if (i >= 3) {
				i = 0;
				add(glm::vec3(point[0], point[1], point[2]));
			}
		}
		if (js["IndexedLineSet"].contains("Width")) {
			Width = js["IndexedLineSet"]["Width"].get<double>() * scaleFactor;
		}
		if (js["IndexedLineSet"].contains("Height")) {
			hasHeight = true;
			height = (double)(js["IndexedLineSet"]["Height"].get<float>()) * scaleFactor;
		}
		Init(nullptr);

	}

	void IndexedLineSet::Init(Track* parent)
	{
		if (points.size() < 2)
			return;
		if (hasHeight) {
			createVertexVector3(points, height);
		}
		else {
			createVertexVector(points);
		}
	}


	XTrkObj* IndexedLineSet::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "IndexedLineSet";
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return PolyLineSegment::OnSelect(properties);
	}




	BSpline::BSpline(json& js, const glm::mat4& M, float scaleFactor) :PolyLineSegment(0, 0, M, 2)
	{
		int i(0);
		hasHeight = false;
		height = 0;
		for (auto& p : js["BSpline"]["Color"]) {
			color[i++] = p.get<float>();
		}
		ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);
		surface.resize(1);

		i = 0;
		float point[3];
		for (auto& p : js["BSpline"]["Coord"]) {
			point[i++] = p.get<float>() * scaleFactor;
			if (i >= 3) {
				i = 0;
				add(glm::vec3(point[0], point[1], point[2]));
			}
		}
		if (js["BSpline"].contains("Width")) {
			Width = (double)( js["BSpline"]["Width"].get<float>()) * scaleFactor;
		}
		if (js["BSpline"].contains("Height")) {
			hasHeight = true;
			height= (double)(js["BSpline"]["Height"].get<float>()) * scaleFactor;
		}
		Init(nullptr);
	}

	//This function evaluates the uniform cubic B-spline.
	float BSpline::Bsplineparameter(double t)
	{
		double tp2, tp1, tm2, tm1;

		if (t <= -2.0)
			return 0.0;
		else if (t <= -1.0)
		{
			tp2 = t + 2.0;
			return (float)(tp2 * tp2 * tp2 / 6.0);
		}
		else if (t <= 0.0)
		{
			tp2 = t + 2.0;
			tp1 = t + 1.0;
			tp2 = tp2 * tp2 * tp2 / 6.0;
			tp1 = 2.0 * tp1 * tp1 * tp1 / 3.0;
			return (float)(tp2 - tp1);
		}
		else if (t <= 1.0)
		{
			tm1 = 1.0 - t;
			tm2 = 2.0 - t;
			tm1 = 2.0 * tm1 * tm1 * tm1 / 3.0;
			tm2 = tm2 * tm2 * tm2 / 6.0;
			return (float)(tm2 - tm1);
		}
		else if (t <= 2.0)
		{
			tm2 = 2.0 - t;
			return (float)(tm2 * tm2 * tm2 / 6.0);
		}
		else
			return 0.0;
	}

	void BSpline::Init(Track* parent)
	{
		const float DELTA_T = 0.05f;  // time step factor for drawing
		clear();
		if (points.size() < 2)
			return;

		std::vector<glm::vec3>Bpoints;
		std::vector<glm::vec3>Drawpoints;
		Bpoints.push_back(points[0]);
		Bpoints.push_back(points[0]);
		for (auto& pP : points) {
			Bpoints.push_back(pP);
		}
		Bpoints.push_back(points.back());
		Bpoints.push_back(points.back());
		unsigned int pointSize(points.size());
		Drawpoints.push_back(Bpoints[0]);
		for (unsigned int i = 0; i <= pointSize; i++) {
			for (float t = DELTA_T; t < 1.0 + DELTA_T / 2.0; t += DELTA_T)
			{
				float bt1 = Bsplineparameter(t - 2.0);
				float bt2 = Bsplineparameter(t - 1.0);
				float bt3 = Bsplineparameter(t);
				float bt4 = Bsplineparameter(t + 1.0);
				glm::vec3 p(Bpoints[i] * bt4 + Bpoints[i + 1] * bt3 + Bpoints[i + 2] * bt2 + Bpoints[i + 3] * bt1);
				Drawpoints.push_back(p);
			}
		}
		if (hasHeight) {
			createVertexVector3(Drawpoints, height);
		}
		else {
			createVertexVector(Drawpoints);
		}
	}

	XTrkObj* BSpline::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "BSpline";
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return PolyLineSegment::OnSelect(properties);
	}

	Spline::Spline(json& js, const glm::mat4& M, float scaleFactor) :PolyLineSegment(0, 0, M, 2)
	// https://en.wikipedia.org/wiki/Centripetal_Catmull%E2%80%93Rom_spline
	{
		int i(0);
		for (auto& p : js["Spline"]["Color"]) {
			color[i++] = p.get<float>();
		}
		ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);
		surface.resize(1);

		i = 0;
		float point[3];
		for (auto& p : js["Spline"]["Coord"]) {
			point[i++] = p.get<float>() * scaleFactor;
			if (i >= 3) {
				i = 0;
				add(glm::vec3(point[0], point[1], point[2]));
			}
		}
		if (js["Spline"].contains("Width")) {
			Width = js["Spline"]["Width"].get<double>() * scaleFactor;
		}
		if (js["Spline"].contains("Height")) {
			hasHeight = true;
			height = (double)(js["BSpline"]["Height"].get<float>()) * scaleFactor;
		}
		Init(nullptr);
	}
	void Spline::CatmullRomSpline(glm::vec3 P0, glm::vec3 P1, glm::vec3 P2, glm::vec3 P3, float delta, std::vector<glm::vec3>&Drawpoints,float alfa)
	{
		float t1(tj(0, P0, P1, alfa));
		float t2(tj(t1, P1, P2, alfa));
		float t3(tj(t2, P2, P3, alfa));
		for (float t = t1 + delta; t < t2 + delta / 2.0; t += delta) {
			glm::vec3 A1 = (t1 - t) / t1 * P0 + t / t1 * P1;
			glm::vec3 A2 = (t2 - t) / (t2 - t1) * P1 + (t - t1) / (t2 - t1) * P2;
			glm::vec3 A3 = (t3 - t) / (t3 - t2) * P2 + (t - t2) / (t3 - t2) * P3;

			glm::vec3 B1 = (t2 - t) / t2 * A1 + t / t2 * A2;
			glm::vec3 B2 = (t3 - t) / (t3 - t1) * A2 + (t - t1) / (t3 - t1) * A3;

			glm::vec3 C = (t2 - t) / (t2 - t1) * B1 + (t - t1) / (t2 - t1) * B2;
			Drawpoints.push_back(C);
		}
	}

	void Spline::Init(Track* parent)
	{
		const float DELTA_T = 0.05f;  // time step factor for drawing
		const float alfa = 0.5f;      // Parametric constant: 0.5 for the centripetal spline, 0.0 for the uniform spline, 1.0 for the chordal spline.
		clear();
		if (points.size() < 2)
			return;

		std::vector<glm::vec3>Bpoints;
		std::vector<glm::vec3>Drawpoints;
		Bpoints.push_back(points[0]*2.0f-points[1]);//mirror point[1] to get start vector
		for (auto& pP : points) {
			Bpoints.push_back(pP);
		}
		Bpoints.push_back(points.back() * 2.0f - points[points.size() - 2]);//create end vector
		unsigned int pointSize(points.size());
		Drawpoints.push_back(Bpoints[1]);

		for (unsigned int i = 0; i < pointSize-1; i++) {
			CatmullRomSpline(Bpoints[i], Bpoints[i + 1], Bpoints[i + 2], Bpoints[i + 3], DELTA_T, Drawpoints, alfa);
		}
		if (hasHeight) {
			createVertexVector3(Drawpoints, height);
		}
		else {
			createVertexVector(Drawpoints);
		}
	}
	XTrkObj* Spline::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "Spline";
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return PolyLineSegment::OnSelect(properties);
	}



	Use::Use(json& js, const glm::mat4& M, std::map<std::string, Xtrk3D::XTrkObj*>& Defines, float scaleFactor) :LineSegment(0, 0, M), Defptr(nullptr)
	{
		defineName = js["Use"].get<std::string>();
		if (Defines.count(defineName) == 1) {
			Defptr = Defines[defineName];
			strcpy(SelectText, (std::string("Use of ")+defineName).c_str());
			CG = (M * glm::vec4(Defptr->CG,1)).xyz;
			return;
		}
		throw std::runtime_error("ImportJson::Use of "+defineName+"! Definition not found");

	}

	SceneNode* Use::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
	{
		if (Defptr != nullptr && count<20) {	//max recursion depth
			SceneNode* Node = new SceneNode();
			auto sub = Defptr->createSceneNode(Shaders, myLayout,++count);
			//Node->boundingSphere = sub->boundingSphere;
			Node->TrackObj = this;
			Node->setTransform(m_transformMatrix);
			Node->addNode(sub);
			Node->Update();
			return Node;
		}
		return nullptr;
	}
	XTrkObj* Use::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = SelectText;
		item.type = VariantType::isHeader;
		properties.push_back(item);
		SELECT_TYPE item2;

//		item2.label = std::string(SelectText).c_str();
		item2.label = "Use: ";
		item2.str = SelectText;
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		item2.label = "Center";
		item2.point = &CG;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);
		item2.label = "# Segments:";
		item2.i = Defptr->getSegmentSize();
		item2.type = VariantType::isInt;
		properties.push_back(item2);
		return this;
	}


	Group::Group(json& js, const glm::mat4& M, float scalFactor)
	{
		m_transformMatrix = M;
	}

	void Group::addChild(Xtrk3D::XTrkObj* Child)
	{
		if (Child == nullptr)
			return;
		Children.push_back(Child); 
	}

	SceneNode* Group::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count)
	{
		if (Children.empty())
			return nullptr;

		SceneNode* Node = new SceneNode();
		Node->setTransform(m_transformMatrix);
		Node->Update();
		for (auto& I : Children) {
			auto sub = I->createSceneNode(Shaders, myLayout,count);
			Node->addNode(sub);
		}
		return Node;
	}

	XTrkObj* Group::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		XTrkObj* obj(nullptr);
		for (auto& ii : Children) {
			obj = ii->OnSelect(properties);
		}
		return obj;

		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Group";
		item.type = VariantType::isHeader;
		properties.push_back(item);
		return this;
	}
}