#pragma once
#include "Scene.h"

namespace Xtrk3D {
	class Scale;
	class CrossTie : public Shape
	{
	public:
		~CrossTie();
		CrossTie(OpenGLShaderPrograms* Shaders, Scale& param, const int LayerColor);
		CrossTie(OpenGLShaderPrograms* Shaders, glm::vec3 size, const glm::vec3 Color);
		BoundSphere BoundingSphere;
	protected:
		CrossTie();
		void Set(glm::vec2 size, float top, float bot, const glm::vec3 TieColor);

	};
}
