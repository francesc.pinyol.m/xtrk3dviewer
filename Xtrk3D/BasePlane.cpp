#include "BasePlane.h"
#include "EarcutWrapper.h"
#include "PolyWrapper.h"
#include "track.h"

using namespace ClipperLib;


void myCallback(const IntPoint& e1bot, IntPoint& e1top, IntPoint& e2bot, IntPoint& e2top, IntPoint& pt)
{
	//Just let new z be lowest z
	std::vector<double>z;
	double z2(0);
	if (!e1bot.Z.Flag) z.push_back(e1bot.Z.Pos.z);
	if (!e1top.Z.Flag) z.push_back(e1top.Z.Pos.z);
	if (!e2bot.Z.Flag) z.push_back(e2bot.Z.Pos.z);
	if (!e2top.Z.Flag) z.push_back(e2top.Z.Pos.z);
	for (unsigned int i = 0; i < z.size(); ++i) {
		if (i == 0) z2 = z[0];
		else z2 = fmin(z2, z[i]);
	}

	double z1 = std::fmin(std::fmin(e1bot.Z.Pos.z, e1top.Z.Pos.z), std::fmin(e2bot.Z.Pos.z, e2top.Z.Pos.z));
	pt.Z.Pos = glm::vec3(pt.X / Iscale, pt.Y / Iscale, z2);
	pt.Z.Flag = (e1bot.Z.Flag || e1top.Z.Flag || e2bot.Z.Flag || e2top.Z.Flag); // if flag at least one point in iPath
}

Paths joinBkgPaths(Paths &pgs, Paths &clp)
{
	Clipper c;
	Paths sol;
	c.ZFillFunction((ZFillCallback)&myCallback);
	c.AddPaths(pgs, ptSubject, true);
	c.AddPaths(clp, ptClip, true);
	c.Execute(ctUnion, sol, pftEvenOdd, pftEvenOdd);
	return(sol);
}

Paths subtractBkgPaths(Paths &pgs, Paths &clp)
{
	Clipper c;
	Paths sol;
	c.ZFillFunction((ZFillCallback)&myCallback);
	c.AddPaths(pgs, ptSubject, true);
	c.AddPaths(clp, ptClip, true);
	c.Execute(ctDifference, sol, pftEvenOdd, pftEvenOdd);
	return(sol);
}

namespace Xtrk3D
{
	BasePlane::BasePlane()
	{
		color = glm::vec3(0.1, 0.6, 0.1);
		transparancy = 0.8f;
		visible = false;
		z = 0.0f;
	}


	BasePlane::~BasePlane()
	{
	}

	void BasePlane::initSurface(TrackPlan* myLayout)
	{
		m_plane.clear();
		m_plane.resize(1);
		glm::vec3 p1(myLayout->LL.x, myLayout->LL.y, z);
		m_plane[0] << IntPoint(p1,true);
		p1.x = myLayout->UR.x;
		m_plane[0] << IntPoint(p1,true);
		p1.y = myLayout->UR.y;
		m_plane[0] << IntPoint(p1,true);
		p1.x = myLayout->LL.x;
		m_plane[0] << IntPoint(p1,true);
	}

	SceneNode* BasePlane::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout)
	{
		initSurface(myLayout);
//		EarcutWrapper T(m_plane, color, transparancy);
		PolyWrapper T(PolyWrapper::polyMode::EarCut,m_plane, color,transparancy);
		SceneNode* Node = new SceneNode();
		for (unsigned int i = 0; i < T.vertices.size(); ++i) {
			Node->addShape(Shape(ShapeType::OtherType, T.vertices[i], T.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		}
		return Node;
	}
}