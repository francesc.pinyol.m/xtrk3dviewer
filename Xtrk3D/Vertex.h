#pragma once
#define GLM_FORCE_RADIANS
// third-party libraries
#include <glew/include/GL/glew.h>
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>

typedef struct texture_data {
	int w;
	int h;
	unsigned char* Data;
} texture_data;

struct vertex {
	glm::vec3 position;	//	GLfloat position[3];	layout (location = 0) offset 0
	glm::vec3 color;	//	GLfloat color[3];		layout (location = 1) offset 3
	glm::vec3 normal;	//	GLfloat normal[3];		layout (location = 2) offset 6
	GLfloat alpha;		//	GLfloat alpha			layout (location = 3) offset 9
	glm::vec2 texCoord;	//	GLfloat texcoord[2];	layout (location = 4) offset 10
						//	GLfloat shininess;		layout (location =  ) offset 12
						//	GLubyte specular[4];
	vertex(glm::vec3 Position, glm::vec3 Normal, glm::vec3 Color, glm::vec2 TexCoord,GLfloat transparancy=1.0f) :position(Position), normal(Normal), color(Color), texCoord(TexCoord),alpha(transparancy) {};
	vertex(glm::vec3 Position, glm::vec3 Color, GLfloat transparancy = 1.0f) :position(Position), color(Color),alpha(transparancy) {};
};
const GLuint vertex_size(sizeof(vertex));
const GLuint vertex_positionIndex(0);
const GLuint vertex_colorIndex(sizeof(glm::vec3));
const GLuint vertex_normalIndex(sizeof(glm::vec3)+ vertex_colorIndex);
const GLuint vertex_alphaIndex(sizeof(glm::vec3) + vertex_normalIndex);
const GLuint vertex_texCoordIndex(sizeof(GLfloat) + vertex_alphaIndex);

