#pragma once

#include <vector>
#include "Vertex.h"
#include "clipper.hpp"

class EarcutWrapper
{
protected:
	EarcutWrapper() {};
public:
	EarcutWrapper(const ClipperLib::Paths& emb, glm::vec3 Color= glm::vec3(1, 0, 0), GLfloat transparancy=1.0f);
	EarcutWrapper(const std::vector<vertex>&vertices, std::vector<GLuint>& indices);
	~EarcutWrapper();
	std::vector<std::vector<vertex>> vertices;
	std::vector<std::vector<GLuint>> indices;
};

