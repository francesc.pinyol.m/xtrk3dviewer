#include "XTrkObj.h"
#include "EarcutWrapper.h"
#include "segment.h"

using namespace ClipperLib;

namespace Xtrk3D {
	void XTrkObj::add3DShapesToNode(
		SceneNode* Node,
		OpenGLShaderPrograms* Shaders,
		glm::vec3& ObjColor,
		ClipperLib::Paths& surface,
		float baseZ
	)
	{
		Paths floor;
		floor.resize(1);
		std::vector <vertex>V;
		std::vector<GLuint>I;
		std::vector <vertex>V2;
		std::vector<GLuint>I2;
		glm::vec3 RevColor(1 - ObjColor.x, 1 - ObjColor.y, 1 - ObjColor.z);
		int i = 0;
		for (auto &ii : surface[0]) {
			glm::vec3 p1(ii.Z.Pos);
			V.push_back(vertex(p1, ObjColor));
			V2.push_back(vertex(p1, RevColor));
			p1.z = baseZ;
			V.push_back(vertex(p1, ObjColor));
			V2.push_back(vertex(p1, RevColor));
			floor[0] << IntPoint(p1.xyz);
			I.push_back(i);
			I.push_back(i + 1);
			I.push_back(i + 2);
			I.push_back(i + 1);
			I.push_back(i + 3);
			I.push_back(i + 2);
			I2.push_back(i);
			I2.push_back(i + 1);
			I2.push_back(i);
			I2.push_back(i + 2);
			I2.push_back(i + 1);
			I2.push_back(i + 3);
			i += 2;
		}
		V.push_back(V[0]);
		V.push_back(V[1]);
		V2.push_back(V2[0]);
		V2.push_back(V2[1]);
		Node->addShape(Shape(ShapeType::OtherType, V, I, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		Node->addShape(Shape(ShapeType::OtherType, V2, I2, Shaders, Appearance::solidColor, Appearance::solidColor, GL_LINES, GL_NONE));

		EarcutWrapper T(floor, ObjColor);
		for (unsigned int i = 0; i < T.vertices.size(); ++i) {
			Node->addShape(Shape(ShapeType::OtherType, T.vertices[i], T.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		}
	}

	XTrkObj::XTrkObj(const int t_index, const int t_layer, const glm::vec3& t_pos, const double t_rotation)
		: index(t_index)
		, layer(t_layer)
		, m_pos(t_pos)
		, m_rotation(t_rotation)
	{
		m_transformMatrix = glm::rotate(glm::translate(glm::mat4(), m_pos), float(m_rotation / 180.0*M_PI), glm::vec3(0, 0, -1));
	};

	void XTrkObj::clearSegments()
	{
		for (auto &ii : Segments)
			delete ii;
		Segments.clear();
	}

	void XTrkObj::dump(json& j, std::string lbl) {
		j[lbl]["Index"] = index;
		j[lbl]["Layer"] = layer;
		j[lbl]["Translation"] = { std::to_string(m_pos.x), std::to_string(m_pos.y), std::to_string(m_pos.z) };
		j[lbl]["Rotation"] = std::to_string(m_rotation) ;
/*		for (unsigned int i = 0; i < 4; ++i) {
			for (unsigned int ii = 0; ii < 4; ++ii) {
				j[lbl]["M"].push_back(m_transformMatrix[i][ii]);
			}
		}//*/
	}

	glm::vec3 to_vec3(Xtrk3D::VEC3 v) { return glm::vec3(v.x, v.y, v.z); };
}