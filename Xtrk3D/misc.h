#pragma once
#include <sstream>
#include <iomanip>

static std::string ConvertToStrFixed(int val,int width)
{
	std::stringstream ss;
	ss.width(6);
	ss << std::right << val;
	return ss.str();
}



static std::string ConvertToStr(int val)
{
	std::stringstream ss;
	ss << val;
	return ss.str();
}

static std::string ConvertToStr(double val)
{
	std::stringstream ss;
	ss << val;
	return ss.str();
}

static std::string ConvertToStr(double val, float scale)
{
	std::stringstream ss;
	ss << (val / scale);
	return ss.str();
}

static std::string ConvertToStr(const double value, const bool toMetric)
{
	std::stringstream ss;
	ss << std::fixed << std::setprecision(2);
	ss << ( toMetric ? value *2.54 : value);
//	ss << (value / (metric ? 1.0f : 25.4f));
	return ss.str();
}

