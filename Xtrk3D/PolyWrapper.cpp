#include "PolyWrapper.h"
#include "polypartition.h"
//https://github.com/ivanfratric/polypartition


PolyWrapper::PolyWrapper()
{
}


PolyWrapper::~PolyWrapper()
{
}

PolyWrapper::PolyWrapper(polyMode triangulateMode, const ClipperLib::Paths& emb, glm::vec3 Color, GLfloat transparancy)
{
	for (unsigned int i = 0; i < emb.size(); ++i) {
		vertices.resize(i + 1);
		indices.resize(i + 1);
		for (auto ii = emb[i].begin(); ii != emb[i].end(); ++ii)
		{
			vertex v(ii->Z.Pos, glm::vec3(), Color, glm::vec2(ii->Z.Pos.x, ii->Z.Pos.y), transparancy);
			float z = v.position.z;
			vertices[i].push_back(v);
		}
		if (triangulateMode == polyMode::OPT) {
			execute_OPT(i);
		} else
			execute_EC(i);
	}
}
void PolyWrapper::execute_OPT(unsigned int subvector)
{
	TPPLPoly polygon;
	polygon.Init(vertices[subvector].size());
	for (unsigned int i = 0; i < vertices[subvector].size(); ++i) {
		polygon[i].x = vertices[subvector][i].position.x;
		polygon[i].y = vertices[subvector][i].position.y;
		polygon[i].id = i;
	}
	TPPLPartition TP;
	TPPLPolyList triangles;
	TP.Triangulate_OPT(&polygon, &triangles);
	for (auto &ii : triangles) {
		indices[subvector].push_back(ii.GetPoint(0).id);
		indices[subvector].push_back(ii.GetPoint(1).id);
		indices[subvector].push_back(ii.GetPoint(2).id);
	}
}

void PolyWrapper::execute_EC(unsigned int subvector)
{
	TPPLPoly polygon;
	polygon.Init(vertices[subvector].size());
	for (unsigned int i = 0; i < vertices[subvector].size(); ++i) {
		polygon[i].x = vertices[subvector][i].position.x;
		polygon[i].y = vertices[subvector][i].position.y;
		polygon[i].id = i;
	}
	TPPLPartition TP;
	TPPLPolyList triangles;
	TP.Triangulate_EC(&polygon, &triangles);
	for (auto &ii : triangles) {
		indices[subvector].push_back(ii.GetPoint(0).id);
		indices[subvector].push_back(ii.GetPoint(1).id);
		indices[subvector].push_back(ii.GetPoint(2).id);
	}
}
