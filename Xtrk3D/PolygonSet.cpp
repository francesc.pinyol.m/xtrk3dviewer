#include "PolygonSet.h"
#include "Structure.h"

#include <filesystem>

#include "EarcutWrapper.h"
#include "zip/zip_file.hpp" //https://github.com/tfussell/miniz-cpp

namespace Xtrk3D {

	PolygonSet::PolygonSet(json& js, const glm::mat4& M, float scaleFactor,std::string default_path): _path(default_path)
	{
		int i(0);
		for (auto& p : js["Model"]["Color"]) {
			color[i++] = p.get<float>();
		}
		if (i > 0) {
			ObjColor = glm::vec3(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f);
		}
		else
		{
			ObjColor = glm::vec3(0.6, 0.2, 0.2);
		}
		if (js["Model"].contains("Units")) {
			auto units = js["Model"]["Units"].get<std::string>();
			if (units == "inch")
				scaleFactor = 1.0;
			else if (units == "feet")
				scaleFactor = 12;
			else if (units == "mm")
				scaleFactor = 1 / 25.4f;
			else if (units == "cm")
				scaleFactor = 1 / 2.54f;
			else if (units == "dm")
				scaleFactor = 10 / 2.54f;
			else if (units == "m")
				scaleFactor = 100 / 2.54f;
		}
		if (js["Model"].contains("Scale")) {
			auto scale = js["Model"]["Units"].get<float>();
			scaleFactor /= scale;
		}
		if (js["Model"].contains("Path")) {
			readFile(js["Model"]["Path"].get<std::string>(),M,scaleFactor);
		}
	}

	void PolygonSet::readFile(std::string path, const glm::mat4& M, float scaleFactor)
	{
		try {
			std::filesystem::path p(path);
			if (!p.has_stem()) return;
			if (p.is_relative()) {
				p = std::filesystem::path(_path).append(path);
			}
			_filename = p.lexically_normal().string();

			std::string ext(p.extension().string());
			if (ext == ".stl") {
				ReadSTLfile(M, scaleFactor);
				return;
			}
			if (ext == ".obj") {
				std::ifstream file(_filename);
				ReadOBJfile(file, M, scaleFactor);
				return;
			}
			if (ext == ".zip") {
				ReadZIPfile(_filename, M, scaleFactor);
				return;
			}
		}
		catch (const std::exception& e) {
			std::string msg(e.what());
			throw std::runtime_error(msg+ " at "+path);
		}
		catch (...) {
			throw std::runtime_error("error reading "+path);
		}
	}


	void PolygonSet::ReadSTLfile(const glm::mat4& M , float scaleFactor )
	{
		std::vector<float> coords, normals;
		std::vector<unsigned int>solids;
		stl_reader::ReadStlFile(_filename.c_str(), coords, normals, Indices, solids);

		for (unsigned int i = 0; i < coords.size();i+=3) {
			glm::vec3 pos(coords[i], coords[i + 1], coords[i + 2]);
			pos *= scaleFactor;
			glm::vec3 norm(normals[i], normals[i + 1], normals[i + 2]);
			vertex v(((M * glm::vec4(pos,1)).xyz), ((M * glm::vec4(norm, 1)).xyz), ObjColor,glm::vec2());
			Verts.push_back(v);
		}
	}

	 SceneNode* PolygonSet::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count ) {
		 SceneNode* Node = new SceneNode();
		 Node->boundingSphere = BoundSphere(Verts);
		 Shape Surface(ShapeType::OtherType, Verts, Indices, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE, glm::vec2(1.0));
		 Node->addShape(Surface);
		 return Node;
	 }

	void PolygonSet::Init(Track* parent)
	{
	}

	void PolygonSet::ReadZIPfile(std::string Path, const glm::mat4& M, float scaleFactor)
	{
		miniz_cpp::zip_file file(Path);
		std::vector<std::string>dir = file.getdir();
		std::stringstream objfile;
		std::stringstream mtlfile;
		for (auto& ii : dir) {
			std::string ext = ii.substr(ii.length() - 4);
			if (ext == ".obj") {
				objfile.str(file.read(ii));
			}
			if (ext == ".mtl") {
				mtlfile.str(file.read(ii));
				Readmtlfile(mtlfile);
			}
		}
		ReadOBJfile(objfile, M, scaleFactor);
	}

	XTrkObj* PolygonSet::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		int p = _filename.rfind("\\");
		std::string namepart = _filename.substr(p+1);
		std::vector<std::string>tokens;
		split(_filename, "\\", tokens);

		SELECT_TYPE item;
		item.label = "Model: ";
		item.str = _filename.c_str();
		item.type = VariantType::isStr;
		properties.push_back(item);
		SELECT_TYPE item2;
		item2.str = "";
		item2.label = "Orign:";
		item2.point = &CG;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);

		return this;
	}


	void PolygonSet::split(const std::string& str, const std::string& delimiter, std::vector<std::string>& tokens)
	{
		size_t start(0), end(0);
		while (end !=std::string::npos) {
			start = str.find_first_not_of(delimiter,end); //skip leading delimiters
			if (start == std::string::npos) return;
			auto s = str.find("#", start);
			if (str.find("#",start)==start) return;
			end = str.find_first_of(delimiter, start);
			if (end != std::string::npos) {
				tokens.push_back(str.substr( start, end - start));
			} else
				tokens.push_back(str.substr(start));
		}
	}

	void PolygonSet::ReadOBJfile(std::istream& file, const glm::mat4& M, float scaleFactor)
	{
		glm::vec3 fColor(ObjColor);
		std::string curline;
		std::map<unsigned int, unsigned int>mapVertex; // create new vertex if color change and map from old vertex
		while (std::getline(file, curline))
		{
			std::vector<std::string>tokens;
			split(curline, std::string(" \t"), tokens);
			if (tokens.size() == 0) continue;
			while (tokens.back() == std::string("\\")) {
				tokens.pop_back();
				std::getline(file, curline);
				split(curline, std::string(" \t"), tokens);
			}
			if (tokens[0]=="v" && tokens.size()>3) {
				glm::vec3 pos(std::stof(tokens[1]), std::stof(tokens[2]), std::stof(tokens[3]));
				pos *= scaleFactor;
				vertex v(((M * glm::vec4(pos, 1)).xyz), glm::vec3(), glm::vec3(-1,-1,-1), glm::vec2());
				//vertex v(pos, glm::vec3(), glm::vec3(-1,-1,-1), glm::vec2());
				Verts.push_back(v);
				continue;
			}
			if (tokens[0] == "usemtl" && tokens.size() > 1) {
				if (materials.count(tokens[1]) > 0) {
					mapVertex.clear();
					fColor=materials[tokens[1]].Kd;
				}
				continue;
			}
			if (tokens[0] == "vt" && tokens.size() > 3) { continue; }
			if (tokens[0] == "vn" && tokens.size() > 3) { continue; }
			if (tokens[0] == "vp" && tokens.size() > 3) { continue; }
			if (tokens[0] == "g" && tokens.size() > 3) { continue; }
			if (tokens[0] == "p" && tokens.size() > 3) { continue; }
			if (tokens[0] == "l" && tokens.size() > 3) { continue; }
			if (tokens[0] == "f" && tokens.size() > 3) {
				if (tokens.size() == 4) {
					unsigned int ind[3];
					for (unsigned int i = 0; i < 3; ++i) {
						int v = std::stoi(tokens[i + 1]);
						ind[i] = v < 0 ? Verts.size() + v : v - 1;
					}
					//ind[0] = std::stoi(tokens[1]) - 1;
					//ind[1] = std::stoi(tokens[2]) - 1;
					//ind[2] = std::stoi(tokens[3]) - 1;
					createIndices(ind, mapVertex, fColor);
					continue;
				}
				if (tokens.size() > 4) { //if more verticles then tesselate
					std::vector<GLuint> I1;
					std::vector<GLuint>I2;
					std::vector<vertex>V2;
					for (unsigned int i = 1; i < tokens.size(); ++i) {
						(std::stoi(tokens[i]) < 0) ? I2.push_back(Verts.size() + std::stoi(tokens[i])) : I2.push_back(std::stoi(tokens[i]) - 1);
					}
					for (auto& j : I2) {
						V2.push_back(Verts[j]);
					}
					EarcutWrapper E(V2, I1);
					for (unsigned int i = 0; i < I1.size();i+=3) {
						unsigned int ind[3];
						ind[0] = I2[I1[i]];
						ind[1] = I2[I1[i+1]];
						ind[2] = I2[I1[i+2]];
						createIndices(ind, mapVertex, fColor);
					}
					continue;
				}
			}
		}
	}

	void PolygonSet::createIndices(unsigned int ind[3], std::map<unsigned int, unsigned int>& mapVertex, glm::vec3& fColor)
	{
		for (unsigned int i = 0; i < 3; ++i) {
			if (Verts[ind[i]].color.x < 0) {
				Verts[ind[i]].color = fColor;
				Indices.push_back(ind[i]);
				mapVertex[ind[i]] = ind[i];
			}
			else if (mapVertex.count(ind[i]) > 0) {
				Indices.push_back(mapVertex[ind[i]]);
			}
			else
			{
				//create new vertex
				int vertId = Verts.size();
				mapVertex[ind[i]] = vertId;
				Verts.push_back(Verts[ind[i]]);
				Verts[vertId].color = fColor;
				Indices.push_back(vertId);
			}
		}
	}

	void PolygonSet::Readmtlfile(std::istream& file)
	{
		struct material mtrl;
		std::string mtrlname;
		std::string curline;
		while (std::getline(file, curline))
		{
			std::vector<std::string>tokens;
			split(curline, std::string(" \t"), tokens);
			if (tokens.size() == 0) continue;

			if (tokens[0] == "newmtl" && tokens.size() > 1) {
				if (!mtrlname.empty())//flush
					materials[mtrlname] = mtrl;
				mtrlname = tokens[1];
			}
			else if (tokens[0] == "Ka" && tokens.size() > 3)
				mtrl.Ka = glm::vec3(std::stof(tokens[1]), std::stof(tokens[2]), std::stof(tokens[3]));
			else if (tokens[0] == "Kd" && tokens.size() > 3)
				mtrl.Kd = glm::vec3(std::stof(tokens[1]), std::stof(tokens[2]), std::stof(tokens[3]));
			else if (tokens[0] == "Ks" && tokens.size() > 3)
				mtrl.Ks = glm::vec3(std::stof(tokens[1]), std::stof(tokens[2]), std::stof(tokens[3]));
		}
		if (!mtrlname.empty())//flush
			materials[mtrlname] = mtrl;
	}
}