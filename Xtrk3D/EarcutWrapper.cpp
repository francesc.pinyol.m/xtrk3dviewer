#define GLM_SWIZZLE
#include "EarcutWrapper.h"
#include <earcut/earcut.hpp>
#include "Plane.h"

namespace mapbox {
	namespace util {

		template <>
		struct nth<0, vertex> {
			inline static auto get(const vertex &t) {
				return t.position.x;
			};
		};
		template <>
		struct nth<1, vertex> {
			inline static auto get(const vertex &t) {
				return t.position.y;
			};
		};

		template <>
		struct nth<0, glm::vec3> {
			inline static auto get(const glm::vec3 &t) {
				return t.x;
			};
		};
		template <>
		struct nth<1, glm::vec3> {
			inline static auto get(const glm::vec3 &t) {
				return t.y;
			};
		};
	} // namespace util
} // namespace mapbox


EarcutWrapper::EarcutWrapper(const ClipperLib::Paths& emb, glm::vec3 Color,GLfloat transparancy)
{
	for (unsigned int i = 0; i < emb.size(); ++i) {
		vertices.resize(i+1);
		indices.resize(i+1);
		for (auto ii = emb[i].begin(); ii != emb[i].end(); ++ii)
		{
			vertex v(ii->Z.Pos, glm::vec3(), Color, glm::vec2(ii->Z.Pos.xy),transparancy);
			float z = v.position.z;
			vertices[i].push_back(v);
		}
		using Coord = float;
		using N = GLuint;
		// Create array
		std::vector<std::vector<vertex>> polygon;
		polygon.push_back(vertices[i]);
		indices[i] = mapbox::earcut<N>(polygon);
	}
}


EarcutWrapper::EarcutWrapper(const std::vector<vertex>&vertices, std::vector<GLuint>& indices)
{
	using Coord = float;
	using N = GLuint;

	Plane P = Plane::planeFromPoints(vertices);
	//create transformation matrix
	glm::mat4 Local(1);
	Local = glm::transpose(glm::lookAt( P.Normal(), glm::vec3(), P.mainAxis + P.Normal()));
	std::vector<glm::vec3>planePoints;
	for (auto &i : vertices) {
		glm::vec4 tmp = glm::vec4(P.closestPoint(i.position), 1)*Local;
		planePoints.push_back(tmp.xyz);
	}

	// Create array
	std::vector<std::vector<glm::vec3>> polygon;
	polygon.push_back(planePoints);
	indices = mapbox::earcut<N>(polygon);
}//*/


EarcutWrapper::~EarcutWrapper()
{
}
