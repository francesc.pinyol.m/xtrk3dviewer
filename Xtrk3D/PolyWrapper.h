#pragma once
//https://github.com/ivanfratric/polypartition
#include <vector>
#include "Vertex.h"
#include "clipper.hpp"


class PolyWrapper
{
public:
	enum class polyMode {OPT,EarCut};
	PolyWrapper(polyMode mode,const ClipperLib::Paths& emb, glm::vec3 Color = glm::vec3(1, 0, 0), GLfloat transparancy = 1.0f);
	~PolyWrapper();
	std::vector<std::vector<vertex>> vertices;
	std::vector<std::vector<GLuint>> indices;
protected:
	PolyWrapper();
	void execute_OPT(unsigned int subvector);
	void execute_EC(unsigned int subvector);
};

