#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/quaternion.hpp>

namespace Xtrk3D {
	struct Frustrum
	{
		glm::vec3 farTopLeft;
		glm::vec3 farTopRight;
		glm::vec3 farBottomLeft;
		glm::vec3 farBottomRight;

		glm::vec3 nRight;
		glm::vec3 nLeft;
		glm::vec3 nUp;
		glm::vec3 nDown;

		glm::vec3 MidTopLeft;
		glm::vec3 MidTopRight;
		glm::vec3 MidBottomLeft;
		glm::vec3 MidBottomRight;

		glm::vec3 Normals[4];
		float farDistance;
		float nearDistance;
		glm::vec3 forward;
		glm::vec3 up;
		glm::vec3 left;
		Frustrum() :farDistance(0.0f), nearDistance(0.0f) {};
	};


	class Camera
	{
	public:
		Camera();
		~Camera();

		//Camera position
//		const glm::vec3& GetPosition() const { return mPosition; };
		void SetPosition(const glm::vec3& position);	//set position in world coordinate system
		void Move(const glm::vec3& offset); // move in world coordinate system
		void MoveForward(float movement);	// move forward in camera coord system
		void MoveLeft(float movement);		// move left in camera coord system
		void MoveUp(float movement);		// move up in camera coord system
		glm::vec3 getPosition() const { return mPosition; };

		//viewing
		float fieldOfView() const { return _fieldOfView; };
		void setFieldOfView(float fieldOfViewDeg);
		float nearPlane() const { return _nearPlane; };
		float farPlane() const { return _farPlane; };
		void setClipping(float nearPlane, float farPlane);
		float viewportAspectRatio() const { return _aspectRatio; };
		void setViewportAspectRatio(float viewportAspectRatio);

		//Camera Orientation
		void Rotate(float angleRadians, const glm::vec3& axis);
		void LookAt(glm::vec3 position,glm::vec3 orientation= glm::vec3(0, 1, 0));
		glm::vec3 GetUp() const;
		glm::vec3 GetLeft() const;
		glm::vec3 GetForward() const;
		glm::mat4 getMatrix() const;

		Frustrum F;
		void SetFrustrum() { F = GetPlanes(); };

	private:
		glm::mat4 projection() const;
		glm::mat4 view() const;
		glm::mat4 orientation() const;

	private:
		glm::vec3 mPosition;
		glm::quat mOrientation;
		float _fieldOfView;
		float _nearPlane;
		float _farPlane;
		float _aspectRatio;
		Frustrum GetPlanes();
	};
}