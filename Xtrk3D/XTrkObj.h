#pragma once
#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE

#include <vector>
#include<list>
#include <string>
#include <map>
#include <stack>
#include <fstream>
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#include <glm/glm/gtx/vector_angle.hpp>
#include "clipper.hpp"
#include "Vertex.h"
#include "Scene.h"
#include "TrackPlan.h"
#include <niohman_json/json.hpp>
#include "Interface.h"

using json = nlohmann::json; // for convenience
namespace Xtrk3D {
	class XTrkObj
	{
	public:
		XTrkObj() : m_rotation(0), index(0), layer(0) {};
		XTrkObj(const int t_index, const int t_layer, const glm::vec3& t_pos, const double t_rotation);
		virtual ~XTrkObj(void) { clearSegments(); };
		void clearSegments();
		void addSegment(Segment* pS) { Segments.push_back(pS); };
		virtual void initSurface(TrackPlan* myLayout) {};
		virtual enum xtrkObjType getObjType() { return xtrkUndefined; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count = 0) { return nullptr; };
		virtual void updateSceneNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, TrackPlan* myLayout) {};
		virtual bool OnSelect(bool cm, std::string layer) { return false; };
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index); };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties) { return nullptr; };
		void add3DShapesToNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, glm::vec3& ObjColor, ClipperLib::Paths& surface, float baseZ);
		const glm::mat4& getMatrix() const { return m_transformMatrix; };
		const glm::vec3 getCGposition() const { return CG; };
		const glm::vec3 getPos() const { return m_pos; };
		void setCG_Z(float t_z) { CG.z = t_z; };
		virtual bool isStraightTrack() { return false; };
		virtual void dump(json& j) = 0;
		void dump(json& j, std::string lbl);
		const int getSegmentSize() { return Segments.size(); };
		unsigned int index;
		unsigned int layer;
		glm::vec3 CG;

	protected:
		glm::vec3 m_pos;
		double m_rotation;
		glm::mat4 m_transformMatrix;
		std::vector<Segment*> Segments;
	};

	struct endPoint
	{
		endPoint();
		endPoint(int t_Node2Index, glm::vec3 t_pos, double t_Normal, int t_zDefined);
		int Node2;
		glm::vec3 pos;
		int Z_def;
		int Node2index;
		double Normal;
		int PointListIndex;
		//	std::pair<bool, float>Z_midPoint;
		//	int version;
		bool isOnTurnTable;
		bool isBridgeEdge;
		double distToCurve;
		void dump(json& j);
	};

	class Surface
	{
	public:
		ClipperLib::Paths embTop;
		ClipperLib::Paths embBot;

		ClipperLib::Paths tunnel; //outer shape of tunnel
		ClipperLib::Paths clearence; //cut out shape 

		std::vector<vertex>sideVerts;
		std::vector<GLuint>sideIndices;
		std::vector<vertex>railVerts;
		std::vector<GLuint>railIndices;
		std::vector<glm::mat4>TiePos;
		void clear() {
			embTop.clear();
			embBot.clear();
			tunnel.clear();
			clearence.clear();
			sideVerts.clear();
			sideIndices.clear();
			railVerts.clear();
			railIndices.clear();
			TiePos.clear();
			embTop.resize(1);
			embBot.resize(1);
			tunnel.resize(1);
			clearence.resize(1);
		};
	};

	glm::vec3 to_vec3(Xtrk3D::VEC3 v);
}