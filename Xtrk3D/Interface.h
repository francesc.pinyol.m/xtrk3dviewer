#pragma once

namespace Xtrk3D {

	typedef enum TRK3DTYPE { noObj = 0, turnoutObj = 1, curveObj = 2, straightObj = 3, cornuObj = 4, joinObj = 5, turntableObj = 6, structureObj = 7, drawObj = 8, bzrlinObj = 9} TRK3DTYPE;

	typedef struct VEC3 {
		double x, y, z;
	} VEC3;

	typedef struct ENDPOINT_TYPE {
		int index;		// -1 if unconnected (E4)
		VEC3 position;
		double angle;
		unsigned int options;
		ENDPOINT_TYPE* next;
	} ENDPOINT_TYPE;

	typedef struct SEGMENT_TYPE {
		char type;
		int color;
		double width;
		SEGMENT_TYPE* next;
		union {
			struct {	//type is: S,L,Q,B
				VEC3 p1, p2;
				unsigned int mode;
			} two_points;
			struct {	//type is: C,A,G
				VEC3 center;
				double radius;
				double a0, a1;
			} curve;
			struct {	//type is: Y,F,
				VEC3 center;
				unsigned int count;
				unsigned int mode;
				VEC3* points;
			} pointArray;
			struct {	//type is: Z
				VEC3 pos;
				double angle;
				double height;
				char* str;
			} textString;
		} data_;
	} SEGMENT_TYPE;

	typedef struct OBJ_TYPE {
		int index;
		TRK3DTYPE type;
		unsigned int layer;
		unsigned int width;
		char scale[10];
		int visibility;
		VEC3 point;
		double rotation;
		double radius;
		int turns;
		char* label;
		ENDPOINT_TYPE* endpoints;
		SEGMENT_TYPE* segments;
		OBJ_TYPE* next;
	} OBJ_TYPE;

	extern "C" void init();										// do initialization
	extern "C" void close();									//destroy all interface objects
//	extern "C" int readFile(const char* layoutfile);			//parse layout as a file
	extern "C" void resetScene();								// reset to default camera position
	extern "C" void clearScene();								//clear scene, destroy all draw objects
	extern "C" void prepareViewport(int width, int height);		//Set window size and aspect ratio

	extern "C" void newTrack();									//delete any previus layout and prepare for adding new tracks
	extern "C" int addTracks(OBJ_TYPE* track);					// and add tracks from linked list

	/*
	draw Scene
	caller is responsible for:
		- creating an opengl context, including call to glewInit() and set as current before call to drawScene
		- call prepareViewport when window size change
		- swap buffers
	*/
	extern "C" int drawScene();									// draw Scene --return number of objexts drawn

	extern "C" void moveCamera(float forward, float left, float up);
	extern "C" void rotateCamera(float forwardVector, float leftVector, float upVector);
	extern "C" void setCameraPosition(double x, double y, double z);
	extern "C" void setCameraAt(int x, int y);					//move camera close to object at screen coordinates x,y
	extern "C" void lookAt(double x, double y, double z);

	extern "C" void defineLayer(int layerId, char* name, int color, bool visible);	//set layer properties
	extern "C" bool setLayerVisibility(int layerId, bool visible);
	extern "C" void setVisibleLayers(int argc, bool argv[]);	//set Layer visibilities for all Layers, NB:Layer name and color is not set! 
	extern "C" void setRoomsize(double X, double Y);

	extern "C" void setOptions(bool colorByLayer, int tunnelMode, bool Metric, bool DrawStruct, bool DrawElevations, bool resolution);
	extern "C" void setBenchworkHeight(float datum);

	//Param[15]:	Gauge	code	RailH	RailW	TieL	TieH	TieW	TieCC	embTop	embBot	embH	MaxW	MaxH	ExtendW	ExtendL
	extern "C" void defineScaleParameters(const char* Scale, float param[15]);

	extern "C" int getSelectedObjectId(int x, int y);			//screen coordinates --return track index, if no match: return -1
	extern "C" const char* getLayer(int layerId, int& color, bool& visible);	//get layer properties
	extern "C" float getBenchworkHeight();
}

//*
#include "../Xtrk3DViewer/Select.h"
#include <vector>
namespace Xtrk3D {
	class TrackPlan;
	class SceneNode;
	class BasePlane;
	TrackPlan* getTrackplan();
	SceneNode* getPropertiesAtScreenCoordinates(int x, int y, std::vector<SELECT_TYPE>& properties);
	std::string getObjInfoAtScreenCoordinates(int x, int y);
	void updatePropeties(SceneNode* obj);
	BasePlane* getBasePlane();
	void updateBasePlane();
	void setTrainMode(bool Set, int x, int y);
	void goTrainForward(float speed,bool verbose=false);
	void setTrainReverse(bool Set);
	std::string getTrainPos();
}
//*/
