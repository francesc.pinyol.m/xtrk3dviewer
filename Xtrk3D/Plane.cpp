#include "Plane.h"


Plane::Plane() :a(0), b(0), c(0), d(0)
{
}


Plane::~Plane()
{
}
Plane::Plane(const Plane &P)
{
	a = P.a;
	b = P.b;
	c = P.c;
	d = P.d;
	centroid = P.centroid;
	mainAxis = P.mainAxis;
}

Plane Plane::normalize()
{
	Plane Result;
	float Distance = sqrtf(a * a + b * b + c * c);
	Result.a = a / Distance;
	Result.b = b / Distance;
	Result.c = c / Distance;
	Result.d = d / Distance;
	return Result;
}

Plane Plane::ConstructFromPoints(const glm::vec3 &Pt1, const glm::vec3 &Pt2, const glm::vec3 &Pt3)
{
	glm::vec3 Normal = glm::normalize(glm::cross(Pt2 - Pt1, Pt3 - Pt1));
	return ConstructFromPointNormal(Pt1, Normal);
}

Plane Plane::ConstructFromPointNormal(const glm::vec3 &Pt, const glm::vec3 &Normal)
{
	Plane Result;
	glm::vec3 NormalizedNormal = glm::normalize(Normal);
	Result.a = NormalizedNormal.x;
	Result.b = NormalizedNormal.y;
	Result.c = NormalizedNormal.z;
	Result.d = -glm::dot(Pt, NormalizedNormal);
	return Result;
}

Plane Plane::ConstructFromPointVectors(const glm::vec3 &Pt, const glm::vec3 &V1, const glm::vec3 &V2)
{
	glm::vec3 Normal = glm::cross(V1, V2);
	return ConstructFromPointNormal(Pt, Normal);
}


float Plane::signedDistance(const glm::vec3 &Pt) const
{
	return (a * Pt.x + b * Pt.y + c * Pt.z + d);
}

glm::vec3 Plane::closestPoint(const glm::vec3 &Point)
{
	return (Point - Normal() * signedDistance(Point));
}


Plane Plane::planeFromPoints(const std::vector<vertex> points)
{
	std::vector<glm::vec3>V;
	for (auto &i : points) {
		V.push_back(i.position);
	}
	return planeFromPoints(V);
}

//reference: https://www.ilikebigbits.com/2015_03_04_plane_from_points.html 
Plane Plane::planeFromPoints(const std::vector<glm::vec3> points)
{
	if (points.size() < 3)
		return Plane(); // At least 3 points required

	glm::vec3 centroid;
	for (auto &i : points)
	{
		centroid += i;
	}
	centroid = centroid / (float)(points.size());
	float xx(0), xy(0), xz(0), yy(0), yz(0), zz(0);
	float maxdist(0);
	glm::vec3 maxpoint;
	for (auto &i : points)
	{
		auto r = i - centroid;
		xx += r.x*r.x;
		xy += r.x*r.y;
		xz += r.x*r.z;
		yy += r.y*r.y;
		yz += r.y*r.z;
		zz += r.z*r.z;
		if (glm::length(r) > maxdist) {
			maxdist = glm::length(r);
			maxpoint = i;
		}
	}
	auto det_x = yy * zz - yz * yz;
	auto det_y = xx * zz - xz * xz;
	auto det_z = xx * yy - xy * xy;
	auto det_max = fmax(fmax(det_x, det_y), det_z);
	Plane result;
	if (det_max <= 0.0)
		return result; //points don't span a plane
	if (det_x > det_y && det_x > det_z)
	{
		result = ConstructFromPointNormal(centroid, glm::vec3(det_x, xz*yz - xy * zz, xy*yz - xz * yy));
	}
	else if (det_y > det_z)
	{
		result = ConstructFromPointNormal(centroid, glm::vec3(xz * yz - xy * zz, det_y, xy * xz - yz * xx));
	}
	else {
		result = ConstructFromPointNormal(centroid, glm::vec3(xy*yz - xz * yy, xy*xz - yz * xx, det_z));
	}
	result.centroid = centroid;
	result.mainAxis = result.closestPoint(maxpoint) - centroid;
	return result;
}
