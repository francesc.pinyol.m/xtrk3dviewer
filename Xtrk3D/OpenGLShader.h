#pragma once

#define GLM_FORCE_RADIANS
// third-party libraries
#include <glew/include/GL/glew.h>
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#include "Bitmap.h"
#include "Vertex.h"

#include <vector>
#include <list>
#include <map>
#include <string>
#include <stdexcept>

namespace Xtrk3D {
	typedef enum class Appearance
	{
		gravelSurface
		, concreteSurface
		, darkConcreteSurface
		, tieSurface
		, railSurface
		, bufferstop
		, solidColor
		, digits
		, PlankSurface
		, backgroundImage
	} Appearance;

	static GLuint make_buffer(GLenum target, GLsizei buffer_size, const void *buffer_data)
	{
		GLuint buffer;
		glGenBuffers(1, &buffer);
		glBindBuffer(target, buffer);
		glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);

		GLenum err;
		if ((err = glGetError()) != GL_NO_ERROR)
			throw std::runtime_error("OpenGL::make_buffer failed with code="+std::to_string(err));
		return buffer;
	}


	class OpenGLShader
	{
	public:
		OpenGLShader(const std::string& shaderCode, const GLenum shaderType, const std::string& outName = "");
		~OpenGLShader();
		GLuint GetObject() const;
		OpenGLShader(const OpenGLShader& other);
		OpenGLShader& operator =(const OpenGLShader& other);
		std::string GetVariableName() const { return oName; };
		GLenum GetShaderType() const;
	private:
		GLuint object;
		unsigned* refCount;
		void retain();
		void release();
		std::string oName;
		GLenum oType;
	};

	class OpenGLProgram {
		//Creates a program by linking a list of tdogl::Shader objects

	public:
		OpenGLProgram(const std::vector<OpenGLShader>& shaders);
		~OpenGLProgram();

		GLuint GetObject() const;
		void Use() const;
		bool IsInUse() const;
		void StopUsing() const;

		GLint QueryAttrib(const GLchar* attribName) const;
		GLint GetAttrib(const GLchar* attribName) const;
		GLint QueryUniform(const GLchar* uniformName) const;
		GLint GetUniform(const GLchar* uniformName) const;

		GLint UniformModelMatrix;
		GLint Uniformtex;
		GLint UniformtexScale;

	private:
		GLuint object;
		//copying disabled
		OpenGLProgram(const OpenGLProgram&);
		const OpenGLProgram& operator=(const OpenGLProgram&);
	};


	class OpenGLShaderPrograms {
	public:
		OpenGLShaderPrograms();
		~OpenGLShaderPrograms();
		void Init();
		OpenGLProgram* use(Appearance texAppearance, const glm::mat4& transform, const glm::vec2 textureScale=glm::vec2(1.0f));
		bool TextureExists(const Appearance A) const { return (texture.count(A) != 0); };
		OpenGLProgram* GL_ColorPgm;
		OpenGLProgram* GL_TexPgm;
		OpenGLProgram* GL_PickPgm;
		OpenGLProgram* GL_CharPgm;
		OpenGLProgram* GL_LinePgm;

	private:
		OpenGLProgram*  CreateOpenGLProgram(const GLchar* VertexSource, const GLchar* FragmentSource, const GLchar* GeomertySource=nullptr);
		void RegisterTexture(Appearance A, const unsigned int width, const unsigned int height, const unsigned char* data, OpenGLProgram* Pgm);
		void RegisterTexture(Appearance A, texture_data t_texture, OpenGLProgram* Pgm);
		void RegisterTexture(Appearance A, std::string filename, OpenGLProgram* Pgm);

		std::map<Appearance, std::pair<Texture*, OpenGLProgram*>>texture;
	};
}