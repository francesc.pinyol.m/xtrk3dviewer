#include "segment.h"
#include "track.h"
#include "EarcutWrapper.h"
#include <complex> 
using namespace ClipperLib;
namespace Xtrk3D {

	float Segment::resolution = 1.0f;
	float Segment::minLineWidth = 0.05f;
	void Segment::addPoint(Track* parent, glm::vec3 point, glm::vec3 normal, glm::mat4 &M, float ExtendW, bool ClearanceOnly)
	{
		Scale* param = &parent->param;
		glm::vec4 p(point, 1);
		glm::vec4 n(normal, 0);
		n = glm::normalize(n);
		if (!ClearanceOnly) {
			glm::vec4 p1 = M * (p + n * (float)param->embTopW() / 2.0f + glm::vec4(0, 0, -param->RailH() - param->TieH(), 0));
			embTop[0] << IntPoint(p1.xyz);
			parent->sideVerts.push_back(vertex(p1.xyz, glm::vec3(), glm::vec3(), p1.xy));

			glm::vec4 p2 = M * (p + n * (float)param->embBotW() / 2.0f + glm::vec4(0, 0, -param->embH() - param->RailH() - param->TieH(), 0));
			embBot[0] << IntPoint(p2.xyz);
			glm::vec4 T = (p1 + M * (n*glm::length(p2 - p1)));
			parent->sideVerts.push_back(vertex(p2.xyz, glm::vec3(), glm::vec3(), T.xy));

			glm::vec4 pT = M * (p + n * (ExtendW + param->MaxW() / 2.0f) + glm::vec4(0, 0, param->MaxH(), 0));
			tunnel[0] << IntPoint(pT.xyz);

			float a = (float)param->gauge() / 2;
			float b = (float)(a + param->RailW());
			glm::vec4 p3 = M * (p + n * a + glm::vec4(0, 0, -param->RailH(), 0));
			glm::vec4 p4 = M * (p + n * a);
			glm::vec4 p5 = M * (p + n * b);
			glm::vec4 p6 = M * (p + n * b + glm::vec4(0, 0, -param->RailH(), 0));
			glm::vec3 railColor(0.4f, 0.4f, 0.5f);
			parent->railVerts.push_back(vertex(p3.xyz, glm::vec3(), glm::vec3(), glm::vec2(0, 0)));
			parent->railVerts.push_back(vertex(p4.xyz, glm::vec3(), glm::vec3(), glm::vec2(0, 0.4)));
			parent->railVerts.push_back(vertex(p5.xyz, glm::vec3(), glm::vec3(), glm::vec2(0, 0.6)));
			parent->railVerts.push_back(vertex(p6.xyz, glm::vec3(), glm::vec3(), glm::vec2(0, 1)));
		}
		glm::vec4 pC = M * (p + n * (ExtendW + param->MaxW() / 2.1f) + glm::vec4(0, 0, param->MaxH(), 0));
		clearence[0] << IntPoint(pC.xyz);
	}

	void Segment::setIndices(Track* parent)
	{
		int cur = parent->sideVerts.size() - 1;

		glm::vec3 v1 = glm::cross(parent->sideVerts[cur - 2].position - parent->sideVerts[cur].position, parent->sideVerts[cur - 2].position - parent->sideVerts[cur - 3].position);
		bool up = (glm::dot(v1, glm::vec3(0, 0, 1)) > 0);
		if (up) {
			parent->sideIndices.push_back(cur - 3); parent->sideIndices.push_back(cur - 2); parent->sideIndices.push_back(cur - 1);
			parent->sideIndices.push_back(cur - 2); parent->sideIndices.push_back(cur); parent->sideIndices.push_back(cur - 1);
		}
		else {
			parent->sideIndices.push_back(cur - 3); parent->sideIndices.push_back(cur - 1); parent->sideIndices.push_back(cur - 2);
			parent->sideIndices.push_back(cur - 2); parent->sideIndices.push_back(cur - 1); parent->sideIndices.push_back(cur);
		}

		cur = parent->railVerts.size() - 1;
		if (up) {
			parent->railIndices.push_back(cur - 6);	parent->railIndices.push_back(cur - 5);	parent->railIndices.push_back(cur - 1);
			parent->railIndices.push_back(cur - 6);	parent->railIndices.push_back(cur - 1);	parent->railIndices.push_back(cur - 2);
			parent->railIndices.push_back(cur - 7);	parent->railIndices.push_back(cur - 6);	parent->railIndices.push_back(cur - 3);
			parent->railIndices.push_back(cur - 6);	parent->railIndices.push_back(cur - 2);	parent->railIndices.push_back(cur - 3);
			parent->railIndices.push_back(cur - 4);	parent->railIndices.push_back(cur);	parent->railIndices.push_back(cur - 5);
			parent->railIndices.push_back(cur);	parent->railIndices.push_back(cur - 1);	parent->railIndices.push_back(cur - 5);
		}
		else {
			parent->railIndices.push_back(cur - 6);	parent->railIndices.push_back(cur - 1);	parent->railIndices.push_back(cur - 5);
			parent->railIndices.push_back(cur - 6);	parent->railIndices.push_back(cur - 2);	parent->railIndices.push_back(cur - 1);
			parent->railIndices.push_back(cur - 7);	parent->railIndices.push_back(cur - 3);	parent->railIndices.push_back(cur - 6);
			parent->railIndices.push_back(cur - 6);	parent->railIndices.push_back(cur - 3);	parent->railIndices.push_back(cur - 2);
			parent->railIndices.push_back(cur - 4);	parent->railIndices.push_back(cur - 5);	parent->railIndices.push_back(cur);
			parent->railIndices.push_back(cur);	parent->railIndices.push_back(cur - 5);	parent->railIndices.push_back(cur - 1);
		}
	}

	glm::vec3 Segment::getPoint(float dL)
	{
		glm::vec3 p = point1 + (point2 - point1)* dL / (float)len;
		glm::vec4 P4 = m_transformMatrix * glm::vec4(p, 1);
		return P4.xyz;
	}

	glm::vec3 Segment::getEndpoint(unsigned int i)
	{
		return (m_transformMatrix *glm::vec4((i == 0 ? point1 : point2), 1)).xyz;
	}

	glm::vec3 Segment::getNormal(unsigned int i)
	{
		return (i == 0 ? norm1 : norm2);
	}

	void Segment::setNormal(unsigned int i, glm::vec3 norm)
	{
		if (i == 0) {
			norm1 = norm;
		}
		else {
			norm2 = norm;
		}
	}

	glm::vec3 Segment::getCenterline_Point(glm::vec3 t_pos) {
		glm::vec3 p1((m_transformMatrix*glm::vec4(point1, 1)).xyz);//convert to global coordinates
		glm::vec3 p2((m_transformMatrix*glm::vec4(point2, 1)).xyz);//convert to global coordinates
		float dL(glm::dot(glm::normalize(p2 - p1), t_pos - p1));
		glm::vec3 p(getPoint(dL));
		return p;
	}

	glm::vec3 Segment::getTangent(glm::vec3 t_pos) {
		glm::vec3 p1((m_transformMatrix * glm::vec4(point1, 1)).xyz);//convert to global coordinates
		glm::vec3 p2((m_transformMatrix * glm::vec4(point2, 1)).xyz);//convert to global coordinates
		return glm::normalize(p2 - p1);
		return glm::vec3();
	}

	//2021-06-10

	glm::vec3 Segment::toPosWithinEmbTop(glm::vec3 t_pos, float dR)
	{
		glm::vec3 c_pos(getCenterline_Point(t_pos));
		t_pos.z = c_pos.z;
		float offset= fmax(glm::length(t_pos.xy - c_pos.xy),dR);
		return (glm::normalize(t_pos-c_pos)*offset+c_pos);
	}


	glm::vec3 Segment::toPosWithinEmbTop(glm::vec3 t_from, glm::vec3 t_to, float dR)
	{
		glm::vec3 c_from(getCenterline_Point(t_from));
		glm::vec3 c_to(getCenterline_Point(t_to));
		auto offset(glm::normalize(t_from - c_from)*dR);
		if (glm::length(t_to - c_to) > dR)
			return c_to + offset;
		return t_to;
	}


	void Segment::dump(json& j) {
		dump(j, "Segment");
	}

	void Segment::dump(json& j, std::string lbl) {
		j[lbl]["Point1"] = { std::to_string(point1.x), std::to_string(point1.y), std::to_string(point1.z) };
		j[lbl]["Point2"] = { std::to_string(point2.x), std::to_string(point2.y), std::to_string(point2.z) };
/*		j[lbl]["M"] = json::array();
		for (unsigned int i = 0; i < 4; ++i) {
			for (unsigned int ii = 0; ii < 4; ++ii) {
				j[lbl]["M"].push_back(m_transformMatrix[i][ii]);
			}
		}//*/
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////
	StraightSegment::StraightSegment(glm::vec3& p1, glm::vec3& p2, const glm::mat4& M) : Segment(p1, p2, M)
	{
		len = glm::length(point2.xy() - point1.xy());
		CG = (point1 + point2) / 2.0f;
		glm::vec4 CG4 = M * glm::vec4(CG, 1);
		CG = CG4.xyz;
		norm1 = norm2 = glm::normalize(glm::cross(point2 - point1, glm::vec3(0, 0, 1)));
	}

	void StraightSegment::dump(json& j) {
		Segment::dump(j, "StraightSegment");
	}

	void StraightSegment::Init(Track* parent)
	{
		clear();
		float z1(point1.z);
		float z2(point2.z);
		float E1(parent->param.ExtendW());
		float E2(parent->param.ExtendW());
		std::vector<std::pair<glm::vec3, float>>MidlePoints;

		if (parent->visibility == 10)
		{
			E1 = E2 = 0;
		}

		//calculate tunnel width
		if (parent->isStraightTrack()&& parent->visibility!=10) {//is not on Turnout and is not bridge
			if (glm::length(parent->endp.front().pos - getPoint(0)) < parent->param.gauge()) {
				float zero1 = (float)fmax(0, parent->param.ExtendL() - parent->endp.front().distToCurve);
				E1 = (float)fmax(0, (1 - parent->endp.front().distToCurve / parent->param.ExtendL())*parent->param.ExtendW());
				float zero2 = (float)(len - fmax(0, parent->param.ExtendL() - parent->endp.back().distToCurve));
				E2 = (float)fmax(0, (1 - parent->endp.back().distToCurve / parent->param.ExtendL())*parent->param.ExtendW());

				if (zero1 > zero2) {
					float lowest = (zero1 + zero2) / 2;
					if (lowest > 0.0f && lowest < len)
					{
						float E = (float)fmax(0, (1 - (parent->endp.front().distToCurve + lowest) / parent->param.ExtendL())*parent->param.ExtendW());
						MidlePoints.push_back(std::make_pair(point1 + (point2 - point1)* lowest / (float)len, E));
					}
				}
				else {
					if (zero1 > 0 && zero1 < len) { //addPoint
						MidlePoints.push_back(std::make_pair(point1 + (point2 - point1)* zero1 / (float)len, 0.0f));
					}
					if (zero1 > 0 && zero2 < len) {//addPoint
						MidlePoints.push_back(std::make_pair(point1 + (point2 - point1)* zero2 / (float)len, 0.0f));
					}
				}
			}
		}
		addPoint(parent, point1, norm1, m_transformMatrix, E1);
		for (auto &ii : MidlePoints) {
			addPoint(parent, ii.first, norm1, m_transformMatrix, ii.second);
			setIndices(parent);
		}
		addPoint(parent, point2, norm2, m_transformMatrix, E2);
		setIndices(parent);
		addPoint(parent, point2, -norm2, m_transformMatrix, E2);
		if (MidlePoints.size() > 0)
		{
			if (MidlePoints.size() > 1) {
				addPoint(parent, MidlePoints[1].first, -norm1, m_transformMatrix, MidlePoints[1].second);
				setIndices(parent);
			}
			addPoint(parent, MidlePoints[0].first, -norm1, m_transformMatrix, MidlePoints[0].second);
			setIndices(parent);
		}

		addPoint(parent, point1, -norm1, m_transformMatrix, E1);
		setIndices(parent);

		int tieCount = (int)(len / parent->param.TieCC());
		if (tieCount == 0) {
			if (len / parent->param.TieCC() > 0.5) tieCount = 1;
		}
		glm::vec3 p(glm::normalize(point2 - point1));
		float Angle = glm::orientedAngle(glm::normalize(glm::vec2(p)), glm::vec2(1, 0));

		glm::mat4 Tmatrix = glm::translate(glm::rotate(glm::translate(m_transformMatrix, point1), -Angle, glm::vec3(0, 0, 1)), glm::vec3(parent->param.TieCC() / 2, 0, 0));
		// to be accurate, for steep track rotate along y-axis also
		// Gamma=atan((z2-z1)/len)
		float gamma = (float)(atanf((z2 - z1) / (float)len));

		//sleepers
		float dist = (float)len / tieCount;
		float dz = (z2 - z1) / tieCount;
		for (int i = 0; i < tieCount; ++i) {
			parent->TiePos.push_back(glm::rotate(glm::translate(Tmatrix, glm::vec3(dist*i, 0, (i + 0.5)*dz)), gamma, glm::vec3(0, -1, 0)));
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	JointSegment::JointSegment(glm::vec3& p1, glm::vec3& p2, glm::mat4& M, std::map<int, Track*> *Tracks) : Segment(p1, p2, M)
	{
		len = glm::length(point2.xy() - point1.xy());
		CG = (point1 + point2) / 2.0f;
		glm::vec4 CG4 = M * glm::vec4(CG, 1);
		CG = CG4.xyz;
		T = Tracks;
	}

	void JointSegment::dump(json& j)
	{
		Segment::dump(j, "JointSegment");
	}


	void JointSegment::Init(Track* parent)
	{
		clear();

		float z1(point1.z);
		float z2(point2.z);
		float dz = z2 - z1;
		float E(parent->param.ExtendW());

		glm::vec3 p2p1(point2 - point1);
		p2p1[2] = 0;
		float d(glm::length(p2p1));

		int i(-1);
		if (parent->endp[0].Node2 >= 0 && parent->endp[1].Node2 >= 0) {
			if ((*T)[parent->endp[0].Node2]->getObjType() == XtrkStraight)
				i = 0;
			else if ((*T)[parent->endp[1].Node2]->getObjType() == XtrkStraight)
				i = 1;
		}
		if(i<0) {
			//Exeption: Joint is not between a straight and a curved track
			//so draw a straight segment instead
			//TODO: replace with logarithmical spiral, 

			double A1 = parent->endp[0].Normal;
			double A2 = parent->endp[1].Normal;
			glm::vec3 N[2];
			N[0] = glm::vec3(-cos(A1*M_PI / 180), sin(A1*M_PI / 180), 0);
			N[1] = glm::vec3(-cos(A2*M_PI / 180), sin(A2*M_PI / 180), 0);
			if (glm::dot(N[0], N[1]) < 0) N[1] = -N[1];

			addPoint(parent, point1, N[0], m_transformMatrix, E);
			addPoint(parent, point2, N[1], m_transformMatrix, E);
			setIndices(parent);
			addPoint(parent, point2, -N[1], m_transformMatrix, E);
			addPoint(parent, point1, -N[0], m_transformMatrix, E);
			setIndices(parent);

			//sleepers
			int tieCount = (int)(d / parent->param.TieCC());
			float gamma = atan((z2 - z1) / d);
			glm::vec3 p(glm::normalize(point2 - point1));
			float Angle = glm::orientedAngle(glm::normalize(glm::vec2(p)), glm::vec2(1, 0));
			glm::mat4 Tmatrix = glm::translate(glm::rotate(glm::translate(m_transformMatrix, point1), -Angle, glm::vec3(0, 0, 1)), glm::vec3(parent->param.TieCC() / 2, 0, 0));
			if (tieCount == 0) {
				if (len / parent->param.TieCC() > 0.5) tieCount = 1;
			}
			float dist = (float)len / tieCount;
			float dz = (float)(z2 - z1) / tieCount;
			for (int i = 0; i < tieCount; ++i) {
				parent->TiePos.push_back(glm::rotate(glm::translate(Tmatrix, glm::vec3(dist*i, 0, (i + 0.5)*dz)), gamma, glm::vec3(0, -1, 0)));
			}

			return;
		}

		glm::vec3 straight = (*T)[parent->endp[i].Node2]->endp[0].pos - (*T)[parent->endp[i].Node2]->endp[1].pos;
		straight[2] = 0.0f;
		straight = glm::normalize(straight);
		float L = glm::dot(p2p1, straight);
		glm::vec3 y = p2p1 - L * straight;

		double A1 = parent->endp[0].Normal;
		double A2 = parent->endp[1].Normal;
		glm::vec3 N[2];
		N[0] = glm::vec3(-cos(A1*M_PI / 180), sin(A1*M_PI / 180), 0);
		N[1] = glm::vec3(-cos(A2*M_PI / 180), sin(A2*M_PI / 180), 0);

		glm::vec3 p1(parent->endp[i].pos);
		glm::vec3 p[9];
		p[0] = p1;
		p[1] = p1 + 0.3f*L*straight + 0.03f*y + dz * 0.3f*glm::vec3(0, 0, 1);
		p[2] = p1 + 0.4f*L*straight + 0.06f*y + dz * 0.4f*glm::vec3(0, 0, 1);
		p[3] = p1 + 0.5f*L*straight + 0.125f*y + dz * 0.5f*glm::vec3(0, 0, 1);
		p[4] = p1 + 0.6f*L*straight + 0.21f*y + dz * 0.6f*glm::vec3(0, 0, 1);
		p[5] = p1 + 0.7f*L*straight + 0.33f*y + dz * 0.7f*glm::vec3(0, 0, 1);
		p[6] = p1 + 0.8f*L*straight + 0.49f*y + dz * 0.8f*glm::vec3(0, 0, 1);
		p[7] = p1 + 0.9f*L*straight + 0.72f*y + dz * 0.9f*glm::vec3(0, 0, 1);
		p[8] = parent->endp[i == 0 ? 1 : 0].pos;

		glm::vec3 Norm[9];
		//	Norm[0] = glm::cross(p[1] - p[0], glm::vec3(0, 0, 1));
		Norm[1] = glm::cross(p[2] - p[1], glm::vec3(0, 0, 1));
		Norm[2] = glm::cross(p[3] - p[2], glm::vec3(0, 0, 1));
		Norm[3] = glm::cross(p[4] - p[3], glm::vec3(0, 0, 1));
		Norm[4] = glm::cross(p[5] - p[4], glm::vec3(0, 0, 1));
		Norm[5] = glm::cross(p[6] - p[5], glm::vec3(0, 0, 1));
		Norm[6] = glm::cross(p[7] - p[6], glm::vec3(0, 0, 1));
		Norm[7] = glm::cross(p[8] - p[7], glm::vec3(0, 0, 1));
		Norm[8] = -N[i == 0 ? 1 : 0];

		float TiePos = parent->param.TieCC() / 2;

		std::stack<std::pair<glm::vec3, glm::vec3>>R;
		addPoint(parent, p1, N[i], m_transformMatrix, E);
		R.push(std::make_pair(p1, -N[i]));

		for (int k = 1; k < 9; ++k)
		{
			addPoint(parent, p[k], Norm[k], m_transformMatrix, E);
			R.push(std::make_pair(p[k], -Norm[k]));
			setIndices(parent);
			TiePos = addTies(p[k - 1], p[k], parent, TiePos);
		}

		if (!R.empty()) {
			addPoint(parent, R.top().first, R.top().second, m_transformMatrix, E);
			R.pop();
			while (!R.empty())
			{
				addPoint(parent, R.top().first, R.top().second, m_transformMatrix, E);
				setIndices(parent);
				R.pop();
			}
		}
	}

	float JointSegment::addTies(glm::vec3 point1, glm::vec3 point2, Track* parent, float Start)
	{
		glm::vec3 p(point2 - point1);
		float L = glm::length(p);
		float gamma = (float)(atan((point2[2] - point1[2])) / len);
		float cc = parent->param.TieCC();
		int i = 0;
		if (Start < cc / 2) Start = cc / 2;
		float Angle = glm::orientedAngle(glm::normalize(glm::vec2(p)), glm::vec2(1, 0));
		glm::mat4 Tmatrix = glm::translate(glm::rotate(glm::translate(m_transformMatrix, point1), -Angle, glm::vec3(0, 0, 1)), glm::vec3(Start, 0, 0));
		for (; i*cc + Start < L; ++i) {
			parent->TiePos.push_back(glm::rotate(glm::translate(Tmatrix, glm::vec3(i*cc, 0, (i*cc + Start) / L * p[2])), gamma, glm::vec3(0, -1, 0)));
		}
		return i * cc - L;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////
	CurveSegment::CurveSegment(glm::vec3& center, double radius, double startAngle, double diffAngle, int turns, const glm::mat4& M)
		:Center(center)
		, Radius(radius)
		, StartAngle(startAngle)
		, DiffAngle(diffAngle + turns * 360.0)
		, Turns(turns)
	{
		reverse = (Radius < 0);
		Radius = fabs(Radius);

		float StartRad = (float)(StartAngle / 180.0*M_PI);
		float DiffRad = (float)(DiffAngle / 180.0*M_PI);
		len = fabs(DiffRad*Radius);
		point1 = glm::vec3(center.x + sin(StartRad)*Radius, center.y + cos(StartRad)*Radius, 0);
		point2 = glm::vec3(center.x + sin(StartRad + DiffRad)*Radius, center.y + cos(StartRad + DiffRad)*Radius, 0);
		len = fmax(len, glm::length(point2 - point1));

		CG = glm::vec3(center.x + sin(StartRad + DiffRad / 2.0f)*Radius, center.y + cos(StartRad + DiffRad / 2.0f)*Radius, 0);
		glm::vec4 CG4 = M * glm::vec4(CG, 1);
		CG = CG4.xyz;
		m_transformMatrix = M;
		norm1 = glm::normalize(point1 - Center);
		norm2 = glm::normalize(point2 - Center);
	}

	void CurveSegment::dump(json& j)
	{
		Segment::dump(j, "CurveSegment");
		j["CurveSegment"]["Center"] = { std::to_string(Center.x), std::to_string(Center.y), std::to_string(Center.z) };
		j["CurveSegment"]["Radius"] = std::to_string(Radius);
		j["CurveSegment"]["StartAngle"] = std::to_string(StartAngle);
		j["CurveSegment"]["DiffAngle"] = std::to_string(DiffAngle);
		j["CurveSegment"]["Reverse"] = reverse;
		j["CurveSegment"]["Turns"] = Turns;
	}

	void CurveSegment::Init(Track* parent)
	{
		clear();
		float z1(point1.z);
		float z2(point2.z);
		float E(parent->param.ExtendW());
		if (parent->visibility == 10)
		{
			E = 0;
		}

		//	float resolution = 2.0f;
		int count = std::max((int)(DiffAngle / resolution), 1);
		float angleRad = (float)(StartAngle / 180.0*M_PI);
		float DC = (float)(DiffAngle / count * M_PI / 180);
		const float uDC = (float)(0.5*M_PI / 180);
		std::stack<std::pair<glm::vec3, glm::vec3>>R;
		glm::vec3 P0;
		for (int i = 0; i <= count; ++i)
		{
			if (i == 0) {
				P0 = point1;
				auto Pminus = glm::vec3(Center.x + sin(angleRad - uDC)*Radius, Center.y + cos(angleRad - uDC)*Radius, z1);
				glm::vec3 Offset(P0 - Center);
				Offset = glm::normalize(Offset);
				addPoint(parent, Pminus, Offset, m_transformMatrix, E / 2, true);
			}
			else if (i == count)
				P0 = point2;
			else
				P0 = glm::vec3(Center.x + sin(angleRad)*Radius, Center.y + cos(angleRad)*Radius, ((z2 - z1)*i / count + z1));

			angleRad += DC;
			glm::vec3 Offset(P0 - Center);
			Offset.z = 0;
			Offset = glm::normalize(Offset);
			R.push(std::make_pair(P0, -Offset));
			addPoint(parent, P0, Offset, m_transformMatrix, E);
			if (i > 0)
				setIndices(parent);
		}
		//*
		glm::vec3 Pplus = glm::vec3(Center.x + sin(angleRad + uDC)*Radius, Center.y + cos(angleRad + uDC)*Radius, z2);
		glm::vec3 Offset(Pplus - Center);
		Offset.z = 0;
		Offset = glm::normalize(Offset);
		addPoint(parent, Pplus, Offset, m_transformMatrix, E / 2, true);
		//*/

		if (!R.empty()) {
			addPoint(parent, R.top().first, R.top().second, m_transformMatrix, E);
			R.pop();
			while (!R.empty())
			{
				addPoint(parent, R.top().first, R.top().second, m_transformMatrix, E);
				setIndices(parent);
				R.pop();
			}
			int tieCount = (int)(len / parent->param.TieCC());
			float tieDC = (float)(DiffAngle / tieCount * M_PI / 180);
			angleRad = (float)(StartAngle / 180.0*M_PI);
			for (auto i = 0; i < tieCount; ++i)
			{
				float Angle((float)(angleRad + tieDC * (i + 0.5)));
				float dz = (float)(z1 + (z2 - z1) / tieCount * (i + 0.5));
				float gamma = (float)atanf((z2 - z1) / (float)len);
				parent->TiePos.push_back(
					glm::rotate(
						glm::translate(glm::rotate(glm::translate(m_transformMatrix, Center), -Angle, glm::vec3(0, 0, 1)), glm::vec3(0, Radius, dz))
						, gamma, glm::vec3(0, -1, 0))
				);
			}
		}
		if (Turns > 0) {  // helix
	//		parent->sideIndices.clear();
			embTop.clear();
			embBot.clear();
			tunnel.clear();
			clearence.clear();
			embTop.resize(1);
			embBot.resize(1);
			tunnel.resize(1);
			clearence.resize(1);//*/
			InitHelix(parent);
		}
	}

	void CurveSegment::InitHelix(Track* parent)
	{
		Scale* param = &parent->param;
		int count = std::max((int)(DiffAngle / resolution), 1);
		float angleRad = (float)(StartAngle / 180.0*M_PI);
		float DC = (float)(DiffAngle / count * M_PI / 180);
		float z1(point1.z);
		float z2(point2.z);
		glm::vec3 P0;
		for (int i = 0; i <= count; ++i)
		{
			if (i == 0)
				P0 = point1;
			else if (i == count)
				P0 = point2;
			else
				P0 = glm::vec3(Center.x + sin(angleRad)*Radius, Center.y + cos(angleRad)*Radius, ((z2 - z1)*i / count + z1));

			angleRad += DC;
			glm::vec3 Offset(P0 - Center);
			Offset.z = 0;
			Offset = glm::normalize(Offset);

			glm::vec4 p(P0, 1);
			glm::vec4 n(Offset, 0);
			n = glm::normalize(n);

			glm::vec4 pb1 = m_transformMatrix * (p + n * (float)param->embBotW() / 2.0f + glm::vec4(0, 0, -param->embH() - param->RailH() - param->TieH(), 0));
			glm::vec4 pt1 = m_transformMatrix * (p + n * (float)param->embTopW() / 2.0f + glm::vec4(0, 0, -param->RailH() - param->TieH(), 0));
			glm::vec4 pt2 = m_transformMatrix * (p - n * (float)param->embTopW() / 2.0f + glm::vec4(0, 0, -param->RailH() - param->TieH(), 0));
			glm::vec4 pb2 = m_transformMatrix * (p - n * (float)param->embBotW() / 2.0f + glm::vec4(0, 0, -param->embH() - param->RailH() - param->TieH(), 0));
			parent->sideVerts.push_back(vertex(pb1.xyz, glm::vec3(), glm::vec3(), pb1.xy));
			parent->sideVerts.push_back(vertex(pt1.xyz, glm::vec3(), glm::vec3(), pt1.xy));
			parent->sideVerts.push_back(vertex(pt2.xyz, glm::vec3(), glm::vec3(), pt2.xy));
			parent->sideVerts.push_back(vertex(pb2.xyz, glm::vec3(), glm::vec3(), pb2.xy));
			if (i > 0) {
				int cur = parent->sideVerts.size() - 1;

				//			glm::vec3 v1 = glm::cross(parent->sideVerts[cur - 2].position - parent->sideVerts[cur].position, parent->sideVerts[cur - 2].position - parent->sideVerts[cur - 3].position);
				//			bool up = (glm::dot(v1, glm::vec3(0, 0, 1)) > 0);
				//			if (up) {
				parent->sideIndices.push_back(cur - 7); parent->sideIndices.push_back(cur - 3); parent->sideIndices.push_back(cur - 2);
				parent->sideIndices.push_back(cur - 7); parent->sideIndices.push_back(cur - 2); parent->sideIndices.push_back(cur - 6);

				parent->sideIndices.push_back(cur - 6); parent->sideIndices.push_back(cur - 2); parent->sideIndices.push_back(cur - 1);
				parent->sideIndices.push_back(cur - 6); parent->sideIndices.push_back(cur - 1); parent->sideIndices.push_back(cur - 5);

				parent->sideIndices.push_back(cur - 5); parent->sideIndices.push_back(cur - 1); parent->sideIndices.push_back(cur);
				parent->sideIndices.push_back(cur - 5); parent->sideIndices.push_back(cur); parent->sideIndices.push_back(cur - 4);

				parent->sideIndices.push_back(cur - 4); parent->sideIndices.push_back(cur); parent->sideIndices.push_back(cur - 3);
				parent->sideIndices.push_back(cur - 4); parent->sideIndices.push_back(cur - 3); parent->sideIndices.push_back(cur - 7);
			}
			/*			else {
							parent->sideIndices.push_back(cur - 7); parent->sideIndices.push_back(cur - 2); parent->sideIndices.push_back(cur - 3);
							parent->sideIndices.push_back(cur - 7); parent->sideIndices.push_back(cur - 6); parent->sideIndices.push_back(cur - 2);

							parent->sideIndices.push_back(cur - 6); parent->sideIndices.push_back(cur - 1); parent->sideIndices.push_back(cur - 2);
							parent->sideIndices.push_back(cur - 6); parent->sideIndices.push_back(cur - 5); parent->sideIndices.push_back(cur - 1);

							parent->sideIndices.push_back(cur - 5); parent->sideIndices.push_back(cur); parent->sideIndices.push_back(cur-1);
							parent->sideIndices.push_back(cur - 5); parent->sideIndices.push_back(cur-4); parent->sideIndices.push_back(cur);

							parent->sideIndices.push_back(cur - 4); parent->sideIndices.push_back(cur-3); parent->sideIndices.push_back(cur);
							parent->sideIndices.push_back(cur - 4); parent->sideIndices.push_back(cur - 7); parent->sideIndices.push_back(cur - 3);
						}
					}//*/
		}
	}

	glm::vec3 CurveSegment::getPoint(float dL)
	{
		double angleRad = StartAngle / 180.0*M_PI + dL / Radius;
		float dz = (point2.z - point1.z)*dL / (float)len + point1.z;
		glm::vec4 p = glm::vec4(Center.x + sin(angleRad)*Radius, Center.y + cos(angleRad)*Radius, dz, 1);
		glm::vec4 P4 = m_transformMatrix * p;	//convert to global coordinates
		return P4.xyz;
	}

	glm::vec3 CurveSegment::getCenterline_Point(glm::vec3 t_pos)
	{
		glm::vec2 CenterPoint((m_transformMatrix * glm::vec4(Center, 1)).xy);
		glm::vec2 p1((m_transformMatrix * glm::vec4(point1, 1)).xy);//convert to global coordinates
		p1 = p1 - CenterPoint;
		//glm::vec2 p2((m_transformMatrix * glm::vec4(point2, 1)).xy);//convert to global coordinates
		//p2 = p2 - CenterPoint;
		glm::vec2 relativePos(glm::normalize(t_pos.xy - CenterPoint));

		double phi = std::arg(std::complex<double>(relativePos.y,relativePos.x));
		while (phi < 0) phi += 2 * M_PI;

		double A1 = std::arg(std::complex<double>(p1.y, p1.x));
		while (A1 < 0) A1 += 2*M_PI;

		double A_diff = phi - A1;
		while (A_diff < 0) A_diff += 2 * M_PI;

		double L = A_diff * Radius;
		glm::vec3 p_dL(getPoint((float)L));
		return p_dL;

		// don�t handle when curve > 180 degrees
		/*glm::vec2 p1((m_transformMatrix * glm::vec4(point1, 1)).xy);//convert to global coordinates
		glm::vec2 a(glm::normalize(p1 - CenterPoint));
		glm::vec2 b(glm::normalize(t_pos.xy - CenterPoint));
		glm::vec3 p(getPoint(acosf(glm::dot(a, b)) * (float)Radius));
		if (glm::all(glm::isnan(p))) {
			return t_pos;
		}
		return p;//*/
	}

	glm::vec3 CurveSegment::getTangent(glm::vec3 t_pos) {
		glm::vec3 C((m_transformMatrix * glm::vec4(Center, 1)).xyz);
		return glm::normalize(glm::cross(t_pos - C, glm::vec3(0, 0, 1)));
 }

	glm::vec3 CurveSegment::toPosWithinEmbTop(glm::vec3 t_pos, float dR)
	{
		glm::vec3 C((m_transformMatrix*glm::vec4(Center, 1)).xyz);
		float R = glm::length(t_pos - C);
		R = (float) fmax(fmin(R, Radius + dR), Radius - dR);
		glm::vec3 p(glm::normalize(t_pos - C)*R + C);
		p.z = getCenterline_Point(p).z;
		return(p);
	}

	glm::vec3 CurveSegment::toPosWithinEmbTop(glm::vec3 t_from, glm::vec3 t_to, float dR)
	{
		glm::vec3 C((m_transformMatrix*glm::vec4(Center, 1)).xyz);
		C.z = getCenterline_Point(t_to).z;
		double R = glm::length(t_from - C);
		if (R > Radius + dR) R = Radius + dR;
		if (R < Radius - dR) R = Radius - dR;
		return(glm::normalize(t_to - C)*(float)R + C);
	}

}