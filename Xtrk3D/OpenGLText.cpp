#define GLM_FORCE_RADIANS
#include "OpenGLText.h"
#include "Vertex.h"
#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/type_ptr.hpp>

namespace Xtrk3D {
	OpenGLText::OpenGLText() :shaders(nullptr)
	{
		/*	glyphs['0'] = glm::vec2(0, 0);
			glyphs['1'] = glm::vec2(1 / 16.0f, 0);
			glyphs['2'] = glm::vec2(2 / 16.0f, 0);
			glyphs['3'] = glm::vec2(3 / 16.0f, 0);
			glyphs['4'] = glm::vec2(4 / 16.0f, 0);
			glyphs['5'] = glm::vec2(5 / 16.0f, 0);
			glyphs['6'] = glm::vec2(6 / 16.0f, 0);
			glyphs['7'] = glm::vec2(7 / 16.0f, 0);
			glyphs['8'] = glm::vec2(8 / 16.0f, 0);
			glyphs['9'] = glm::vec2(9 / 16.0f, 0);
			glyphs['.'] = glm::vec2(10 / 16.0f, 0);
			glyphs['+'] = glm::vec2(11 / 16.0f, 0);
			glyphs['-'] = glm::vec2(12 / 16.0f, 0);
			glyphs['*'] = glm::vec2(13 / 16.0f, 0);
			glyphs['/'] = glm::vec2(14 / 16.0f, 0);
			glyphs['%'] = glm::vec2(15 / 16.0f, 0);//*/


		std::vector<vertex> vertices;
		vertices.push_back(vertex(glm::vec3(0, 0, 0), glm::vec3(), glm::vec3(0, 0, 0), glm::vec2(0, 0)));
		vertices.push_back(vertex(glm::vec3(0.2, 0, 0), glm::vec3(), glm::vec3(0, 0, 0), glm::vec2(1 / 16.0f, 0)));
		vertices.push_back(vertex(glm::vec3(0, 0.5, 0), glm::vec3(), glm::vec3(0, 0, 0), glm::vec2(0, 1 / 6.0f)));
		vertices.push_back(vertex(glm::vec3(0.2, 0.5, 0), glm::vec3(), glm::vec3(0, 0, 0), glm::vec2(1 / 16.0f, 1 / 6.0f)));
		std::vector<GLuint> elements = { 0,1,2, 1,3,2 };
		drawCount = elements.size();
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		//vertex data
		vbo = make_buffer(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertex), vertices.data());

		glEnableVertexAttribArray(0);//position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), 0);

		glEnableVertexAttribArray(2);//texCoord
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_TRUE, sizeof(vertex), (const GLvoid*)(vertex_texCoordIndex));

		// Create an element array
		ebo = make_buffer(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(GLuint), elements.data());

		// unbind the VAO
//		glBindVertexArray(0);
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}


	OpenGLText::~OpenGLText()
	{
	}

	OpenGLText::OpenGLText(OpenGLShaderPrograms* programs) :OpenGLText()
	{
		shaders = programs;
	}

	int OpenGLText::Render(const glm::mat4& transform, const std::string& str, const glm::vec3& color)
	{
		OpenGLProgram* pgm = shaders->use(Appearance::digits, transform);
		if (pgm == nullptr)
			return 0;
		GLint glyphoffset = pgm->QueryUniform("glyphoffset");
		GLint glyphColor = pgm->QueryUniform("textColor");
//		GLint CameraUniform = pgm->QueryUniform("cameraMatrix");

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glm::mat4 M(transform);
		int row(0);
		int col(0);
		if (str.length() == 0)
			return 0;
		for (auto ii = str.begin(); ii != str.end(); ++ii)
		{
			if (*ii == '\\')
			{
				++ii;
				if (ii == str.end())
					return 0;
				switch (*ii) {
				case 'n':
					M = glm::translate(transform, glm::vec3(0, -0.5*(++row), 0));
					col = 0;
					continue;
				case 't': {
					//                int move = ((col / 8)+1);
					col = ((col / 8) + 1) * 8;
					M = glm::translate(transform, glm::vec3(col*0.2, -0.5*(row), 0));
					continue;
				}
				case '\\':
					break;
				default:
					continue;
				}
			}
			else if (*ii == '"')
			{
				//check ""
				++ii;
				if (ii == str.end())
					return 0;
				if (*ii != '"')
					continue;
			}
			else if (*ii < 0x20 || *ii>0x7f) {
				continue;
			}

			int x = (*ii - 32) & 0xf;
			int y = (*ii - 16) >> 4;
			glm::vec2 offset(x / 16.0f, -y / 6.0f);

			glUniformMatrix4fv(pgm->UniformModelMatrix, 1, GL_FALSE, glm::value_ptr(M));
//			glUniformMatrix4fv(CameraUniform, 1, GL_FALSE, glm::value_ptr(myCamera.getMatrix()));
			M = glm::translate(M, glm::vec3(0.2, 0, 0));
			col++;

			glUniform2fv(glyphoffset, 1, glm::value_ptr(offset));
			glUniform3fv(glyphColor, 1, glm::value_ptr(color));
			glDrawElements(GL_TRIANGLES, drawCount, GL_UNSIGNED_INT, 0);
		}
		return str.length() * 2;
	}

}