#pragma once

#include "XTrkObj.h"
namespace Xtrk3D {
	class Segment :public XTrkObj, public Surface
	{
	public:
		Segment() :len(0) {};

		Segment(glm::vec3& p1, glm::vec3& p2, const glm::mat4& M) :len(0), point1(p1), point2(p2) { m_transformMatrix = M; };
		virtual void setSegmentZ(glm::vec3 P1, glm::vec3 P2) {};
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkSegment; };
		glm::vec3 point1, point2;
		virtual glm::vec3 getPoint(float dL);
		glm::vec3 getEndpoint(unsigned int i);
		glm::vec3 getNormal(unsigned int i);
		void setNormal(unsigned int i, glm::vec3 norm);
		virtual void setEndpointZ(int pointNr, float value) {};
		virtual void add(const glm::vec3& point) {};

		virtual glm::vec3 getTangent(glm::vec3 t_pos);
		virtual glm::vec3 getCenterline_Point(glm::vec3 t_pos);
		virtual glm::vec3 toPosWithinEmbTop(glm::vec3 t_pos, float dR);
		virtual glm::vec3 toPosWithinEmbTop(glm::vec3 t_from, glm::vec3 t_to,float dR); //depreciated
		virtual void dump(json& j);

	protected:
		void dump(json& j, std::string lbl);
		glm::vec3 norm1, norm2;
		void addPoint(Track* parent, glm::vec3 point, glm::vec3 normal, glm::mat4 &M, float dL, bool ClearanceOnly = false);
		void setIndices(Track* parent);

	public:
		double len;
		static float resolution;
		static float minLineWidth;
	};

	class StraightSegment :public Segment
	{
	public:
		StraightSegment(glm::vec3& p1, glm::vec3& p2, const glm::mat4& M);
		virtual void setSegmentZ(glm::vec3 P1, glm::vec3 P2) {
			point1.z = P1.z;
			point2.z = P2.z;
		};
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return XtrkStraight; };
		virtual void dump(json& j);
	};

	//jointSegment calulate curvature:
	// if neighbour tracks are straight and curve: according to MOROP NEM113 chapter 4.1 (https://www.morop.eu/downloads/nem/de/nem113_d.pdf)
	// else straight segmemnt between endpoints
	class JointSegment :public Segment
	{
	public:
		JointSegment(glm::vec3& p1, glm::vec3& p2, glm::mat4& M, std::map<int, Track*> *Tracks);
		virtual void setSegmentZ(glm::vec3 P1, glm::vec3 P2) {
			point1.z = P1.z;
			point2.z = P2.z;
		};
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return XtrkJoint; };
		virtual void dump(json& j);

	private:
		std::map<int, Track*> *T;
		float addTies(glm::vec3 point1, glm::vec3 point2, Track* parent, float Start);
	};

	class CurveSegment :public Segment
	{
	public:
		CurveSegment(glm::vec3& center, double radius, double startAngle, double diffAngle, int turns, const glm::mat4& M);
		virtual void setSegmentZ(glm::vec3 P1, glm::vec3 P2) {
			if (!reverse) {
				point1.z = P1.z;
				point2.z = P2.z;
			}
			else {
				point1.z = P2.z;
				point2.z = P1.z;
			}
		};
		virtual glm::vec3 getPoint(float dL);
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return XtrkCurve; };
		virtual glm::vec3 getTangent(glm::vec3 t_pos);
		virtual glm::vec3 getCenterline_Point(glm::vec3 t_pos);
		virtual glm::vec3 toPosWithinEmbTop(glm::vec3 t_pos, float dR);
		virtual glm::vec3 toPosWithinEmbTop(glm::vec3 t_from, glm::vec3 t_to, float dR);//depreciated
		virtual void dump(json& j);

	protected:
		glm::vec3 Center;
		double Radius;
		double StartAngle;
		double DiffAngle;
		bool reverse;
		int Turns;
		void InitHelix(Track* parent);
	};
}