#pragma once

#include"segment.h"
#include "../thirdparty/stl_reader/stl_reader.h"

namespace Xtrk3D {


	class PolygonSet : public Segment
	{
	public:
		PolygonSet() { ObjColor = glm::vec3(0.6, 0.2, 0.2); };
		PolygonSet(json& js, const glm::mat4& M, float scaleFactor, std::string default_path);
		void readFile(std::string path, const glm::mat4& M, float scaleFactor);
		void ReadSTLfile(const glm::mat4& M, float scaleFactor);
		void ReadOBJfile(std::istream& file, const glm::mat4& M, float scaleFactor);
		void Readmtlfile(std::istream& file);
		void ReadZIPfile(std::string Path, const glm::mat4& M, float scaleFactor);

		virtual void Init(Track* parent);
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void initSurface(TrackPlan* myLayout) {};
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count = 0);

		std::vector<vertex>Verts;
		std::vector<GLuint>Indices;
		std::string _filename;

	protected:
		float color[3]{ 0,0,0 };
		glm::vec3 ObjColor;
		struct material {
			glm::vec3 Ka;
			glm::vec3 Kd;
			glm::vec3 Ks;
		};
		void split(const std::string& str, const std::string& delimiter, std::vector<std::string>& tokens);
		void createIndices(unsigned int ind[3], std::map<unsigned int, unsigned int>& mapVertex, glm::vec3& fColor);
		std::map<std::string, material> materials;
		std::string _path;
	};

}