#pragma once

#define _USE_MATH_DEFINES
#include <glew/include/GL/glew.h>
#include <vector>

// include OpenGL
#ifdef __WXMAC__
#include "OpenGL/glu.h"
#include "OpenGL/gl.h"
#else
#include <GL/glu.h>
#include <GL/gl.h>
#endif

#include "Camera.h"
namespace Xtrk3D {

	class OpenGLProgram;

//*/

	class Axes
	{
	public:
		~Axes();
		Axes() :drawCount(0), drawTrianglesProgram(nullptr), drawLineProgram(nullptr), vao(-1), vbo(-1), ebo(-1) {};
		void InitDraw(OpenGLProgram* TriangleShaders, OpenGLProgram* LineShaders);
		void Render(const Camera& myCamera);
	protected:
		OpenGLProgram* drawTrianglesProgram;
		OpenGLProgram* drawLineProgram;
		GLuint vbo;
		GLuint ebo;
		GLuint vao;
		GLint drawCount;
		std::vector<GLuint> elements = { 0,1,3,4,6,7 };
		std::vector<GLuint> elementsX[2];
		std::vector<GLuint> elementsY[2];
		std::vector<GLuint> elementsZ[2];
	};
}