#pragma once

#include <glm/glm/glm.hpp>
#include <vector>
#include "Vertex.h"

class Plane
{
public:
	//Initalization
	Plane();
	Plane(const Plane &P);

	// Static constructors
	static Plane ConstructFromPointNormal(const glm::vec3 &Pt, const glm::vec3 &Normal);                //loads the plane from a point on the surface and a normal vector
	static Plane ConstructFromPointVectors(const glm::vec3 &Pt, const glm::vec3 &V1, const glm::vec3 &V2);    //loads the plane from a point on the surface and two vectors in the plane
	static Plane ConstructFromPoints(const glm::vec3 &Pt1, const glm::vec3 &Pt2, const glm::vec3 &Pt3);        //loads the plane from 3 points on the surface
	static Plane planeFromPoints(const std::vector<glm::vec3> points);
	static Plane planeFromPoints(const std::vector<vertex> points);

	~Plane();

	//Normalization
	Plane normalize();

	//Math functions
	glm::vec3 Normal() { return glm::vec3(a, b, c); };
	glm::vec3 closestPoint(const glm::vec3 &point);
	float signedDistance(const glm::vec3 &Pt) const;

	float a, b, c, d;        //the (a, b, c, d) in a*x + b*y + c*z + d = 0.
	glm::vec3 mainAxis;
	glm::vec3 centroid;
};

