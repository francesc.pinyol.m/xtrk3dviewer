#include "track.h"
#include <sstream>
#include <iomanip>

#include "EarcutWrapper.h"
#include "PolyWrapper.h"
#include "crossTie.h"
#include "turnout.h" //need straightSegment and curveSegment declarations
#include "Structure.h"

using namespace ClipperLib;

//const float Iscale = 1000.0f;  // defined in clipper.hpp instead
std::map<unsigned int, int> defaultLayerColor = {
 {0,255}
,{1,128}
,{2,32768}
,{3,16776960}
,{4,65280}
,{5,65535}
,{6,8388608}
,{7,8388736}
,{8,8421376}
,{9,16711935}
};

//glm::vec3 to_vec3(Xtrk3D::VEC3 v) { return glm::vec3(v.c, v.y, v.z); };

//-----------------------------------------------------------------------------------------------------------
void CalcZCallback(const IntPoint& e1bot, IntPoint& e1top, IntPoint& e2bot, IntPoint& e2top, IntPoint& pt)
{
	//Just let new z be average of z at intersection
	glm::vec3 pt1 = glm::vec3(pt.X / Iscale, pt.Y / Iscale, 0);

	auto x11 = glm::length(e1top.Z.Pos.xy - e1bot.Z.Pos.xy);
	auto x12 = glm::length(pt1.xy - e1bot.Z.Pos.xy);
	auto z11 = (e1top.Z.Pos.z - e1bot.Z.Pos.z)*x12 / x11 + e1bot.Z.Pos.z;
	auto x21 = glm::length(e2top.Z.Pos.xy - e2bot.Z.Pos.xy);
	auto x22 = glm::length(pt1.xy - e2bot.Z.Pos.xy);
	auto z21 = (e2top.Z.Pos.z - e2bot.Z.Pos.z)*x22 / x21 + e2bot.Z.Pos.z;
	pt1.z = (z11 + z21) / 2;
	pt.Z.Pos = pt1;

	//	double z1 = (e1bot.Z.Pos.z + e1top.Z.Pos.z + e2bot.Z.Pos.z + e2top.Z.Pos.z) / 4;
	//	pt.Z.Pos = glm::vec3(pt.X / Iscale, pt.Y / Iscale, z1);
	pt.Z.Flag = (e1bot.Z.Flag || e1top.Z.Flag || e2bot.Z.Flag || e2top.Z.Flag); // if flag at least one point in iPath
}

//-----------------------------------------------------------------------------------------------------------
Paths join(Paths &pgs, Paths &clp)
{
	Clipper c;
	Paths sol;
	c.ZFillFunction((ZFillCallback)&CalcZCallback);
	c.AddPaths(pgs, ptSubject, true);
	c.AddPaths(clp, ptClip, true);
	c.Execute(ctUnion, sol, pftEvenOdd, pftEvenOdd);
	return(sol);
}

//-----------------------------------------------------------------------------------------------------------
Paths subtract(Paths &pgs, Paths &clp)
{
	Clipper c;
	Paths sol;
	c.ZFillFunction((ZFillCallback)&CalcZCallback);
	c.AddPaths(pgs, ptSubject, true);
	c.AddPaths(clp, ptClip, true);
	c.Execute(ctDifference, sol, pftEvenOdd, pftEvenOdd);
	return(sol);
}

//-----------------------------------------------------------------------------------------------------------
Path intersect(Paths &pgs, Paths &clp,bool closedPath=true)
{
	Clipper c;
	PolyTree sol;
	c.ZFillFunction((ZFillCallback)&CalcZCallback);
	c.AddPaths(pgs, ptSubject, closedPath);
	c.AddPaths(clp, ptClip, true);
	c.Execute(ctIntersection, sol, pftEvenOdd, pftEvenOdd);
	if (sol.ChildCount() > 0)
		return (sol.GetFirst()->Contour);
	return Path();
}

//===========================================================================================================
//-----------------------------------------------------------------------------------------------------------
namespace Xtrk3D {
	endPoint::endPoint(int t_Node2Index, glm::vec3 t_pos, double t_Normal, int t_zDefined)
		:Node2(t_Node2Index)
		, pos(t_pos)
		, Normal(t_Normal)
		, Z_def(t_zDefined)
		, Node2index(-1)
		, PointListIndex(-1)
		, isOnTurnTable(false)
		, isBridgeEdge(false)
		, distToCurve(0.0f)
	{}

	//-----------------------------------------------------------------------------------------------------------
	endPoint::endPoint()
		:Z_def(0), Node2(-1)
		, Normal(0)
		, Node2index(-1)
		, PointListIndex(-1)
		, isOnTurnTable(false)
		, isBridgeEdge(false)
		, distToCurve(0.0f)
	{}

	//-----------------------------------------------------------------------------------------------------------
	void endPoint::dump(json& j)
	{
		json jj;
		jj["Coord"]= { std::to_string(pos.x),std::to_string(pos.y),std::to_string(pos.z) };
		jj["TrackId"] = Node2;
		jj["pointIndex"] = Node2index;
		jj["Normal"] = std::to_string(Normal);
		jj["Zdef"] = Z_def;
		jj["OnTurntable"] = isOnTurnTable;
		jj["BridgeEdge"] = isBridgeEdge;
		jj["CurveDistance"] = distToCurve;
		jj["Normal"] = std::to_string(Normal);

		j["EndPoint"].push_back(jj);
	}

	//-----------------------------------------------------------------------------------------------------------
	Track::Track(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3& t_pos, const double t_rotation)
		:XTrkObj(t_index, t_layer, t_pos, t_rotation)
		, lineWidth(t_lineWidth)
		, scale(t_scale)
		, visibility(t_visibility)
		, len(0)

	{}

	//-----------------------------------------------------------------------------------------------------------
	void Track::dump(json& j, std::string lbl) {
		XTrkObj::dump(j, lbl);
		j[lbl]["LineWidth"] = std::to_string(lineWidth);
		j[lbl]["Visibility"] = visibility;
		j[lbl]["Scale"] = scale.c_str();
		j[lbl]["Length"] = std::to_string(len);

		for (auto &ii : Segments) {
			json jj;
			ii->dump(jj);
			j[lbl]["Segments"].push_back(jj);
		}
		for (auto &ii : endp) {
			json jj;
			ii.dump(j[lbl]);
		}
	}

	//-----------------------------------------------------------------------------------------------------------
	void Track::addSegment(Xtrk3D::Segment* pS)
	{
		Segments.push_back(pS);
	};

	//-----------------------------------------------------------------------------------------------------------
	void Track::addSegment(Xtrk3D::SEGMENT_TYPE* obj)
	{
		switch (obj->type) {
		case 'S': {
			StraightSegment* S = new StraightSegment(to_vec3(obj->data_.two_points.p1), to_vec3(obj->data_.two_points.p2), m_transformMatrix);
			Segments.push_back(S);
			break;
		}
		case'C': {
			CurveSegment* C = new CurveSegment(to_vec3(obj->data_.curve.center), obj->data_.curve.radius, obj->data_.curve.a0, obj->data_.curve.a1, 0, m_transformMatrix);
			Segments.push_back(C);
			break;
		}

		}
	}
	//-----------------------------------------------------------------------------------------------------------
	void Track::addSegmentExt(Xtrk3D::SEGMENT_TYPE* obj) // include arc lines and straight lines for tracks -- implemented to handle M�rklin turntable. 
	{
		switch (obj->type) {
		case 'S': {
			StraightSegment* S = new StraightSegment(to_vec3(obj->data_.two_points.p1), to_vec3(obj->data_.two_points.p2), m_transformMatrix);
			Segments.push_back(S);
			break;
		}
		case'C': {
			CurveSegment* C = new CurveSegment(to_vec3(obj->data_.curve.center), obj->data_.curve.radius, obj->data_.curve.a0, obj->data_.curve.a1, 0, m_transformMatrix);
			Segments.push_back(C);
			break;
		}
		case 'A': {  //2023-09-27
			ArcSegment* C = new ArcSegment(obj->color, obj->width, obj->data_.curve.radius, to_vec3(obj->data_.curve.center), obj->data_.curve.a0, obj->data_.curve.a1, m_transformMatrix);
			Segments.push_back(C);
			break;
		}
		case 'L': { //2023-09-27
			LineSegment* C = new LineSegment(to_vec3(obj->data_.two_points.p1), to_vec3(obj->data_.two_points.p2), obj->color, obj->width, m_transformMatrix);
			Segments.push_back(C);
			break;
		}
		}
	}
	//-----------------------------------------------------------------------------------------------------------
	glm::vec3 Track::SetMidPoint()
	{
		if (Segments.size() == 1) {
			len = (*Segments.begin())->len;
			return (*Segments.begin())->getCGposition();
		}
		else {
			float maxX(endp[0].pos.x), maxY(endp[0].pos.y), minX(endp[0].pos.x), minY(endp[0].pos.y);
			for (auto &ii : endp)
			{
				if (ii.pos.x < minX)
					minX = ii.pos.x;
				if (ii.pos.y < minY)
					minY = ii.pos.y;
				if (ii.pos.x > maxX)
					maxX = ii.pos.x;
				if (ii.pos.y > maxY)
					maxY = ii.pos.y;
			}
			len = (fmax(maxX - minX, maxY - minY));
			return glm::vec3((minX + maxX) / 2, (minY + maxY) / 2, 0);
		}
	}

	//-----------------------------------------------------------------------------------------------------------
	void Track::setSegmentZ()
	{
		if (endp.size() == 2) {
			if (Segments.size() == 1) {
				(*Segments.begin())->setSegmentZ(endp[0].pos, endp[1].pos);
				return;
			}
			//Don�t worry: Cornu is not here, it overrides Track::setSegmentZ
			return;
		}

		//set z for points to same z value as for closest point in Endp
		//2020-08-05
		//calculate better values -- works well with "normal" elevations. Keep this
		std::vector<int>TrackNodes[2];  //Node sides
		double angle1 = endp.begin()->Normal;
		for (unsigned int i = 0; i < endp.size(); ++i) {
			double diff = fabs(endp[i].Normal - angle1);
			bool side = (diff - ((int)(diff / 360.0)) * 360.0) < 90;
			TrackNodes[side ? 0 : 1].push_back(i);
		}
		glm::vec3 m1(TrackPlan::averagePos(endp, TrackNodes[0]));
		glm::vec3 m2(TrackPlan::averagePos(endp, TrackNodes[1]));
		glm::vec3 trackvector(glm::normalize(m2.xy - m1.xy), 0);
		float deltaZ = m2.z - m1.z;
		float len = glm::length(m2.xy - m1.xy);
		auto slope = deltaZ / len;

		for (auto &ii : Segments)
		{
			glm::vec4 p1(ii->getMatrix()*glm::vec4(ii->point1, 1));
			glm::vec4 p2(ii->getMatrix()*glm::vec4(ii->point2, 1));
			std::priority_queue<std::pair<float, int>, std::vector<std::pair<float, int>>, std::greater<std::pair<float, int>>> Q1;
			std::priority_queue<std::pair<float, int>, std::vector<std::pair<float, int>>, std::greater<std::pair<float, int>>> Q2;
			for (unsigned int i = 0; i < endp.size(); ++i) {
				glm::vec4 p(endp[i].pos.xy, 0, 1);
				glm::vec4 diff1_4(p - p1);
				glm::vec4 diff2_4(p - p2);
				Q1.push(std::make_pair(glm::length(diff1_4.xy()), i));
				Q2.push(std::make_pair(glm::length(diff2_4.xy()), i));
			}

			auto d1 = glm::dot(p1.xyz - m1, trackvector) / len;
			float z1 = d1 * deltaZ + m1.z;
			auto d2 = glm::dot(p2.xyz - m1, trackvector) / len;
			float z2 = d2 * deltaZ + m1.z;
			float x = Q1.top().first;
			if (Q1.top().first < 0.01)
				z1 = endp[Q1.top().second].pos.z;
			if (Q2.top().first < 0.01)
				z2 = endp[Q2.top().second].pos.z;//
			ii->point1.z = z1;
			ii->point2.z = z2;
		}
		return;
	}

	//-----------------------------------------------------------------------------------------------------------
	bool Track::pointIsOnTrack(glm::vec3 t_pos)
	{
		for (auto &ii : embTop) {
			if (PointInPolygon(IntPoint(t_pos), ii) != 0)
				return true;
		}
		return false;
	}

	//-----------------------------------------------------------------------------------------------------------
	Segment* Track::getSegmentAtPoint(glm::vec3 t_pos)
	{
		for (auto &ii : Segments) {
			for (auto &jj : ii->embTop) {
				if (ClipperLib::PointInPolygon(IntPoint(t_pos), jj) != 0) {
					return ii;
				}
			}
		}
		return nullptr;
	}

	//-----------------------------------------------------------------------------------------------------------
	glm::vec3 Track::getCenterline_Point(glm::vec3 t_pos)
	{
		Segment* s = getSegmentAtPoint(t_pos);
		if (s != nullptr) {
			return s->getCenterline_Point(t_pos);
		}
		return t_pos;
	}

	//-----------------------------------------------------------------------------------------------------------
	// get centerline point closest to t_pos among segments
	bool Track::getCenterline_Point(glm::vec3 t_pos,glm::vec3 &centerpos)
	{
		std::priority_queue<std::pair<float, int>, std::vector<std::pair<float, int>>, std::greater<std::pair<float, int>>> Q;
		std::vector < glm::vec3> list;
		for (auto& ii : Segments) {
			for (auto& jj : ii->embTop) {
				if (ClipperLib::PointInPolygon(IntPoint(t_pos), jj) != 0) {
					glm::vec3 cp=ii->getCenterline_Point(t_pos);

					int check = ClipperLib::PointInPolygon(IntPoint(cp), jj);
					if (check == 0) {
						int i = 0; //Oops! t_pos is in polygon, but centerline is not
					}

					std::pair<float, int> elem(glm::length(cp - t_pos), list.size());
					list.push_back(cp);
					Q.push(elem);
				}
			}
		}
		if (!Q.empty()) {
			centerpos= list[Q.top().second];
			return true;
		}
		return false;
	}
	//-----------------------------------------------------------------------------------------------------------
	glm::vec3 Track::getTangent(glm::vec3 t_pos)
	{
		Segment* s = getSegmentAtPoint(t_pos);
		if (s != nullptr) {
			return s->getTangent(t_pos);
		}
		return glm::vec3();
	}

	//-----------------------------------------------------------------------------------------------------------
	void Track::initSurface(TrackPlan* myLayout)
	{
		clear();
		int i(0);
		for (auto &ii : Segments) {
			ii->Init(this);
			embTop = join(embTop, ii->embTop);
			embBot = join(embBot, ii->embBot);
			tunnel = join(tunnel, ii->tunnel);
			clearence = join(clearence, ii->clearence);
		}
		for (auto &ii : endp) {
			float extend = (float)(1 - ii.distToCurve / param.ExtendL())*param.ExtendW();
			clearence = join(clearence, SetClearenceEnd(ii, extend, 32));
		}
#ifdef _DEBUG
		bool verbose(false);  //set a breeakpoint and set to true in debugger
		if (verbose) {
			FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "w");
			for (auto &ii : tunnel[0])
			{
				fprintf(fp, "%d\t%d\n", (int)ii.X, (int)ii.Y);
			}
			fprintf(fp, "-----------\n");
			for (auto &ii : clearence[0])
			{
				fprintf(fp, "%d\t%d\n", (int)ii.X, (int)ii.Y);
			}

			fclose(fp);
		}
#endif
	}

	//-----------------------------------------------------------------------------------------------------------
	void Track::SetClearenceEnd(glm::vec3 point, ClipperLib::Paths& c, int divs)
	{
		double delta = 2.0*M_PI / double(divs);
		double sin_delta = sin(delta);
		double cos_delta = cos(delta);
		double u1;
		double u = param.MaxW() / 2.1;
		double v = 0.0;

		Paths temp;
		temp.resize(1);
		for (int i = 0; i < divs; ++i) {
			u1 = u * cos_delta - v * sin_delta;
			v = u * sin_delta + v * cos_delta;
			u = u1;
			temp[0] << IntPoint(point + glm::vec3(u, v, param.MaxH()));
		}
		c = join(c, temp);
	}

	//-----------------------------------------------------------------------------------------------------------
	ClipperLib::Paths Track::SetClearenceEnd(endPoint endp, float ExtendW, int divs)
	{
		Paths temp;
		temp.resize(1);

		glm::vec3 point = endp.pos;
		////2020-01-27
		ExtendW = fmax(ExtendW, 0.0f);
		float alfa = (float)(endp.Normal*M_PI / 180);
		glm::vec3 N = glm::vec3(-cos(alfa), sin(alfa), 0)*(ExtendW + param.MaxW() / 2.1f);
		glm::vec3 V = glm::vec3(sin(alfa), cos(alfa), 0)*param.RailW();
		glm::vec3 H(glm::vec3(0, 0, param.MaxH()));
		temp[0] << IntPoint(point + V + N + H);
		temp[0] << IntPoint(point + V - N + H);
		temp[0] << IntPoint(point - V - N + H);
		temp[0] << IntPoint(point - V + N + H);
		return temp;
	}

	//-----------------------------------------------------------------------------------------------------------
	SceneNode* Track::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout, int count)
	{
		glm::vec2 ratio(param.ratio()/160);

		SceneNode* Node;
		Shape Side(ShapeType::EmbankmentType, sideVerts, sideIndices, Shaders, Appearance::gravelSurface, Appearance::darkConcreteSurface, GL_TRIANGLES, GL_BACK,ratio);
		Shape Rails(ShapeType::TrackType, railVerts, railIndices, Shaders, Appearance::railSurface, Appearance::railSurface, GL_TRIANGLES, GL_BACK);
		Node = new SceneNode();
		Node->TrackObj = this;
		Node->addShape(Rails);
		Node->layer = this->layer;
		Node->isTrack = true;
		Node->boundingSphere = BoundSphere(railVerts);
		if (visibility != 10) {
			Node->addShape(Side);
			EarcutWrapper T(embTop);
			//	PolyWrapper T(embTop); //ercutwrapper gives better result
			EarcutWrapper B(embBot);
			for (unsigned int i = 0; i < T.vertices.size(); ++i)
				Node->addShape(Shape(ShapeType::EmbankmentType, T.vertices[i], T.indices[i], Shaders, Appearance::gravelSurface, Appearance::concreteSurface, GL_TRIANGLES, GL_BACK,ratio));
			for (unsigned int i = 0; i < B.vertices.size(); ++i)
				Node->addShape(Shape(ShapeType::EmbankmentType, B.vertices[i], B.indices[i], Shaders, Appearance::gravelSurface, Appearance::concreteSurface, GL_TRIANGLES, GL_FRONT,ratio));
		}
#ifdef _DEBUG
		Node->addShape(Disc(ShapeType::ElevationType, CG, 0.1f, 24, glm::vec3(0.2, 0.2, 1), Shaders));
#endif
		for (auto& ii : Segments) { //2023-09-27
			int i = 0;
			if (ii->getObjType() == xtrkArcSegment || ii->getObjType() == xtrkLineSegment) {
				int i = 0;
				Node->addNode(ii->createSceneNode(Shaders, myLayout,count));
			}
		}

		if (endp.size() == 2) {
			double grad = fabs(endp[0].pos[2] - endp[1].pos[2]) / len;

			std::stringstream stream;
			stream << std::fixed << std::setprecision(1) << grad * 100 << "%";
			TextNode grade(stream.str(), glm::vec3(0, 0, 1));
			grade.setTransform(glm::translate(glm::mat4(), CG + glm::vec3(0, 0, 0.2f)));
			Node->addText(grade);
		}
		for (auto &ii : endp) {
			if (ii.Node2 == -1) {
				Node->addNode(setEndTrack(ii, Shaders, true)); //draw end of track
			}
			else {
				if (ii.isOnTurnTable) {
					Node->addNode(setEndTrack(ii, Shaders, false)); //draw end of rails
				}
				else {
					if (ii.isBridgeEdge)
						Node->addNode(setEndTrack(ii, Shaders, false, true)); //draw end of embankment only
				}
			}
		}
		if (myLayout->Layers.count(layer) == 0)
		{
			if (defaultLayerColor.count(layer % 10) > 0)
				myLayout->Layers[layer].Color = defaultLayerColor[layer % 10];
		}
		CrossTie Tie(Shaders, param, myLayout->Layers[layer].Color);

		//*
		for (unsigned int i = 0; i < TiePos.size(); ++i)
		{
			SceneNode* N = new SceneNode();
			N->setTransform(TiePos[i]);
			N->addShape(Tie);
			N->boundingSphere = Tie.BoundingSphere;
			Node->addNode(N);
			N->isTie = true;  //2019-10-02
		}//*/
		return Node;

	}

	//-----------------------------------------------------------------------------------------------------------
	ClipperLib::Paths GetPath(const glm::vec3 pos, const int id, float limit, TrackPlan* myLayout, std::vector<int>& visited)
	{
		ClipperLib::Paths pgs = myLayout->Tracks[id]->clearence;
		for (auto &ep : myLayout->Tracks[id]->endp)
		{
			if (ep.Node2 == -1) continue;
			if (myLayout->Tracks[ep.Node2]->isHelix()) continue;
			bool found = (std::find(visited.begin(), visited.end(), ep.Node2) != visited.end());
			if (found) continue;
			if (glm::length(pos - ep.pos) > limit) continue;
			visited.push_back(ep.Node2);
			pgs = join(pgs, GetPath(pos, ep.Node2, limit, myLayout, visited));
		}

		return pgs;
	}

	//-----------------------------------------------------------------------------------------------------------
	SceneNode* Track::CreateBridgeNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout)
	{
		std::vector<vertex> vert;
		std::vector<GLuint> ind;
		glm::vec3 bridgeColor(0.529f, 0.336f, 0.219f);
		glm::vec3 bridgeTopColor(0.632f, 0.465f, 0.305f);
		glm::vec3 bridgeBotColor(0.492f, 0.312f, 0.203f);

		SceneNode* Node(nullptr);
		Node = new SceneNode();
		Node->layer = this->layer;
		Node->isTrack = true;
		Node->TrackObj = this;
		ClipperLib::Paths pgs = subtract(tunnel, clearence);
		std::vector<int>visited;
		//for all neighbours subtract clearance
		for (auto &ep : endp)
		{
			if (ep.Node2 != -1)
			{
				pgs = subtract(pgs, GetPath(ep.pos, ep.Node2, fmax((float)this->len, this->param.gauge() * 10), myLayout, visited));
			}
		}

		float deltaHtop(param.MaxH() - param.embH());
		float deltaHbottom(param.MaxH() + param.embH() + param.TieH());

		for (Paths::size_type i = 0; i < pgs.size(); ++i)
		{
			for (Path::size_type j = 0; j < pgs[i].size(); ++j)
			{
				Path::size_type k = (j < pgs[i].size() - 1) ? j + 1 : 0; //next item
				vert.push_back(vertex(pgs[i][j].Z.Pos - glm::vec3(0, 0, deltaHtop), bridgeColor));
				vert.push_back(vertex(pgs[i][j].Z.Pos - glm::vec3(0, 0, deltaHbottom), bridgeColor));
				vert.push_back(vertex(pgs[i][k].Z.Pos - glm::vec3(0, 0, deltaHtop), bridgeColor));
				vert.push_back(vertex(pgs[i][k].Z.Pos - glm::vec3(0, 0, deltaHbottom), bridgeColor));
				ind.push_back(vert.size() - 3);
				ind.push_back(vert.size() - 1);
				ind.push_back(vert.size() - 2);
				ind.push_back(vert.size() - 3);
				ind.push_back(vert.size() - 2);
				ind.push_back(vert.size() - 4);
			}
			Node->addShape(Shape(ShapeType::TunnelSideType, vert, ind, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_BACK));
		}

		//-----------------------------------------------------------------------------------------------------------
		EarcutWrapper T_SideTop(pgs, bridgeTopColor);
		//   Need to set correct height abvove track
		glm::mat4 T = glm::translate(glm::mat4(), glm::vec3(0, 0, -deltaHtop));
		SceneNode* TN = new SceneNode();
		TN->setTransform(T);
		for (unsigned int i = 0; i < T_SideTop.vertices.size(); ++i) {
			TN->addShape(Shape((ShapeType)(1), T_SideTop.vertices[i], T_SideTop.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		}
		Node->addNode(TN);
		EarcutWrapper T_base(tunnel, bridgeBotColor);
		T = glm::translate(glm::mat4(), glm::vec3(0, 0, -deltaHbottom));
		TN = new SceneNode();
		TN->setTransform(T);
		for (unsigned int i = 0; i < T_base.vertices.size(); ++i) {
			TN->addShape(Shape((ShapeType)(1), T_base.vertices[i], T_base.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		}
		Node->addNode(TN);

		/*   Need to set correct height abvove track
		EarcutWrapper T_SideTop(pgs, bridgeTopColor);
		for (unsigned int i = 0; i < T_SideTop.vertices.size(); ++i) {
			for (auto &ii : T_SideTop.vertices[i]) {
				ii.position.z -= deltaHtop;
			}
			Node->addShape(Shape((ShapeType)(1), T_SideTop.vertices[i], T_SideTop.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		}
		EarcutWrapper T_base(tunnel, bridgeBotColor);
		for (unsigned int i = 0; i < T_base.vertices.size(); ++i) {
			for (auto &ii : T_base.vertices[i]) {
				ii.position.z -= deltaHbottom;
			}
			Node->addShape(Shape((ShapeType)(1), T_base.vertices[i], T_base.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		}//*/
		Node->boundingSphere = BoundSphere(railVerts);//*/
		return Node;
	}

	//-----------------------------------------------------------------------------------------------------------
	SceneNode* Track::CreateTunnelNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout)
	{
		std::vector<vertex> vert;
		std::vector<GLuint> ind;
		glm::vec3 SideColor(0.5f, 0.5f, 0.5f);
		glm::vec3 RoofColor(0.4f, 0.4f, 0.4f);

		SceneNode* Node(nullptr);
		Node = new SceneNode();
		Node->layer = this->layer;
		Node->isTrack = true;
		Node->TrackObj = this;
		ClipperLib::Paths pgs = subtract(tunnel, clearence);


		std::vector<int>visited;
		//for all neighbours subtract clearance
		for (auto &ep : endp)
		{
			if (ep.Node2 != -1)
			{
				pgs = subtract(pgs, GetPath(ep.pos, ep.Node2, fmax((float)this->len, this->param.gauge() * 10), myLayout, visited));
			}
		}

		for (Paths::size_type i = 0; i < pgs.size(); ++i)
		{
			for (Path::size_type j = 0; j < pgs[i].size(); ++j)
			{
				Path::size_type k = (j < pgs[i].size() - 1) ? j + 1 : 0; //next item
				vert.push_back(vertex(pgs[i][j].Z.Pos, SideColor));
				vert.push_back(vertex(pgs[i][j].Z.Pos - glm::vec3(0, 0, param.MaxH()), SideColor));
				vert.push_back(vertex(pgs[i][k].Z.Pos, SideColor));
				vert.push_back(vertex(pgs[i][k].Z.Pos - glm::vec3(0, 0, param.MaxH()), SideColor));
				ind.push_back(vert.size() - 3);
				ind.push_back(vert.size() - 1);
				ind.push_back(vert.size() - 2);
				ind.push_back(vert.size() - 3);
				ind.push_back(vert.size() - 2);
				ind.push_back(vert.size() - 4);
			}
			Node->addShape(Shape(ShapeType::TunnelSideType, vert, ind, Shaders, Appearance::darkConcreteSurface, Appearance::darkConcreteSurface, GL_TRIANGLES, GL_BACK));
		}

		EarcutWrapper T_Roof(tunnel, RoofColor);
		EarcutWrapper T_SideTop(pgs);
		//	EarcutWrapper T_Roof(clearence);  //debug purpose

		for (unsigned int i = 0; i < T_SideTop.vertices.size(); ++i)
			Node->addShape(Shape((ShapeType)(8), T_SideTop.vertices[i], T_SideTop.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		for (unsigned int i = 0; i < T_Roof.vertices.size(); ++i)
			Node->addShape(Shape((ShapeType)(0x10), T_Roof.vertices[i], T_Roof.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
		Node->boundingSphere = BoundSphere(railVerts);
		return Node;
	}

	//-----------------------------------------------------------------------------------------------------------
	void Track::addRailEnd(SceneNode* Node, glm::vec3& pos, glm::vec3& N, float inner, float outer, OpenGLShaderPrograms* Shaders) {
		std::vector<vertex>Verts;
		std::vector<GLuint>Indices = { 0,1,2,3 };
		SceneNode* subNode = new SceneNode();
		Verts.push_back(vertex(pos + N * outer + glm::vec3(0, 0, -param.RailH()), glm::vec3(), glm::vec3(), glm::vec2(1, 0)));
		Verts.push_back(vertex(pos + N * outer + glm::vec3(0, 0, 0), glm::vec3(), glm::vec3(), glm::vec2(1, 0.4)));
		Verts.push_back(vertex(pos + N * inner + glm::vec3(0, 0, 0), glm::vec3(), glm::vec3(), glm::vec2(1, 0.4)));
		Verts.push_back(vertex(pos + N * inner + glm::vec3(0, 0, -param.RailH()), glm::vec3(), glm::vec3(), glm::vec2(1, 0)));
		subNode->addShape(Shape(ShapeType::TrackType, Verts, Indices, Shaders, Appearance::railSurface, Appearance::railSurface, GL_TRIANGLE_FAN, GL_NONE));
		subNode->boundingSphere = BoundSphere(Verts);
		subNode->boundingSphere.setFarDistance(10);
		Node->addNode(subNode);
	}

	//-----------------------------------------------------------------------------------------------------------
	SceneNode* Track::setEndTrack(endPoint endp, OpenGLShaderPrograms* Shaders, bool bufferStop, bool bridgeEnd)
	{
		float alfa = (float)(endp.Normal*M_PI / 180);
		glm::vec3 N = glm::vec3(-cos(alfa), sin(alfa), 0);
		//N = glm::normalize(N); //N is already normalized

		SceneNode* Node = new SceneNode();
		float a = (float)param.gauge() / 2;
		float b = (float)(a + param.RailW());

		if (!bridgeEnd) {
			addRailEnd(Node, endp.pos, N, a, b, Shaders);
			addRailEnd(Node, endp.pos, -N, a, b, Shaders);
		}
		std::vector<vertex>Verts;
		//std::vector<GLuint>Indices = { 0,1,2,3 };
		std::vector<GLuint>Indices = { 0,1,5,1,2,5,2,4,5,2,3,4,0,7,6,0,6,3 };
		float zb = -param.RailH() - param.TieH() - param.embH();
		float zt = -param.RailH() - param.TieH();
		glm::vec3 Ext(sin(alfa), cos(alfa), 0);

		if (bufferStop || bridgeEnd) {
			//Embankment end
			glm::vec3 xybot = N * (float)param.embBotW() / 2.0f;
			glm::vec3 xytop = N * (float)param.embTopW() / 2.0f;

			glm::vec2 ratio(param.ratio() / 160);
			Verts.clear();
			Verts.push_back(vertex(endp.pos + xybot + glm::vec3(0, 0, zb), glm::vec3(), glm::vec3(), glm::vec2(param.embBotW() / 2.0f, 0)));
			Verts.push_back(vertex(endp.pos + xytop + glm::vec3(0, 0, zt), glm::vec3(), glm::vec3(), glm::vec2(param.embTopW() / 2.0f, param.embH())));
			Verts.push_back(vertex(endp.pos - xytop + glm::vec3(0, 0, zt), glm::vec3(), glm::vec3(), -glm::vec2(param.embTopW() / 2.0f, param.embH())));
			Verts.push_back(vertex(endp.pos - xybot + glm::vec3(0, 0, zb), glm::vec3(), glm::vec3(), -glm::vec2(param.embBotW() / 2.0f, 0)));

			Verts.push_back(vertex(endp.pos - xytop+ glm::vec3(0, 0, zb)+Ext*param.embH(), glm::vec3(), glm::vec3(), -glm::vec2(param.embTopW() / 2.0f, -0.4*param.embH())));
			Verts.push_back(vertex(endp.pos + xytop + glm::vec3(0, 0, zb)+Ext * param.embH(), glm::vec3(), glm::vec3(), glm::vec2(param.embTopW() / 2.0f, -0.4*param.embH())));

			Verts.push_back(vertex(endp.pos - xytop + glm::vec3(0, 0, zb) + Ext * param.embH(), glm::vec3(), glm::vec3(), -glm::vec2(param.embTopW() / 2.0f, param.embH())));
			Verts.push_back(vertex(endp.pos + xytop + glm::vec3(0, 0, zb) + Ext * param.embH(), glm::vec3(), glm::vec3(), glm::vec2(param.embTopW() / 2.0f, param.embH())));


			Node->addShape(Shape(ShapeType::TrackType, Verts, Indices, Shaders, Appearance::gravelSurface, Appearance::darkConcreteSurface, GL_TRIANGLES, GL_BACK, ratio));
//			Node->addShape(Shape(ShapeType::TrackType, Verts, Indices, Shaders, Appearance::gravelSurface, Appearance::darkConcreteSurface, GL_TRIANGLE_FAN, GL_BACK,ratio));
			BoundSphere boundSphere(Verts);
			Node->boundingSphere = boundSphere;
		}
		if (bufferStop) {
			//Buffer
			glm::vec3 H(0, 0, 1.5f*a); //glm::vec3 H(0, 0, param.gauge()*0.8);

			CrossTie Tie(Shaders, glm::vec3(0.1*a, 2 * a, 0.6*a), glm::vec3(1, 0, 0));   // param, 0x733737);
			glm::vec3 Ext1(-1.55f*a*Ext);//glm::vec3 Ext1(-0.5f*param.gauge()*Ext);

			glm::mat4 T = glm::rotate(glm::translate(glm::mat4(), endp.pos + H + Ext1), (float)(M_PI_2 - alfa), glm::vec3(0, 0, 1));
			SceneNode* TN = new SceneNode();
			TN->setTransform(T);
			TN->addShape(Tie);
			TN->boundingSphere = Tie.BoundingSphere;
			Node->addNode(TN);

			auto c = param.RailW();
			glm::vec3 Ext2(-3*c*Ext);
			glm::vec3 Ext3((3*c - 1.45f*a)*Ext);
			glm::vec3 Ext4(-1.45f*a*Ext);
			glm::vec3 H2(0, 0, 1.5f*a-3*c );

			std::vector<GLuint>Indices2 = {
				0,1,5, 0,5,6, 4,2,3, 4,1,2,
				7,13,12, 7,12,8, 11,10,9, 11,9,8,
				0,7,8, 0,8,1, 1,8,9, 1,9,2,
				2,9,10, 2,10,3, 4,11,12, 4,12,5,
				5,12,13, 5,13,6
			};
			Verts.clear();
			ComputeBufferSupports(endp.pos - N * b, Ext2, Ext3, Ext4, H, H2, Verts);
			ComputeBufferSupports(endp.pos - N * a, Ext2, Ext3, Ext4, H, H2, Verts);
			Node->addShape(Shape(ShapeType::TrackType, Verts, Indices2, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_BACK));

			Verts.clear();
			ComputeBufferSupports(endp.pos + N * a, Ext2, Ext3, Ext4, H, H2, Verts);
			ComputeBufferSupports(endp.pos + N * b, Ext2, Ext3, Ext4, H, H2, Verts);
			Node->addShape(Shape(ShapeType::TrackType, Verts, Indices2, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_BACK));

			std::vector<GLuint>Indices3 = {
				0,1,2, 0,2,3, 4,5,6, 4,6,7
				,8,10,9, 8,11,10, 12,14,13, 12,15,14
				,2,1,9, 2,9,10, 6,5,13, 6,13,14
				,0,8,9, 0,9,1, 3,2,10, 3,10,11
				,4,12,13, 4,13,5, 7,6,14, 7,14,15
			};

			//LOD (dist <20)
			SceneNode* TsubN = new SceneNode();
			Verts.clear();
			ComputeVertexes(endp.pos - N * 1.04f*b + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			ComputeVertexes(endp.pos - N * b + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			TsubN->addShape(Shape(ShapeType::TrackType, Verts, Indices3, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_BACK));

			Verts.clear();
			ComputeVertexes(endp.pos - N * a + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			ComputeVertexes(endp.pos - N * 0.96f*a + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			TsubN->addShape(Shape(ShapeType::TrackType, Verts, Indices3, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_BACK));

			Verts.clear();
			ComputeVertexes(endp.pos + N * 0.96f*a + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			ComputeVertexes(endp.pos + N * a + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			TsubN->addShape(Shape(ShapeType::TrackType, Verts, Indices3, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_BACK));

			Verts.clear();
			ComputeVertexes(endp.pos + N * b + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			ComputeVertexes(endp.pos + N * 1.04f*b + glm::vec3(0, 0, zt), 1.05f*Ext2, 0.95f*Ext3, 1.05f*Ext4, glm::vec3(0, 0, -1.2*zt), Verts);
			TsubN->addShape(Shape(ShapeType::TrackType, Verts, Indices3, Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_BACK));

			Node->addNode(TsubN);
			BoundSphere boundSphere(Verts);
			boundSphere.setFarDistance(20);
			TsubN->boundingSphere = boundSphere;//*/
		}
		return Node;
	}

	//-----------------------------------------------------------------------------------------------------------
	void Track::ComputeBufferSupports(glm::vec3& pos, glm::vec3& Ext2, glm::vec3& Ext3, glm::vec3& Ext4, glm::vec3& H, glm::vec3& H2, std::vector<vertex>& Verts)
	{
		float t2 = glm::dot(Ext2, Ext4);
		float t3 = glm::dot(Ext3, Ext4);
		glm::vec3 bottom = pos + glm::vec3(0, 0, -param.RailH());
		Verts.push_back(vertex(pos, glm::vec3(), glm::vec3(0.48, 0.42, 0.31), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos + Ext3 + H, glm::vec3(), glm::vec3(0.41, 0.31, 0.25), glm::vec2(t3, 1))); //1
		Verts.push_back(vertex(pos + Ext4 + H, glm::vec3(), glm::vec3(0.48, 0.37, 0.31), glm::vec2(1, 1))); //2
		Verts.push_back(vertex(bottom + Ext4, glm::vec3(), glm::vec3(0.48, 0.37, 0.31), glm::vec2(1, 0))); //3
		Verts.push_back(vertex(bottom + Ext3, glm::vec3(), glm::vec3(0.41, 0.27, 0.21), glm::vec2(t3, 0))); //4
		Verts.push_back(vertex(pos + Ext3 + H2, glm::vec3(), glm::vec3(0.41, 0.27, 0.25), glm::vec2(t3, H2.z / H.z))); //5
		Verts.push_back(vertex(pos + Ext2, glm::vec3(), glm::vec3(0.48, 0.37, 0.31), glm::vec2(t2, 0)));  //6
	}

	//-----------------------------------------------------------------------------------------------------------
	void Track::ComputeVertexes(glm::vec3& pos1, glm::vec3& Ext2, glm::vec3& Ext3, glm::vec3& Ext4, glm::vec3& H, std::vector<vertex>& Verts)
	{
		Verts.push_back(vertex(pos1, glm::vec3(), glm::vec3(0.56, 0.48, 0.36), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos1 + H, glm::vec3(), glm::vec3(0.56, 0.48, 0.36), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos1 + Ext2 + H, glm::vec3(), glm::vec3(0.52, 0.48, 0.36), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos1 + Ext2, glm::vec3(), glm::vec3(0.52, 0.42, 0.36), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos1 + Ext3, glm::vec3(), glm::vec3(0.52, 0.48, 0.36), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos1 + Ext3 + H, glm::vec3(), glm::vec3(0.52, 0.48, 0.36), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos1 + Ext4 + H, glm::vec3(), glm::vec3(0.56, 0.42, 0.36), glm::vec2(0, 0))); //0
		Verts.push_back(vertex(pos1 + Ext4, glm::vec3(), glm::vec3(0.56, 0.42, 0.36), glm::vec2(0, 0))); //0
	}

#ifdef _DEBUG
	//-----------------------------------------------------------------------------------------------------------
	void Dump(std::string str)
	{
		FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
		fprintf(fp, "%s",str.c_str());
		fflush(fp);
		fclose(fp);
	}
	//-----------------------------------------------------------------------------------------------------------
	void DumpVec3(FILE* fp, glm::vec3 v, bool eol) {
		fprintf(fp, " [%.3f | %.3f | %.3f]", v.x, v.y, v.z);
		if (eol) fprintf(fp, "\n");
	}
	//-----------------------------------------------------------------------------------------------------------
	void Dump(std::string lbl, glm::vec3 t_pos, bool eol=true)
	{
		FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
		fprintf(fp, "%s:", lbl.c_str());
		DumpVec3(fp, t_pos, eol);
		fflush(fp);
		fclose(fp);
	}
	//-----------------------------------------------------------------------------------------------------------
	void Dump(std::string lbl, int t_int, std::string str = std::string())
	{
		FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
		fprintf(fp, "%s: %d%s", lbl.c_str(), t_int,str.c_str());
		fflush(fp);
		fclose(fp);
	}

	void Dumpf(std::string lbl, float t_float, std::string str = std::string())
	{
		FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
		fprintf(fp, "%s: %.3f%s", lbl.c_str(), t_float, str.c_str());
		fflush(fp);
		fclose(fp);
	}

#endif

	Track* Track::MoveAlongTrackFwd(Camera& myCamera, float speed, bool verbose, Scene *myScene)
	{
		auto curpos = myCamera.getPosition();
		if (glm::all(glm::isnan(curpos))) {
			return nullptr;
		}
		auto forward = myCamera.GetForward();
		if (glm::all(glm::isnan(forward)))  {
			return nullptr;
		}
		Track::TrainpathPosition T = getNextPosAlongTrack(forward, curpos, speed, verbose);
		if (T.track == nullptr) {
			myCamera.SetPosition(curpos);
			return this;
		}
		//Show forward path
		for (auto& ii : myScene->dynamicObjects) {
			delete ii;
		}
		myScene->dynamicObjects.clear();
		SceneNode* Node = new SceneNode();
		glm::vec3 res=T.track->getNextTest(forward, T.point, speed * 5, Node, &myScene->MyShaders, 20);
		myScene->dynamicObjects.push_back(Node);

#ifdef _DEBUG
		if (verbose) {
			Dump("Track ", this->index);
			Dump(" curpos", curpos, false);
			Dump("/Segments", Segments.size(), " ");
		}
#endif
		myCamera.LookAt(res + curpos, glm::vec3(0, 0, 1));

		if (this->index != T.track->index || Segments.size()==1) {
			myCamera.SetPosition(T.centerPoint);
#ifdef _DEBUG
			if (verbose) {
				Dump(" /next (Centerpos): ", T.track->index);
				auto delta = myCamera.getPosition() - curpos;
				float dd = glm::dot(delta, forward);
				Dumpf("/dd", dd);
				Dump("\n");
			}
#endif
/*			auto tangent = T.track->getTangent(T.centerPoint);

			if (glm::length(tangent) == 0) {
				myCamera.SetPosition(curpos);
				return this;// nullptr;
			}
			auto d = glm::dot(forward, tangent);
			if (d < 0) tangent = -tangent;
			myCamera.LookAt(T.centerPoint + tangent, glm::vec3(0, 0, 1));
			return T.track;//*/
		}

		glm::vec3 heading = curpos + myCamera.GetForward() * speed;
		if (PointInPolygon(IntPoint(heading), embTop.at(0)) == 0) {
			myCamera.SetPosition(T.point);

#ifdef _DEBUG
			if (verbose) {
				Dump(" /next (On Border): ", T.track->index);
				auto delta = myCamera.getPosition() - curpos;
				float dd = glm::dot(delta, forward);
				Dumpf("/dd", dd);
				Dump("\n");
			}
#endif

			auto tangent = T.track->getTangent(T.point);
			auto d = glm::dot(forward, tangent);
			if (d < 0) tangent = -tangent;
			myCamera.LookAt(T.point + tangent, glm::vec3(0, 0, 1));
			return T.track;
		}


#ifdef _DEBUG
		if (verbose) {
			auto delta = T.point - curpos;
			float dd = glm::dot(delta, forward);
			Dumpf("/dd", dd);
			Dump("\n");
		}
#endif
		myCamera.SetPosition(T.point);
		return T.track;
	}

	void Track::ReverseMoveDirection(Camera& myCamera)
	{
		glm::vec3 lookatPos(myCamera.getPosition() - myCamera.GetForward()*10.0f);
		lookatPos.z = getCenterline_Point(lookatPos).z + param.MaxH()*0.6f;
		myCamera.LookAt(lookatPos, glm::vec3(0, 0, 1));
	}


	//-----------------------------------------------------------------------------------------------------------
	Track::TrainpathPosition Track::getNextPos(Camera& myCamera, glm::vec3 t_pos, float speed, bool verbose)
	{
		if (isHelix())
			return Track::TrainpathPosition();
		if (PointInPolygon(IntPoint(t_pos), embTop.at(0)) == 0) {
			return Track::TrainpathPosition(); //t_pos not on embTop
		}
		glm::vec3 nextpos = t_pos + myCamera.GetForward() * speed;
		glm::vec3 nextCenterP;
		auto stat = getCenterline_Point(nextpos, nextCenterP);
		if (stat && Segments.size() == 1) {
			return Track::TrainpathPosition(this, nextCenterP, nextCenterP);
		}
		if (pointIsOnTrack(nextpos)) {
			return Track::TrainpathPosition(this, nextpos, nextCenterP);
		}

		return Track::TrainpathPosition();
	}

	glm::vec3 Track::getNextTest(glm::vec3 t_heading, glm::vec3 t_pos, float speed, SceneNode* Node, OpenGLShaderPrograms* Shaders,int count)
	{
		Track::TrainpathPosition T2 = getNextPosAlongTrack(t_heading, t_pos, speed , false);
		if (T2.track != nullptr) {
			glm::vec3 forward(glm::normalize(T2.point - t_pos));
#ifdef _DEBUG
//			Node->addShape(Ring(ShapeType::OtherType, forward*0.5f+ T2.point - glm::vec3(0, 0, 1) * (param.MaxH() * 0.6f - param.RailH() * 4f), 0.02f,0.04f,0.02f, 24, glm::vec3(0, 1, 0), Shaders, Appearance::solidColor));
			Node->addShape(Disc(ShapeType::OtherType, T2.point - glm::vec3(0, 0, 1) * (param.MaxH() * 0.6f - param.RailH() * 4), 0.08f, 24, glm::vec3(1, 0, 0), Shaders, Appearance::solidColor, GL_NONE));
			Node->addShape(Arrow(ShapeType::OtherType, T2.point - forward * 0.1f - glm::vec3(0, 0, 1) * (param.MaxH() * 0.6f - param.RailH() * 4.1f), T2.point - glm::vec3(0, 0, 1) * (param.MaxH() * 0.6f - param.RailH() * 4.1f), glm::vec3(0, 1, 0), Shaders));
#else
			if (count % 4 == 0 && count > 5) {
				Node->addShape(Disc(ShapeType::OtherType, T2.point - glm::vec3(0, 0, 1) * (param.MaxH() * 0.6f - param.RailH() * 4), 0.08f, 24, glm::vec3(1, 0, 0), Shaders, Appearance::solidColor, GL_NONE));
				Node->addShape(Arrow(ShapeType::OtherType, T2.point - forward * 0.1f - glm::vec3(0, 0, 1) * (param.MaxH() * 0.6f - param.RailH() * 4.1f), T2.point - glm::vec3(0, 0, 1) * (param.MaxH() * 0.6f - param.RailH() * 4.1f), glm::vec3(0, 1, 0), Shaders));
		}
#endif
			if (count > 0)
				return glm::normalize(t_heading+ T2.track->getNextTest(forward, T2.point, speed, Node, Shaders, count - 1));
		}
		return glm::vec3();
	}

	//-----------------------------------------------------------------------------------------------------------
	Track::TrainpathPosition Track::getNextPosAlongTrack(glm::vec3 t_heading, glm::vec3 t_pos, float speed, bool verbose)
	{

		if (isHelix())
			return Track::TrainpathPosition();
		if(embTop.empty())
			return Track::TrainpathPosition();

		if (PointInPolygon(IntPoint(t_pos), embTop.at(0)) == 0) {
			return Track::TrainpathPosition(); //t_pos not on embTop
		}

		glm::vec3 curCenterP = getCenterline_Point(t_pos);
		t_pos.z = curCenterP.z;
		glm::vec3 nextpos = t_pos + t_heading * speed;

		glm::vec3 nextCenterP = getCenterline_Point(nextpos);
		if (Segments.size() == 1) {
			nextpos = nextCenterP;
		}

		if (PointInPolygon(IntPoint(nextpos), embTop.at(0)) == 0) { //nextpos outside embTop
#ifdef _DEBUG
			if (verbose) {
				Dump("#nextpos outside embtop, segments=",Segments.size());
			}
#endif
			glm::vec3 cp = toPosWithinEmbTop(t_pos, nextpos);
			cp.z = getCenterline_Point(cp).z + param.MaxH() * 0.6f;

			if (PointInPolygon(IntPoint(cp), embTop.at(0)) == 0)
			{
#ifdef _DEBUG
				if (verbose) {
					Dump("-Centerline also outside. ");
				}
#endif
				endPoint* closest(nullptr);
				for (auto& ii : endp) {
//					if (ii.Node2 < 0) continue;
					if (closest == nullptr)
						closest = &ii;
					else
						if (glm::distance(t_pos, ii.pos) < glm::distance(t_pos, closest->pos))
							closest = &ii;
					auto nextTrack = getTrackplan()->Tracks[ii.Node2];
					if (nextTrack != nullptr)
						if (!nextTrack->isHelix()) {
							if (nextTrack->embTop.size() > 0) {
								if (ClipperLib::PointInPolygon(IntPoint(cp), nextTrack->embTop.at(0)) != 0) {
#ifdef _DEBUG
									if (verbose) {
										Dump("-found cp inside embtop at Track ",nextTrack->index,"\n");
									}
#endif
									return Track::TrainpathPosition(nextTrack, cp, getCenterline_Point(cp));
								}
							}
						}
				}
				if (closest != nullptr) {
					auto nextTrack = getTrackplan()->Tracks[closest->Node2];
					glm::vec3 nextpos = closest->pos + t_heading * speed;
#ifdef _DEBUG
					if (verbose) {
						Dump("-closest", nextpos);
					}
#endif
					if (nextTrack== nullptr) {
						return Track::TrainpathPosition();
					}
					if (nextTrack->isHelix()) {
						return Track::TrainpathPosition();
					}
					if(nextTrack->getObjType()==XtrkTurntable){
						return Track::TrainpathPosition();
					}
					return Track::TrainpathPosition(nextTrack, nextpos + glm::vec3(0, 0, 1) * param.MaxH() * 0.6f, getCenterline_Point(nextpos) + glm::vec3(0, 0, 1) * param.MaxH() * 0.6f);
				}
#ifdef _DEBUG
				if (verbose) {
					Dump("-null\n");
				}
#endif
				return Track::TrainpathPosition();
			}
#ifdef _DEBUG
			if (verbose) {
				Dump("-this:",cp);
			}
#endif
			return Track::TrainpathPosition(this, cp,cp);
		}
		glm::vec3 nextCenterP2 = getCenterline_Point(nextpos) + glm::vec3(0, 0, 1) * param.MaxH() * 0.6f;
		nextpos.z = nextCenterP2.z;

		return Track::TrainpathPosition(this, nextpos, nextCenterP2);
	}

	//-----------------------------------------------------------------------------------------------------------
	glm::vec3 Track::toPosWithinEmbTop(glm::vec3 t_from, glm::vec3 t_to)
	{
		std::priority_queue<std::pair<float, int>, std::vector<std::pair<float, int>>, std::greater<std::pair<float, int>>> Q;
		std::vector < glm::vec3> list;
		for (auto &ii : Segments) {
			if (ClipperLib::PointInPolygon(IntPoint(t_from), ii->embTop.at(0)) != 0) {
				glm::vec3 p(ii->toPosWithinEmbTop(t_from, t_to, param.embTopW()/2.2f));//2021-06-08
				std::pair<float, int> elem(glm::length(p - t_to), list.size());
				list.push_back(p);
				Q.push(elem);
			}
		}
		if (list.size() > 1) {
			return list[Q.top().second];
		}
		if (list.empty())
			return glm::vec3();
		else
			return list.front();
	}
}