#include "Interface.h"
#include <string>
#include "TrackPlan.h"
#include "Scene.h"
#include "Camera.h"
#include "XTrkObj.h"
#include "turnout.h"
#include "Structure.h"
//#include "MyDoc.h"


namespace Xtrk3D {

static struct data {
	TrackPlan* plan;
	Scene* myScene;
	Camera* myCamera;
	data() :plan(nullptr), myScene(nullptr), myCamera(nullptr) {};
} _data;

	void init()
	{
		if (_data.plan == nullptr) {
			_data.plan = new TrackPlan;
			_data.myScene = new Scene;
			_data.myCamera = new Camera;
			_data.myCamera->setClipping(0.1f, 1000.0f);  // set near and far plane
			_data.myScene->Reset(*_data.myCamera);
		}
		_data.myScene->Init(_data.plan);
	}

	void close()
	{
		delete _data.plan;
		_data.plan = nullptr;
		delete _data.myScene;
		_data.myScene = nullptr;
		delete _data.myCamera;
		_data.myCamera = nullptr;
	}

/*	int readFile(const char* layoutfile)
	{
		std::ifstream in;
		in.open(layoutfile);
		if (in.good()) {
			MyDoc Doc(in);
			in.close();
			Xtrk3D::init();
			Xtrk3D::resetScene();
			return 0;
		}
		return -1;
	}//*/

	void resetScene()
	{
		_data.myScene->Reset(*_data.myCamera);
	}

	void clearScene()
	{
		_data.myScene->Clear();
	}

	int drawScene()
	{
		if (_data.myCamera != nullptr) {
			_data.myCamera->SetFrustrum();
			int nodeCount = _data.myScene->Render(*_data.myCamera);
			return nodeCount;
		}
		return -1;
	}

	void prepareViewport(int width, int height)
	{
		if (_data.myCamera != nullptr) {
			double ratio = (height == 0) ? width : (double)width / (double)height;
			_data.myCamera->setViewportAspectRatio((float)ratio);
			glViewport(0, 0, width, height);
		}
	}

	void moveCamera(float forward, float left, float up)
	{
		_data.myCamera->MoveForward(forward);
		_data.myCamera->MoveLeft(left);
		_data.myCamera->MoveUp(up);
	}

	void rotateCamera(float forwardVector, float leftVector, float upVector)
	{
		_data.myCamera->Rotate(forwardVector, glm::vec3(1, 0, 0));
		_data.myCamera->Rotate(leftVector, glm::vec3(0, 1, 0));
		_data.myCamera->Rotate(upVector, glm::vec3(0, 0, 1));
	}

	void setCameraPosition(double x, double y, double z)
	{
		_data.myCamera->SetPosition(glm::vec3(x, y, z));
	}


	void setVisibleLayers(int argc, bool argv[])
	{
		for (int i = 0; i < argc; ++i) {
			_data.plan->Layers[i].Visible = argv[i];
		}
	}

	void defineLayer(int layerId, char* name, int color, bool visible)
	{
		_data.plan->Layers[layerId] = Layer(name, color, visible);
	}

	const char* getLayer(int layerId, int& color, bool& visible)
	{
		if (_data.plan->Layers.count(layerId) == 0)
			return "undefined";
		color = _data.plan->Layers[layerId].Color;
		visible = _data.plan->Layers[layerId].Visible;
		return _data.plan->Layers[layerId].Name.c_str();
	}

	bool setLayerVisibility(int layerId, bool visible)
	{
		if (_data.plan->Layers.count(layerId) > 0) {
			_data.plan->Layers[layerId].Visible = visible;
			return true;
		}
		return false;
	}

	void setRoomsize(double X, double Y)
	{
		_data.plan->UR = glm::vec3(X, Y, 0);
	}


	void setOptions(bool colorByLayer, int tunnelMode, bool Metric, bool DrawStruct, bool DrawElevations, bool resolution)
	{
		_data.plan->ColorByLayer = colorByLayer;
		_data.plan->TunnelMode = (tunnelMode >= 0 && tunnelMode < 3 ? tunnelMode : 0);
		_data.plan->metric = Metric;
		_data.plan->DrawStruct = DrawStruct;
		_data.plan->DrawElevations = DrawElevations;
	}

	void setBenchworkHeight(float datum)
	{
		_data.plan->benchworkZeroLevel = datum;
		_data.myScene->InitNonTracks();
	}

	float getBenchworkHeight()
	{
		return _data.plan->benchworkZeroLevel;
	}

	int getSelectedObjectId(int x, int y)
	{
		SceneNode* S = _data.myScene->Select(*_data.myCamera, x, y);
		if (S != nullptr)
			return S->TrackObj->index;
		return -1;
	}

	void setCameraAt(int x, int y)
	{
		SceneNode* S = _data.myScene->Select(*_data.myCamera, x, y);
		if (S != nullptr) {
			_data.myCamera->SetPosition(S->TrackObj->getCGposition());
			_data.myCamera->MoveForward(-10);
		}
	}

	void lookAt(double x, double y, double z)
	{
		_data.myCamera->LookAt(glm::vec3(x, y, z));
	}

	void defineScaleParameters(const char* Scale, float param[15])
	{
		std::vector<float>p(param, param + 15);
		std::string name(Scale);
		_data.plan->Dimensions[name] = p;
	}


	void newTrack()
	{
		_data.plan->clear();
		Xtrk3D::clearScene();
	}

	int addTracks(OBJ_TYPE* track)
	{
		int count(0);
		OBJ_TYPE* obj(track);
		while (obj != nullptr) {
			if (_data.plan->Tracks.count(obj->index) != 0) {
				//index already used -- skip entry. TODO: report error
				obj = obj->next;
				continue;
			}
			switch (obj->type)
			{
			case turnoutObj:
			{
				_data.plan->Tracks[obj->index] = new Turnout(obj);
				break;
			}
			case curveObj:
			{
				_data.plan->Tracks[obj->index] = new CurveTrack(obj);
				break;
			}
			case straightObj:
			{
				_data.plan->Tracks[obj->index] = new StraightTrack(obj);
				break;
			}
			case joinObj:
			{
				_data.plan->Tracks[obj->index] = new Joint(obj, _data.plan->Tracks);
				break;
			}
			case cornuObj:
			{
				_data.plan->Tracks[obj->index] = new Cornu(obj);
				break;
			}
			case turntableObj:
			{
				_data.plan->Tracks[obj->index] = new Turntable(obj);
				break;
			}
			case structureObj:
			{
				_data.plan->Objects.push_back(new Structure(obj));
				break;
			}
			case drawObj:
			{
				_data.plan->Objects.push_back(new Draw(obj));
				break;
			}
			case bzrlinObj:
			{
				_data.plan->Objects.push_back(new Bzrln(obj));
				break;
			}
			default:
			{
				obj = obj->next;
				continue;	//do not increment count
			}
			}
			obj = obj->next;
			++count;
		}
		_data.plan->prepareLayout();
		return count;
	}


	SceneNode* getPropertiesAtScreenCoordinates(int x, int y, std::vector<SELECT_TYPE>& properties)
	{
		SceneNode* obj = _data.myScene->Select(*_data.myCamera, x, y);
		if (obj != nullptr) {
			obj->TrackObj->OnSelect(properties);
		}
		return obj;
	}
	std::string getObjInfoAtScreenCoordinates(int x, int y)
	{
		SceneNode* obj = _data.myScene->Select(*_data.myCamera, x, y);
		if (obj != nullptr) {
			auto id=obj->TrackObj->index;
			std::string  title=obj->TrackObj->OnSelect();
			return title;
		}
		return std::string();
	}


	void updatePropeties(SceneNode* obj)
	{
		_data.myScene->updateObj(obj);
	}

	TrackPlan* getTrackplan() {
		return _data.plan;
	}
	BasePlane* getBasePlane()
	{
		return &_data.myScene->Table;
	}
	void updateBasePlane()
	{
		_data.myScene->updateTable();
	}

	Track* T(nullptr);
	SceneNode* lastS(nullptr);
	unsigned int endpid(0);
	bool TrainModeSelect(false);
	bool TrainReverseSelect(false);

	void setTrainMode(bool Set, int x, int y)
	{
		if (Set && !TrainModeSelect) {
			SceneNode* S = _data.myScene->Select(*_data.myCamera, x, y);
			if (S != nullptr) {
				if (!S->isTrack) return;
				T = (Track*)(S->TrackObj);
				if(T->isHelix()) return;
				if (T->getObjType() == XtrkTurntable) return;
				double H = T->param.MaxH()*0.6;
				glm::vec3 pos = T->getCGposition();
				_data.myCamera->SetPosition(pos + glm::vec3(0, 0, H));
				glm::vec3 heading = T->getTangent(pos);
				_data.myCamera->LookAt(pos + heading * 5.0f + glm::vec3(0, 0, H), glm::vec3(0, 0, 1));
				/*
				if (lastS == S) {
					endpid++;
					if (endpid >= T->endp.size()) endpid = 0;
				}
				else {
					endpid = 0;
				}
				_data.myCamera->LookAt(T->endp[endpid].pos + glm::vec3(1, 0, H), glm::vec3(0, 0, 1)); //TODO: change to heading at CG point instead.
				//*/
			}
			lastS = S;
		};
		TrainModeSelect = Set;
	}

	std::string getTrainPos()
	{
		if (T != nullptr) {
			return T->OnSelect();
		}
		return std::string();
	}

	void goTrainForward(float speed, bool verbose)
	{
		if (T != nullptr) {
			T = T->MoveAlongTrackFwd(*_data.myCamera, speed, verbose, _data.myScene);
		}
	}
	void setTrainReverse(bool Set)
	{
		if (Set && !TrainReverseSelect) {
			if (T != nullptr) {
				T->ReverseMoveDirection(*_data.myCamera);
			}
		}
		TrainReverseSelect = Set;
	}

}

