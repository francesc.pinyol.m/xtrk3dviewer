#pragma once

#define _USE_MATH_DEFINES
#include"segment.h"
#include <array>
//#include "Zbase.h"
//#include "ObjInfoDialog.h"

namespace Xtrk3D {
	struct Rect {
		glm::vec3 LL;
		glm::vec3 UR;
		bool valid;
		Rect(glm::vec3 p) :LL(p), UR(p), valid(true) { LL.z = 0; UR.z = 0; };
		Rect() :valid(false) {};
	};

	class Structure : public XTrkObj
	{
	public:
		Structure(const int t_index, const int t_layer, std::string t_scale, const glm::vec3& t_pos, const float t_rotation, std::string t_label);
		//virtual ~Structure();
		Structure(Xtrk3D::OBJ_TYPE* obj);
		virtual void initSurface(TrackPlan* myLayout);
		virtual enum xtrkObjType getObjType() { return XtrkStructure; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual void updateSceneNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, TrackPlan *myLayout);
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Structure "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j);
		void dump(json& j, std::string lbl);

		std::string scale;
		std::string label;

	protected:
		Structure();
		void addSegment(Xtrk3D::SEGMENT_TYPE* obj);
		//	std::vector<Segment*> Segments;
		ClipperLib::Paths path;
		bool baseZset;
		double baseZValue;
	};

	class Draw : public Structure
	{
	public:
		Draw(const int t_index, const int t_layer, const glm::vec3& t_pos, const float t_rotation)
			:Structure(t_index, t_layer, std::string(), t_pos, t_rotation, std::string()) {}; //empty structure
		virtual ~Draw(void) { clearSegments(); };
		Draw(Xtrk3D::OBJ_TYPE* obj);
		virtual enum xtrkObjType getObjType() { return XtrkDraw; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect();
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void updateSceneNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, TrackPlan *myLayout);
		virtual void dump(json& j);
	protected:
		Draw() {};
	};

	class Bzrln : public Structure
	{
	public:
		Bzrln(const int t_index, const int t_layer)
			:Structure(t_index, t_layer, std::string(), glm::vec3(), 0, std::string()),objLength(0) {}; //empty structure
		//virtual ~Bzrln();
		Bzrln(Xtrk3D::OBJ_TYPE* obj);
		virtual void initSurface(TrackPlan* myLayout);
		virtual enum xtrkObjType getObjType() { return XtrkBzrlin; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Bzrln "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void updateSceneNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, TrackPlan *myLayout);
		virtual void dump(json& j);
		void setSegmentZ();
	protected:
		Bzrln() :objLength(0) {};
		typedef struct Node
		{
			Segment* Item;
			int A[2];
			int index[2];
			double dL;
			Node() {
				Item = NULL;
				A[0] = A[1] = -1;
				index[0] = index[1] = -1;
				dL = 0;
			};
			Node(Segment* pItem, double itemLen) {
				Item = pItem;
				A[0] = A[1] = -1;
				index[0] = index[1] = -1;
				dL = itemLen;
			};
		} Node;
		std::vector<Node>nodeList;
		bool sort(int first, int second);
		glm::vec3 point1, point2;
		double objLength;
		std::vector<std::pair<int, int>>ep;//endpointlist
		std::vector<std::pair<int, int>>getNextVertex(std::pair<int, int> nodeId);

	};


	class LineSegment : public Segment
	{
	public:
		LineSegment(int t_color, double t_width, const glm::mat4& M);
		LineSegment(glm::vec3 t_color, double t_width, const glm::mat4& M);

		LineSegment(glm::vec3& p1, glm::vec3& p2, int t_color, double t_width, const glm::mat4& M);
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return xtrkLineSegment; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual void add(const glm::vec3& point);
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Line Segment "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual glm::vec3 getPoint(float dL);
		virtual void setEndpointZ(int pointNr, float value) { pointNr == 0 ? points.front().z = value : points.back().z = value; };
		virtual void dump(json& j) { dump(j, "LineSegment"); };
		void dump(json& j, std::string lbl);

		//int Color;
		double Width;
		ClipperLib::Paths surface;
		std::vector<vertex>Verts;
		std::vector<GLuint>Indices;

	protected:
		glm::vec3 ObjColor;
		std::vector<glm::vec3>points;
		GLenum mode;
		void addPoint(glm::vec3 point);
		void addVertex(glm::vec3 point, glm::vec3 offset);
		void addVertex(glm::vec3 point, glm::vec3 miter, glm::vec3 offset1, glm::vec3 offset2);
		void setMidPoint(glm::vec3 point);
		void clear();
		Rect rect;
	};

	class ArcSegment : public LineSegment
	{
	public:
		ArcSegment(int t_color, double t_width, double t_radius, glm::vec3 t_center, double t_angle, double t_swing, const glm::mat4& M);
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Arc Segment "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return xtrkArcSegment; };
		virtual glm::vec3 getPoint(float dL);
		virtual void setEndpointZ(int pointNr, float value) { pointNr == 0 ? point1.z = value : point2.z = value; };
		virtual void dump(json& j);// { LineSegment::dump(j, "Arc_Segment"); };

	protected:
		bool m_reverse;
		double m_radius;
		glm::vec3 m_center;
		double m_angle;
		double m_swing;
	};


	class PolyLineSegment : public LineSegment
	{
	public:
		PolyLineSegment(int t_color, double t_width, const glm::mat4& M, int t_polygonType = 0);
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return xtrkPolyLineSegment; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Polygon "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j) { LineSegment::dump(j, "Polygon"); };
		void createVertexVector(std::vector<glm::vec3>& Drawpoints);
		void createVertexVector3(std::vector<glm::vec3>& Drawpoints, double t_hight);
		void addVertex3(glm::vec3 point, glm::vec3 offset, double t_hight,bool t_end);

	protected:
		int m_polygonType;
	};

	class FilledSegment : public LineSegment
	{
	public:
		FilledSegment(int t_color, double t_width, const glm::mat4& M) :LineSegment(t_color, t_width, M), baseZset(false),baseZValue(0.0) {};
		FilledSegment(glm::vec3 t_color, double t_width, const glm::mat4& M) :LineSegment(t_color, t_width, M), baseZset(false), baseZValue(0.0) {};
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkFilledSegment; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual void add(const glm::vec3& point);
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Filled Polygon "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j);// { LineSegment::dump(j, "Filled_Segment"); };

		bool baseZset;
		double baseZValue;
	};

	class CircleSegment : public LineSegment
	{
	public:
		CircleSegment(int t_color, double t_width, double t_radius, glm::vec3 t_center, const glm::mat4& M)
			:LineSegment(t_color, t_width, M), radius(t_radius), center(t_center) {
			glm::vec4 p1 = m_transformMatrix * glm::vec4(t_center, 1);
			center = glm::vec3(p1.x, p1.y, p1.z);
			CG = center;
		};
		virtual void Init(Track* parent) {};
		virtual enum xtrkObjType getObjType() { return xtrkCircleSegment; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Circle "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j);// { LineSegment::dump(j, "Circle_Segment"); };

	protected:
		double radius;
		glm::vec3 center;

	};

	class TableEdgeSegment : public LineSegment
	{
	public:
		TableEdgeSegment(glm::vec3& p1, glm::vec3& p2, const glm::mat4& M) :LineSegment(p1, p2, 0, 0, M) {};
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return xtrkTableEdgeSegment; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " TableEdge "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j) { LineSegment::dump(j, "TableEdge"); };
	};


	class BenchworkSegment : public LineSegment
	{
	public:
		BenchworkSegment(glm::vec3& p1, glm::vec3& p2, const glm::mat4& M, unsigned int mode) :LineSegment(p1, p2, 0, 0, M), shape(mode) { ObjColor = glm::vec3(0.7f, 0.7f, 0); };
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return xtrkBenchWorkSegment; };
		virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " BenchWork "; };
		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual void dump(json& j) { LineSegment::dump(j, "BenchWork"); };

	protected:
		struct CS { glm::vec2 hw; float t; glm::vec2 uv; CS() :t(0) {};  CS(glm::vec2 HW, float T, glm::vec2 UV) :hw(HW), t(T), uv(UV) {}; };
		void addVertex(glm::vec3 point, glm::vec3 offset);
		unsigned int shape;
		std::vector<struct CS> CrossSection;
		std::vector<GLuint> EndIndices;
		std::vector<vertex>EndVerts;
		void setCrossSection();
		void setEndIndices(GLuint Indexes[6]);
	};

	class TextSegment : public LineSegment
	{
	public:
		TextSegment(int color, glm::vec3 point, double angle, double height, std::string& str, const glm::mat4& M);
		virtual void Init(Track* parent);
		virtual enum xtrkObjType getObjType() { return xtrkTextSegment; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		//    virtual bool OnSelect(bool cm, std::string layer);
		virtual std::string OnSelect() { return std::string("T") + std::to_string(index) + " Text "; };

//		virtual XTrkObj* OnSelect(std::vector<SELECT_TYPE>& properties);
		virtual void dump(json& j);// { LineSegment::dump(j, "Text_Segment"); };

	protected:
		std::string m_str;
		double m_height;
		double m_angle;
	};
}