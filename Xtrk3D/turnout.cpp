#include "turnout.h"

namespace Xtrk3D {

std::map <std::string, enum xtrkObjType> TrackTokens =
{
	{"END",XtrkEnd}
	,{"END$SEGS",XtrkEnd}
};



	Turnout::Turnout(Xtrk3D::OBJ_TYPE* obj) :Track(obj->index, obj->layer, obj->width, obj->scale, obj->visibility, to_vec3(obj->point), obj->rotation)
	{
		bool IncludeLineSegments(false);
		Label = std::string(obj->label);
		auto ep = obj->endpoints;
		while (ep != nullptr) {
			endPoint p(ep->index, to_vec3(ep->position), ep->angle, ep->options);
			endp.push_back(p);
			ep = ep->next;
		}
		auto s = obj->segments;
		while (s != nullptr) {
			if (s->color != 16777215) { //2023-09-27 M�rklin turntable and transfertable uses this color to draw invisible connections between endpoints
				IncludeLineSegments?addSegmentExt(s): addSegment(s);
			}
			else {
				IncludeLineSegments=true;
			}
			s = s->next;
		}
		CG = SetMidPoint();
		if (Segments.size() == 1) {
			len = Segments[0]->len;
			if (endp.size() == 1) //Stopblock
			{
				endPoint p(-1, Segments[0]->getPoint((float)len), endp[0].Normal<180? endp[0].Normal+180: endp[0].Normal-180, 0);
				endp.push_back(p);
			}
		}
		else {
			len = 0.001f;
		}
	}

	Turnout::Turnout(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3& t_pos, const float t_rotation, std::string& t_label)
		:Track(t_index, t_layer, t_lineWidth, t_scale, t_visibility, t_pos, t_rotation), Label(t_label)
	{}

	Turnout::~Turnout()
	{}

	void Turnout::dump(json& j)
	{
		Track::dump(j, "Turnout");
	}

	std::string Turnout::OnSelect()
	{ 
//		std::string Msg(Segments.size() == 1 ? " Sectional Track " : " Turnout ");
		std::string info(Label);
		size_t n(info.find((size_t)0xC2));
		while (n != std::string::npos)
		{
			info.replace(n, 1, "");
			n=info.find((size_t)0xC2);
		}
		return Xtrk3D::XTrkObj::OnSelect() + std::string(" ") + info;
	}



	bool Turnout::OnSelect(bool cm, std::string layer)
	{
		Select Menu(this->index, Segments.size() == 1 ? "Sectional Track" : "Turnout", cm);
		int i = 1;
		for (auto &ii : endp)
		{
			Menu.insert("pt " + std::to_string(i++) + ":", ii.pos, false);
		}
		Menu.insert("CG:", CG, false);
		Menu.insert("Name:", this->Label);
		Menu.insert("Layer:", layer);
		Menu.insert("Scale:", scale);
		return Menu.showModal();
	}

	XTrkObj* Turnout::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = Segments.size() == 1 ? "Sectional Track" : "Turnout";
		item.type = VariantType::isHeader;
		properties.push_back(item);
		int i = 1;
		for (auto &ii : endp)
		{
			SELECT_TYPE item;
			item.label = "pt";
			item.point = &ii.pos;
			item.type = VariantType::isVec3;
			properties.push_back(item);
		}
		SELECT_TYPE item2;
		item2.label = "Name:";
		item2.str = Label.c_str();
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		item2.label = "Layer:";
		item2.i = this->layer;
		item2.type = VariantType::isLayer;
		properties.push_back(item2);
		item2.label = "Scale:";
		item2.str = scale.c_str();
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return this;
	}



	bool Turnout::isStraightTrack()
	{
		if (Segments.size() != 1)
			return false;
		return Segments.front()->getObjType() == XtrkStraight;
	}

	double Turnout::getDistance(int index1, int index2)
	{
		if (endp.size() > 2)
		{
			float posdiff = fmax(glm::length(endp[index1].pos.xy - endp[index2].pos.xy), 0.001f); //2020-08-22, part of solution to fix TrackPlan::calculateZValues issue
			float cgdiff = fmax(glm::length(CG.xy - endp[index2].pos.xy), 0.001f);
			float cgdist1 = glm::length(CG.xy - endp[index1].pos.xy);
			float cgdist2 = glm::length(CG.xy - endp[index2].pos.xy);
			float cgdistdiff = fabs(cgdist1 - cgdist2);

			if (posdiff < cgdiff) {
				return fmax(fabs(cgdistdiff), 0.001);
			}
			else {
				return fmax(glm::length(endp[index1].pos.xy - endp[index2].pos.xy), 0.001);
			}
		}
		else {
			return len;
		}
	}

	StraightTrack::StraightTrack(Xtrk3D::OBJ_TYPE* obj) :Track(obj->index, obj->layer, obj->width, obj->scale, obj->visibility, glm::vec3(), 0)
	{
		auto ep = obj->endpoints;
		while (ep != nullptr) {
			endPoint p(ep->index, to_vec3(ep->position), ep->angle, ep->options);
			endp.push_back(p);
			ep = ep->next;
		}
		auto s = obj->segments; //should be zero
		while (s != nullptr) {
			addSegment(s);
			s = s->next;
		}
		if (endp.size() == 2) { //should always be 2
			StraightSegment* S = new StraightSegment(endp[0].pos, endp[1].pos, m_transformMatrix);
			len = S->len;
			CG = S->getCGposition();
			Segments.push_back(S);
		}
	}

	StraightTrack::StraightTrack(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility)
		:Track(t_index, t_layer, t_lineWidth, t_scale, t_visibility, glm::vec3(), 0)
	{}


	void StraightTrack::dump(json& j)
	{
		Track::dump(j, "StraightTrack");

	}

	bool StraightTrack::OnSelect(bool cm, std::string layer)
	{
		Select Menu(this->index, "Straight Track", cm);
		Menu.insert("pt 1:", endp.front().pos, false);
		Menu.insert("pt 2:", endp.back().pos, false);
		Menu.insert("Length:", this->len);
		Menu.insert("Layer:", layer);
		Menu.insert("Scale:", scale);
		return Menu.showModal();
	}

	XTrkObj* StraightTrack::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Straight Track";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.label = "pt 1";
		item2.point = &endp.front().pos;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);

		item2.label = "pt 2";
		item2.point = &endp.back().pos;
		properties.push_back(item2);

		item2.label = "Length:";
		item2.d = this->len;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "Layer:";
		item2.i = this->layer;
		item2.type = VariantType::isLayer;
		properties.push_back(item2);

		item2.label = "Scale:";
		item2.str = scale.c_str();
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return this;
	}

	inline double wrap0_360(double angle) {
		angle -= 360.0f * std::floor(angle / 360.0f);
		return angle;
	}

	CurveTrack::CurveTrack(Xtrk3D::OBJ_TYPE* obj) :Track(obj->index, obj->layer, obj->width, obj->scale, obj->visibility, glm::vec3(), 0)
	{
		m_center = glm::vec3(obj->point.x, obj->point.y, 0);
		m_radius = obj->radius;
		m_turns = obj->turns;

		auto ep = obj->endpoints;
		while (ep != nullptr) {
			endPoint p(ep->index, to_vec3(ep->position), ep->angle, ep->options);
			endp.push_back(p);
			ep = ep->next;
		}
		if (endp.size() == 2) {
			glm::vec3 point1 = glm::vec3(endp[0].pos - m_center);
			glm::vec3 point2 = glm::vec3(endp[1].pos - m_center);
			m_angle1 = wrap0_360(glm::atan(point1.x, point1.y) * (180.0f / M_PI));
			m_angle2 = wrap0_360(glm::atan(point2.x, point2.y) * (180.0f / M_PI));
			double swing = wrap0_360(m_angle2 - m_angle1);

			CurveSegment* S = new CurveSegment(m_center, m_radius, m_angle1, swing, m_turns, m_transformMatrix);
			len = S->len;
			S->setCG_Z((endp[0].pos.z + endp[1].pos.z) / 2);
			CG = S->getCGposition();
			Segments.push_back(S);
		}
	}

	CurveTrack::CurveTrack(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3 t_center, const double t_radius, const int t_turns)
		:Track(t_index, t_layer, t_lineWidth, t_scale, t_visibility, glm::vec3(), 0), m_center(t_center), m_radius(t_radius), m_turns(t_turns),m_angle1(0),m_angle2(0)
	{}


	void CurveTrack::dump(json& j)
	{
		Track::dump(j, "CurveTrack");
		j["CurveTrack"]["Radius"] = std::to_string(m_radius);
		j["CurveTrack"]["Turns"] = m_turns;
		j["CurveTrack"]["Center"] = { std::to_string(m_center.x),std::to_string(m_center.y),std::to_string(m_center.z) };
	}


	bool CurveTrack::OnSelect(bool cm, std::string layer)
	{
		Select Menu(this->index, (m_turns > 0 ? "Helix" : "Curved Track"), cm);
		Menu.insert("pt 1:", endp.front().pos, false);
		Menu.insert("pt 2:", endp.back().pos, false);
		Menu.insert("Center", m_center.xy);
		if (m_turns > 0) {
			Menu.insert("# of Turns:", m_turns);
		}
		Menu.insert("Radius:", this->m_radius);
		Menu.insert("Length:", this->len);
		Menu.insert("Layer:", layer);
		Menu.insert("Scale:", scale);
		return Menu.showModal();
	}

	XTrkObj* CurveTrack::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = (m_turns > 0 ? "Helix" : "Curved Track");
		item.type = VariantType::isHeader;
		properties.push_back(item);
		SELECT_TYPE item2;
		item2.label = "pt 1";
		item2.point = &endp.front().pos;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);

		item2.label = "pt 2";
		item2.point = &endp.back().pos;
		properties.push_back(item2);

		item2.label = "Center:";
		item2.point = &m_center;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);
		if (m_turns > 0) {
			SELECT_TYPE item;
			item.label = "# of Turns:";
			item.i=m_turns;
			item.type = VariantType::isInt;
			properties.push_back(item);
		}
		item2.label = "Radius:";
		item2.d = this->m_radius;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "Length:";
		item2.d = this->len;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "Layer:";
		item2.i = this->layer;
		item2.type = VariantType::isLayer;
		properties.push_back(item2);

		item2.label = "Scale:";
		item2.str = scale.c_str();
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return this;
	}

	Joint::Joint(Xtrk3D::OBJ_TYPE* obj, std::map<int, Track*>&Tracks) :Track(obj->index, obj->layer, obj->width, obj->scale, obj->visibility, glm::vec3(), 0)
	{
		auto ep = obj->endpoints;
		while (ep != nullptr) {
			endPoint p(ep->index, to_vec3(ep->position), ep->angle, ep->options);
			endp.push_back(p);
			ep = ep->next;
		}
		if (endp.size() == 2) {
			JointSegment* S = new JointSegment(endp[0].pos, endp[1].pos, m_transformMatrix, &Tracks);
			len = S->len;
			CG = S->getCGposition();
			Segments.push_back(S);
		}

	}

	Joint::Joint(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility)
		:Track(t_index, t_layer, t_lineWidth, t_scale, t_visibility, glm::vec3(), 0)
	{}

	double Joint::getDistance(int p1, int p2) {
		glm::vec3 P1(endp[0].pos);
		glm::vec3 P2(endp[1].pos);
		P1.z = 0.0f;
		P2.z = 0.0f;
		return glm::length(P1 - P2);
	}

	void Joint::dump(json& j)
	{
		Track::dump(j, "Joint");
	}

	bool Joint::OnSelect(bool cm, std::string layer)
	{
		Select Menu(this->index, "Joint", cm);
		Menu.insert("pt 1", endp.front().pos, false);
		Menu.insert("pt 2", endp.back().pos, false);
		Menu.insert("Length", this->len);
		Menu.insert("Layer", layer);
		Menu.insert("Scale:", scale);
		return Menu.showModal();
	}

	XTrkObj* Joint::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Joint";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.label = "pt 1";
		item2.point = &endp.front().pos;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);

		item2.label = "pt 2";
		item2.point = &endp.back().pos;
		properties.push_back(item2);
		item2.label = "Length:";
		item2.d = this->len;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "Layer:";
		item2.i = this->layer;
		item2.type = VariantType::isLayer;
		properties.push_back(item2);

		item2.label = "Scale:";
		item2.str = scale.c_str();
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return this;

	}

	Turntable::Turntable(Xtrk3D::OBJ_TYPE* obj) : Track(obj->index, obj->layer, 0, obj->scale, obj->visibility, to_vec3(obj->point), 0)
	{
		m_radius = obj->radius;
		auto ep = obj->endpoints;
		while (ep != nullptr) {
			endPoint p(ep->index, to_vec3(ep->position), ep->angle, ep->options);
			endp.push_back(p);
			ep = ep->next;
		}
		CG = m_pos;
	}


	Turntable::Turntable(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3& t_pos, const double t_rotation, const double t_radius)
		:Track(t_index, t_layer, t_lineWidth, t_scale, t_visibility, t_pos, t_rotation)
		, m_radius(t_radius)
	{}

	void Turntable::dump(json& j)
	{
		Track::dump(j, "Turntable");
		//XTrkObj::dump(j, "Turntable");
		//j["Turntable"]["Visibility"] = visibility;
		//j["Turntable"]["Scale"] = scale.c_str();
		j["Turntable"]["Radius"] = std::to_string(m_radius);
	}


	SceneNode* Turntable::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
	{
		SceneNode* Node(nullptr);
		float Gauge(param.gauge());
		float InnerRadius = (float)sqrt(m_radius*m_radius + Gauge * Gauge / 4);
		float EmbankmentW = param.embBotW() - param.embTopW();
		float OuterRadius = (float)m_radius + EmbankmentW;
		float RailH = 0.9f*param.embH();
		glm::vec3 Bot(0, 0, param.embH() + param.TieH()*1.05f + param.RailH());

		Ring MyRing(ShapeType::TrackType, CG - Bot, InnerRadius, OuterRadius, (param.embH() + param.TieH()*1.1), 96, glm::vec3(0, 0, 1), Shaders, Appearance::darkConcreteSurface);
		Disc MyDisc(ShapeType::TrackType, CG - Bot, OuterRadius, 96, glm::vec3(0, 0, 1), Shaders, Appearance::concreteSurface, GL_NONE,2.0f);
		Node = new SceneNode();
		Node->TrackObj = this;
		Node->addShape(MyRing);
		Node->addShape(MyDisc);
		Node->layer = this->layer;
		Node->isTrack = true;
		std::vector<glm::vec3>S = { CG + glm::vec3(OuterRadius,0,0), CG - glm::vec3(OuterRadius,0,0) };
		Node->boundingSphere = BoundSphere(S);
		return Node;
	}

	bool Turntable::OnSelect(bool cm, std::string layer)
	{
		Select Menu(this->index, "TurnTable", cm);
		Menu.insert("Orign", m_pos, false);
		Menu.insert("Diameter", this->m_radius * 2);
		Menu.insert("End points", (int)this->endp.size());
		Menu.insert("Layer", layer);
		Menu.insert("Scale:", scale);
		return Menu.showModal();
	}

	XTrkObj* Turntable::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "TurnTable";
		item.type = VariantType::isHeader;
		properties.push_back(item);
		SELECT_TYPE item2;
		item2.label = "Center:";
		item2.point = &m_pos;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);
		item2.label = "Diameter:";
		item2.d = this->m_radius*2;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "End points:";
		item2.i = this->endp.size();
		item2.type = VariantType::isInt;
		properties.push_back(item2);

		item2.label = "Layer:";
		item2.i = this->layer;
		item2.type = VariantType::isLayer;
		properties.push_back(item2);

		item2.label = "Scale:";
		item2.str = scale.c_str();
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return this;
	}

	Cornu::Cornu(Xtrk3D::OBJ_TYPE* obj) : Track(obj->index, obj->layer, obj->width, obj->scale, obj->visibility, to_vec3(obj->point), obj->rotation)
	{
		m_transformMatrix = glm::mat4(); //override Track
		len = 0;
		auto ep = obj->endpoints;
		while (ep != nullptr) {
			endPoint p(ep->index, to_vec3(ep->position), ep->angle, ep->options);
			endp.push_back(p);
			ep = ep->next;
		}
		auto s = obj->segments;
		while (s != nullptr) {
			addSegment(s);
			s = s->next;
		}
		CG = setMidPoint();
	}

	Cornu::Cornu(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3 t_pos, const double t_rotation)
		:Track(t_index, t_layer, t_lineWidth, t_scale, t_visibility, t_pos, t_rotation)
	{
		m_transformMatrix = glm::mat4(); //override Track
	}

	void Cornu::dump(json& j)
	{
		Track::dump(j, "Cornu");
		j["Cornu"]["pos"] = { std::to_string(m_pos.x), std::to_string(m_pos.y), std::to_string(m_pos.z) };
	}

	void Cornu::setSegmentZ()
	{
		double dL = 0;
		float dz = endp[1].pos.z - endp[0].pos.z;
		for (auto ii = subPointList.begin(); ii != subPointList.end(); ++ii)
		{
			double z1 = endp[0].pos.z + dz * dL / len;
			dL += ii->parent->len;
			double z2 = endp[0].pos.z + dz * dL / len;
			if (ii->index == 0) {
				ii->parent->point1.z = (float)z1;
				ii->parent->point2.z = (float)z2;
			}
			else {
				ii->parent->point2.z = (float)z1;
				ii->parent->point1.z = (float)z2;
			}
		}
		return;
	}

	glm::vec3 Cornu::setMidPoint()
	{
		//create subpointList and find midpoint
		len = 0;
		for (auto &ii : Segments)
			len += ii->len;
		std::vector<Segment*> SegmentList(Segments);
		glm::vec3 point = endp[0].pos;
		glm::vec3 norm;
		glm::vec3 midpoint;

		if (Segments.size() == 1) {
			auto side = glm::length(SegmentList.at(0)->point1.xy - point.xy) < len / 2 ? 0 : 1;
			//(*ii)->point1.xy - point.xy)
			subpoint s1(*SegmentList.begin(), side);
			subPointList.push_back(s1);
			return (*Segments.begin())->getCGposition();
		}
		double dL = 0;
		double mid = len / 2;
		while (SegmentList.size() > 0) {
			subpoint s;
			s = NextSub(SegmentList, point);
			if (s.parent == nullptr)
				break;
			subPointList.push_back(s);


			if (glm::length(norm) > 0.0) {
				glm::vec3 ns = s.parent->getNormal(s.index);
				auto x = glm::dot(ns, norm);
				if (glm::dot(ns, norm) > 0) {
					s.parent->setNormal(s.index, norm);
				}
				else {
					s.parent->setNormal(s.index, -norm);
				}
				s.index == 0 ? s.parent->point1 = point : s.parent->point2 = point;
			}//*/

			point = (s.index == 0) ? s.parent->point2 : s.parent->point1;
			norm = s.parent->getNormal((s.index == 0) ? 1 : 0);

			double diff = mid - dL;
			if ((mid >= dL) && (diff < s.parent->len)) //midpoint on this segment
			{
				if (s.index > 0) diff = s.parent->len - diff;
				midpoint = s.parent->getPoint((float)diff);
			}
			dL += s.parent->len;
		}
		return midpoint;
	}

	Cornu::subpoint Cornu::NextSub(std::vector<Segment*>& SegmentList, const glm::vec3 &point)
	{
		for (auto ii = SegmentList.begin(); ii != SegmentList.end(); ++ii)
		{
			if (glm::length((*ii)->point1.xy - point.xy) < param.gauge()) {
				subpoint s(*ii, 0);
				SegmentList.erase(ii);
				return(s);
			}
			if (glm::length((*ii)->point2.xy - point.xy) < param.gauge()) {
				subpoint s(*ii, 1);
				SegmentList.erase(ii);
				return(s);
			}
		}
		return(subpoint());
	}

	bool Cornu::OnSelect(bool cm, std::string layer)
	{
		Select Menu(this->index, "Cornu", cm);
		Menu.insert("pt 1", endp.front().pos, false);
		Menu.insert("pt 2", endp.back().pos, false);
		Menu.insert("Length", this->len);
		Menu.insert("Segments", (int)Segments.size());
		Menu.insert("Layer", layer);
		Menu.insert("Scale:", scale);
		return Menu.showModal();
	}

	XTrkObj* Cornu::OnSelect(std::vector<SELECT_TYPE>& properties)
	{
		SELECT_TYPE item;
		item.header.id = this->index;
		item.header.layer = this->layer;
		item.header.title = "Cornu";
		item.type = VariantType::isHeader;
		properties.push_back(item);

		SELECT_TYPE item2;
		item2.label = "pt 1";
		item2.point = &endp.front().pos;
		item2.type = VariantType::isVec3;
		properties.push_back(item2);

		item2.label = "pt 2";
		item2.point = &endp.back().pos;
		properties.push_back(item2);
		item2.label = "Length:";
		item2.d = this->len;
		item2.type = VariantType::isDouble;
		properties.push_back(item2);

		item2.label = "Segments:";
		item2.i = Segments.size();
		item2.type = VariantType::isInt;
		properties.push_back(item2);

		item2.label = "Layer:";
		item2.i = this->layer;
		item2.type = VariantType::isLayer;
		properties.push_back(item2);

		item2.label = "Scale:";
		item2.str = scale.c_str();
		item2.type = VariantType::isStr;
		properties.push_back(item2);
		return this;

	}


	void Cornu::initSurface(TrackPlan* myLayout)
	{
		Track::initSurface(myLayout);
	}


}