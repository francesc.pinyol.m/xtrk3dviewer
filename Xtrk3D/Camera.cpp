#define GLM_FORCE_RADIANS
#include "Camera.h"
#include <glm/glm/gtc/matrix_transform.hpp>

namespace Xtrk3D {
	Camera::Camera() :
		mPosition(0.0f, 0.0f, 5.0f)
		,_fieldOfView(40.0f)
		,_nearPlane(0.1f)
		,_farPlane(100.0f)
		,_aspectRatio(4.0f / 3.0f)
	{
	}

	Camera::~Camera()
	{
	}

	void Camera::SetPosition(const glm::vec3& position)
	{
		mPosition = position;
	}

	void Camera::setFieldOfView(float fieldOfView) {
		assert(fieldOfView > 0.0f && fieldOfView < 180.0f);
		_fieldOfView = fieldOfView;
	}

	void Camera::setClipping(float nearPlane, float farPlane) {
		assert(nearPlane > 0.0f);
		assert(farPlane > nearPlane);
		_nearPlane = nearPlane;
		_farPlane = farPlane;
	}

	glm::mat4 Camera::getMatrix() const {
		return projection() * view();
	}



	void Camera::setViewportAspectRatio(float viewportAspectRatio) {
		assert(viewportAspectRatio > 0.0);
		_aspectRatio = viewportAspectRatio;
	}

	glm::mat4 Camera::projection() const {
		return glm::perspective(glm::radians(_fieldOfView), _aspectRatio, _nearPlane, _farPlane);
	}

	glm::mat4 Camera::view() const {
		return orientation() * glm::translate(glm::mat4(), -mPosition);
	}

	glm::mat4 Camera::orientation() const {
		return glm::mat4_cast(mOrientation);
		//glm::mat4 viewMatrix = glm::mat4_cast(mOrientation);
		//return viewMatrix;
	}

	void Camera::Move(const glm::vec3& offset)
	{
		mPosition += offset;
	}

	void Camera::Rotate(float angleRadians, const glm::vec3& axis) {
		glm::quat rotation = glm::angleAxis(angleRadians, axis);
		mOrientation = rotation * mOrientation;
	}

	void Camera::MoveForward(float movement) {
		mPosition += GetForward() * movement;
	}

	void Camera::MoveLeft(float movement) {
		mPosition += GetLeft() * movement;;
	}

	void Camera::MoveUp(float movement) {
		mPosition += GetUp() * movement;
	}

	void Camera::LookAt(glm::vec3 position,glm::vec3 orientation)
	{
		glm::mat4 M = glm::lookAt(glm::vec3(0, 0, 0), position - mPosition, orientation);
		mOrientation = glm::quat_cast(M);
	}

	glm::vec3 Camera::GetForward() const {
		return glm::conjugate(mOrientation)*glm::vec3(0.0f, 0.0f, -1.0f);
	}
	glm::vec3 Camera::GetLeft() const {
		return glm::conjugate(mOrientation)*glm::vec3(-1.0f, 0.0f, 0.0f);
	}
	glm::vec3 Camera::GetUp() const {
		return glm::conjugate(mOrientation)*glm::vec3(0.0f, 1.0f, 0.0f);
	}

	Frustrum Camera::GetPlanes()
	{

		double angle = glm::radians(_fieldOfView);

		Frustrum F;

		//	angle = 0.9*angle; //test purpose --remove

		/*
			double hnear = 2.0f*tan(angle / 2) *_nearPlane;
			double wnear = hnear * _aspectRatio;
			float hnearhalf = (float)(hnear / 2.0);
			float wnearhalf = (float)(wnear / 2.0f);

			double hfar = 2.0f*tan(angle / 2) * _farPlane;
			double wfar = hfar * _aspectRatio;
			float hfarhalf = (float)(hfar / 2.0f);
			float wfarhalf = (float)(wfar / 2.0f);
		//*/

		float hfarhalf = (float)tan(angle / 2) *  _farPlane;
		float wfarhalf = hfarhalf * _aspectRatio;
		F.forward = GetForward();
		F.up = GetUp();
		F.left = GetLeft();

		F.farTopLeft = F.forward *_farPlane + F.left*wfarhalf + F.up*hfarhalf;
		F.farTopRight = F.forward *_farPlane - F.left*wfarhalf + F.up*hfarhalf;
		F.farBottomLeft = F.forward *_farPlane + F.left*wfarhalf - F.up*hfarhalf;
		F.farBottomRight = F.forward *_farPlane - F.left*wfarhalf - F.up*hfarhalf;

		F.Normals[0] = F.nLeft = glm::normalize(glm::cross(F.farBottomLeft, F.farTopLeft));
		F.Normals[1] = F.nRight = glm::normalize(glm::cross(F.farTopRight, F.farBottomRight));
		F.Normals[2] = F.nUp = glm::normalize(glm::cross(F.farTopLeft, F.farTopRight));
		F.Normals[3] = F.nDown = glm::normalize(glm::cross(F.farBottomRight, F.farBottomLeft));

		float MidPlane = 20;
		float hMidhalf = (float)tan(angle / 2) *  MidPlane;
		float wMidhalf = hMidhalf * _aspectRatio;
		F.MidTopLeft = GetForward()*MidPlane + GetLeft()*wMidhalf + GetUp()*hMidhalf;
		F.MidTopRight = GetForward()*MidPlane - GetLeft()*wMidhalf + GetUp()*hMidhalf;
		F.MidBottomLeft = GetForward()*MidPlane + GetLeft()*wMidhalf - GetUp()*hMidhalf;
		F.MidBottomRight = GetForward()*MidPlane - GetLeft()*wMidhalf - GetUp()*hMidhalf;
		F.farDistance = _farPlane;
		F.nearDistance = _nearPlane;

		return F;
	}
}
/*
literature:
https://stackoverflow.com/questions/9715776/using-quaternions-for-opengl-rotations
http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/
https://github.com/hmazhar/moderngl_camera/blob/master/camera.cpp
https://github.com/hmazhar/moderngl_camera
http://in2gpu.com/2016/03/14/opengl-fps-camera-quaternion/
http://in2gpu.com/2016/02/26/opengl-fps-camera/
https://www.tomdalling.com/blog/modern-opengl/04-cameras-vectors-and-input/
*/
