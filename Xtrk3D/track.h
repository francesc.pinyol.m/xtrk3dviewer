#pragma once

#include "segment.h"
namespace Xtrk3D {
	class Track : public XTrkObj, public Surface
	{
	public:
		Track() : XTrkObj(), lineWidth(0), visibility(0), len(0) {};
		Track(const int t_index, const int t_layer, const int t_lineWidth, const std::string& t_scale, const int t_visibility, const glm::vec3& t_pos, const double t_rotation);

		typedef struct TrainpathPosition {
			Track* track;
			glm::vec3 point;
			glm::vec3 centerPoint;
			TrainpathPosition() :track(nullptr) {};
			TrainpathPosition(Track* t_track, glm::vec3 t_pos,glm::vec3 t_center) :track(t_track), point(t_pos),centerPoint(t_center) {};
		}TrainpathPosition;

		//virtual ~Track();
		virtual void setSegmentZ();
		virtual void initSurface(TrackPlan* myLayout);
		virtual double getDistance(int p1, int p2) { return 0.001; };
		virtual enum xtrkObjType getObjType() { return xtrkUndefined; };
		virtual bool isHelix() { return false; };
		virtual SceneNode* createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count=0);
		virtual SceneNode* CreateTunnelNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout);
		virtual SceneNode* CreateBridgeNode(OpenGLShaderPrograms* Shaders, TrackPlan* myLayout);

		SceneNode* setEndTrack(endPoint endp, OpenGLShaderPrograms* Shaders, bool bufferStop = true, bool bridgeEnd = false);
		void addSegment(Xtrk3D::Segment* pS);
		void addSegment(Xtrk3D::SEGMENT_TYPE* obj);
		void addSegmentExt(Xtrk3D::SEGMENT_TYPE* obj);
		virtual glm::vec3 SetMidPoint();
		unsigned int lineWidth;
		unsigned int visibility;

		double len;
		std::string scale;
		Scale param; //scale parameters
		std::vector<endPoint> endp;
		virtual void dump(json& j) = 0;
		void dump(json& j, std::string lbl);
		void SetClearenceEnd(glm::vec3 point, ClipperLib::Paths& c, int divs);

		Segment* getSegmentAtPoint(glm::vec3 t_pos);
		bool pointIsOnTrack(glm::vec3 t_pos);
		glm::vec3 getCenterline_Point(glm::vec3 t_pos); //depreciated
		bool getCenterline_Point(glm::vec3 t_pos, glm::vec3 &centerpos);
		glm::vec3 getTangent(glm::vec3 t_pos);

		Track* MoveAlongTrackFwd(Camera& myCamera, float speed, bool verbose, Scene *myScene);
		void ReverseMoveDirection(Camera& myCamera);
		glm::vec3 toPosWithinEmbTop(glm::vec3 t_from, glm::vec3 t_to); //depreciated
		TrainpathPosition getNextPosAlongTrack(glm::vec3 t_heading, glm::vec3 t_pos, float speed, bool verbose);
		virtual TrainpathPosition Track::getNextPos(Camera& myCamera, glm::vec3 t_pos, float speed, bool verbose);
		glm::vec3 getNextTest(glm::vec3 t_heading, glm::vec3 t_pos, float speed, SceneNode* Node, OpenGLShaderPrograms* Shaders,int count);

	protected:
		//	std::vector<Segment*> Segments;
		ClipperLib::Paths SetClearenceEnd(endPoint endp, float ExtendW, int divs);
		void ComputeBufferSupports(glm::vec3& pos, glm::vec3& Ext2, glm::vec3& Ext3, glm::vec3& Ext4, glm::vec3& H, glm::vec3& H2, std::vector<vertex>& Verts2);
		void ComputeVertexes(glm::vec3& pos1, glm::vec3& Ext2, glm::vec3& Ext3, glm::vec3& Ext4, glm::vec3& H, std::vector<vertex>& Verts);
		void addRailEnd(SceneNode* Node, glm::vec3& pos, glm::vec3& N, float inner, float outer, OpenGLShaderPrograms* Shaders);
	};
}
