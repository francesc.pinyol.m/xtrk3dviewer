#include "Structure.h"
#include <algorithm>
#include <sstream>
#include <iomanip>
#include "EarcutWrapper.h"
#include "Scene.h"
#include "misc.h"
using namespace ClipperLib;



void ClipperCallback(const IntPoint& e1bot, IntPoint& e1top, IntPoint& e2bot, IntPoint& e2top, IntPoint& pt)
{
	//Just let new z be average -- good enough
	double z1 = (e1bot.Z.Pos.z + e1top.Z.Pos.z + e2bot.Z.Pos.z + e2top.Z.Pos.z) / 4;
	pt.Z.Pos = glm::vec3(pt.X / Iscale, pt.Y / Iscale, z1);
	int myFlag = (e1bot.Z.Flag ? 1 : 0) + (e1top.Z.Flag ? 1 : 0) + (e2bot.Z.Flag ? 1 : 0) + (e2top.Z.Flag ? 1 : 0);
	pt.Z.Flag = (e1bot.Z.Flag || e1top.Z.Flag || e2bot.Z.Flag || e2top.Z.Flag); // if flag at least one point in iPath
}

Paths joinPaths(Paths &pgs, Paths &clp)
{
	Clipper c;
	Paths sol;
	c.ZFillFunction((ZFillCallback)&ClipperCallback);
	c.AddPaths(pgs, ptSubject, true);
	c.AddPaths(clp, ptClip, true);
	c.Execute(ctUnion, sol, pftEvenOdd, pftEvenOdd);
	return(sol);
}

namespace Xtrk3D {
	std::map <std::string, enum xtrkObjType> StructTokens =
	{
		{"END",XtrkEnd}
		,{"END$SEGS",XtrkEnd}
	};


Structure::Structure() :baseZset(false),baseZValue(0)
{
}


/*Structure::~Structure()
{
	for (auto &ii : Segments)
		delete ii;
	Segments.clear();
}//*/

Structure::Structure(Xtrk3D::OBJ_TYPE* obj):baseZset(false)
{
	index = obj->index;
	layer = obj->layer;
	scale = obj->scale;
	m_pos = to_vec3(obj->point);
	m_rotation = obj->rotation;
	label = std::string(obj->label);
	m_transformMatrix = glm::rotate(glm::translate(glm::mat4(), m_pos), float(m_rotation / 180.0*M_PI), glm::vec3(0, 0, -1));
	auto s = obj->segments;
	while (s != nullptr) {
		addSegment(s);
		s = s->next;
	}
}

Structure::Structure(const int t_index, const int t_layer,  std::string t_scale, const glm::vec3& t_pos, const float t_rotation, std::string t_label)
	:XTrkObj(t_index, t_layer, t_pos, t_rotation)
	, scale(t_scale)
	, label(t_label)
	, baseZset(false)
{
	baseZValue = 0;
}; //empty structure


void Structure::dump(json& j, std::string lbl)
{
	j[lbl]["Index"] = index;
	j[lbl]["Layer"] = layer;
	j[lbl]["Translation"] = { std::to_string(m_pos.x), std::to_string(m_pos.y), std::to_string(m_pos.z) };
	j[lbl]["Rotation"] = std::to_string(m_rotation);
	j[lbl]["BaseZ"] = baseZset;
	if (baseZset) {
		j[lbl]["BaseZValue"] = baseZValue;
	}
	for (auto &ii : Segments) {
		json jj;
		ii->dump(jj);
		j[lbl]["Segments"].push_back(jj);
	}
}

void Structure::dump(json& j)
{
	dump(j, "Structure");
}

void Structure::addSegment(Xtrk3D::SEGMENT_TYPE* obj)
{
	switch (obj->type) {
	case 'L': //Straight line segment L3
	{
		//L3<sp>color<sp>width<sp>x1<sp>y1<sp>0<sp>x2<sp>y2<sp>0
		auto pS = new LineSegment(to_vec3(obj->data_.two_points.p1), to_vec3(obj->data_.two_points.p2),obj->color,obj->width, m_transformMatrix);
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	case 'A': //Curved line segment A3
	{
		//A3<sp>color<sp>line-width<sp>radius<sp>center-X<sp>center-Y<sp>0<sp>angle<sp>degrees of swing.
		auto pS = new ArcSegment(obj->color, obj->width, obj->data_.curve.radius, to_vec3(obj->data_.curve.center), obj->data_.curve.a0, obj->data_.curve.a1, m_transformMatrix);
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	case 'G': //Filled circle segment G3
	{
		//G3<sp>color<sp>width<sp>radius<sp>center-X<sp>center-Y<sp>0
		auto pS = new CircleSegment(obj->color, obj->width, obj->data_.curve.radius, to_vec3(obj->data_.curve.center), m_transformMatrix);
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	case 'Y': //Poly segment Y/Y3/Y4
	{
		//Y3<sp>color<sp>width<sp>end-points
		auto pS = new PolyLineSegment(obj->color, obj->width, m_transformMatrix,obj->data_.pointArray.mode);
		unsigned int i(0);
		while (i< obj->data_.pointArray.count) {
			pS->add(to_vec3(obj->data_.pointArray.points[i++]));
		}
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	case 'F': //Filled poly segment F/F3/F4
	{
		//F3<sp>color<sp>width<sp>#end - points
		auto pS = new FilledSegment(obj->color, obj->width, m_transformMatrix);
		unsigned int i(0);
		while (i < obj->data_.pointArray.count) {
			pS->add(to_vec3(obj->data_.pointArray.points[i++]));
		}
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	case 'Z': //Text segment Z
	{
		auto pS = new TextSegment(obj->color, to_vec3(obj->data_.textString.pos),obj->data_.textString.angle,obj->data_.textString.height,std::string(obj->data_.textString.str),m_transformMatrix);
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	case 'Q': //table-edge line Q3
	{
		//Q3<sp>0<sp>0.187500<sp>startX<sp>startY<sp>0<sp>endX<sp>endY<sp>0
		auto pS = new TableEdgeSegment(to_vec3(obj->data_.two_points.p1), to_vec3(obj->data_.two_points.p2), m_transformMatrix);
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	case 'B': //benchwork line B3
	{
		// read Benchwork
		//    B3 16760832 0.000000 12.000000 34.000000 0 97.000000 34.000000 0 33818624
		//      color            pt1 x      pt1 y        pt2 x     pt2 y     (hex 2 04 08 00= L-shape 2" 4" orientation=0 )
		auto pS = new BenchworkSegment(to_vec3(obj->data_.two_points.p1), to_vec3(obj->data_.two_points.p2), m_transformMatrix, obj->data_.two_points.mode);
		pS->index = this->index;
		pS->layer = this->layer;
		Segments.push_back(pS);
		break;
	}
	}
}

void Structure::initSurface(TrackPlan* myLayout)
{
	if (!Segments.empty()) {
		CG = glm::vec3();
		for (unsigned int i = 0; i < Segments.size();++i) {
			if ((Segments[i]->getObjType() == xtrkBenchWorkSegment)&&(myLayout!=nullptr))
				Segments[i]->setCG_Z(myLayout->benchworkZeroLevel);
			Segments[i]->Init(nullptr);
			CG += Segments[i]->getCGposition();
		}
		CG /= Segments.size();
	}
}

void Structure::updateSceneNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, TrackPlan *myLayout)
{
	Node->clear();

	for (auto &ii : Segments) {
		Node->addNode(ii->createSceneNode(Shaders, myLayout));
	}
	if (!path.empty() && baseZset) {
		add3DShapesToNode(Node, Shaders, glm::vec3(0.7, 0.7, 0.7), path, (float)baseZValue -m_pos.z);
	}

	glm::mat4 transformMatrix = glm::translate(glm::mat4(), glm::vec3(0, 0, m_pos.z));
	CG.z = m_pos.z;
	Node->setTransform(transformMatrix);
	Node->Update();
}

SceneNode* Structure::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
{
	SceneNode* Node = new SceneNode();
	Node->layer = layer;
	path.clear();
	for (auto &ii : Segments) {
		if (ii->getObjType() == xtrkFilledSegment || ii->getObjType() == xtrkExtrusion) {
			FilledSegment* p = (FilledSegment*)ii;
			path = joinPaths(path, p->surface);
		}
		Node->addNode(ii->createSceneNode(Shaders, myLayout,count));
	}
	if (Node->hasChildren()) {
		Node->TrackObj = this;
		if (!path.empty()&& baseZset) {
			add3DShapesToNode(Node, Shaders, glm::vec3(0.7, 0.7, 0.7), path, (float)baseZValue-m_pos.z);
			glm::mat4 transformMatrix = glm::translate(glm::mat4(), glm::vec3(0, 0, m_pos.z));
			Node->setTransform(transformMatrix);
			Node->Update();
		}
		return Node;
	}
	delete Node;
	return nullptr;
}

bool Structure::OnSelect(bool cm, std::string layer)
{
	int pz;
	bool status(false);

	std::stringstream ss;
	ss << std::fixed << std::setprecision(0);
	ss << m_rotation;
	ss << " degrees";

	Select Menu(this->index, "Structure", cm,nullptr);

	pz=Menu.insert("Orign:", m_pos,true);
	Menu.insert("Angle:", ss.str());
	Menu.insert("# Segments:", (int) Segments.size());
	Menu.insert("Label:", label);
	Menu.insert("Layer:", layer);

	status= Menu.showModal();
	if (status) {
		float z = (float)(Menu.getValue(pz) / (cm ? 2.54 : 1));
		m_pos.z = z;
		initSurface(nullptr);
		CG.z = z;
	}
	return status;
}

XTrkObj* Structure::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Structure";
	item.type = VariantType::isHeader;
	item.hasBase = true;
	item.header.base_z_set = &this->baseZset;
	item.header.z = &this->baseZValue;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "Orign:";
	item2.point = &m_pos;
	item2.type = VariantType::isVec3;
	item2.z_Editable = true;
	properties.push_back(item2);
	item2.z_Editable = false;

	item2.label = "Angle:";
	item2.d = this->m_rotation;
	item2.type = VariantType::isAngle;
	properties.push_back(item2);

	item2.label = "# Segments:";
	item2.i = Segments.size();
	item2.type = VariantType::isInt;
	properties.push_back(item2);

	item2.label = "Label:";
	item2.str = label.c_str();
	item2.type = VariantType::isStr;
	properties.push_back(item2);

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);

	return this;
}

/*Draw::~Draw()
{
	for (auto &ii : Segments)
		delete ii;
	Segments.clear();
}//*/

Draw::Draw(Xtrk3D::OBJ_TYPE* obj)
{
	index = obj->index;
	layer = obj->layer;
	m_pos = to_vec3(obj->point);
	m_rotation = obj->rotation;
	m_transformMatrix = glm::rotate(glm::translate(glm::mat4(), m_pos), float(m_rotation / 180.0*M_PI), glm::vec3(0, 0, -1));
	auto s = obj->segments;
	while (s != nullptr) {
		addSegment(s);
		s = s->next;
	}
}

void Draw::dump(json& j)
{
	Structure::dump(j, "Draw");
}

void Draw::updateSceneNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, TrackPlan *myLayout)
{
	Node->clear();
	for (auto &ii : Segments) {
		ii->Init(nullptr);
		Node->addNode(ii->createSceneNode(Shaders, myLayout));
	}
	CG = this->Segments.front()->getCGposition();
}

bool Draw::OnSelect(bool cm, std::string layer)
{
	return this->Segments.front()->OnSelect(cm, layer);
}

std::string  Draw::OnSelect()
{
	return (this->Segments.empty() ? std::string("T") + std::to_string(index) : this->Segments.front()->OnSelect()); 
};


XTrkObj* Draw::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Draw Object";
	item.type = VariantType::isHeader;
	properties.push_back(item);
	if (Segments.size() == 1) {
		return this->Segments.front()->OnSelect(properties);
	}

	XTrkObj* obj(nullptr);
	int count(0);
	for (auto& ii : Segments) {
		item.type = VariantType::isDelimiter;
		item.i = ++count;
		properties.push_back(item);
		obj = ii->OnSelect(properties);
		item.type = VariantType::isStr;
		item.label = "-----------------------";
		item.str = "";
		properties.push_back(item);
	}
	return obj;
}


Bzrln::Bzrln(Xtrk3D::OBJ_TYPE* obj)
{
	index = obj->index;
	layer = obj->layer;
	objLength = 0;
	auto s = obj->segments;
	while (s != nullptr) {
		addSegment(s);
		s = s->next;
	}
}

void Bzrln::dump(json& j)
{
	Structure::dump(j, "Bzrln");
}

bool Bzrln::sort(int first, int second)
{
	for (unsigned int i = 0; i < 2; ++i)
		for (unsigned int j = 0; j < 2; ++j)
			if (glm::length(nodeList[first].Item->getEndpoint(i) - nodeList[second].Item->getEndpoint(j)) < 0.01) {
				nodeList[first].A[i] = second;
				nodeList[first].index[i] = j;
				nodeList[second].A[j] = first;
				nodeList[second].index[j] = i;
				return true;
			}
	return false;
}

std::vector<std::pair<int, int>> Bzrln::getNextVertex(std::pair<int, int> nodeId)
{
	std::vector<std::pair<int, int>> res;
	int j= (nodeId.second == 0 ? 1 : 0);
	res.push_back(std::make_pair(nodeId.first, j));

	if (nodeId.first > -1) {
		int ii = nodeList[nodeId.first].A[j];
		int jj = nodeList[nodeId.first].index[j];
		res.push_back(std::make_pair(ii, jj));
	}
	else {
		res.push_back(std::make_pair(-1,-1));
	}
	return res;
}

void Bzrln::initSurface(TrackPlan* myLayout)
{
	objLength=0;
	for (auto &ii : Segments)
	{
		objLength += ii->len;
		nodeList.push_back(Node(ii, ii->len));
	}

	for (unsigned int i = 0; i < nodeList.size(); ++i)
	{
		for (unsigned int j = i + 1; j < nodeList.size(); ++j)
		{
			if (sort(i, j))
				break;
		}
		if (nodeList[i].A[0] < 0) ep.push_back(std::make_pair(i, 0));
		if (nodeList[i].A[1] < 0) ep.push_back(std::make_pair(i, 1));
	}

	float dL = 0;
	float mid = (float) objLength / 2;

	if (ep.size() > 1) {
		point1 = nodeList[ep[0].first].Item->getEndpoint(ep[0].second);
		point2 = nodeList[ep[1].first].Item->getEndpoint(ep[1].second);

		std::vector<std::pair<int, int>>vert;
		vert.push_back(ep[0]);
		vert.push_back(ep[0]);
		while (vert[0].first >= 0 && vert[1].first>=0)
		{
			vert = getNextVertex(vert[1]);
			if (vert[1].first < 0)
				break;
			auto norm1 = nodeList[vert[0].first].Item->getNormal(vert[0].second);
			auto norm2 = nodeList[vert[1].first].Item->getNormal(vert[1].second);
			auto d = glm::dot(norm1, norm2);
			if (d < 0) {
				nodeList[vert[0].first].Item->setNormal(vert[0].second, -norm2);
			}
			else {
				nodeList[vert[0].first].Item->setNormal(vert[0].second, norm2);
			}
			float diff = mid - dL;
			if (diff > 0 && diff < nodeList[vert[0].first].Item->len)
			{
				CG = nodeList[vert[0].first].Item->getPoint(diff);
			}
			dL += (float) nodeList[vert[0].first].Item->len;
		}
	}

	for (auto &ii : Segments)
	{
		ii->Init(nullptr);
	}
}

void Bzrln::updateSceneNode(SceneNode* Node, OpenGLShaderPrograms* Shaders, TrackPlan *myLayout)
{
	Node->clear();
	setSegmentZ();
	float sumZ(0);
	for (auto &ii : Segments) {
		sumZ += ii->CG.z;
		Node->addNode(ii->createSceneNode(Shaders, myLayout));
	}
	CG.z = sumZ / Segments.size();
}

void Bzrln::setSegmentZ()
{
	double slope = ((double)point2[2] - (double)point1[2]) / objLength;
	double PzVal = point1[2];
	int i = ep[0].first;
	int j = ep[0].second;
	while (i >= 0) {
		nodeList[i].Item->setEndpointZ(j, (float)PzVal);
		PzVal += slope * nodeList[i].dL;
		j = (j == 0 ? 1 : 0);
		nodeList[i].Item->setEndpointZ(j, (float)PzVal);
		nodeList[i].Item->clear();
		nodeList[i].Item->Init(nullptr);
		int ii = nodeList[i].A[j];
		j = nodeList[i].index[j];
		i = ii;
	}
}
bool Bzrln::OnSelect(bool cm, std::string layer)
{
	std::vector<int> pz;
	bool status(false);

	Select Menu(this->index, "Bezier Line", cm);
	pz.push_back(Menu.insert("pt 1:", point1));
	pz.push_back(Menu.insert("pt 2:", point2));
	Menu.insert("Length:", objLength);
//	if(!Segments.empty())
//	Menu.insert("Line width", Segments.front()->Width);
	Menu.insert("Layer:", layer);
	status= Menu.showModal();
	if (status) {
		point1.z = (float)(Menu.getValue(pz.front()) / (cm ? 2.54 : 1));
		point2.z = (float)(Menu.getValue(pz.back()) / (cm ? 2.54 : 1));
	}
	return status;
}

XTrkObj* Bzrln::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Bezier Line";
	item.type = VariantType::isHeader;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "pt 1";
	item2.point = &point1;
	item2.type = VariantType::isVec3;
	item2.z_Editable = true;
	properties.push_back(item2);

	item2.label = "pt 2";
	item2.point = &point2;
	properties.push_back(item2);
	item2.z_Editable = false;

	item2.label = "Length:";
	item2.d = objLength;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);

	return this;
}

LineSegment::LineSegment(glm::vec3& p1, glm::vec3& p2, int t_color, double t_width, const glm::mat4& M) :Segment(p1, p2, M),Width(t_width),mode(0)
{
	ObjColor = glm::vec3((t_color & 0xff0000) / 65536/255.0f, (t_color & 0xff00) / 256/255.0f, (t_color & 0xff)/255.0f);
	addPoint(p1);
	addPoint(p2);
	norm1 = norm2 = glm::normalize(glm::cross(point2 - point1, glm::vec3(0, 0, 1)));
}

LineSegment::LineSegment(int t_color, double t_width, const glm::mat4& M) :  Width(t_width), mode(0)
{
	m_transformMatrix = M;
	ObjColor=glm::vec3(((t_color & 0xff0000) / 65536)/255.0f, ((t_color & 0xff00) / 256)/255.0f, (t_color & 0xff)/255.0f);
	surface.resize(1);
}

LineSegment::LineSegment(glm::vec3 t_color, double t_width, const glm::mat4& M) : Width(t_width), mode(0),ObjColor(t_color)
{
	m_transformMatrix = M;
	surface.resize(1);
}

void LineSegment::add(const glm::vec3& point)
{
	if (!points.empty())
		if (glm::length(point - points.back()) < 0.01)
			return;
	addPoint(point);
}

void LineSegment::dump(json& j, std::string lbl)
{
//	Segment::dump(j, lbl);
	j[lbl]["Color"]={ std::to_string(ObjColor.x), std::to_string(ObjColor.y), std::to_string(ObjColor.z) };
	j[lbl]["Width"]= std::to_string(Width);
	for (auto &ii : points) {
		j[lbl]["Coord"].push_back({ std::to_string(ii.x), std::to_string(ii.y), std::to_string(ii.z) });
	}
}

void LineSegment::addPoint(glm::vec3 point)
{
	glm::vec4 p1 = m_transformMatrix * glm::vec4(point, 1);
	points.push_back(point);
	if (points.size() == 1){
		rect = Rect(p1.xyz);
	}
	else {
		setMidPoint(p1.xyz);
	}
	CG = (rect.LL + rect.UR) / 2.0f;
}

void LineSegment::setMidPoint(glm::vec3 point)
{
	if (point.x < rect.LL.x)
		rect.LL.x = point.x;
	if (point.y < rect.LL.y)
		rect.LL.y = point.y;
	if (point.x > rect.UR.x)
		rect.UR.x = point.x;
	if (point.y > rect.UR.y)
		rect.UR.y = point.y;
}


void LineSegment::addVertex(glm::vec3 point, glm::vec3 offset)
{
	bool first(Verts.empty());
	glm::vec4 p(point, 1);
	glm::vec4 n(offset, 0);

	int endpos = Verts.size();
	Verts.push_back(vertex((m_transformMatrix*(p + n)).xyz, glm::vec3(), ObjColor, point.xy));
	Verts.push_back(vertex((m_transformMatrix*(p - n)).xyz, glm::vec3(), ObjColor, point.xy));
	if (!first) {
		Indices.push_back(endpos+1);
		Indices.push_back(endpos);
		Indices.push_back(endpos - 1);
		Indices.push_back(endpos - 1);
		Indices.push_back(endpos);
		Indices.push_back(endpos - 2);
	}
}
void LineSegment::addVertex(glm::vec3 point, glm::vec3 miter, glm::vec3 offset1, glm::vec3 offset2)
{
	float sign = glm::dot(glm::cross(offset1, offset2), glm::vec3(0, 0, 1)) > 0 ? 1.0f : -1.0f;
	glm::vec4 p(point, 1);
	glm::vec4 m(miter, 0);
	glm::vec4 n1(offset1, 0);
	glm::vec4 n2(offset2, 0);
	glm::vec4 neg_m= glm::vec4(-glm::normalize(miter), 0)*glm::length(n1);

	int endpos = Verts.size();
	if (sign > 0) {
		Verts.push_back(vertex((m_transformMatrix * (p + n1)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p - m)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p - neg_m)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p + n2)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p - m)).xyz, glm::vec3(), ObjColor, point.xy));
		Indices.push_back(endpos + 1);
		Indices.push_back(endpos);
		Indices.push_back(endpos + 3);

		Indices.push_back(endpos);
		Indices.push_back(endpos+3);
		Indices.push_back(endpos+2);

	}
	else {
		Verts.push_back(vertex((m_transformMatrix * (p + m)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p - n1)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p + neg_m)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p + m)).xyz, glm::vec3(), ObjColor, point.xy));
		Verts.push_back(vertex((m_transformMatrix * (p - n2)).xyz, glm::vec3(), ObjColor, point.xy));
		Indices.push_back(endpos + 1);
		Indices.push_back(endpos);
		Indices.push_back(endpos + 4);

		Indices.push_back(endpos+1);
		Indices.push_back(endpos+2);
		Indices.push_back(endpos+4);
	}

	Indices.push_back(endpos + 1);
	Indices.push_back(endpos);
	Indices.push_back(endpos - 1);
	Indices.push_back(endpos - 1);
	Indices.push_back(endpos);
	Indices.push_back(endpos - 2);
}

void LineSegment::Init(Track* parent)
{
	clear();
	if (points.size() != 2)
		return;
	mode = GL_TRIANGLES;
	float t = (float)std::fmax(Width / 2, minLineWidth);
	glm::vec3 p1 = points[0];
	glm::vec3 p2 = points[1];
	glm::vec3 v(glm::vec3(p2 - p1).xy, 0);
	addVertex(p1 + glm::vec3(0, 0, 0.05f), norm1*t);
	addVertex(p2 + glm::vec3(0, 0, 0.05f), norm2*t);
	len = glm::length(v);
//	CG = (p1 + p2) / 2.0f;
}

SceneNode* LineSegment::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
{ 
	SceneNode* Node=new SceneNode();
	Node->boundingSphere = BoundSphere(Verts);
	Shape Surface(ShapeType::OtherType , Verts, Indices, Shaders, Appearance::solidColor, Appearance::solidColor, mode, GL_NONE);
	Node->addShape(Surface);
	Node->layer = this->layer;
	Node->isTrack = false;
	return Node;
}
void LineSegment::clear()
{
	Verts.clear();
	Indices.clear();
	surface.clear();
}

bool LineSegment::OnSelect(bool cm, std::string layer)
{
	std::vector< int> pz;
	bool status(false);
	Select Menu(index, "Straight Line", cm);
	pz.push_back(Menu.insert("pt 1:", points[0]));
	pz.push_back(Menu.insert("pt 2:", points[1]));
	Menu.insert("Length:", len);
	Menu.insert("Line width:", Width);
	Menu.insert("Layer:", layer);
	status = Menu.showModal();
	if (status) {
		for (unsigned int i = 0; i < pz.size(); ++i) {
			points[i].z = (float)(Menu.getValue(pz[i]) / (cm ? 2.54 : 1));
		}
		//Init(nullptr);
	}
	return status;
}

XTrkObj* LineSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Straight Line";
	item.type = VariantType::isHeader;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "pt 1";
	item2.point = &points[0];
	item2.type = VariantType::isVec3;
	item2.z_Editable = true;
	properties.push_back(item2);

	item2.label = "pt 2";
	item2.point = &points[1];
	properties.push_back(item2);
	item2.z_Editable = false;

	item2.label = "Length:";
	item2.d = this->len;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "Line width:";
	item2.d = Width;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);

	return this;
}

glm::vec3 LineSegment::getPoint(float dL)
{
	return point1 + (point2 - point1)* (float)(dL / len);
}

PolyLineSegment::PolyLineSegment(int t_color, double t_width, const glm::mat4& M, int t_polygonType) : LineSegment(t_color, t_width, M),m_polygonType(t_polygonType)
{
//	ObjColor = glm::vec3(((t_color & 0xff0000) / 65536) / 255.0f, ((t_color & 0xff00) / 256) / 255.0f, (t_color & 0xff) / 255.0f);
	surface.resize(1);
}

void PolyLineSegment::Init(Track* parent)
{
	clear();
	if (points.size() < 2)
		return;
	createVertexVector(points);
	if (m_polygonType < 2) {
		Indices.push_back(Verts.size() - 1);
		Indices.push_back(Verts.size() - 2);
		Indices.push_back(1);
		Indices.push_back(Verts.size() - 2);
		Indices.push_back(1);
		Indices.push_back(0);
	}
}

void PolyLineSegment::addVertex3(glm::vec3 point, glm::vec3 offset, double t_hight, bool t_end)
{
	glm::vec4 p(point, 1);
	glm::vec4 n(offset, 0);

	int endpos = Verts.size();
	Verts.push_back(vertex((m_transformMatrix * (p + n + glm::vec4(0, 0, t_hight,0))).xyz, glm::vec3(), ObjColor, point.xy));
	Verts.push_back(vertex((m_transformMatrix * (p - n + glm::vec4(0, 0, t_hight,0))).xyz, glm::vec3(), ObjColor, point.xy));
	Verts.push_back(vertex((m_transformMatrix * (p + n)).xyz, glm::vec3(), ObjColor, point.xy));
	Verts.push_back(vertex((m_transformMatrix * (p - n)).xyz, glm::vec3(), ObjColor, point.xy));
	if (t_end) {
		Indices.push_back(endpos + 1);
		Indices.push_back(endpos);
		Indices.push_back(endpos + 3);
		Indices.push_back(endpos + 3);
		Indices.push_back(endpos);
		Indices.push_back(endpos + 2);
	}
	if (endpos>3) {
		Indices.push_back(endpos + 1);
		Indices.push_back(endpos);
		Indices.push_back(endpos - 3);
		Indices.push_back(endpos - 3);
		Indices.push_back(endpos);
		Indices.push_back(endpos - 4);

		Indices.push_back(endpos + 3);
		Indices.push_back(endpos + 2);
		Indices.push_back(endpos - 1);
		Indices.push_back(endpos - 1);
		Indices.push_back(endpos + 2);
		Indices.push_back(endpos - 2);

		Indices.push_back(endpos - 4);
		Indices.push_back(endpos);
		Indices.push_back(endpos - 2);
		Indices.push_back(endpos - 2);
		Indices.push_back(endpos);
		Indices.push_back(endpos + 2);

		Indices.push_back(endpos + 3);
		Indices.push_back(endpos + 1);
		Indices.push_back(endpos - 1);
		Indices.push_back(endpos - 1);
		Indices.push_back(endpos + 1);
		Indices.push_back(endpos - 3);//*/
	}
}

void PolyLineSegment::createVertexVector3(std::vector<glm::vec3>& Drawpoints, double t_hight)
{
	clear();
	mode = GL_TRIANGLES;
	float t = (float)std::fmax(Width / 2, minLineWidth);
	float sumZ(0);
	for (unsigned int i = 0; i < Drawpoints.size(); ++i) {
		sumZ += Drawpoints[i].z;
		if ( i == 0) {
			glm::vec3 p(Drawpoints[i].xy, 0);
			glm::vec3 pnext(Drawpoints[i + 1].xy, 0);
			glm::vec3 v = glm::normalize(pnext - p);
			glm::vec3 n = glm::cross(v, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
			glm::vec3 N = n * t;
			addVertex3(Drawpoints[i] , N, t_hight,true);
			continue;
		}
		if ( i == (Drawpoints.size() - 1)) {
			glm::vec3 p(Drawpoints[i].xy, 0);
			glm::vec3 pprev(Drawpoints[i - 1].xy, 0);
			glm::vec3 v = glm::normalize(p - pprev);
			glm::vec3 n = glm::cross(v, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
			glm::vec3 N = n * t;
			addVertex3(Drawpoints[i] , N, t_hight,true);
			continue;
		}
		glm::vec3 pprev(Drawpoints[ i - 1].xy, 0);
		glm::vec3 p(Drawpoints[i].xy, 0);
		glm::vec3 pnext(Drawpoints[i + 1].xy, 0);
		glm::vec3 v1 = glm::normalize(p - pprev);
		glm::vec3 v2 = glm::normalize(pnext - p);
		glm::vec3 n1 = glm::cross(v1, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
		glm::vec3 n2 = glm::cross(v2, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
		glm::vec3 miter = glm::normalize(n1 + n2);
		float a = glm::dot(miter, n1);

		if (fabs(a) < 0.001) a = 1;
		float length = t / a;
//		if (a > 0.4)
			addVertex3(Drawpoints[i] , length * miter, t_hight,false);
//		else {
//			addVertex3(Drawpoints[i] , length * miter, n1 * t, n2 * t);
//		}
	}
}

void PolyLineSegment::createVertexVector(std::vector<glm::vec3>& Drawpoints)
{
	clear();
	mode = GL_TRIANGLES;
	float t = (float)std::fmax(Width / 2, minLineWidth);
	float sumZ(0);
	for (unsigned int i = 0; i < Drawpoints.size(); ++i) {
		sumZ += Drawpoints[i].z;
		if (m_polygonType == 2 && i == 0) {
			glm::vec3 p(Drawpoints[i].xy, 0);
			glm::vec3 pnext(Drawpoints[i + 1].xy, 0);
			glm::vec3 v = glm::normalize(pnext - p);
			glm::vec3 n = glm::cross(v, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
			glm::vec3 N = n * t;
			addVertex(Drawpoints[i] + glm::vec3(0, 0, 0.05f), N);
			continue;
		}
		if (m_polygonType == 2 && i == (Drawpoints.size() - 1)) {
			glm::vec3 p(Drawpoints[i].xy, 0);
			glm::vec3 pprev(Drawpoints[i - 1].xy, 0);
			glm::vec3 v = glm::normalize(p - pprev);
			glm::vec3 n = glm::cross(v, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
			glm::vec3 N = n * t;
			addVertex(Drawpoints[i] + glm::vec3(0, 0, 0.05f), N);
			continue;
		}

		glm::vec3 pprev(Drawpoints[(i == 0 ? Drawpoints.size() - 1 : i - 1)].xy, 0);
		glm::vec3 p(Drawpoints[i].xy, 0);
		glm::vec3 pnext(Drawpoints[(i == (Drawpoints.size() - 1) ? 0 : i + 1)].xy, 0);
		glm::vec3 v1 = glm::normalize(p - pprev);
		glm::vec3 v2 = glm::normalize(pnext - p);
		glm::vec3 n1 = glm::cross(v1, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
		glm::vec3 n2 = glm::cross(v2, glm::vec3(0, 0, 1));//    (-v1.y, v1.x, 0);
		glm::vec3 miter = glm::normalize(n1 + n2);
		float a = glm::dot(miter, n1);

		if (fabs(a) < 0.001) a = 1;
		float length = t / a;
		if (a > 0.4)
			addVertex(Drawpoints[i] + glm::vec3(0, 0, 0.05f), length * miter);
		else {
			addVertex(Drawpoints[i] + glm::vec3(0, 0, 0.05f), length * miter, n1 * t, n2 * t);
		}
	}
	CG.z = sumZ / Drawpoints.size();
}

bool PolyLineSegment::OnSelect(bool cm, std::string layer)
{
	std::vector< int> pz;
	bool status(false);
	Select Menu(index, "Polygon", cm);
	Menu.insert("points", (int)points.size());

	for (auto &ii : points) {
		pz.push_back(Menu.insert("", ii));
	}

	Menu.insert("Line width:", Width);
	Menu.insert("Layer:", layer);
	status = Menu.showModal();

	if (status) {
		for (unsigned int i = 0; i < pz.size(); ++i) {
			points[i].z = (float)(Menu.getValue(pz[i]) / (cm ? 2.54 : 1));
		}
		//Init(nullptr);
	}
	return status;
}

XTrkObj* PolyLineSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Polygon";
	item.type = VariantType::isHeader;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "points:";
	item2.i = points.size();
	item2.type = VariantType::isInt;
	properties.push_back(item2);

	for (auto &ii : points)
	{
		SELECT_TYPE item;
		item.label = "";
		item.point = &ii;
		item.type = VariantType::isVec3;
		item.z_Editable = true;
		properties.push_back(item);
	}

	item2.label = "Line width:";
	item2.d = Width;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);
	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);
	return this;
}

ArcSegment::ArcSegment(int t_color, double t_width, double t_radius, glm::vec3 t_center, double t_angle, double t_swing, const glm::mat4& M)
	:LineSegment(t_color, t_width, M)
	, m_radius(fabs(t_radius))
	, m_center(t_center)
	, m_angle(t_angle)
	, m_swing(t_swing)
	, m_reverse(t_radius < 0)
{
	if (m_swing < 0)
		m_swing += 360;

	double angleRad = m_angle * M_PI / 180;
	double DiffRad = m_swing * M_PI / 180;
//	point1 = (M*glm::vec4(center.x + sin(angleRad)*radius, center.y + cos(angleRad)*radius, 0,0)).xyz;
//	point2 = (M*glm::vec4(center.x + sin(angleRad + DiffRad)*radius, center.y + cos(angleRad + DiffRad)*radius, 0,0)).xyz;
	point1 = glm::vec3(m_center.x + sin(angleRad)*m_radius, m_center.y + cos(angleRad)*m_radius, 0);
	point2 = glm::vec3(m_center.x + sin(angleRad + DiffRad)*m_radius, m_center.y + cos(angleRad + DiffRad)*m_radius, 0);
	len = (float)fmax(fabs(DiffRad) * m_radius, glm::length(point2 - point1));
	norm1 = glm::normalize(point1 - m_center);
	norm2 = glm::normalize(point2 - m_center);
}
void ArcSegment::Init(Track* parent)
{
	clear();
	int count = std::max((int)(m_swing / resolution), 1);//2019-02-10 prevent div0
	double DiffRad = m_swing * M_PI / 180;
	double DC = DiffRad / count;// *M_PI / 180;
	double angleRad = m_angle * M_PI / 180;
	float z1(point1.z);
	float z2(point2.z);
	float dz = (z2 - z1) / count;
	CG = glm::vec3(m_center.x + sin(angleRad + DiffRad / 2.0f)*m_radius, m_center.y + cos(angleRad + DiffRad / 2.0f)*m_radius, (z1 + z2) / 2.0f);

	float t = (float)std::fmax(Width / 2, minLineWidth);

//	if (Width > 0.01)
		mode = GL_TRIANGLES;
//	else
//		mode = GL_LINE_STRIP;

	glm::vec3 p;
	for (int i = 0; i <= count; i++) {
		glm::vec3 Z(0, 0, z1 + i * dz);
		glm::vec3 N(sin(angleRad), cos(angleRad), 0);
		angleRad += DC;
		if (i == 0) {
			p = point1;
			N = norm1;
		}
		else
			if (i == count) {
				p = point2;
				N = norm2;
			}
			else
				p = Z + m_center + N * (float)m_radius;
//		if (mode == GL_TRIANGLES)
			addVertex(p, N*t);
/*		else { // GL_LINE_STRIP
			Indices.push_back(Verts.size());
			Verts.push_back(vertex((m_transformMatrix*(glm::vec4(p + glm::vec3(0, 0, 0.05f), 1))).xyz, glm::vec3(), ObjColor, p.xy));
		}//*/
	}
}

glm::vec3 ArcSegment::getPoint(float dL)
{
	double angleRad = (float)(m_angle * M_PI / 180) + dL / m_radius;
	double dz = (point1.z + point2.z) / 2;
	return glm::vec3(m_center.x + sin(angleRad)*m_radius, m_center.y + cos(angleRad)*m_radius, dz);
}

bool ArcSegment::OnSelect(bool cm, std::string layer)
{
	std::vector<int> pz;
	Select Menu(this->index, "Curved Line", cm);
	Menu.insert("Center:", m_center,false);
	Menu.insert("Radius:", m_radius);
	pz.push_back(Menu.insert("Pt 1:", point1));
	pz.push_back(Menu.insert("Pt 2:", point2));
	Menu.insert("Line width:", Width);
	Menu.insert("Layer:", layer);
	bool status= Menu.showModal();
	if(status)
	{
		point1.z = (float)(Menu.getValue(pz[0]) / (cm ? 2.54 : 1));
		point2.z = (float)(Menu.getValue(pz[1]) / (cm ? 2.54 : 1));
		//Init(nullptr);
	}
	return status;
}

XTrkObj* ArcSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	if (m_swing >= 360.0) {
		item.header.title = "Circle";
	}
	else {
		item.header.title = "Curved Line";
	}
	item.type = VariantType::isHeader;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "Center:";
	item2.point = &m_center;
	item2.type = VariantType::isVec3;
	properties.push_back(item2);

	item2.label = "Radius:";
	item2.d = m_radius;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "pt 1";
	item2.point = &point1;
	item2.type = VariantType::isVec3;
	item2.z_Editable = true;
	properties.push_back(item2);

	item2.label = "pt 2";
	item2.point = &point2;
	properties.push_back(item2);
	item2.z_Editable = false;

	item2.label = "Line width:";
	item2.d = Width;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);

	return this;
}

void ArcSegment::dump(json& j)
{ 
	LineSegment::dump(j, "ArcSegment");
	j["ArcSegment"]["Radius"]= std::to_string(m_radius);
	j["ArcSegment"]["Center"] = { std::to_string(m_center.x),std::to_string(m_center.y),std::to_string(m_center.z) };
	j["ArcSegment"]["Angle"]= std::to_string(m_angle);
	j["ArcSegment"]["Swing"]= std::to_string(m_swing);
	j["ArcSegment"]["Reverse"]=m_reverse;
}


void FilledSegment::add(const glm::vec3& point)
{
	glm::vec4 p1 = m_transformMatrix * glm::vec4(point, 1);
	surface[0] << IntPoint(p1.xyz);
	if(surface[0].size()==1){
		rect = Rect(p1.xyz);
	}
	else {
		setMidPoint(p1.xyz);
	}
	CG = (rect.LL + rect.UR) / 2.0f;
}

SceneNode* FilledSegment::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
{
	EarcutWrapper T(surface,ObjColor);
	SceneNode* Node	= new SceneNode();
	Node->boundingSphere = BoundSphere(T.vertices[0]);
	Node->layer = this->layer;
	float sumZ(0);
	float i(0);
	for (auto &ii : surface) {
		for (auto &jj : ii) {
			sumZ += jj.Z.Pos.z;
			++i;
		}
		
	}
	CG.z = sumZ / i;
	for (unsigned int i = 0; i < T.vertices.size(); ++i) {
		Node->addShape(Shape(ShapeType::OtherType, T.vertices[i], T.indices[i], Shaders, Appearance::solidColor, Appearance::solidColor, GL_TRIANGLES, GL_NONE));
	}
	if (baseZset) {
		add3DShapesToNode(Node, Shaders, ObjColor, surface, (float)baseZValue);
	}
	return Node;
}

bool FilledSegment::OnSelect(bool cm, std::string layer)
{
	std::vector<int> pz;
	bool status(false);
	Select Menu(this->index, "Filled Polygon", cm,nullptr);
	Menu.insert("points:", (int)surface[0].size());
	for (auto &ii : surface[0])	{
		pz.push_back(Menu.insert("", ii.Z.Pos));
	}
	Menu.insert("Layer:", layer);
	status = Menu.showModal();
	if (status) {
		for (unsigned int i = 0; i < pz.size(); ++i) {
			surface[0][i].Z.Pos.z = (float)(Menu.getValue(pz[i]) / (cm ? 2.54 : 1));
		}
	}
	return status;
}

XTrkObj* FilledSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Filled Polygon";
	item.type = VariantType::isHeader;
	item.hasBase = true;
	item.header.base_z_set = &this->baseZset;
	item.header.z = &this->baseZValue;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "points:";
	item2.i = surface[0].size();
	item2.type = VariantType::isInt;
	properties.push_back(item2);

	for (auto &ii : surface[0])
	{
		SELECT_TYPE item;
		item.label = "";
		item.point = &ii.Z.Pos;
		item.type = VariantType::isVec3;
		item.z_Editable = true;
		properties.push_back(item);
	}

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);
	return this;
}

void FilledSegment::dump(json& j)
{
	//LineSegment::dump(j, "FilledPolygon");

	j["FilledPolygon"]["Color"]={ std::to_string(ObjColor.x), std::to_string(ObjColor.y), std::to_string(ObjColor.z) };
	for (auto &ii : surface[0]) {
		j["FilledPolygon"]["Coord"].push_back({ std::to_string(ii.Z.Pos.x), std::to_string(ii.Z.Pos.y), std::to_string(ii.Z.Pos.z) });
	}
}

SceneNode* CircleSegment::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
{
	SceneNode* Node = new SceneNode();
	Node->boundingSphere = BoundSphere();
	Node->boundingSphere.center = this->center;
	Node->boundingSphere.radius = (GLfloat) this->radius;
	Node->layer = this->layer;
	Node->addShape(Disc(ShapeType::OtherType, center, radius, 48, ObjColor, Shaders, Appearance::solidColor, GL_NONE));
	CG.z = center.z;
	return Node;
}

bool CircleSegment::OnSelect(bool cm, std::string layer)
{
	int  pz;
	bool status(false);
	Select Menu(this->index, "Circle", cm);
	pz=Menu.insert("Center:", center, true);
	Menu.insert("Radius:", radius);
	Menu.insert("Layer:", layer);
	status= Menu.showModal();
	if (status) {
		center.z = (float)(Menu.getValue(pz) / (cm ? 2.54 : 1));
	}
	return status;
}

XTrkObj* CircleSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Circle";
	item.type = VariantType::isHeader;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "Center:";
	item2.point = &center;
	item2.type = VariantType::isVec3;
	item2.z_Editable = true;
	properties.push_back(item2);
	item2.z_Editable = false;

	item2.label = "Radius:";
	item2.d = radius;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);

	return this;
}

void CircleSegment::dump(json& j)
{
	LineSegment::dump(j, "Circle");
	j["Circle"]["Radius"]= std::to_string(radius);
	j["Circle"]["Center"]={ std::to_string(center.x),std::to_string(center.y),std::to_string(center.z) };
}

void TableEdgeSegment::Init(Track* parent)
{
	clear();
	mode = GL_TRIANGLES;
	float t = 2;
	glm::vec3 p1 = points[0];
	glm::vec3 p2 = points[1];
	glm::vec3 N(glm::vec3(0, 0,t));
	p1.z -= t;
	p2.z -= t;
	addVertex(p1, N);
	addVertex(p2, N);
	glm::vec3 v(glm::vec3(p2 - p1).xy, 0);
	len = glm::length(v);
}

bool TableEdgeSegment::OnSelect(bool cm, std::string layer)
{
	std::vector<int> pz;
	bool status(false);
	Select Menu(index, "Table Edge", cm);
	pz.push_back(Menu.insert("pt 1:", points[0]));
	pz.push_back(Menu.insert("pt 2:", points[1]));
	Menu.insert("Length:", len);
	Menu.insert("Layer:", layer);
	status = Menu.showModal();
	if (status) {
		for (unsigned int i = 0; i < pz.size(); ++i) {
			points[i].z = (float)(Menu.getValue(pz[i]) / (cm ? 2.54 : 1));
		}
		//Init(nullptr);
	}
	return status;
}

XTrkObj* TableEdgeSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Table Edge";
	item.type = VariantType::isHeader;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "pt 1";
	item2.point = &points[0];
	item2.type = VariantType::isVec3;
	item2.z_Editable = true;
	properties.push_back(item2);

	item2.label = "pt 2";
	item2.point = &points[1];
	properties.push_back(item2);
	item2.z_Editable = false;

	item2.label = "Length:";
	item2.d = this->len;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);

	return this;
}


void BenchworkSegment::Init(Track* parent)
{
	clear();
	mode = GL_TRIANGLES;
	setCrossSection();
	addVertex(points[0]+glm::vec3(0,0,CG.z), norm1);
	addVertex(points[1]+ glm::vec3(0, 0, CG.z), norm2);
	int n(CrossSection.size());
	for (int index = 0; index < n; ++index)
	{
		if (index < n - 1) {
			Indices.push_back(index);
			Indices.push_back(index + 1);
			Indices.push_back(index + n);
			Indices.push_back(index + 1);
			Indices.push_back(index + n + 1);
			Indices.push_back(index + n);
		}
		else {
			Indices.push_back(index);
			Indices.push_back(0);
			Indices.push_back(index + n);
			Indices.push_back(0);
			Indices.push_back(n);
			Indices.push_back(index + n);
		}
	}
	glm::vec3 v(glm::vec3(points[1] - points[0]).xy, 0);
	len = glm::length(v);
}

bool BenchworkSegment::OnSelect(bool cm, std::string layer)
{
	std::vector<int> pz;
	bool status(false);
	Select Menu(index, "Benchwork", cm);
	pz.push_back(Menu.insert("pt 1:", points[0]));
	pz.push_back(Menu.insert("pt 2:", points[1]));
	Menu.insert("Length:", len);
	Menu.insert("Layer:", layer);
	status = Menu.showModal();
	if (status) {
		for (unsigned int i = 0; i < pz.size(); ++i) {
			points[i].z = (float)(Menu.getValue(pz[i]) / (cm ? 2.54 : 1));
		}
		//Init(nullptr);
	}
	return status;
}

XTrkObj* BenchworkSegment::OnSelect(std::vector<SELECT_TYPE>& properties)
{
	SELECT_TYPE item;
	item.header.id = this->index;
	item.header.layer = this->layer;
	item.header.title = "Benchwork";
	item.type = VariantType::isHeader;
	properties.push_back(item);

	SELECT_TYPE item2;
	item2.label = "pt 1";
	item2.point = &points[0];
	item2.type = VariantType::isVec3;
	item2.z_Editable = true;
	properties.push_back(item2);

	item2.label = "pt 2";
	item2.point = &points[1];
	properties.push_back(item2);
	item2.z_Editable = false;

	item2.label = "Length:";
	item2.d = this->len;
	item2.type = VariantType::isDouble;
	properties.push_back(item2);

	item2.label = "Layer:";
	item2.i = this->layer;
	item2.type = VariantType::isLayer;
	properties.push_back(item2);

	return this;
}



SceneNode* BenchworkSegment::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
{

	SceneNode* Node = new SceneNode();
	Node->boundingSphere = BoundSphere(Verts);
	Shape Surface(ShapeType::OtherType, Verts, Indices, Shaders, Appearance::PlankSurface, Appearance::PlankSurface, mode, GL_BACK);
	Node->addShape(Surface);
	Shape Ends(ShapeType::OtherType, EndVerts, EndIndices, Shaders, Appearance::PlankSurface, Appearance::PlankSurface, mode, GL_BACK);
	Node->addShape(Ends);
	Node->layer = this->layer;
	Node->isTrack = false;
	return Node;
}


void BenchworkSegment::addVertex(glm::vec3 point, glm::vec3 offset)
{
	bool first(Verts.empty());
	glm::vec4 p(point, 1);
	glm::vec4 n(offset, 0);
	glm::vec4 z(0, 0, 1, 0);
	n = glm::normalize(n);

	for (auto &ii : CrossSection)
	{
		glm::vec4 node(p + n * ii.hw.y + z * ii.hw.x);
		glm::vec2 T((first ? 0 : 0.9), ii.t);
		Verts.push_back(vertex((m_transformMatrix*(node)).xyz, glm::vec3(), ObjColor, T));
		glm::vec2 UV(1 + ii.uv.x / 10.0f, 0.5 + ii.uv.y / 2.0f);
		EndVerts.push_back(vertex((m_transformMatrix*(node)).xyz, glm::vec3(), ObjColor, UV));
	}
}
void BenchworkSegment::setEndIndices(GLuint Indexes[6])
{
	for (unsigned int i = 0; i < 6; ++i)
		EndIndices.push_back(Indexes[i]);
}

void BenchworkSegment::setCrossSection()
{
	CrossSection.clear();
	EndIndices.clear();
	EndVerts.clear();
	float H((float)((shape & 0xff00) >> 9));
	float W((float)((shape & 0xff0000) >> 17));
	float const t(1);
	switch (shape & 0x30000ff) {
	case 0x0: {
		CrossSection.push_back(struct CS(glm::vec2(-H, -W / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, -W / 2), 1, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, W / 2), 2, glm::vec2(0, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-H, W / 2), 1, glm::vec2(-1, 0.5)));
		GLuint I1[6]= { 0, 2, 1, 0, 3, 2 };
		setEndIndices(I1);
		GLuint I2[6] = { 4, 5, 6, 4, 6, 7 };
		setEndIndices(I2);
		break;
	}
	case 0x1: {
		CrossSection.push_back(struct CS(glm::vec2(-W, -H / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2( 0, -H / 2), 1, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2( 0,  H / 2), 2, glm::vec2(0,  0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-W,  H / 2), 1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 2, 1, 0, 3, 2 };
		setEndIndices(I1);
		GLuint I2[6] = { 4, 5, 6, 4, 6, 7 };
		setEndIndices(I2);
		break;
	}

	case 0x1000000: {
		CrossSection.push_back(struct CS(glm::vec2(-H, -W / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, -W / 2), 1, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, W / 2), 2, glm::vec2(0, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, W / 2), 3, glm::vec2(-t/H, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, t-W / 2), 2, glm::vec2(-t/H, t/W-0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-H, t-W / 2), 1, glm::vec2(-1, t/W-0.5)));


		GLuint I1[6] = { 0, 4, 1, 0, 5, 4 };
		setEndIndices(I1);
		GLuint I2[6] = { 1, 4, 2, 2, 4, 3 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 10, 6, 10, 11 };
		setEndIndices(I3);
		GLuint I4[6] = { 7, 8, 10, 8, 9,10 };
		setEndIndices(I4);
		break;
	}
	case 0x1000001: {
		CrossSection.push_back(struct CS(glm::vec2(-H, W / 2-t), 0, glm::vec2(-1, 0.5-t/W)));
		CrossSection.push_back(struct CS(glm::vec2(-t, W / 2-t), 1, glm::vec2(-t/H, 0.5-t/W)));
		CrossSection.push_back(struct CS(glm::vec2(-t, -W / 2),  2, glm::vec2(-t/H, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, -W / 2),   3, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, W / 2),    2, glm::vec2(0,  0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-H, W / 2),   1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 5, 1, 1, 5, 4 };
		setEndIndices(I1);
		GLuint I2[6] = { 1, 3, 2, 1, 4, 3 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 11, 7, 10, 11 };
		setEndIndices(I3);
		GLuint I4[6] = { 7, 8, 9, 7, 9,10 };
		setEndIndices(I4);
		break;
	}
	case 0x1000002: {
		CrossSection.push_back(struct CS(glm::vec2(-W, H / 2 - t), 0, glm::vec2(-1, 0.5 - t / H)));
		CrossSection.push_back(struct CS(glm::vec2(-t, H / 2 - t), 1, glm::vec2(-t / H, 0.5 - t / H)));
		CrossSection.push_back(struct CS(glm::vec2(-t, -H / 2), 2, glm::vec2(-t / H, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, -H / 2), 3, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, H / 2), 2, glm::vec2(0, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-W, H / 2), 1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 5, 1, 1, 5, 4 };
		setEndIndices(I1);
		GLuint I2[6] = { 1, 3, 2, 1, 4, 3 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 11, 7, 10, 11 };
		setEndIndices(I3);
		GLuint I4[6] = { 7, 8, 9, 7, 9,10 };
		setEndIndices(I4);
		break;
	}
	case 0x1000003: {
		CrossSection.push_back(struct CS(glm::vec2(-W, -H / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, -H / 2), 1, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, H / 2), 2, glm::vec2(0, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, H / 2), 3, glm::vec2(-t / W, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, t - H / 2), 2, glm::vec2(-t / W, t / H - 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-W, t - H / 2), 1, glm::vec2(-1, t / H - 0.5)));

		GLuint I1[6] = { 0, 4, 1, 0, 5, 4 };
		setEndIndices(I1);
		GLuint I2[6] = { 1, 4, 2, 2, 4, 3 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 10, 6, 10, 11 };
		setEndIndices(I3);
		GLuint I4[6] = { 7, 8, 10, 8, 9,10 };
		setEndIndices(I4);
		break;
	}
	case 0x1000004: {
		CrossSection.push_back(struct CS(glm::vec2(-W, -H / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, -H / 2), 1, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, t- H / 2), 2, glm::vec2(0, t/H- 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t-W, t- H / 2), 3, glm::vec2(t / W-1, t/H-0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t-W, H / 2), 2, glm::vec2(t / W-1, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-W, H / 2), 1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 3, 1, 1, 3, 2 };
		setEndIndices(I1);
		GLuint I2[6] = { 0, 5, 3, 3, 5, 4 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 9, 7, 8, 9 };
		setEndIndices(I3);
		GLuint I4[6] = { 6, 9, 11, 9, 10, 11 };
		setEndIndices(I4);
		break;
	}
	case 0x1000005: {
		CrossSection.push_back(struct CS(glm::vec2(-W, -H / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t - W, -H / 2), 1, glm::vec2(t / W - 1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t - W, H / 2-t), 2, glm::vec2(t / W - 1, 0.5- t / H)));
		CrossSection.push_back(struct CS(glm::vec2(0, H / 2-t), 3, glm::vec2(0, 0.5 - t / H)));
		CrossSection.push_back(struct CS(glm::vec2(0, H / 2), 2, glm::vec2(0, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-W, H / 2), 1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 2, 1, 0, 5, 2 };
		setEndIndices(I1);
		GLuint I2[6] = { 2, 4, 3, 2, 5, 4 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 8, 6, 8, 11 };
		setEndIndices(I3);
		GLuint I4[6] = { 8, 9, 10, 8, 10, 11 };
		setEndIndices(I4);
		break;
	}
	case 0x1000006: {
		CrossSection.push_back(struct CS(glm::vec2(-H, -W / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t - H, -W / 2), 1, glm::vec2(t / H - 1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t - H, W / 2 - t), 2, glm::vec2(t / H - 1, 0.5 - t / W)));
		CrossSection.push_back(struct CS(glm::vec2(0, W / 2 - t), 3, glm::vec2(0, 0.5 - t / W)));
		CrossSection.push_back(struct CS(glm::vec2(0, W / 2), 2, glm::vec2(0, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-H, W / 2), 1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 2, 1, 0, 5, 2 };
		setEndIndices(I1);
		GLuint I2[6] = { 2, 4, 3, 2, 5, 4 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 8, 6, 8, 11 };
		setEndIndices(I3);
		GLuint I4[6] = { 8, 9, 10, 8, 10, 11 };
		setEndIndices(I4);
		break;
	}
	case 0x1000007: {
		CrossSection.push_back(struct CS(glm::vec2(-H, -W / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, - W / 2), 1, glm::vec2( 0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, t- W / 2), 2, glm::vec2(0, t/W-0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t - H, t -W / 2), 3, glm::vec2(t / H - 1, t/W-0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t - H, W / 2), 2, glm::vec2(t / H - 1, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-H, W / 2), 1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 3, 1, 1, 3, 2 };
		setEndIndices(I1);
		GLuint I2[6] = { 0, 5, 3, 3, 5, 4 };
		setEndIndices(I2);
		GLuint I3[6] = { 6, 7, 9, 7, 8, 9 };
		setEndIndices(I3);
		GLuint I4[6] = { 6, 9, 11, 9, 10, 11 };
		setEndIndices(I4);
		break;
	}

	case 0x2000000: {
		CrossSection.push_back(struct CS(glm::vec2(-H, -t / 2), 0, glm::vec2(-1, -t/W*0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, -t / 2), 1, glm::vec2(-t/H, -t/W*0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, - W / 2), 2,glm::vec2(-t/H, - 0.5)));
		CrossSection.push_back(struct CS(glm::vec2( 0, - W / 2), 3,glm::vec2(0,  - 0.5)));
		CrossSection.push_back(struct CS(glm::vec2( 0, W / 2), 4,  glm::vec2(0, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, W / 2), 3,  glm::vec2(-t/H, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t, t / 2), 2,  glm::vec2(-t / H, t/W*0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-H, t / 2), 1,  glm::vec2(-1, t / W * 0.5)));

		GLuint I1[6] = { 0, 7, 1, 1, 7, 6 };
		setEndIndices(I1);
		GLuint I2[6] = { 2, 4, 3, 2, 5, 4 };
		setEndIndices(I2);
		GLuint I3[6] = { 8, 9, 15, 9, 14, 15 };
		setEndIndices(I3);
		GLuint I4[6] = { 10, 11, 12, 10, 12, 13 };
		setEndIndices(I4);
		break;
	}
	case 0x2000001: {
		CrossSection.push_back(struct CS(glm::vec2(-W, -H / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2( 0, -H / 2), 1, glm::vec2(0, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2( 0, t -H / 2), 2, glm::vec2(0, t/H-0.5)));
		CrossSection.push_back(struct CS(glm::vec2( t / 2 - W / 2, t-H / 2), 3, glm::vec2(t/W*0.5-0.5, t/H-0.5)));
		CrossSection.push_back(struct CS(glm::vec2( t / 2 - W / 2, H / 2), 4, glm::vec2(t / W * 0.5 - 0.5, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t / 2 - W / 2, H / 2), 3, glm::vec2(-t / W * 0.5 - 0.5, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-t / 2 - W / 2, t -H/ 2), 2, glm::vec2(-t / W * 0.5 - 0.5, t / H * 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-W, t -H/ 2), 1, glm::vec2(-1, -t / H - 0.5)));

		GLuint I1[6] = { 0, 2, 1, 0, 7, 2 };
		setEndIndices(I1);
		GLuint I2[6] = { 3, 5, 4, 3, 6, 5 };
		setEndIndices(I2);
		GLuint I3[6] = { 8, 9, 10, 8, 10, 15 };
		setEndIndices(I3);
		GLuint I4[6] = { 11, 12, 13, 11, 13, 14 };
		setEndIndices(I4);
		break;
	}
	case 0x2000002: {
		CrossSection.push_back(struct CS(glm::vec2(-W, H / 2), 0, glm::vec2(-1, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-W, H / 2-t), 1, glm::vec2(-1, 0.5-t/H)));
		CrossSection.push_back(struct CS(glm::vec2(-W/2-t/2, H / 2-t), 2, glm::vec2(-0.5-t/W*0.5, 0.5-t / H)));
		CrossSection.push_back(struct CS(glm::vec2(-W / 2 - t / 2, -H / 2), 3, glm::vec2(-0.5 - t / W * 0.5, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2( t / 2 - W / 2, -H / 2),  4, glm::vec2(t / W * 0.5 - 0.5, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2( t / 2 - W / 2, H / 2-t), 3, glm::vec2(t / W * 0.5 - 0.5, 0.5-t/H)));
		CrossSection.push_back(struct CS(glm::vec2(0, H / 2-t), 2, glm::vec2(0, 0.5-t / H)));
		CrossSection.push_back(struct CS(glm::vec2(0, H / 2), 1, glm::vec2(0, 0.5)));

		GLuint I1[6] = { 0, 6, 1, 0, 7, 6 };
		setEndIndices(I1);
		GLuint I2[6] = { 2, 4, 3, 2, 5, 4 };
		setEndIndices(I2);
		GLuint I3[6] = { 8, 9, 14, 8, 14, 15 };
		setEndIndices(I3);
		GLuint I4[6] = { 10, 11, 12, 10, 12, 13 };
		setEndIndices(I4);
		break;
	}
	case 0x2000003: {
		CrossSection.push_back(struct CS(glm::vec2(-H, -W / 2), 0, glm::vec2(-1, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t-H, -W / 2), 1, glm::vec2(t / H-0.5, -0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t-H, -t / 2), 2, glm::vec2(t / H-0.5,-t/W*0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, -t / 2), 3, glm::vec2(0, -t/W*0.5)));
		CrossSection.push_back(struct CS(glm::vec2(0, t / 2), 4, glm::vec2(0, t/W*0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t-H, t / 2), 3, glm::vec2(t / H - 0.5, t / W * 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(t-H, W / 2), 2, glm::vec2(t / H - 0.5, 0.5)));
		CrossSection.push_back(struct CS(glm::vec2(-H, W / 2), 1, glm::vec2(-1, 0.5)));

		GLuint I1[6] = { 0, 6, 1, 0, 7, 6 };
		setEndIndices(I1);
		GLuint I2[6] = { 2, 4, 3, 2, 5, 4 };
		setEndIndices(I2);
		GLuint I3[6] = { 8, 9, 14, 8, 14, 15 };
		setEndIndices(I3);
		GLuint I4[6] = { 10, 11, 12, 10, 12, 13 };
		setEndIndices(I4);
		break;
	}
	}
}

TextSegment::TextSegment(int color, glm::vec3 point, double angle, double height, std::string& str, const glm::mat4& M) :LineSegment(color, 0, M), m_str(str), m_height(height)
{
	point1 = point;
	m_angle = angle;
	m_transformMatrix = glm::rotate(glm::translate(M, point), float(angle / 180.0*M_PI), glm::vec3(0, 0, 1));
}

void TextSegment::Init(Track* parent)
{
	CG = (m_transformMatrix * glm::vec4(point1, 1)).xyz;
}

SceneNode* TextSegment::createSceneNode(OpenGLShaderPrograms* Shaders, TrackPlan *myLayout, int count)
{
	SceneNode* Node = new SceneNode();
	Node->layer = this->layer;
//	glm::vec3 TextColor(glm::vec3((Color & 0xff0000) / 65536 / 255.0f, (Color & 0xff00) / 256 / 255.0f, (Color & 0xff) / 255.0f));
	TextNode T(m_str, ObjColor);
	T.setTransform(m_transformMatrix);
	Node->addText(T);
	return Node;
}
void TextSegment::dump(json& j)
{ 
	LineSegment::dump(j, "Text");
	j["Text"]["Coord"] = { std::to_string(point1.x), std::to_string(point1.y), std::to_string(point1.z) };
	j["Text"]["Angle"] = std::to_string(m_angle);
	j["Text"]["Height"]= std::to_string(m_height);
	j["Text"]["String"]= m_str;

//	int color, glm::vec3 point, double angle, double height, std::string& str
}


}