#define GLM_FORCE_RADIANS
#include "OpenGLShader.h"
#include <glm/glm/gtc/type_ptr.hpp>

#include "ascii.h"
#include "Concrete.h"
#include "DarkConcrete.h"
#include "gravel.h"
#include "rail_img.h"
#include "bufferstop_img.h"
#include "rr_sleeper2.h"
#include "Benchwork.h"

namespace Xtrk3D {

	bool ShaderStatus(GLuint &Shader)
	{
		GLint status;
		glGetShaderiv(Shader, GL_COMPILE_STATUS, &status);
		//throw exception if compile error occurred
		if (status == GL_FALSE) {
			char buffer[512];
			glGetShaderInfoLog(Shader, 512, NULL, buffer);
			std::string msg("Compile failure in shader:\n");
			msg += buffer;
			//		TRACE("opengl Compile failure in shader:\n%s\n", buffer);
			glDeleteShader(Shader); Shader = 0;
			throw std::runtime_error(msg);
		}
		return status;
	}
	bool ProgramStatus(GLuint &Prog)
	{
		GLint status;
		glGetProgramiv(Prog, GL_LINK_STATUS, &status);
		if (status == GL_FALSE) {
			char buffer[512];
			glGetProgramInfoLog(Prog, 512, NULL, buffer);
			std::string msg("Program linking failed:\n");
			msg += buffer;
			//		TRACE("opengl Program linking failed\n%s\n", buffer);
			glDeleteProgram(Prog); Prog = 0;
			throw std::runtime_error(msg);
		}
		return status;
	}

	// returns a new tdogl::Texture created from the given filename
	static Texture* LoadTexture(const char* filename) {
		Bitmap bmp = Bitmap::bitmapFromFile(filename);// ResourcePath(filename));
		bmp.flipVertically();
		return new Texture(bmp);
	}

	static Texture* LoadTexture(const unsigned int width, const unsigned int height, const unsigned char* data) {
		Bitmap bmp(width, height, Bitmap::Format_RGBA, data);
		bmp.flipVertically();
		return new Texture(bmp);
	}

	//===========================================
	//Color shapes shader sources 
	//===========================================

	//-------------------------------------------
	//Color Vertex Shader CVS
	//-------------------------------------------

	const GLchar* ColorVertexShader = R"glsl(
    #version 420

    layout (std140) uniform Camera
	{
		mat4 cameraMatrix;
	};

    uniform mat4 modelMatrix;

    layout (location = 0) in vec3 position;
    layout (location = 1) in vec3 color;
    layout (location = 3) in float alfa;

	out data{
		vec4 color;
		vec3 pos;
	} DataOut;

    void main()
    {
        DataOut.color = vec4(color,clamp(alfa,0,1));
		DataOut.pos=position;

		// Apply all matrix transformations to vert
		gl_Position = cameraMatrix*modelMatrix*vec4(position,1);
    }
)glsl";

	//-------------------------------------------
	//Color Geometry Shader CGS
	//-------------------------------------------
	const GLchar* ColorGeometryShader = R"glsl(
	#version 420

    layout (std140) uniform Camera
	{
		mat4 cameraMatrix;
	};

    uniform mat4 modelMatrix;
//	uniform vec3 lightPos;

	layout(triangles) in;

	in data{
		vec4 color;
		vec3 pos;
	} DataIn[];

	layout(triangle_strip,max_vertices=6) out;
	out data{
		vec4 color;
		vec3 pos;
	} DataOut;

	vec3 GetNormal() {
		vec3 a = DataIn[0].pos-DataIn[1].pos;
		vec3 b = DataIn[2].pos-DataIn[1].pos;
		vec3 N=normalize(cross(a,b));
		return vec3(cameraMatrix*modelMatrix*vec4(N,0));
	}

	vec4 GetDiffuse(vec3 N, vec3 p) {
		vec3 lightDirection=vec3(0,0,1);
		float D=clamp( abs(dot(N, lightDirection )), 0,1)*0.5+0.5;
		//float D=clamp( abs(dot(N, normalize(lightPos-p) )), 0,1)*0.5+0.5;
		return vec4(D,D,D,1);
	}

	void main() {
		vec3 N=GetNormal();

		gl_Position=gl_in[0].gl_Position;
		vec4 Diffuse = GetDiffuse(N,gl_in[0].gl_Position.xyz);
		DataOut.pos=DataIn[0].pos;
		DataOut.color=DataIn[0].color*Diffuse;
		EmitVertex();

		gl_Position=gl_in[1].gl_Position;
		Diffuse = GetDiffuse(N,gl_in[1].gl_Position.xyz);
		DataOut.pos=DataIn[1].pos;
		DataOut.color=DataIn[1].color*Diffuse;
		EmitVertex();

		gl_Position=gl_in[2].gl_Position;
		Diffuse = GetDiffuse(N,gl_in[2].gl_Position.xyz);
		DataOut.pos=DataIn[2].pos;
		DataOut.color=DataIn[2].color*Diffuse;
		EmitVertex();
		EndPrimitive();
	}
)glsl";

	//-------------------------------------------
	//Color Fragment Shader CFS
	//-------------------------------------------
	const GLchar* ColorFragmentShader = R"glsl(
    #version 420

	in data{
		vec4 color;
		vec3 pos;
	} DataIn;

    out vec4 outColor;

    void main()
    {
        outColor = DataIn.color;
    }
)glsl";

	//===========================================
	//Texture shapes shader sources 
	//===========================================

	//-------------------------------------------
	//Texture Vertex Shader TVS
	//-------------------------------------------
	const GLchar* TextureVertexShader = R"glsl(
//    #version 330 core
    #version 420

	layout (std140) uniform Camera
	{
		mat4 cameraMatrix;
	};

    uniform mat4 modelMatrix;

    layout (location = 0) in vec3 position;
	layout (location = 4) in vec2 vertTexCoord;

	out data{
		vec2 texCoord;
		vec3 pos;
		vec4 diffuse;
	} DataOut;

    void main()
    {
        DataOut.texCoord = vertTexCoord;
		DataOut.pos=position;
		gl_Position = cameraMatrix*modelMatrix*vec4(position,1);
		DataOut.diffuse= vec4(1.0);
    }
)glsl";

	//-------------------------------------------
	//Texture Geometry Shader TGS
	//-------------------------------------------
	const GLchar* TextureGeometryShader = R"glsl(
//	#version 330 core
    #version 420

    layout (std140) uniform Camera
	{
		mat4 cameraMatrix;
	};

    uniform mat4 modelMatrix;
//	uniform vec3 lightPos;

	layout(triangles) in;
	in data{
		vec2 texCoord;
		vec3 pos;
		vec4 diffuse;
	} DataIn[];

	layout(triangle_strip,max_vertices=6) out;
	out data{
		vec2 texCoord;
		vec3 pos;
		vec4 diffuse;
	} DataOut;

	vec3 GetNormal() {
		vec3 a = DataIn[0].pos-DataIn[1].pos;
		vec3 b = DataIn[2].pos-DataIn[1].pos;
		vec3 N=normalize(cross(a,b));
		return vec3(cameraMatrix*modelMatrix*vec4(N,0));
	}

	vec4 GetDiffuse(vec3 N, vec3 p) {
		vec3 lightDirection=vec3(0,0,1);
		float D=clamp( abs(dot(N, lightDirection )), 0,1)*0.5+0.5;
//		float D=clamp( abs(dot(N, normalize(lightPos-p) )), 0,1)*0.5+0.5;
		return vec4(D,D,D,1);
	}

	void main() {
		vec3 N=GetNormal();

		gl_Position=gl_in[0].gl_Position;
		DataOut.diffuse = GetDiffuse(N,gl_in[0].gl_Position.xyz);
		DataOut.texCoord= DataIn[0].texCoord;
		EmitVertex();

		gl_Position=gl_in[1].gl_Position;
		DataOut.diffuse = GetDiffuse(N,gl_in[1].gl_Position.xyz);
		DataOut.texCoord= DataIn[1].texCoord;
		EmitVertex();

		gl_Position=gl_in[2].gl_Position;
		DataOut.diffuse = GetDiffuse(N,gl_in[2].gl_Position.xyz);
		DataOut.texCoord= DataIn[2].texCoord;
		EmitVertex();
		EndPrimitive();
	}
)glsl";

	//-------------------------------------------
	//Texture Fragment Shader TFS
	//-------------------------------------------
	const GLchar* TextureFragmentShader = R"glsl(
    #version 420

	uniform sampler2D tex;
	uniform vec2 tScale;

	in data{
		vec2 texCoord;
		vec3 pos;
		vec4 diffuse;
	} DataIn;

	out vec4 outColor;

	void main() {
		outColor = texture(tex, fract(DataIn.texCoord*tScale))*DataIn.diffuse;
	}
)glsl";

	//===========================================
	//Character shader sources 
	//===========================================
	const GLchar* CharacterVertexShader = R"glsl(
    #version 420

    layout (std140) uniform Camera
	{
		mat4 cameraMatrix;
	};
//    uniform mat4 cameraMatrix;
    uniform mat4 modelMatrix;

    layout (location = 0) in vec3 position;
	layout (location = 2) in vec2 vertTexCoord;

	out data{
		vec2 texCoord;
		vec3 pos;
		vec4 diffuse;
	} DataOut;

    void main()
    {
        DataOut.texCoord = vertTexCoord;
		DataOut.pos=position;
		gl_Position = cameraMatrix*modelMatrix*vec4(position,1);
		DataOut.diffuse= vec4(1.0);
    }
)glsl";

	//-------------------------------------------
	//Character Fragment Shader CFS
	//-------------------------------------------
	const GLchar* CharacterFragmentShader = R"glsl(
	#version 420

	uniform vec3 textColor;
	uniform vec2 glyphoffset;
	uniform sampler2D tex;
	uniform vec2 tScale;

	in data{
		vec2 texCoord;
		vec3 pos;
		vec4 diffuse;
	} DataIn;

	out vec4 outColor;

	void main() {
		vec4 sampled = vec4(1.0, 1.0, 1.0, step(0.5,texture(tex, fract(DataIn.texCoord+glyphoffset))).r);
		outColor= vec4(textColor, 1.0) * sampled;
	}
)glsl";

	//===========================================
	//Picking shader sources 
	//===========================================

	//-------------------------------------------
	//Pick Vertex Shader PVS
	//-------------------------------------------
	const GLchar* PickVertextShader = R"glsl(
    #version 420


    layout (std140) uniform Camera
	{
		mat4 cameraMatrix;
	};

    uniform mat4 modelMatrix;

    layout (location = 0) in vec3 position;

    void main()
    {
		// Apply all matrix transformations to vert
		gl_Position = cameraMatrix*modelMatrix*vec4(position,1);
    }
)glsl";

	//-------------------------------------------
	//Pick Fragment Shader PFS
	//-------------------------------------------
	const GLchar* PickFragmentShader = R"glsl(
	#version 420

	uniform vec4 PickingColor;
    out vec4 outColor;

	void main() {
		outColor = PickingColor;
	}
)glsl";


	/*-------------------------------------------
	//Tesselation Shaders
	//-------------------------------------------
	const GLchar* TCS= R"glsl(

	)glsl";

	const GLchar* TES = R"glsl(
    uniform mat4 cameraMatrix;
    uniform mat4 modelMatrix;

	)glsl";
	//*/

	//===========================================
	//===========================================



	OpenGLShader::OpenGLShader(const std::string& shaderCode, const GLenum shaderType, const std::string& outName) :
		object(0)
		, refCount(NULL)
		, oType(shaderType)
		, oName(outName)
	{
		//create the shader object
		object = glCreateShader(shaderType);
		if (object == 0)
			throw std::runtime_error("OpenGLShader::OpenGLShader, glCreateShader failed");

		//set the source code
		const char* code = shaderCode.c_str();
		glShaderSource(object, 1, (const GLchar**)&code, NULL);

		//compile
		glCompileShader(object);
		ShaderStatus(object);

		refCount = new unsigned;
		*refCount = 1;
	}

	GLuint OpenGLShader::GetObject() const {
		return object;
	}

	GLenum OpenGLShader::GetShaderType() const {
		return oType;
	}

	OpenGLShader::~OpenGLShader() {
		// if constructor failed and threw an exception refCount will be NULL
		if (refCount) release();
	}


	OpenGLShader::OpenGLShader(const OpenGLShader& other) :
		object(other.object),
		refCount(other.refCount)
		, oType(other.oType)
	{
		retain();
	}

	OpenGLShader& OpenGLShader::operator = (const OpenGLShader& other) {
		release();
		object = other.object;
		refCount = other.refCount;
		oType = other.oType;
		retain();
		return *this;
	}
	void OpenGLShader::retain() {
		assert(refCount);
		*refCount += 1;
	}

	void OpenGLShader::release() {
		assert(refCount && *refCount > 0);
		*refCount -= 1;
		if (*refCount == 0) {
			glDeleteShader(object);
			object = 0;
			delete refCount;
			refCount = NULL;
		}
	}

	OpenGLProgram::OpenGLProgram(const std::vector<OpenGLShader>& shaders) :
		object(0)
		, UniformModelMatrix(0)
		, Uniformtex(0)
		, UniformtexScale(0)
	{
		if (shaders.size() <= 0)
			throw std::runtime_error("OpenGLProgram::OpenGLProgram, No shaders were provided to create the program");

		//create the program object
		object = glCreateProgram();
		if (object == 0)
			throw std::runtime_error("OpenGLProgram::OpenGLProgram, glCreateProgram failed");

		//attach all the shaders
		for (unsigned i = 0; i < shaders.size(); ++i) {
			glAttachShader(object, shaders[i].GetObject());
			if (shaders[i].GetShaderType() == GL_FRAGMENT_SHADER && shaders[i].GetVariableName() != "")
				glBindFragDataLocation(object, 0, shaders[i].GetVariableName().c_str());
		}

		//link the shaders together
		glLinkProgram(object);

		//detach all the shaders
		for (unsigned i = 0; i < shaders.size(); ++i)
			glDetachShader(object, shaders[i].GetObject());

		//throw exception if linking failed
		ProgramStatus(object);

		int uniformBlockIndex = glGetUniformBlockIndex(object, "Camera");
		if (uniformBlockIndex >= 0)
			glUniformBlockBinding(object, uniformBlockIndex, 0);

		UniformModelMatrix = GetUniform("modelMatrix");
		Uniformtex = QueryUniform("tex");
		UniformtexScale = QueryUniform("tScale");
	}

	OpenGLProgram::~OpenGLProgram() {
		//might be 0 if ctor fails by throwing exception
		if (object != 0) glDeleteProgram(object);
	}

	GLuint OpenGLProgram::GetObject() const {
		return object;
	}

	void OpenGLProgram::Use() const
	{
		glUseProgram(object);
	}
	bool OpenGLProgram::IsInUse() const
	{
		GLint currentProgram = 0;
		glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);
		return (currentProgram == (GLint)object);
	}

	void OpenGLProgram::StopUsing() const
	{
		assert(IsInUse());
		glUseProgram(0);
	}

	GLint OpenGLProgram::QueryAttrib(const GLchar* attribName) const {
		if (!attribName)
			return -1;
		return glGetAttribLocation(object, attribName);
	}

	GLint OpenGLProgram::GetAttrib(const GLchar* attribName) const {
		if (!attribName)
			throw std::runtime_error("OpenGLProgram::GetAttrib, attribName was NULL");

		GLint attrib = glGetAttribLocation(object, attribName);
		if (attrib == -1)
			throw std::runtime_error(std::string("OpenGLProgram::GetAttrib, Program attribute not found: ") + attribName);

		return attrib;
	}

	GLint OpenGLProgram::QueryUniform(const GLchar* uniformName) const {
		if (!uniformName)
			return -1;
		return glGetUniformLocation(object, uniformName);
	}

	GLint OpenGLProgram::GetUniform(const GLchar* uniformName) const {
		if (!uniformName)
			throw std::runtime_error(" OpenGLProgram::GetUniform, uniformName was NULL");

		GLint uniform = glGetUniformLocation(object, uniformName);

		if (uniform == -1)
			throw std::runtime_error(std::string(" OpenGLProgram::GetUniform, Program uniform not found: ") + uniformName);

		return uniform;
	}

	OpenGLShaderPrograms::OpenGLShaderPrograms()
		: GL_ColorPgm(nullptr)
		, GL_TexPgm(nullptr)
		, GL_PickPgm(nullptr)
		, GL_CharPgm(nullptr)
		, GL_LinePgm(nullptr)
	{}

	void OpenGLShaderPrograms::Init()
	{
		if (!GL_ColorPgm) {
			GL_ColorPgm = CreateOpenGLProgram(ColorVertexShader, ColorFragmentShader, ColorGeometryShader);
		}
		if (!GL_TexPgm) {
			GL_TexPgm = CreateOpenGLProgram(TextureVertexShader, TextureFragmentShader, TextureGeometryShader);
		}
		if (!GL_PickPgm) {
			GL_PickPgm = CreateOpenGLProgram(PickVertextShader, PickFragmentShader);
		}
		if (!GL_CharPgm) {
			GL_CharPgm = CreateOpenGLProgram(CharacterVertexShader, CharacterFragmentShader);
		}
		if (!GL_LinePgm) {
			GL_LinePgm = CreateOpenGLProgram(ColorVertexShader, ColorFragmentShader);
		}

		RegisterTexture(Appearance::concreteSurface, Concrete_imgTexture, GL_TexPgm);
		//RegisterTexture(Appearance::gravelSurface, "C:\\Users\\Mats\\source\\repos\\wxWidgets\\Xtrk3DViewer\\Gravel004_8K_Color.png", GL_TexPgm);
		RegisterTexture(Appearance::gravelSurface, gravel_width, gravel_height, gravel_Data, GL_TexPgm);
		RegisterTexture(Appearance::darkConcreteSurface, DarkConcrete_imgTexture, GL_TexPgm);
		//RegisterTexture(Appearance::tieSurface, "C:\\Users\\Mats\\source\\repos\\wxWidgets\\Xtrk3DViewer\\rr_sleeper2.png", GL_TexPgm);
		RegisterTexture(Appearance::tieSurface, rr_sleeperTexture, GL_TexPgm);//DarkConcrete_imgTexture
		RegisterTexture(Appearance::bufferstop, bufferstop_imgTexture, GL_TexPgm);
		RegisterTexture(Appearance::railSurface, rail_img_width, rail_img_height, rail_img_Data, GL_TexPgm);
		RegisterTexture(Appearance::PlankSurface, BenchworkTexture, GL_TexPgm);

		RegisterTexture(Appearance::digits, asciiTexture, GL_CharPgm);
		texture[Appearance::solidColor] = std::make_pair(nullptr, GL_ColorPgm);
	};

	OpenGLShaderPrograms::	~OpenGLShaderPrograms()
	{
		if (GL_ColorPgm != nullptr)
			delete GL_ColorPgm;
		if (GL_TexPgm != nullptr)
			delete GL_TexPgm;
		if (GL_CharPgm != nullptr)
			delete GL_CharPgm;
		if (GL_PickPgm != nullptr)
			delete GL_PickPgm;
		if (GL_LinePgm != nullptr)
			delete GL_LinePgm;

		for (auto &ii : texture)
		{
			delete ii.second.first;
		}
		texture.clear();
	}

	void  OpenGLShaderPrograms::RegisterTexture(Appearance A, const unsigned int width, const unsigned int height, const unsigned char* data, OpenGLProgram* Pgm)
	{
		if (texture.count(A) == 0) {
			texture[A] = std::make_pair(LoadTexture(width, height, data), Pgm);
		}
	}
	void OpenGLShaderPrograms::RegisterTexture(Appearance A, texture_data t_texture, OpenGLProgram* Pgm)
	{
		if (texture.count(A) == 0) {
			texture[A] = std::make_pair(LoadTexture(t_texture.w, t_texture.h, t_texture.Data), Pgm);
		}
	}

	void OpenGLShaderPrograms::RegisterTexture(Appearance A, std::string filename, OpenGLProgram* Pgm)
	{
		//TODO: check for texture correctly loaded. (file exist) if wrong format a runtime error will be generated by Bitmap::bitmapFromFile
		if (texture.count(A) == 0) {
			texture[A] = std::make_pair(LoadTexture(filename.c_str()), Pgm);
		}
	}


	OpenGLProgram*  OpenGLShaderPrograms::CreateOpenGLProgram(const GLchar* VertexSource, const GLchar* FragmentSource, const GLchar* GeomertySource)
	{
		std::vector<OpenGLShader> shaders;
		shaders.push_back(OpenGLShader(VertexSource, GL_VERTEX_SHADER));
		shaders.push_back(OpenGLShader(FragmentSource, GL_FRAGMENT_SHADER, "outColor"));
		if (GeomertySource != nullptr && GLEW_ARB_geometry_shader4) {
			shaders.push_back(OpenGLShader(GeomertySource, GL_GEOMETRY_SHADER));
		}
		OpenGLProgram* Pgm = new OpenGLProgram(shaders);
		return Pgm;
	}

	OpenGLProgram* OpenGLShaderPrograms::use(Appearance appearance, const glm::mat4& transform, const glm::vec2 textureScale)  //2020-05-06 add parameter textureScale and set as uniform
	{
		OpenGLProgram* pgm = nullptr;
		if (texture.count(appearance) > 0)
		{
			pgm = texture[appearance].second;
			if (!pgm->IsInUse()) {
				pgm->Use();
				if (texture[appearance].first != nullptr) {
					glActiveTexture(GL_TEXTURE0);
					glUniform1i(pgm->Uniformtex, 0);
				}
			}
			glUniformMatrix4fv(pgm->UniformModelMatrix, 1, GL_FALSE, glm::value_ptr(transform));
			if (texture[appearance].first != nullptr) {
				glBindTexture(GL_TEXTURE_2D, texture[appearance].first->object());
				if(glm::length(textureScale) > 0)
					glUniform2fv(pgm->UniformtexScale, 1, glm::value_ptr(textureScale));
				else 
					glUniform2fv(pgm->UniformtexScale, 1, glm::value_ptr(glm::vec2(1.0f)));
			}
		}
		return pgm;
	}
}