#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include "Scene.h"
#include "track.h"

#include <glm/glm/gtc/type_ptr.hpp>
#include <sstream>
#include <iomanip>
namespace Xtrk3D {
	SceneNode::SceneNode(const SceneNode& other)
		: parent(other.parent)
		, shapeList(other.shapeList)
		, WorldTransform(other.WorldTransform)
		, transform(other.transform)
		, boundingSphere(other.boundingSphere)
		, layer(other.layer)
		, isTrack(other.isTrack)
		, TrackObj(other.TrackObj)
		, isTie(other.isTie)
		, layer2(other.layer2)
	{
		for (auto &ii : other.children) {
			SceneNode* Node = new SceneNode(*ii);
			Node->parent = this;
			children.push_back(Node);
		};
		for (auto &ii : other.textList) {
			TextNode N(ii);
			N.parent = this;
			textList.push_back(N);
		}
	}

	SceneNode::~SceneNode()
	{
		clear();
	}

	void SceneNode::addShape(Shape &Obj)
	{
		shapeList.push_back(Obj);
	}

	void SceneNode::addText(TextNode  &Obj)
	{
		Obj.parent = this;
		Obj.Update();
		textList.push_back(Obj);
	}

	void SceneNode::addNode(SceneNode* childNode)
	{
		if (childNode == nullptr)
			return;
		childNode->parent = this;
		childNode->Update();
		children.push_back(childNode);
	}

	int SceneNode::Render(const Camera& myCamera, const bool LayerColorMode)
	{
		int count(0);
		if (isTie) {
			boundingSphere.setFarDistance(myCamera.farPlane() / 2);
			isTie = false;
		}
		if (!boundingSphere.isVisible(myCamera, WorldTransform)) return 0; //2021-03-29
		for (auto &ii : children)
		count += ii->Render(myCamera, LayerColorMode);

		//Do LOD selection
	/*test of distance culling*
		if (LOD_enabled) {
			glm::vec3 Pos(transform[3]);
			Pos = Pos - myCamera.GetPosition();
			float dist = glm::dot(Pos, myCamera.F.forward);
			switch (Dist in DistVector) DoSomething;
		}
		//*/

		for (auto &ii : shapeList)
			ii.Render(WorldTransform, LayerColorMode);
		return count + shapeList.size();
	}

	int SceneNode::RenderType(const Camera& myCamera, const ShapeType mode)
	{
		int count(0);
		if (!boundingSphere.isVisible(myCamera, transform)) return 0;
		for (auto &ii : children)
			count += ii->RenderType(myCamera, mode);
		for (auto &ii : shapeList)
			if ((int)(ii.Type) & (int)(mode))
				ii.Render(WorldTransform, false);
		return count + shapeList.size();
	}

	int SceneNode::RenderText(OpenGLText& Renderer)
	{
		int count(0);
		for (auto &ii : children)
			count += ii->RenderText(Renderer);
		for (auto &ii : textList)
			Renderer.Render(ii.WorldTransform, ii.str, ii.color);
		return count + textList.size();
	}

	void SceneNode::Select(const Camera& myCamera, OpenGLProgram* pickShader, const unsigned int uuid)
	{
		if (!boundingSphere.isVisible(myCamera, WorldTransform)) return;//2021-05-24
		for (auto &ii : children)
			ii->Select(myCamera, pickShader, uuid);
		for (auto &ii : shapeList)
			ii.Select(WorldTransform, pickShader, uuid);
	}

	void SceneNode::clear()
	{
		for (auto &ii : children) {
			delete ii;
		}
		children.clear();
		shapeList.clear();
	}

	void SceneNode::Update()
	{
		if (parent) {
			glm::mat4 NewTransform = parent->WorldTransform*transform;
			if (NewTransform == WorldTransform)
				return;
			WorldTransform = NewTransform;
		}
		else
			WorldTransform = transform;

		for (auto &ii : children)
			ii->Update();
		for (auto &ii : textList)
			ii.Update();
	}

	int Shape::Render(const glm::mat4& transform, const bool LayerColorMode)
	{
		if (drawCount < 1)
			return 0;

		if (drawType == GL_LINES || drawType == GL_LINE_STRIP) {  //  call OpenGLProgram::Use() directly
			if (!shaders->GL_LinePgm->IsInUse()) {
				shaders->GL_LinePgm->Use();
				glUniformMatrix4fv(shaders->GL_LinePgm->UniformModelMatrix, 1, GL_FALSE, glm::value_ptr(transform));
			}
		}
		else {
			auto pgm = shaders->use(LayerColorMode ? LayerAppearance : appearance, transform, textureScale);
		}

		GLint mode;
		if (glIsEnabled(GL_CULL_FACE))
		{
			glGetIntegerv(GL_CULL_FACE_MODE, &mode);
		}
		else
			mode = GL_NONE;
		if (cullFace != mode)
		{
			if (cullFace != GL_NONE) {
				glEnable(GL_CULL_FACE);
				glCullFace(cullFace);
			}
			else
				glDisable(GL_CULL_FACE);
		}
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo );
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

		glDrawElements(drawType, drawCount, GL_UNSIGNED_INT, 0);
		switch (drawType) {
		case GL_TRIANGLES:
			return drawCount / 3;
		case GL_LINES:
			return drawCount / 2;
		case GL_TRIANGLE_FAN:
			return drawCount - 2;
		case GL_LINE_STRIP:
			return drawCount - 1;
		default:
			return drawCount;
		}
	}

	Shape::Shape(ShapeType ObjType, std::vector<vertex>& vertices, std::vector<GLuint>& indices, OpenGLShaderPrograms* Shaders, Appearance material, Appearance LayerMaterial, GLenum type, GLuint cullface, glm::vec2 texScale)
		:shaders(Shaders)
		, appearance(material)
		, LayerAppearance(LayerMaterial)
		, drawType(type)
		, drawCount(indices.size())
		, cullFace(cullface)
		, Type(ObjType)
		, vbo(-1)
		, ebo(-1)
		, vao(-1)
		, textureScale(texScale)
		, refCount(nullptr)
		//	,parent(NULL)
	{
		Set(vertices, indices);
	}

	void Shape::Set(std::vector<vertex>& vertices, std::vector<GLuint>& elementData)
	{
		if (vertices.size() > 0) {
			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);

			//vertex data
			vbo = make_buffer(GL_ARRAY_BUFFER, vertices.size() * vertex_size, vertices.data());
			glEnableVertexAttribArray(0);//position
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertex_size, 0);

			glEnableVertexAttribArray(1);//color
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, vertex_size, (const GLvoid*)(vertex_colorIndex));

			glEnableVertexAttribArray(4);//normal
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, vertex_size, (const GLvoid*)(vertex_normalIndex));

			glEnableVertexAttribArray(3); //alpha channel
			glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, vertex_size, (const GLvoid*)(vertex_alphaIndex));

			glEnableVertexAttribArray(2);//vertTexCoord
			glVertexAttribPointer(4, 2, GL_FLOAT, GL_TRUE, vertex_size, (const GLvoid*)(vertex_texCoordIndex));

			// Create an element array
			ebo = make_buffer(GL_ELEMENT_ARRAY_BUFFER, elementData.size() * sizeof(GLuint), elementData.data());


			// unbind the VAO
//			glBindBuffer(GL_ARRAY_BUFFER, 0);
//			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//			glBindVertexArray(0);
		}
		else
			vao = -1;
		refCount = new unsigned;
		*refCount = 1;
	}

	void Shape::Select(const glm::mat4& transform, const OpenGLProgram* pickShader, const unsigned int uuid)
	{
		if (drawCount < 1)
			return;
		pickShader->Use();
		//set shader uniforms
		glUniformMatrix4fv(pickShader->UniformModelMatrix, 1, GL_FALSE, glm::value_ptr(transform));

		GLint Uniform = pickShader->QueryUniform("PickingColor");
		if (Uniform >= 0) {
			unsigned int r = (uuid & 0x000000FF);
			unsigned int g = (uuid & 0x0000FF00) >> 8;
			unsigned int b = (uuid & 0x00FF0000) >> 16;
			glm::vec4 uuid4v = glm::vec4(r / 255.0, g / 255.0, b / 255.0, 1);
			glUniform4fv(pickShader->GetUniform("PickingColor"), 1, glm::value_ptr(uuid4v));
		}
		if (cullFace != GL_NONE) {
			glEnable(GL_CULL_FACE);
			glCullFace(cullFace);
		}
		else
			glDisable(GL_CULL_FACE);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

		glDrawElements(drawType, drawCount, GL_UNSIGNED_INT, 0);

	}

	TextNode::TextNode(float val, glm::vec3 Color, int precision) :parent(nullptr), color(Color)
	{
		std::stringstream stream;
		stream << std::fixed << std::setprecision(precision) << val;
		str = stream.str();
	}

	void TextNode::Update()
	{
		WorldTransform = (parent != nullptr) ? parent->getMatrix()*transform : transform;
	}


	Arrow::Arrow(ShapeType objType, glm::vec3 start, glm::vec3 end, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material )
	{
		std::vector<GLuint>indices;
		std::vector<vertex>vertices;

		glm::vec3 normal = glm::normalize(glm::cross(end - start, glm::vec3(0, 0, 1)));
		auto len=glm::length(end - start);
		auto heading = glm::normalize(end - start);
		shaders = Shaders;
		appearance = material;
		LayerAppearance = material;
		cullFace = GL_NONE;
		drawType = GL_LINES;

		vertices.push_back(vertex(start, color));
		vertices.push_back(vertex(end, color));
		vertices.push_back(vertex(start + 0.8f * len * heading + normal * 0.2f * len, color));
		vertices.push_back(vertex(start + 0.8f * len * heading - normal * 0.2f * len, color));

		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);
		indices.push_back(1);
		indices.push_back(3);
		indices.push_back(1);
		drawCount = indices.size();
		Type = objType;
		Set(vertices, indices);
	}

	Disc::Disc(ShapeType objType,glm::vec3 center, double Radius, double Height, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material, GLuint cullface)//, double TexScale)
	{
		double delta = 2.0 * M_PI / double(divs);
		double sin_delta = sin(delta);
		double cos_delta = cos(delta);
		double u1;
		double u = Radius;
		double v = 0.0;

		shaders = Shaders;
		appearance = material;
		LayerAppearance = material;
		cullFace = cullface;
		drawType = GL_TRIANGLES;

		vertices.push_back(vertex(center , glm::vec3(), color, glm::vec2(Radius, 0)));
		vertices.push_back(vertex(center + glm::vec3(0, 0, Height), glm::vec3(), color, glm::vec2(Radius, 0)));
		vertices.push_back(vertex(center + glm::vec3(Radius, 0, 0), glm::vec3(), color, glm::vec2(Radius, 0)));
		vertices.push_back(vertex(center + glm::vec3(Radius, 0, Height), glm::vec3(), color, glm::vec2(Radius, 0)));
		for (int i = 0; i < divs; ++i) {
			u1 = u * cos_delta - v * sin_delta;
			v = u * sin_delta + v * cos_delta;
			u = u1;
			int index = vertices.size();
			vertices.push_back(vertex(center + glm::vec3(u, v, 0), glm::vec3(), color, glm::vec2(u, v))); //index
			vertices.push_back(vertex(center + glm::vec3(u, v, Height), glm::vec3(), color, glm::vec2(u, v))); //index+1
			indices.push_back(0); indices.push_back(index); indices.push_back(index-2);
			indices.push_back(1); indices.push_back(index - 1); indices.push_back(index+1);

			indices.push_back(index); indices.push_back(index + 1); indices.push_back(index - 2);
			indices.push_back(index-2); indices.push_back(index + 1); indices.push_back(index - 1);
		}
		drawCount = indices.size();
		Type = objType;
		Set(vertices, indices);
	}

	Disc::Disc(ShapeType objType, glm::vec3 center, double Radius, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material, GLuint cullface, float TexScale)
	{
		double delta = 2.0*M_PI / double(divs);
		double sin_delta = sin(delta);
		double cos_delta = cos(delta);
		double u1;
		double u = Radius;
		double v = 0.0;

		shaders = Shaders;
		appearance = material;
		LayerAppearance = material;
		cullFace = cullface;
		drawType = GL_TRIANGLE_FAN;
		indices.push_back(vertices.size());
		vertices.push_back(vertex(center, glm::vec3(), color, glm::vec2()));

		indices.push_back(vertices.size());
		vertices.push_back(vertex(center + glm::vec3(Radius, 0, 0), glm::vec3(), color, glm::vec2(TexScale, 0)));
		for (int i = 0; i < divs; ++i) {
			u1 = u * cos_delta - v * sin_delta;
			v = u * sin_delta + v * cos_delta;
			u = u1;
			indices.push_back(vertices.size());
			vertices.push_back(vertex(center + glm::vec3(u, v, 0), glm::vec3(), color, glm::normalize( glm::vec2(u, v))* TexScale));
		}
		drawCount = indices.size();
		Type = objType;
		Set(vertices, indices);
	}

	Ring::Ring(ShapeType objType, glm::vec3 center, double Ri, double Ry, double height, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material)
	{
		shaders = Shaders;
		appearance = material;
		drawType = GL_TRIANGLES;
		double delta = 2.0*M_PI / double(divs);
		double sin_delta = sin(delta);
		double cos_delta = cos(delta);
		double u1;
		double u = Ri;
		double v = 0.0;
		double U1;
		double U = Ry;
		double V = 0.0;
		double alfa = 0;
		vertices.push_back(vertex(center + glm::vec3(Ri, 0, 0), glm::vec3(), color, glm::vec2(Ri, 0)));
		vertices.push_back(vertex(center + glm::vec3(Ri, 0, height), glm::vec3(), color, glm::vec2(Ri, 0)));
		vertices.push_back(vertex(center + glm::vec3(Ry, 0, height), glm::vec3(), color, glm::vec2(Ry, 0)));
		vertices.push_back(vertex(center + glm::vec3(Ry, 0, 0), glm::vec3(), color, glm::vec2(Ry, 0)));
		for (int i = 0; i < divs; ++i) {
			u1 = u * cos_delta - v * sin_delta;
			v = u * sin_delta + v * cos_delta;
			u = u1;
			U1 = U * cos_delta - V * sin_delta;
			V = U * sin_delta + V * cos_delta;
			U = U1;
			alfa += delta;
			int index = vertices.size();
			vertices.push_back(vertex(center + glm::vec3(u, v, 0), glm::vec3(), color, glm::vec2(u, v))); //index
			vertices.push_back(vertex(center + glm::vec3(u, v, height), glm::vec3(), color, glm::vec2(u, v))); //index+1
			vertices.push_back(vertex(center + glm::vec3(U, V, height), glm::vec3(), color, glm::vec2(U, V))); //index+2
			vertices.push_back(vertex(center + glm::vec3(U, V, 0), glm::vec3(), color, glm::vec2(U, V))); //index+3

			indices.push_back(index - 4); indices.push_back(index - 3); indices.push_back(index);
			indices.push_back(index - 3); indices.push_back(index + 1); indices.push_back(index);

			indices.push_back(index - 3); indices.push_back(index - 2); indices.push_back(index + 1);
			indices.push_back(index - 2); indices.push_back(index + 2); indices.push_back(index + 1);

			indices.push_back(index - 2); indices.push_back(index - 1); indices.push_back(index + 2);
			indices.push_back(index - 1); indices.push_back(index + 3); indices.push_back(index + 2);
		}
		drawCount = indices.size();
		Type = objType;
		Set(vertices, indices);
	}

	Ring::Ring(ShapeType objType, glm::vec3 center, double Ri, double Ry, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material, GLuint cullface)
		//Disc::Disc(float Ry, float Ri, float Zy, float Zi, int divs,  OpenGLShaderPrograms* Shaders, Appearance material)
	{
		shaders = Shaders;
		appearance = material;
		cullFace = cullface;
		drawType = GL_TRIANGLES;

		double delta = 2.0*M_PI / double(divs);
		double sin_delta = sin(delta);
		double cos_delta = cos(delta);
		double u1;
		double u = Ri;
		double v = 0.0;
		double U1;
		double U = Ry;
		double V = 0.0;
		glm::vec3 Color(1, 0, 0);

		vertices.push_back(vertex(center + glm::vec3(Ri, 0, 0), glm::vec3(), Color, glm::vec2(Ri, 0)));
		vertices.push_back(vertex(center + glm::vec3(Ry, 0, 0), glm::vec3(), Color, glm::vec2(Ry, 0)));
		for (int i = 0; i < divs; ++i) {
			u1 = u * cos_delta - v * sin_delta;
			v = u * sin_delta + v * cos_delta;
			u = u1;
			U1 = U * cos_delta - V * sin_delta;
			V = U * sin_delta + V * cos_delta;
			U = U1;
			int index = vertices.size();
			vertices.push_back(vertex(center + glm::vec3(u, v, 0), glm::vec3(), Color, glm::vec2(u, v)));
			vertices.push_back(vertex(center + glm::vec3(U, V, 0), glm::vec3(), Color, glm::vec2(U, V)));

			indices.push_back(index - 2);
			indices.push_back(index - 1);
			indices.push_back(index);
			indices.push_back(index - 1);
			indices.push_back(index + 1);
			indices.push_back(index);
		}
		drawCount = indices.size();
		Type = objType;
		Set(vertices, indices);
	}

	BoundSphere::BoundSphere(std::vector<glm::vec3>& points)
		:radius(0)
		, start(0)
		, farDistance(-1.0f)
	{
		if (points.empty()) {
			radius = -1;
			return;
		}
		//not smallest bounding sphere, but good enough in this case
		center = points[0];
		for (unsigned int i = 1; i < points.size(); ++i)
			center += points[i];

		center /= points.size();
		for (auto &ii : points)
			radius = fmax(radius, glm::distance(center, ii));
	}

	BoundSphere::BoundSphere(std::vector<vertex> vertices) : start(0), farDistance(-1.0f)
	{
		if (vertices.empty()) {
			radius = -1;
			return;
		}
		center = vertices[0].position;
		radius = 0.0001f;
		glm::vec3 pos;
		//glm::vec3 diff;
		float len, alpha, alphaSq;
		std::vector<vertex>::iterator it;

		for (int i = 0; i < 2; i++) {
			for (it = vertices.begin(); it != vertices.end(); it++) {
				pos = it->position;
				len = glm::distance(pos, center);
				if (len > radius) {
					alpha = len / radius;
					alphaSq = alpha * alpha;
					radius = 0.5f * (alpha + 1 / alpha) * radius;
					center = 0.5f * ((1 + 1 / alphaSq) * center + (1 - 1 / alphaSq) * pos);
				}
			}
		}

		for (it = vertices.begin(); it != vertices.end(); it++) {
			pos = it->position;
			glm::vec3 diff = pos - center;
			len = glm::length(diff);
			if (len > radius) {
				radius = (radius + len) / 2.0f;
				center = center + ((len - radius) / len * diff);
			}
		}
	}

	bool BoundSphere::isVisible(const Camera &camera, const glm::mat4 &transform)
	{
		if (radius < 0) //never cull if radius<0
			return true;
		if (farDistance < 0) {
			farDistance = camera.farPlane();
		}
		glm::vec3 Pos(transform[3]);
		Pos = Pos - camera.getPosition() + center;
		float dist = glm::dot(Pos, camera.F.forward);
		if (dist > farDistance + radius)
			return false;
		if (dist < -radius)
			return false;

		for (auto i = 0; i < 4; ++i)
			if (glm::dot(Pos, camera.F.Normals[order[i + start]]) < -radius) { // start with previously false normal
				start = order[i + start];
				return false;
			}
		return true;
	}


	Scene::Scene() :myText(nullptr), myPlan(nullptr), backGround(nullptr),uboCamera(-1)
	{
	}


	Scene::~Scene()
	{
		Clear();
	}


	void Scene::Clear()
	{
		for (auto &ii : trackNodes)
			delete ii;
		trackNodes.clear();
		for (auto &ii : tunnels)
			delete ii;
		tunnels.clear();
		for (auto &ii : nonTrackNodes)
			delete ii;
		nonTrackNodes.clear();
		for (auto &ii : overlayNodes)
			delete ii;
		overlayNodes.clear();
		for (auto &ii : endPoints)
			delete ii;
		endPoints.clear();
		for (auto& ii : dynamicObjects)
			delete ii;
		dynamicObjects.clear();

		if (myText != nullptr) {
			delete myText;
			myText = nullptr;
		}
		if (backGround != nullptr) {
			delete backGround;
			backGround = nullptr;
		}
	}

	void Scene::addEndPoint(PointNode* Item, int layer1, int layer2, TrackPlan*  layout, OpenGLShaderPrograms& ShaderPrograms)
	{
		SceneNode* Node = new SceneNode();
		glm::vec3 color(1, 0, 1);
		if (Item->Z_def == 2) color = glm::vec3(0.2, 0.2, 0.2);
		Disc myDisc(ShapeType::ElevationType, Item->pos, 0.08f, 24, glm::vec3(0.2, 0.2, 0.2), &ShaderPrograms);
		Node->addShape(myDisc);
		TextNode height(layout->metric ? Item->pos.z*2.54f : Item->pos.z, color);
		height.setTransform(glm::translate(glm::mat4(), Item->pos + glm::vec3(0, 0, 0.2f)));
		Node->addText(height);
		Node->layer = layer1;
		if (Item->Nodes[1].first > 0) {
			Node->layer2 = layer2;
		}
		else {
			Node->layer2 = Node->layer;
		}
		endPoints.push_back(Node);

	}

	int Scene::Render(const Camera& myCamera)
	{
		glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearDepth(1.0f);	// Depth Buffer Setup
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_DEPTH_CLAMP);
		glDepthFunc(GL_LEQUAL); // The Type Of Depth Testing To Do
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindBuffer(GL_UNIFORM_BUFFER, uboCamera);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(myCamera.getMatrix()));
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		Origo.Render(myCamera);
		if (myPlan == nullptr)
			return 0;
		int nodeCount(0);
		
		for (auto &is : trackNodes) {
			int L = is->layer;
			if (!myPlan->Layers.empty())
				if (!myPlan->Layers.at(L).Visible)
					continue;
			nodeCount += is->Render(myCamera, myPlan->ColorByLayer);
		}
		if (myPlan->TunnelMode > 0)
		{
			ShapeType mode = myPlan->TunnelMode == 1 ? ShapeType::TunnelSideTopType : ShapeType::TunnelRoofType;
			for (auto &is : tunnels) {
				int L = is->layer;
				if (!myPlan->Layers.empty())
					if (!myPlan->Layers.at(L).Visible)
						continue;
				nodeCount += is->RenderType(myCamera, mode);
			}
		}
		
		if (myPlan->DrawStruct)
			for (auto &is : nonTrackNodes) {
				int L = is->layer;
				if (myPlan->Layers.empty()) {
					nodeCount += is->Render(myCamera, false);
					nodeCount += is->RenderText(*myText);
				}
				else {
					if (myPlan->Layers.count(L) != 1) {
						myPlan->Layers[L] = Layer("undefined", 255, true);
					}
					if (myPlan->Layers.at(L).Visible) {
						nodeCount += is->Render(myCamera, false);
						nodeCount += is->RenderText(*myText);
					}
				}
			}

		for (auto &is : overlayNodes) {
			nodeCount += is->Render(myCamera, false);
		}

		if (backGround != nullptr &&Table.visible) {
			backGround->Render(myCamera, false);
		}//*/

		for (auto& is : dynamicObjects) {
			is->Render(myCamera, false);
		}

		for (auto &is : endPoints) {
			auto L1 = is->layer;
			auto L2 = is->layer2;
			if (L2 < 0)
				L2 = L1;
			if (myPlan->Layers.at(L1).Visible || myPlan->Layers.at(L2).Visible) {
				nodeCount += is->Render(myCamera, false);
				if (myPlan->DrawElevations) {
					nodeCount += is->RenderText(*myText);
				}
			}
		}//*/

		if (myPlan->DrawElevations)
			for (auto &is : trackNodes) {
				int L = is->layer;
				if (!myPlan->Layers.empty())
					if (!myPlan->Layers.at(L).Visible)
						continue;
				nodeCount += is->RenderText(*myText);
			}


		glFlush();
		return nodeCount;
	}

	void Scene::Init(TrackPlan*  layout)
	{
		myPlan = layout;
		if (uboCamera < 0) {
			uboCamera = make_buffer(GL_UNIFORM_BUFFER, sizeof(glm::mat4), NULL);
			glBindBufferRange(GL_UNIFORM_BUFFER, 0,uboCamera,0, sizeof(glm::mat4));
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
		}
		MyShaders.Init();
		Origo.InitDraw(MyShaders.GL_ColorPgm,MyShaders.GL_LinePgm);

		Clear();
		if (layout != nullptr) {
			myText = new OpenGLText(&MyShaders);
			InitTracks();
			InitNonTracks();
			InitPointNodes();
			backGround = Table.createSceneNode(&MyShaders, myPlan);

#ifdef _DEBUG
			FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
			fprintf(fp, "Scene::Init done\n");
			fclose(fp);

#endif
		}
	}

	void Scene::InitTracks()
	{
		for (auto &ii : trackNodes)
			delete ii;
		trackNodes.clear();
		for (auto &ii : tunnels)
			delete ii;
		tunnels.clear();
		if (myPlan != nullptr) {
			for (auto &ii : myPlan->Tracks) { //C++ 11
				trackNodes.push_back(ii.second->createSceneNode(&MyShaders, myPlan));
				if ((ii.second->visibility & 2) == 0) {
					tunnels.push_back(ii.second->CreateTunnelNode(&MyShaders, myPlan));
				}
				if (ii.second->visibility == 10) {
					trackNodes.push_back(ii.second->CreateBridgeNode(&MyShaders, myPlan));
				}
			}
		}
	}
	void Scene::InitNonTracks()
	{
		for (auto &ii : nonTrackNodes)
			delete ii;
		nonTrackNodes.clear();
		for (auto &ii : overlayNodes)
			delete ii;
		overlayNodes.clear();

		if (myPlan != nullptr) {
			for (auto &ii : myPlan->Objects) {
				ii->initSurface(myPlan);
				auto N = ii->createSceneNode(&MyShaders, myPlan);
				if (N != nullptr)
					nonTrackNodes.push_back(N);
				else
					int i = 0;//no segments to create scene from (unsupported segment, i.e dimension line)
			}
			for (auto &ii : myPlan->OverlayObjects) {
//				ii->initSurface(myPlan);
				auto N = ii->createSceneNode(&MyShaders, myPlan);
				if (N != nullptr)
					overlayNodes.push_back(N);
			}
		}
	}

	void Scene::InitPointNodes()
	{
		for (auto &ii : endPoints)
			delete ii;
		endPoints.clear();
		if (myPlan != nullptr) {
			for (auto &ii : myPlan->PointNodeList) {
				int layer1(myPlan->Tracks[ii.Nodes[0].first]->layer);
				int layer2(ii.Nodes[1].first > 0 ? myPlan->Tracks[ii.Nodes[1].first]->layer : layer1);
				addEndPoint(&ii, layer1, layer2, myPlan, MyShaders);
			}

		}
	}

	void Scene::Reset(Camera& myCamera)
	{
		if (myPlan != nullptr) {
			glm::vec3 Mid = (myPlan->LL + myPlan->UR) / 2.0f;
			float W = (myPlan->LL.x + myPlan->UR.x) / 2.0f;
			float H = (myPlan->LL.y + myPlan->UR.y) / 2.0f;
			float a = myCamera.viewportAspectRatio();
			W = fmax(W, H / a);
			if (W < 0.1) {
				myCamera.SetPosition(glm::vec3(0, 0, 60));
				myCamera.LookAt(Mid);
				return;
			}
			double z = fmax(W / tanf(glm::radians(myCamera.fieldOfView()) / 2), 5);
			myCamera.SetPosition(Mid - glm::vec3(0, 0, -z));
			myCamera.LookAt(Mid);
		}

#ifdef _DEBUG
		FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
		fprintf(fp, "Scene::Reset done\n");
		fclose(fp);
#endif

	}

	void Scene::ObjProperties(SceneNode* Obj)
	{
		if (Obj != nullptr)
			if (Obj->TrackObj != nullptr) {
				bool recalc = Obj->TrackObj->OnSelect(myPlan->metric, myPlan->Layers[Obj->TrackObj->layer].Name);
				if (recalc) {
					updateObj(Obj);
				}//*/
			}
	}
	void Scene::updateObj(SceneNode* Obj)
	{
		Obj->TrackObj->updateSceneNode(Obj, &MyShaders, myPlan);
	}

	void Scene::updateTable()
	{
		if (backGround != nullptr) {
			delete backGround;
		}
		backGround = Table.createSceneNode(&MyShaders, myPlan);
	}

	SceneNode* Scene::Select(const Camera& myCamera, int x, int y)
	{
		if (myPlan == nullptr)
			return nullptr;

		GLint viewport[4];
		SceneNodeList T;

		glFlush();
		glFinish();
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGetIntegerv(GL_VIEWPORT, viewport);
		T.push_back(nullptr);
		for (auto &is : trackNodes) {
			int L = is->layer;
			if (!myPlan->Layers.empty())
				if (!myPlan->Layers.at(L).Visible)
					continue;
			is->Select(myCamera, MyShaders.GL_PickPgm, T.size());
			T.push_back(is);
		}
		if (myPlan->TunnelMode > 0)
		{
			ShapeType mode = !myPlan->TunnelMode == 1 ? ShapeType::TunnelSideTopType : ShapeType::TunnelRoofType;
			for (auto &is : tunnels) {
				int L = is->layer;
				if (!myPlan->Layers.empty())
					if (!myPlan->Layers.at(L).Visible)
						continue;
				is->Select(myCamera, MyShaders.GL_PickPgm, T.size());
				T.push_back(is);
			}
		}
		if (myPlan->DrawStruct)
		{
			for (auto &is : nonTrackNodes) {
				int L = is->layer;
				if (!myPlan->Layers.empty())
					if (!myPlan->Layers.at(L).Visible)
						continue;
				is->Select(myCamera, MyShaders.GL_PickPgm, T.size());
				T.push_back(is);
			}
		}
//		if (myPlan->DrawOverlay)
		for (auto &is : overlayNodes) {
			is->Select(myCamera, MyShaders.GL_PickPgm, T.size());
			T.push_back(is);
		}


		unsigned char res[4];
		glReadPixels(x, viewport[3] - y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &res);
		int pickedID =
			res[0] +
			res[1] * 256 +
			res[2] * 256 * 256;

		return T[pickedID];

	}

	Cone::Cone(ShapeType objType, glm::vec3 center, double Radius, double Height, int divs, glm::vec3 color, OpenGLShaderPrograms* Shaders, Appearance material, GLuint cullface)
	{
		double delta = 2.0 * M_PI / double(divs);
		double sin_delta = sin(delta);
		double cos_delta = cos(delta);
		double u1;
		double u = Radius;
		double v = 0.0;

		shaders = Shaders;
		appearance = material;
		LayerAppearance = material;
		cullFace = cullface;
		drawType = GL_TRIANGLES;

		vertices.push_back(vertex(center, glm::vec3(), color, glm::vec2(Radius, 0)));
		vertices.push_back(vertex(center + glm::vec3(0, 0, Height), glm::vec3(), color, glm::vec2(Radius, 0)));
		vertices.push_back(vertex(center + glm::vec3(Radius, 0, 0), glm::vec3(), color, glm::vec2(Radius, 0)));
		for (int i = 0; i < divs; ++i) {
			u1 = u * cos_delta - v * sin_delta;
			v = u * sin_delta + v * cos_delta;
			u = u1;
			int index = vertices.size();
			vertices.push_back(vertex(center + glm::vec3(u, v, 0), glm::vec3(), color, glm::vec2(u, v))); //index
			indices.push_back(0); indices.push_back(index); indices.push_back(index - 1);
			indices.push_back(1); indices.push_back(index - 1); indices.push_back(index);
		}
		drawCount = indices.size();
		Type = objType;
		Set(vertices, indices);

	}

}