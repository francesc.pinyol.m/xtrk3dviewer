(Work in progress)

In order to compile the project under Linux, some changes needed to be
done from the initial source code [Xtrk3DViewer_0_12_5_source.zip](https://sourceforge.net/projects/xtrk3dviewer/files/source/Xtrk3DViewer_0_12/Xtrk3DViewer_0_12_5_source.zip/download):

- replace backslashes by slashes:
```
sed -i '/^#include/ s*\\*/*g' */*.hpp
sed -i '/^#include/ s*\\*/*g' */*.h
sed -i '/^#include/ s*\\*/*g' */*.cpp
```

- replace references to the correct header files:
```
sed -i '/^#include/ s*interface.h*Interface.h*g' */*.{cpp,h,hpp}
sed -i '/^#include/ s*vertex.h*Vertex.h*g' */*.{cpp,h,hpp}
sed -i '/^#include/ s*Misc.h*misc.h*g' */*.{cpp,h,hpp}
sed -i '/^#include/ s*Trackplan.h*TrackPlan.h*g' */*.{cpp,h,hpp}
```

- fix other references to header files:
```
sed -i 's*#include "../../zip/zip_file.hpp"*#include "zip/zip_file.hpp"*g' ./Xtrk3D/PolygonSet.cpp
```

- add `AdditionalIncludeDirectories` in Xtrk3D/Xtrk3D.vcxproj so as 
  the compiler has the option `-I "../thirdparty"`

- rename the configuration: `Debug|x64` -> `LinuxDebug|x64`
```
sed -i 's/Debug|x64/LinuxDebug|x64/g' Xtrk3DViewer.sln
sed -i 's/Debug|x64/LinuxDebug|x64/g' */*.vcxproj
```

You must also install .NET and `dotnet` tool:

- [install .NET in Linux](https://learn.microsoft.com/en-us/dotnet/core/install/linux) and set the PATH:
```
echo 'export PATH="$HOME/.dotnet:${PATH}"' >>~/.bashrc
```

- install [dotnet-vcxproj](https://github.com/roozbehid/dotnet-vcxproj):
```
cd ~/src/
git clone https://github.com/roozbehid/dotnet-vcxproj.git
cd ~/src/dotnet-vcxproj
dotnet new install GCC.Build.Template
```

- prepare the
  compilation with dotnet cli:

```
cd ~/src/xtrk3dviewer/ # or the dir where you cloned this repo
dotnet new gccbuild
```
This will:
- modify:
  - `*/*.vcxproj`
- create:
  - `*/Microsoft.Cpp.Default.props`
  - `*/project.json`


All the previous steps are already present in code as it is in this
repo.

From there, you can build the project using dotnet:
```
dotnet build -v d -p:Configuration=LinuxDebug -p:Platform=x64
```
or clean it with:
```
dotnet clean -v d -p:Configuration=LinuxDebug -p:Platform=x64
```

Now we get some compilation errors... (to be continued)
