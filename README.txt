(This is a fork from https://sourceforge.net/projects/xtrk3dviewer/ 0_12_5)

See linux_build.md for instructions about building for Linux.

Xtrk3DViewer will create elevations based on XTrackCAD file data and will try not to skew turnouts.
Elevation values are calculated and may therefor differ from elevations in XTrackCad.
Slope is calculated at centerline and not shortest rail.

As with XTracCAD lengths and radius are two-dimensional.

Instalation:
Window users: 
	download .exe file at a directory of your choise
	for your convieniance, pin to start meny and/or taskbar.

Linux users:
	All source code except wxWidgets is included in zip.file. Code is not dependent on MS Windows

Usage:
Use keyboard and/or mouse to navigate, See help menu for commands
Elevations can be assign to Non-track objects, but changes are not saved.
Rightclick on object to get properties dialog.

It is possible to oberlay 3D objects such as .stl files on the layout, see user guide
