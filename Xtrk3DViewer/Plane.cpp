#include "Plane.h"
#include <algorithm>


Plane::Plane()
{
}


Plane::~Plane()
{
}

Plane::Plane(const glm::vec3 &Pt1, const glm::vec3 &Pt2, const glm::vec3 &Pt3)
{
	glm::vec3 Normal = glm::normalize(glm::cross(Pt2 - Pt1, Pt3 - Pt1));
	a = Normal.x;
	b = Normal.y;
	c = Normal.z;
	d = -glm::dot(Pt1, Normal);
}
Plane::Plane(const glm::vec3 &Pt, const glm::vec3 &Normal)
{
	glm::vec3 NormalizedNormal = glm::normalize(Normal);
	a = NormalizedNormal.x;
	b = NormalizedNormal.y;
	c = NormalizedNormal.z;
	d = -glm::dot(Pt, NormalizedNormal);
}

Plane::Plane(const std::vector<glm::vec3> Points)
{
	glm::vec3 Pt;
	glm::vec3 Normal;
	bool res = FindLLSQPlane(Points, &Pt, &Normal);
	glm::vec3 NormalizedNormal = glm::normalize(Normal);
	a = NormalizedNormal.x;
	b = NormalizedNormal.y;
	c = NormalizedNormal.z;
	d = -glm::dot(Pt, NormalizedNormal);
}

Plane Plane::normalize()
{
	Plane Result;
	float Distance = sqrtf(a * a + b * b + c * c);
	Result.a = a / Distance;
	Result.b = b / Distance;
	Result.c = c / Distance;
	Result.d = d / Distance;
	return Result;
}

float Plane::getZ(const float x, const float y, bool &Hit) const
{
//A plane stores its coefficients in the following form:
// A.x + B.y + C.z = D
//Converting this to z being a function of x and y:
// C.z =  D - A .x - B .y
//    z = -A/C.x - B/C.y + D/C

	Hit = true;
	if (fabs(c) < 0.001) {
		Hit = false;
		return 0;
	}
	return(-a* x - b* y + d) / c;
}

bool Plane::setZValue(glm::vec3& Pt)
{
	if (fabs(c) < 0.001) {
		return false;
	}
	Pt.z= (-a * Pt.x - b * Pt.y + d) / c;
	return true;
}

float Plane::signedDistance(const glm::vec3 &Pt) const
{
	return (a * Pt.x + b * Pt.y + c * Pt.z + d);
}

float Plane::unsignedDistance(const glm::vec3 &Pt) const
{
	return fabs(signedDistance(Pt));
}
glm::vec3 Plane::closestPoint(const glm::vec3 &Pt)
{
	return (Pt - getNormal() * signedDistance(Pt));
}
float Plane::dotCoord(const Plane &P, const glm::vec3 &Pt)
{
	return P.a * Pt.x + P.b * Pt.y + P.c * Pt.z + P.d;
}

float Plane::dotNormal(const Plane &P, const glm::vec3 &V)
{
	return P.a * V.x + P.b * V.y + P.c * V.z;
}
glm::vec3 Plane::intersectLine(const glm::vec3 &Pt1, const glm::vec3 &Pt2, bool &Hit) const
{
	Hit = true;
	glm::vec3 Diff = Pt2 - Pt1;
	float denominator = a * Diff.x + b * Diff.y + c * Diff.z;
	if (denominator == 0) { Hit = false; return Pt1; }
	float u = (a * Pt1.x + b * Pt1.y + c * Pt1.z + d) / denominator;

	return (Pt1 + u * (Pt2 - Pt1));
}
float Plane::intersectLineRatio(const glm::vec3 &Pt1, const glm::vec3 &Pt2, bool &Hit)
{
	Hit = true;
	glm::vec3 Diff = Pt2 - Pt1;
	float Denominator = a * Diff.x + b * Diff.y + c * Diff.z;
	if (Denominator == 0.0f)
	{
		Hit = false;
		return 0.0f;
	}
	return (a * Pt1.x + b * Pt1.y + c * Pt1.z + d) / -Denominator;
}


// From http://missingbytes.blogspot.com/2012/06/fitting-plane-to-point-cloud.html
float FindLargestEntry(const glm::mat3 &m) {
	float result = 0.0f;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			float entry = fabs(m[i][j]);
			result = std::max(entry, result);
		}
	}
	return result;
}
// From http://missingbytes.blogspot.com/2012/06/fitting-plane-to-point-cloud.html 
// note: This function will perform badly if the largest eigenvalue is complex
glm::vec3 FindEigenVectorAssociatedWithLargestEigenValue(const glm::mat3 &m) {
	//pre-condition
	float scale = FindLargestEntry(m);
	glm::mat3 mc = m * (1.0f / scale);
	mc = mc * mc;
	mc = mc * mc;
	mc = mc * mc;
	glm::vec3 v(1, 1, 1);
	glm::vec3 lastV = v;
	for (int i = 0; i < 100; i++) {
		v = glm::normalize(mc*v);
		if (glm::distance(v, lastV) < 1e-16f) {
			break;
		}
		lastV = v;
	}
	return v;
}
// From http://missingbytes.blogspot.com/2012/06/fitting-plane-to-point-cloud.html 
bool Plane::FindLLSQPlane(std::vector<glm::vec3> points, glm::vec3 *destCenter, glm::vec3 *destNormal) {
	if (points.size()<3)
		return false;

	glm::vec3 sum(0, 0, 0);
	for (auto &ii:points) {
		sum += ii;
	}
	glm::vec3 center = sum * (1.0f / points.size());
	if (destCenter!=nullptr) {
		*destCenter = center;
	}
	if (destNormal==nullptr) {
		return false;
	}
	float sumXX = 0.0f, sumXY = 0.0f, sumXZ = 0.0f;
	float sumYY = 0.0f, sumYZ = 0.0f;
	float sumZZ = 0.0f;
	for (auto &ii :points) {
		float diffX = ii.x - center.x;
		float diffY = ii.y - center.y;
		float diffZ = ii.z - center.z;
		sumXX += diffX * diffX;
		sumXY += diffX * diffY;
		sumXZ += diffX * diffZ;
		sumYY += diffY * diffY;
		sumYZ += diffY * diffZ;
		sumZZ += diffZ * diffZ;
	}
	glm::mat3 m(sumXX, sumXY, sumXZ, \
		sumXY, sumYY, sumYZ, \
		sumXZ, sumYZ, sumZZ);

	float det = glm::determinant(m);
	if (det == 0.0f) {
		*destNormal = Plane(points[0], points[1], points[2]).getNormal();
		return true;
	}
	glm::mat3 mInverse = glm::inverse(m);
	*destNormal = FindEigenVectorAssociatedWithLargestEigenValue(mInverse);
	return true;
}


void Plane::test() {
	std::vector<glm::vec3>points;
	points.push_back(glm::vec3(0, 0, 0));
	points.push_back(glm::vec3(15, 0, 6));
	points.push_back(glm::vec3(10, 5, 4));
	points.push_back(glm::vec3(0, 5, 0));
//	points.push_back(glm::vec3(5, 2, 3));
	glm::vec3 cg;
	glm::vec3 norm;
	bool res=FindLLSQPlane(points, &cg, &norm);
	Plane pl(cg, norm);
	bool hit;
	float z=pl.getZ(5, 2,hit);

	Plane p2(points);
	auto N = p2.getNormal();
}