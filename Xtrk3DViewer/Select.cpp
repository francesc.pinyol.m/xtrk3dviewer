#include <wx/wx.h>
#include "Select.h"
#include "ObjInfoDialog.h"
#include <sstream>
#include <iomanip>
#include <map>
#include <glm/glm/glm.hpp>
#include "../Xtrk3D/Interface.h"

Select::Select(const int id, const std::string obj, const bool metric, Zbase* baseInfo)
{
	std::string title = id < 0 ? obj : "T" + std::to_string(id) + " " + obj;
	selectDialog = new ObjInfoDialog(title, metric,baseInfo);
}

Select::Select(std::vector<SELECT_TYPE>& properties, bool metric,
	bool& status):selectDialog(nullptr)
{
	if (!properties.empty()) {
		Zbase base;
		if (properties.at(0).type == VariantType::isHeader) {
			std::map<wxTextCtrl*, glm::vec3*> hvalues;
			std::string title;
			if (properties.at(0).header.id > 0) {
				title = "T" + std::to_string(properties.at(0).header.id) + " " + properties.at(0).header.title;
			}
			else {
				title = "O" + std::to_string(-properties.at(0).header.id) + " " + properties.at(0).header.title;
			}
			int layerId = properties.at(0).header.layer;
			if (properties.at(0).hasBase) {
				base = Zbase(*properties.at(0).header.base_z_set, *properties.at(0).header.z);
				selectDialog = new ObjInfoDialog(title, metric, &base);
			}
			else {
				selectDialog = new ObjInfoDialog(title, metric, nullptr);
			}
			for (auto &ii : properties) {
				switch (ii.type)
				{
				case VariantType::isHeader:
					break;
				case VariantType::isLayer: {
					int color;
					bool visible;
					selectDialog->insert(ii.label, Xtrk3D::getLayer(layerId,color,visible));
					break;
				}
				case VariantType::isInt:
					selectDialog->insert(ii.label, ii.i);
					break;
				case VariantType::isDouble:
					selectDialog->insert(ii.label, ii.d);
					break;
				case VariantType::isAngle: {
					std::stringstream ss;
					ss << std::fixed << std::setprecision(0);
					ss << ii.d;
					ss << " degrees";
					selectDialog->insert(ii.label, ss.str());
					break;
				}
				case VariantType::isStr:
					selectDialog->insert(ii.label, ii.str);
					break;
				case VariantType::isVec3: {
					wxTextCtrl* h = selectDialog->insert(ii.label, *ii.point, ii.z_Editable);
					if (ii.z_Editable) {
						hvalues[h] = ii.point;
						//push back h and ii.point 
					}
					break;
				}
				case VariantType::isDelimiter: {
					selectDialog->insert("#"+std::to_string(ii.i) + ":", "");
					break;
				}
				case VariantType::isVector:
					//					selectDialog->insert(ii.label, *ii.pointlist, ii.z_Editable);
					break;
				}
			}
			status = showModal();
			//update properties with z values
			if (status) {
				for (auto &ii : hvalues) {
					auto f = selectDialog->getValuef(ii.first);
					ii.second->z = f;
				}
				if (properties.at(0).hasBase) {
					*properties.at(0).header.base_z_set = base.baseZset;
					*properties.at(0).header.z = base.baseZValue;
				}
				else {
					if (hvalues.empty())
						status = false; //no need to update object
				}
			}
		}
	}
	else {
		status = false;
	}
}


Select::~Select()
{
	if (selectDialog != nullptr) {
		delete selectDialog;
	}
}
//*
void Select::insert(const std::string Legend, const int value)
{
	selectDialog->insert(Legend, value);
}

int  Select::insert(const std::string Legend, const double value, bool z_editable)
{
	if (z_editable) {
		int key = handle.size();
		wxTextCtrl* t=selectDialog->insert(Legend, value, z_editable);
		handle.push_back(t);
		return key;// selectDialog->insert(Legend, value, z_editable);
	}
	else
		selectDialog->insert(Legend, value);
	return -1;
}

void Select::insert(const std::string Legend, const glm::vec2 value)
{
	selectDialog->insert(Legend, value);
}

int Select::insert(const std::string Legend, const glm::vec3 value, bool z_editable)
{
	int key = handle.size();
	wxTextCtrl* t = selectDialog->insert(Legend, value,z_editable);
	handle.push_back(t);
	return key;
}

void Select::insert(const std::string Legend, const std::string value)
{
	selectDialog->insert(Legend, value);
}
bool Select::showModal()
{
	return (selectDialog->ShowModal()==wxID_OK);
}

float Select::getValue(wxTextCtrl* pz)
{
	double Value;
	pz->GetValue().ToDouble(&Value);
	return Value;
}

float Select::getValue(int key)
{
	double Value;
	handle.at(key)->GetValue().ToDouble(&Value);
	return Value;
}


