#pragma once
#define GLM_FORCE_RADIANS
#include <vector>
#include <string>
#include <glm/glm/glm.hpp>


class ObjInfoDialog;
class wxTextCtrl;
class Zbase;

enum class VariantType { isUndef, isHeader, isInt, isDouble, isStr, isVec3, isVector,isLayer,isAngle,isDelimiter };

typedef struct SELECT_TYPE {
	SELECT_TYPE() :type(VariantType::isUndef), z_Editable(false), hasBase(false),label(nullptr), header({0,0,nullptr,false,nullptr}) {};
	VariantType type;
	const char* label;
	//int flags;
	bool z_Editable;
	bool hasBase;
	union {
		struct {
			int id;
			int layer;
			char* title;
			bool* base_z_set;
			double* z;
		} header;
		glm::vec3* point;
		std::vector<glm::vec3>* pointlist;
		const char* str;
		int i;
		double d;
	};
} SELECT;



class Select
{
public:
	Select(const int id,const std::string obj, const bool metric, Zbase* baseInfo=nullptr);
	Select(std::vector<SELECT_TYPE>& properties, bool metric, bool& status);
	~Select();
	void insert(const std::string Legend, const int value);
	int insert(const std::string Legend, const double value, bool z_editable = false);
	void insert(const std::string Legend, const glm::vec2 value);
	int insert(const std::string Legend, const glm::vec3 value, bool z_editable = true);
	void insert(const std::string Legend, const std::string value);
	float getValue(wxTextCtrl* pz);
	float getValue(int key);
	bool showModal();
	//int insertBaseZ(bool baseZisSet, const float value);
protected:
	ObjInfoDialog* selectDialog;
	std::vector< wxTextCtrl*> handle;
};

