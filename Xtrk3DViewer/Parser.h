#pragma once
#include <string>
#include <vector>
#include <cctype>
#include <functional>
#include <algorithm>
#include <map>
#include "../Xtrk3D/Interface.h"
//#include <glm\glm\glm.hpp>

enum xtrkType
{
	XtrkVersion,
	XtrkTitle1,
	XtrkTitle2,
	XtrkMapScale,
	XtrkRoomSize,
	XtrkScale,
	XtrkLayers,
	XtrkTurnout,
	XtrkJoint,
	XtrkCurve,
	XtrkStraight,
	XtrkTurntable,
	XtrkBezier,
	XtrkBzrlin,
	XtrkCornu,
	XtrkNote,
	XtrkBlock,
	XtrkControl,
	XtrkSensor,
	XtrkSignal,
	XtrkStructure,
	XtrkDraw,
	XtrkEnd,
	XtrkCurrentLayer,
	xtrkSegment,
	xtrkLineSegment,
	xtrkArcSegment,
	xtrkPolyLineSegment,
	xtrkFilledSegment,
	xtrkCircleSegment,
	xtrkTableEdgeSegment,
	xtrkBenchWorkSegment,
	xtrkTextSegment,
	XtrkCar,
	xtrkUndefined
};


class Parser
{
public:
	explicit Parser(std::istream* in);
	~Parser();
	std::string GetToken(unsigned int nr);
	xtrkType getNextToken(std::map <std::string, enum xtrkType>& TokenList);
	xtrkType getToken(unsigned int nr, std::map <std::string, enum xtrkType>& TokenList);

	std::string getString(){ return m_str; };

	bool parseNextRow();
	bool isInt(const std::string& tkn);
	bool isDouble(const std::string& tkn);
	int count() { return m_tknv.size(); };
	static std::map <std::string, enum xtrkType> XtrkToken;

protected:
	//trim leading spaces
	static inline std::string &ltrim(std::string &str) {
		str.erase(str.begin(), std::find_if(str.begin(), str.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return str;
	}

	// trim trailing spaces
	static inline std::string &rtrim(std::string &str) {
		str.erase(std::find_if(str.rbegin(), str.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), str.end());
		return str;
	}

	// trim from both ends
	static inline std::string &trim(std::string &str) {
		return ltrim(rtrim(str));
	}

	//replace all occurances of Old with New
	static inline std::string &replaceAll(std::string &str, const std::string &Old, const std::string &New) {
		std::string::size_type pos = 0;
		while ((pos = str.find(Old, pos)) != std::string::npos)
		{
			str.replace(pos, Old.length(), New);
			pos += New.length();
		}
		return str;
	}

private:
	std::string getNextToken();
	std::string m_str;
	size_t m_curpos;
	unsigned int m_curtkn;
	std::vector<std::string> m_tknv;
	bool parseNextToken(std::string& tkn, const std::string& delimiters);
	std::istream* instream;
	void tokenize();
};

//namespace Xtrk3D {
//	glm::vec3 _vec3(VEC3 v) { return glm::vec3(v.x, v.y, v.z); };
//}