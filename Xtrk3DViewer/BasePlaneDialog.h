#pragma once
#include "baseplaneMenu.h"
#include "../Xtrk3D/Interface.h"

class Xtrk3D::BasePlane;

class BasePlaneDialog :
	public BasePlaneMenu
{
public:
	BasePlaneDialog(Xtrk3D::BasePlane* table,bool metric);
	~BasePlaneDialog();
	void OnSliderChange(wxCommandEvent& event);
	int status;
};

