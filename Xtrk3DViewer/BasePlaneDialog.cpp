#include "BasePlaneDialog.h"
#include "../Xtrk3D/BasePlane.h"
#include "../Xtrk3D/misc.h"
#include <wx/valnum.h>

BasePlaneDialog::BasePlaneDialog(Xtrk3D::BasePlane* table,bool metric) :BasePlaneMenu(NULL, metric, -1, "BasePlane"), status(false)
{
	float z_value = (m_metric ? table->z * 2.54f : table->z);
	wxFloatingPointValidator<float>  val(4, &z_value);
	m_z_Ctrl->SetValidator(val);
	m_visible->SetValue(table->visible);
	m_transparancy->SetValue((int)(table->transparancy * 100));
	m_percentage->SetLabel(ConvertToStr(table->transparancy));
	m_color->SetColour(wxColour(table->color.x*255, table->color.y*255, table->color.z*255, 1));

	Bind(wxEVT_COMMAND_SLIDER_UPDATED, &BasePlaneDialog::OnSliderChange, this);

	status = ShowModal();
	if (status == wxID_OK) {
		table->z = (m_metric ? z_value / 2.54f : z_value);
		table->visible=m_visible->GetValue();
		table->transparancy = m_transparancy->GetValue() / 100.0;
		auto c= m_color->GetColour();
		table->color = glm::vec3(c.Red() / 255.0f, c.Green() / 255.0f, c.Blue() / 255.0f);
	}
}


BasePlaneDialog::~BasePlaneDialog()
{
}

void BasePlaneDialog::OnSliderChange(wxCommandEvent& event)
{
	m_percentage->SetLabel(ConvertToStr((m_transparancy->GetValue() / 100.0)));
}