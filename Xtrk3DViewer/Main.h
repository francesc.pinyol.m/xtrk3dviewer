#pragma once

#include <glew/include/GL/glew.h>

#include <wx/wx.h>
#include <wx/sizer.h>
#include <wx/glcanvas.h>
#include <list>
#include <vector>
#include <map>

// include OpenGL
#ifdef __WXMAC__
#include "OpenGL/glu.h"
#include "OpenGL/gl.h"
#else
#include <GL/glu.h>
#include <GL/gl.h>
#endif

#include <wx/wxprec.h>
#include <wx/docview.h>
#include <wx/choicdlg.h>

#include "OptionDialog.h"
#include <map>

class OpenGLWindow;

class MyFrame : public wxFrame
{
public:
	MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
	~MyFrame() {};
	void OnClose(wxCloseEvent& event);
	void ReadFile(wxString lpszPathName);

	OpenGLWindow * glPane;
	Options options;
	void dump();


//private:
	void OnHello(wxCommandEvent& event);
	void OnOpen(wxCommandEvent& event);
	void OnImport(wxCommandEvent& event);
	void OnExit(wxCommandEvent& event);
	void OnReset(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);
	void OnHelp(wxCommandEvent& event);
	void OnLayers(wxCommandEvent& event);
	void OnScaleDialog(wxCommandEvent& event);
	void OnOptionDialog(wxCommandEvent& event);
	void OnBenchworkDialog(wxCommandEvent& event);
	void OnBasePlaneDialog(wxCommandEvent& event);
	void OnMenu(wxCommandEvent& event);
	void OnDump(wxCommandEvent& event);
	DECLARE_EVENT_TABLE();

protected:
	wxDocManager* m_docManager;
	void ReadIniFile();
	void SaveIniFile(std::string inifile);
	std::string convert(float val, int precision = 2);

	std::vector<std::string>fileHistory;
	std::map<std::string, std::vector<float>>scales;
	std::string defaultScale;
};

class MyApp : public wxApp
{
public:
	MyApp():frame(nullptr) {};
	virtual bool OnInit();
protected:
	MyFrame* frame;
};

