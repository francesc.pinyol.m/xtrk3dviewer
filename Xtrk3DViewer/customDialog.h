#pragma once
#include <wx/wx.h>

class CustomDialog : public wxDialog
{
public:
	CustomDialog(const wxString& title);
private:
	void OnOk(wxCommandEvent &event);

};
