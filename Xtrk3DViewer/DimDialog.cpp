#include "DimDialog.h"
#include <sstream> 
#include <wx/valnum.h>
#include "../Xtrk3D/misc.h"
#include "../Xtrk3D/TrackPlan.h"

DimDialog::DimDialog(Xtrk3D::TrackPlan *Layout,std::string scale,bool isMetric)
	: wxDialog(NULL, -1, "", wxDefaultPosition, wxDefaultSize)
	, myLayout(Layout)
	, m_scale(scale)
	, m_metric(isMetric)
{
	wxPanel *panel = new wxPanel(this);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

	wxSizer *LeftPanelSizer = new wxFlexGridSizer(2);
	unitStr = new wxStaticText(panel, -1, Layout->metric ? wxT("[mm]") : wxT("[inch]"), wxPoint(300, 5));

	str.Add("-Scale-");
	int myScale(0);
	for (auto &ii : myLayout->Dimensions)
	{
		str.Add(ii.first);
		if (m_scale == ii.first)
			myScale = str.size()-1;
	}
	wxCh = new wxChoice(panel, -1, wxPoint(5, 5), wxSize(70, 30), str);
	wxCh->SetSelection(myScale);
	Bind(wxEVT_COMMAND_CHOICE_SELECTED, &DimDialog::OnSelectScale, this);
	LeftPanelSizer->Add(wxCh);
	LeftPanelSizer->AddSpacer(10);
	LeftPanelSizer->AddSpacer(10);
	LeftPanelSizer->Add(unitStr);

	tGauge = Insert(panel, LeftPanelSizer, std::string("Gauge"));
	tRailH = Insert(panel, LeftPanelSizer, std::string("Rail H"));
	tRailW = Insert(panel, LeftPanelSizer, std::string("Rail W"));

	embTopW = Insert(panel, LeftPanelSizer, std::string("Embankment top width"));
	embBotW = Insert(panel, LeftPanelSizer, std::string("Embankment bottom width"));
	embH = Insert(panel, LeftPanelSizer, std::string("Embankment Height"));

	tieH = Insert(panel, LeftPanelSizer, std::string("Tie Height"));
	tieW = Insert(panel, LeftPanelSizer, std::string("Tie Width"));
	tieL = Insert(panel, LeftPanelSizer, std::string("Tie Length"));
	tieCC = Insert(panel, LeftPanelSizer, std::string("Tie CC"));

	tWidth = Insert(panel, LeftPanelSizer, std::string("Tunnel Width"));
	tHeight = Insert(panel, LeftPanelSizer, std::string("Tunnel Height"));
	tExtend = Insert(panel, LeftPanelSizer, std::string("Width extend"));
	tLength= Insert(panel, LeftPanelSizer, std::string("Extend length"));

	panel->SetSizer(LeftPanelSizer);
	LeftPanelSizer->SetSizeHints(panel);

	wxButton *okButton = new wxButton(this, wxID_OK, wxT("Ok"), wxDefaultPosition, wxSize(70, 30));
	wxButton *closeButton = new wxButton(this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxSize(70, 30));
	vbox->Add(okButton, 1, wxLEFT, 5);
	vbox->Add(closeButton, 1, wxLEFT, 5);
	hbox->AddSpacer(20);
	hbox->Add(panel, 1);
	hbox->Add(vbox, 0, wxALIGN_TOP | wxTOP | wxBOTTOM, 10);
	//hbox->Add(vbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

	SetSizer(hbox);
	hbox->SetSizeHints(this);
	Centre();
	setScale();
	status = ShowModal();
	if (status == wxID_OK)
	{
		int res = wxCh->GetSelection();
		if (res > 0) {
			std::string ss(str[res].ToAscii());
			Xtrk3D::Scale *myScale = &myLayout->Dimensions[ss];
			double Value;
			tGauge->GetValue().ToDouble(&Value);  myScale->Param[0] = (float)Value * (m_metric ? 1.0f : 25.4f);
			tRailH->GetValue().ToDouble(&Value);  myScale->Param[2] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.RailH()));
			tRailW->GetValue().ToDouble(&Value);  myScale->Param[3] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.RailW()));
			tieL->GetValue().ToDouble(&Value);    myScale->Param[4] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.TieL()));
			tieH->GetValue().ToDouble(&Value);    myScale->Param[5] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.TieH()));
			tieW->GetValue().ToDouble(&Value);    myScale->Param[6] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.TieW()));
			tieCC->GetValue().ToDouble(&Value);   myScale->Param[7] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.TieCC()));
			embTopW->GetValue().ToDouble(&Value); myScale->Param[8] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.embTopW()));
			embBotW->GetValue().ToDouble(&Value); myScale->Param[9] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.embBotW()));
			embH->GetValue().ToDouble(&Value);    myScale->Param[10] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.embH()));
			tWidth->GetValue().ToDouble(&Value);  myScale->Param[11] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.MaxW()));
			tHeight->GetValue().ToDouble(&Value); myScale->Param[12] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.MaxH()));
			tExtend->GetValue().ToDouble(&Value); myScale->Param[13] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.ExtendW()));
			tLength->GetValue().ToDouble(&Value); myScale->Param[14] = (float)Value * (m_metric ? 1.0f : 25.4f); //myScale.ExtendL()));
		}
		else
			status = wxID_CANCEL;
	}
	Destroy();
}


DimDialog::~DimDialog()
{
}

wxTextCtrl* DimDialog::Insert(wxPanel* panel, wxSizer* Sizer, std::string legend)
{
	wxPanel *CL11 = new wxPanel(panel);
	wxSizer *CLsizer11 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer11->Add(new wxStaticText(CL11, -1, legend), wxALIGN_BOTTOM);
	CLsizer11->AddSpacer(10);
	CL11->SetSizer(CLsizer11);
	CLsizer11->SetSizeHints(CL11);
	Sizer->Add(CL11);

	wxPanel *CL12 = new wxPanel(panel);
	wxSizer *CLsizer12 = new wxBoxSizer(wxHORIZONTAL);
	wxTextCtrl* Value = new wxTextCtrl(CL12, -1, wxT(""), wxPoint(0, 0), wxSize(60, 20),0, wxFloatingPointValidator<float>());
	CLsizer12->Add(Value);
	CL12->SetSizer(CLsizer12);
	CLsizer12->SetSizeHints(CL12);
	Sizer->Add(CL12);
	return Value;
}

void DimDialog::OnSelectScale(wxCommandEvent &event)
{
	setScale();
}

void DimDialog::setScale()
{
	int res = wxCh->GetSelection();
	if (res >0) {
		std::string ss(str[res].ToAscii());
		Xtrk3D::Scale myScale = myLayout->Dimensions[ss];
		tGauge->SetValue(ConvertToStr(myScale.Param[0], m_metric ? 1.0f : 25.4f));// myScale.gauge()));
		tRailH->SetValue(ConvertToStr(myScale.Param[2], m_metric ? 1.0f : 25.4f)); //myScale.RailH()));
		tRailW->SetValue(ConvertToStr(myScale.Param[3], m_metric ? 1.0f : 25.4f)); //myScale.RailW()));
		tWidth->SetValue(ConvertToStr(myScale.Param[11], m_metric ? 1.0f : 25.4f)); //myScale.MaxW()));
		tHeight->SetValue(ConvertToStr(myScale.Param[12], m_metric ? 1.0f : 25.4f)); //myScale.MaxH()));
		tExtend->SetValue(ConvertToStr(myScale.Param[13], m_metric ? 1.0f : 25.4f)); //myScale.ExtendW()));
		tLength->SetValue(ConvertToStr(myScale.Param[14], m_metric ? 1.0f : 25.4f)); //myScale.ExtendL()));
		embTopW->SetValue(ConvertToStr(myScale.Param[8], m_metric ? 1.0f : 25.4f)); //myScale.embTopW()));
		embBotW->SetValue(ConvertToStr(myScale.Param[9], m_metric ? 1.0f : 25.4f)); //myScale.embBotW()));
		embH->SetValue(ConvertToStr(myScale.Param[10], m_metric ? 1.0f : 25.4f)); //myScale.embH()));
		tieH->SetValue(ConvertToStr(myScale.Param[5], m_metric ? 1.0f : 25.4f)); //myScale.TieH()));
		tieW->SetValue(ConvertToStr(myScale.Param[6], m_metric ? 1.0f : 25.4f)); //myScale.TieW()));
		tieL->SetValue(ConvertToStr(myScale.Param[4], m_metric ? 1.0f : 25.4f)); //myScale.TieL()));
		tieCC->SetValue(ConvertToStr(myScale.Param[7], m_metric ? 1.0f : 25.4f)); //myScale.TieCC()));
	}
	else {
		tGauge->SetValue("");
		tRailH->SetValue("");
		tRailW->SetValue("");
		tWidth->SetValue("");
		tHeight->SetValue("");
		tExtend->SetValue("");
		tLength->SetValue("");
		embTopW->SetValue("");
		embBotW->SetValue("");
		embH->SetValue("");
		tieH->SetValue("");
		tieW->SetValue("");
		tieL->SetValue("");
		tieCC->SetValue("");
	}
}
