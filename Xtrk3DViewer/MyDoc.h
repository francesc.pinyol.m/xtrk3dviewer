#pragma once

#include <vector>
#include <list>
#include <string>
#include <map>
#include <fstream>
#include "Parser.h"


class MyDoc
{
public:
	~MyDoc();
	explicit MyDoc(std::istream& in);
	Xtrk3D::OBJ_TYPE* objListHead;
	int size() {return _size;};
	std::string scale;
protected:
	int _size;
	Xtrk3D::OBJ_TYPE* objListTail;
	void insertObjList(Xtrk3D::OBJ_TYPE* obj);
	void insertEndPointList(Xtrk3D::OBJ_TYPE* obj, Xtrk3D::ENDPOINT_TYPE* endp);
	void insertSegmentList(Xtrk3D::OBJ_TYPE* obj, Xtrk3D::SEGMENT_TYPE* segmp);
	Xtrk3D::OBJ_TYPE* deleteXtrk3DObj(Xtrk3D::OBJ_TYPE* obj);
	void parseObject(std::istream& in, Xtrk3D::OBJ_TYPE* obj);
};

