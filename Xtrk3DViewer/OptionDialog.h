#pragma once
#include <wx/wx.h>
#include <string>

struct Options {
	bool colorByLayer;
	int tunnelMode;
	bool metric;
	bool drawStruct;
	bool drawElevations;
	float resolution;
	Options() :colorByLayer(false), tunnelMode(1), metric(false), drawStruct(false), drawElevations(true), resolution(2) {};
};

class OptionDialog : public wxDialog
{
public:
	OptionDialog(Options& options);
	~OptionDialog();
	int status;
private:
	wxTextCtrl *Insert(wxPanel* panel, wxSizer* Sizer, std::string legend);
};

