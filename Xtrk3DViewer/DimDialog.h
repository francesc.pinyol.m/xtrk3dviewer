#pragma once
#include <string>
#include <wx/wx.h>
#include "../Xtrk3D/Interface.h"
//#include "TrackPlan.h"

class Xtrk3D::TrackPlan;

class DimDialog : public wxDialog
{
public:
	DimDialog(Xtrk3D::TrackPlan *Layout, std::string scale, bool isMetric);
	~DimDialog();
	int status;

private:
	wxTextCtrl* Insert(wxPanel* panel, wxSizer* Sizer, std::string legend);
	void OnSelectScale(wxCommandEvent &event);
	void setScale();

	wxChoice* wxCh;
	wxArrayString str;

	wxTextCtrl *tGauge;
	wxTextCtrl *tRailH;
	wxTextCtrl *tRailW;
	wxTextCtrl *tWidth;
	wxTextCtrl *tHeight;
	wxTextCtrl *tExtend;
	wxTextCtrl *tLength;
	wxTextCtrl *embTopW;
	wxTextCtrl *embBotW;
	wxTextCtrl *embH;
	wxTextCtrl *tieH;
	wxTextCtrl *tieW;
	wxTextCtrl *tieL;
	wxTextCtrl *tieCC;

	wxStaticText* unitStr;

	Xtrk3D::TrackPlan* myLayout;
	std::string m_scale;
	bool m_metric;
};

