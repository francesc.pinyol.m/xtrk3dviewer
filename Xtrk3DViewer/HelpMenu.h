///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __HELPMENU_H__
#define __HELPMENU_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/dialog.h>
#include <wx/hyperlink.h>
#include <wx/statline.h>
#include <wx/scrolwin.h>
#include <wx/button.h>

///////////////////////////////////////////////////////////////////////////
static std::string Xtrk3DViewerVersion("0.12.5");

///////////////////////////////////////////////////////////////////////////////
/// Class HelpMenu
///////////////////////////////////////////////////////////////////////////////
class HelpMenu : public wxDialog
{
private:

protected:
	wxStaticText* m_KeyW;
	wxStaticText* m_static1;
	wxStaticText* m_KeyS;
	wxStaticText* m_static2;
	wxStaticText* m_KeyA;
	wxStaticText* m_static3;
	wxStaticText* m_KeyD;
	wxStaticText* m_static4;
	wxStaticText* m_KeyZ;
	wxStaticText* m_static5;
	wxStaticText* m_KeyC;
	wxStaticText* m_static6;
	wxStaticText* m_KeyQ;
	wxStaticText* m_static7;
	wxStaticText* m_KeyE;
	wxStaticText* m_static8;
	wxStaticText* m_LMB;
	wxStaticText* m_static20;
	wxStaticText* m_CMB;
	wxStaticText* m_static21;
	wxStaticText* m_RMB;
	wxStaticText* m_static22;
	wxStaticText* m_Wheel;
	wxStaticText* m_static23;
	wxStaticText* m_LMB_Dblclk;
	wxStaticText* m_static30;
	wxStaticText* m_RMB_Dblclk;
	wxStaticText* m_static31;
	wxStaticText* m_KeyW1;
	wxStaticText* m_staticText67;
	wxStaticText* m_KeyW2;
	wxStaticText* m_staticText68;
	wxStaticText* m_KeyW3;
	wxStaticText* m_staticText69;
	wxStaticText* m_Keypad7;
	wxStaticText* m_Keypad8;
	wxStaticText* m_static51;
	wxStaticText* m_Keypad9;
	wxStaticText* m_KeypadMinus;
	wxStaticText* m_static53;
	wxStaticText* m_Keypad4;
	wxStaticText* m_static54;
	wxStaticText* m_Keypad5;
	wxStaticText* m_Keypad6;
	wxStaticText* m_static56;
	wxStaticText* m_KeypadPlus;
	wxStaticText* m_static57;
	wxStaticText* m_Keypad1;
	wxStaticText* m_static58;
	wxStaticText* m_Keypad2;
	wxStaticText* m_static59;
	wxStaticText* m_Keypad3;
	wxStaticText* m_static60;
	wxStaticText* m_KeypadEnter;
	wxStaticText* m_KeyShift;
	wxStaticText* m_static40;
	wxStaticText* m_KeyCtrl;
	wxStaticText* m_static41;

public:

	HelpMenu(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
	~HelpMenu();

};

///////////////////////////////////////////////////////////////////////////////
/// Class About
///////////////////////////////////////////////////////////////////////////////
class About : public wxDialog
{
private:

protected:
	wxScrolledWindow* m_scrolledWindow1;
	wxStaticText* m_staticText6;
	wxStaticText* m_staticText53;
	wxHyperlinkCtrl* m_hyperlink1;
	wxStaticText* m_staticText1;
	wxStaticLine* m_staticline1;
	wxStaticText* m_staticText2;
	wxHyperlinkCtrl* m_hyperlink11;
	wxStaticLine* m_staticline2;
	wxStaticText* m_staticText3;
	wxStaticLine* m_staticline3;
	wxStaticText* m_staticText4;
	wxHyperlinkCtrl* m_hyperlink2;
	wxStaticLine* m_staticline4;
	wxStaticText* m_staticText5;
	wxButton* m_OK;

public:

	About(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE);
	~About();

};

#endif //__HELPMENU_H__
