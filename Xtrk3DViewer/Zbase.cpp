#include "Zbase.h"



Zbase::Zbase() :baseZset(false), baseZValue(0.0)
{
}

Zbase::Zbase(bool baseZ_IsSet, const float value)
{
	set(baseZ_IsSet, value); 
}

Zbase::~Zbase()
{
}

void Zbase::set(bool baseZ_IsSet, const float value)
{
	baseZset = baseZ_IsSet;
	baseZValue = baseZ_IsSet ? value : 0.0;
};
