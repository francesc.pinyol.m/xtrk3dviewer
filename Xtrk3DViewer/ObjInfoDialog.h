#pragma once
#include <wx/wx.h>
#include <vector>
#include <string>
#include <glm/glm/glm.hpp>
#include "Zbase.h"


class ObjInfoDialog :public wxDialog
{
public:
	ObjInfoDialog(const std::string title);
	ObjInfoDialog(const std::string title, const bool metric,Zbase* baseInfo=nullptr);
	virtual ~ObjInfoDialog();
	void insert(const std::string Legend, const std::string value);
	void insert(const std::string Legend, const int value);
	void insert(const std::string Legend, const glm::vec2 value);
	wxTextCtrl* insert(const std::string Legend, const double value, bool z_editable=false);
	wxTextCtrl* insert(const std::string Legend, const glm::vec3 value, bool z_editable);
	std::vector< wxTextCtrl*> insert(const std::string Legend, const std::vector<glm::vec3>& value, bool z_editable);
	std::string getValue(wxTextCtrl* w);
	float getValuef(wxTextCtrl* w);
	int ShowModal();
private:
	void insertLegend(const std::string Legend, int flag = wxALIGN_CENTER_VERTICAL);
	wxTextCtrl* insertFloat(const float value, wxPanel* m_vec3Panel, wxFlexGridSizer* vec3Sizer, bool z_editable);
	wxTextCtrl* insertVec3(const glm::vec3 point,  wxPanel* m_vec3Panel,  wxFlexGridSizer* vec3Sizer, bool z_editable);
	wxPanel* mainPanel;
	wxFlexGridSizer* mainSizer;
	wxPanel *leftPanel;
	wxFlexGridSizer* leftSizer;
	wxPanel* rightPanel;
	bool m_metric;
	void OnSelectBaseZ(wxCommandEvent &event);
	wxCheckBox* BaseZCheckedCtrl;
	wxTextCtrl* BaseZvalueCtrl;
	Zbase* BaseZtarget;
	bool BaseZisChecked;
};

