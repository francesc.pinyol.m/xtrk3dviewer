#pragma once
#include <glew/include/GL/glew.h>

#include <wx/wx.h>
#include <wx/sizer.h>
#include <wx/glcanvas.h>
#include <wx/choicdlg.h>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

// include OpenGL
#ifdef __WXMAC__
#include "OpenGL/glu.h"
#include "OpenGL/gl.h"
#else
#include <GL/glu.h>
#include <GL/gl.h>
#endif

#include <wx/wxprec.h>
#include <bitset>

#include "main.h"
#include "../Xtrk3D/Interface.h"

class OpenGLWindow : public wxGLCanvas
{
public:
	OpenGLWindow(MyFrame* parent, int* args);
	virtual ~OpenGLWindow();

	wxTimer* timer;

	// events
	void OnSize(wxSizeEvent& evt);
	void OnDraw(wxPaintEvent& evt);
	void OnMouseMove(wxMouseEvent& event);
	void OnLButtonDown(wxMouseEvent& event);
	void OnLButtonDblClick(wxMouseEvent& event);
	void OnRButtonDblClick(wxMouseEvent& event);
	void OnMouseHWheel(wxMouseEvent& event);
	void OnRButtonDown(wxMouseEvent& event);
	void OnMButtonDown(wxMouseEvent& event);
	void mouseLeftWindow(wxMouseEvent& event);
	void mouseEnterWindow(wxMouseEvent& event);
	void keyPressed(wxKeyEvent& event);
	void keyReleased(wxKeyEvent& event);
	void OnTimer(wxCommandEvent& event);

	DECLARE_EVENT_TABLE()

protected:
	MyFrame* Parent;
	wxGLContext* m_context;
	void CameraMove(float dx, float dy, float dz);
	void CameraRotate(float dx, float dy, float dz);
	std::bitset<32>UserCMDs;
	int MapKey(const int button);
	wxPoint m_downPoint;
	std::string focus;
	std::chrono::time_point<std::chrono::high_resolution_clock> T1;
	int countdown;
	std::string FrameRate;
	bool verbose;

};


