///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "HelpMenu.h"

///////////////////////////////////////////////////////////////////////////

HelpMenu::HelpMenu(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer(0, 2, 0, 0);
	fgSizer1->SetFlexibleDirection(wxBOTH);
	fgSizer1->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	wxStaticBoxSizer* sbSizerKeyBoard;
	sbSizerKeyBoard = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, wxT("Keyboard navigaton")), wxVERTICAL);

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer(0, 2, 0, 0);
	fgSizer2->SetFlexibleDirection(wxBOTH);
	fgSizer2->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	m_KeyW = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("W"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyW->Wrap(-1);
	fgSizer2->Add(m_KeyW, 0, wxALL, 5);

	m_static1 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Pan Up"), wxDefaultPosition, wxDefaultSize, 0);
	m_static1->Wrap(-1);
	fgSizer2->Add(m_static1, 0, wxALL, 5);

	m_KeyS = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("S"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyS->Wrap(-1);
	fgSizer2->Add(m_KeyS, 0, wxALL, 5);

	m_static2 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Pan Down"), wxDefaultPosition, wxDefaultSize, 0);
	m_static2->Wrap(-1);
	fgSizer2->Add(m_static2, 0, wxALL, 5);

	m_KeyA = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("A"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyA->Wrap(-1);
	fgSizer2->Add(m_KeyA, 0, wxALL, 5);

	m_static3 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Pan Left"), wxDefaultPosition, wxDefaultSize, 0);
	m_static3->Wrap(-1);
	fgSizer2->Add(m_static3, 0, wxALL, 5);

	m_KeyD = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("D"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyD->Wrap(-1);
	fgSizer2->Add(m_KeyD, 0, wxALL, 5);

	m_static4 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Pan Right"), wxDefaultPosition, wxDefaultSize, 0);
	m_static4->Wrap(-1);
	fgSizer2->Add(m_static4, 0, wxALL, 5);

	m_KeyZ = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Z"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyZ->Wrap(-1);
	fgSizer2->Add(m_KeyZ, 0, wxALL, 5);

	m_static5 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Yaw Clockwise (CW)"), wxDefaultPosition, wxDefaultSize, 0);
	m_static5->Wrap(-1);
	fgSizer2->Add(m_static5, 0, wxALL, 5);

	m_KeyC = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("C"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyC->Wrap(-1);
	fgSizer2->Add(m_KeyC, 0, wxALL, 5);

	m_static6 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Yaw Counterclockwise (CCW)"), wxDefaultPosition, wxDefaultSize, 0);
	m_static6->Wrap(-1);
	fgSizer2->Add(m_static6, 0, wxALL, 5);

	m_KeyQ = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Q"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyQ->Wrap(-1);
	fgSizer2->Add(m_KeyQ, 0, wxALL, 5);

	m_static7 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Move Forward"), wxDefaultPosition, wxDefaultSize, 0);
	m_static7->Wrap(-1);
	fgSizer2->Add(m_static7, 0, wxALL, 5);

	m_KeyE = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("E"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyE->Wrap(-1);
	fgSizer2->Add(m_KeyE, 0, wxALL, 5);

	m_static8 = new wxStaticText(sbSizerKeyBoard->GetStaticBox(), wxID_ANY, wxT("Move Back"), wxDefaultPosition, wxDefaultSize, 0);
	m_static8->Wrap(-1);
	fgSizer2->Add(m_static8, 0, wxALL, 5);


	sbSizerKeyBoard->Add(fgSizer2, 1, wxEXPAND, 5);


	fgSizer1->Add(sbSizerKeyBoard, 1, wxEXPAND | wxLEFT, 5);

	wxFlexGridSizer* fgMouse;
	fgMouse = new wxFlexGridSizer(0, 1, 0, 0);
	fgMouse->SetFlexibleDirection(wxBOTH);
	fgMouse->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	wxStaticBoxSizer* sbSizerMouse;
	sbSizerMouse = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, wxT("Mouse")), wxVERTICAL);

	wxStaticBoxSizer* sbSizerMove;
	sbSizerMove = new wxStaticBoxSizer(new wxStaticBox(sbSizerMouse->GetStaticBox(), wxID_ANY, wxT("Move")), wxVERTICAL);

	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer(0, 2, 0, 0);

	m_LMB = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("LMB pressed"), wxDefaultPosition, wxDefaultSize, 0);
	m_LMB->Wrap(-1);
	gSizer1->Add(m_LMB, 0, wxALL, 5);

	m_static20 = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("Turn Left/Right, Up/Down"), wxDefaultPosition, wxDefaultSize, 0);
	m_static20->Wrap(-1);
	gSizer1->Add(m_static20, 0, wxALL, 5);

	m_CMB = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("CMB pressed"), wxDefaultPosition, wxDefaultSize, 0);
	m_CMB->Wrap(-1);
	gSizer1->Add(m_CMB, 0, wxALL, 5);

	m_static21 = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("Yaw CW/CCW, Move Forward/Back"), wxDefaultPosition, wxDefaultSize, 0);
	m_static21->Wrap(-1);
	gSizer1->Add(m_static21, 0, wxALL, 5);

	m_RMB = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("RMB pressed"), wxDefaultPosition, wxDefaultSize, 0);
	m_RMB->Wrap(-1);
	gSizer1->Add(m_RMB, 0, wxALL, 5);

	m_static22 = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("Pan Left/Right, Up/Down"), wxDefaultPosition, wxDefaultSize, 0);
	m_static22->Wrap(-1);
	gSizer1->Add(m_static22, 0, wxALL, 5);

	m_Wheel = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("Wheel"), wxDefaultPosition, wxDefaultSize, 0);
	m_Wheel->Wrap(-1);
	gSizer1->Add(m_Wheel, 0, wxALL, 5);

	m_static23 = new wxStaticText(sbSizerMove->GetStaticBox(), wxID_ANY, wxT("Move Forward/Backward"), wxDefaultPosition, wxDefaultSize, 0);
	m_static23->Wrap(-1);
	gSizer1->Add(m_static23, 0, wxALL, 5);


	sbSizerMove->Add(gSizer1, 1, wxEXPAND, 5);


	sbSizerMouse->Add(sbSizerMove, 1, wxLEFT, 5);

	wxStaticBoxSizer* sbSizerSelect;
	sbSizerSelect = new wxStaticBoxSizer(new wxStaticBox(sbSizerMouse->GetStaticBox(), wxID_ANY, wxT("Select")), wxVERTICAL);

	wxGridSizer* gSizer2;
	gSizer2 = new wxGridSizer(0, 2, 0, 0);

	m_LMB_Dblclk = new wxStaticText(sbSizerSelect->GetStaticBox(), wxID_ANY, wxT("LMB dubble click"), wxDefaultPosition, wxDefaultSize, 0);
	m_LMB_Dblclk->Wrap(-1);
	gSizer2->Add(m_LMB_Dblclk, 0, wxALL, 5);

	m_static30 = new wxStaticText(sbSizerSelect->GetStaticBox(), wxID_ANY, wxT("Move to object"), wxDefaultPosition, wxDefaultSize, 0);
	m_static30->Wrap(-1);
	gSizer2->Add(m_static30, 0, wxALL, 5);

	m_RMB_Dblclk = new wxStaticText(sbSizerSelect->GetStaticBox(), wxID_ANY, wxT("RMB dubble click"), wxDefaultPosition, wxDefaultSize, 0);
	m_RMB_Dblclk->Wrap(-1);
	gSizer2->Add(m_RMB_Dblclk, 0, wxALL, 5);

	m_static31 = new wxStaticText(sbSizerSelect->GetStaticBox(), wxID_ANY, wxT("Object properties"), wxDefaultPosition, wxDefaultSize, 0);
	m_static31->Wrap(-1);
	gSizer2->Add(m_static31, 0, wxALL, 5);


	sbSizerSelect->Add(gSizer2, 1, wxALIGN_TOP | wxEXPAND, 5);


	sbSizerSelect->Add(0, 0, 1, wxEXPAND, 5);


	sbSizerMouse->Add(sbSizerSelect, 1, wxEXPAND | wxLEFT, 5);


	fgMouse->Add(sbSizerMouse, 1, wxALIGN_LEFT | wxEXPAND | wxLEFT, 5);


	fgSizer1->Add(fgMouse, 1, wxEXPAND | wxLEFT, 5);

	wxStaticBoxSizer* sbTrainMode;
	sbTrainMode = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, wxT("Train mode")), wxVERTICAL);

	wxGridSizer* gSizer5;
	gSizer5 = new wxGridSizer(0, 2, 0, 0);


	wxStaticText* m_KeyW;
	m_KeyW = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("RMB"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyW->Wrap(-1);
	gSizer5->Add(m_KeyW, 0, wxALL, 5);
	wxStaticText* m_staticTextA;
	m_staticTextA = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("Select start track"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticTextA->Wrap(-1);
	gSizer5->Add(m_staticTextA, 0, wxALL, 5);


	m_KeyW1 = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("T"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyW1->Wrap(-1);
	gSizer5->Add(m_KeyW1, 0, wxALL, 5);

	m_staticText67 = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("Enter Train mode"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText67->Wrap(-1);
	gSizer5->Add(m_staticText67, 0, wxALL, 5);

	m_KeyW2 = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("F"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyW2->Wrap(-1);
	gSizer5->Add(m_KeyW2, 0, wxALL, 5);

	m_staticText68 = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("Move Forward along track"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText68->Wrap(-1);
	gSizer5->Add(m_staticText68, 0, wxALL, 5);

	m_KeyW3 = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("R"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyW3->Wrap(-1);
	gSizer5->Add(m_KeyW3, 0, wxALL, 5);

	m_staticText69 = new wxStaticText(sbTrainMode->GetStaticBox(), wxID_ANY, wxT("Reverse Direction"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText69->Wrap(-1);
	gSizer5->Add(m_staticText69, 0, wxALL, 5);


	sbTrainMode->Add(gSizer5, 1, wxEXPAND, 5);


	fgSizer1->Add(sbTrainMode, 1, wxALIGN_TOP | wxEXPAND, 5);

	wxFlexGridSizer* fgKeyPad;
	fgKeyPad = new wxFlexGridSizer(0, 2, 0, 0);
	fgKeyPad->SetFlexibleDirection(wxBOTH);
	fgKeyPad->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	wxStaticBoxSizer* sbSizerKeypad;
	sbSizerKeypad = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, wxT("Keypad navigation")), wxVERTICAL);

	wxGridSizer* gSizer4;
	gSizer4 = new wxGridSizer(3, 4, 0, 0);

	wxBoxSizer* bSizer4_1;
	bSizer4_1 = new wxBoxSizer(wxVERTICAL);

	m_Keypad7 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("7"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad7->Wrap(-1);
	m_Keypad7->Enable(false);

	bSizer4_1->Add(m_Keypad7, 0, wxALL, 5);


	gSizer4->Add(bSizer4_1, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_2;
	bSizer4_2 = new wxBoxSizer(wxVERTICAL);

	m_Keypad8 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("8"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad8->Wrap(-1);
	bSizer4_2->Add(m_Keypad8, 0, wxALL, 5);

	m_static51 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Turn Up"), wxDefaultPosition, wxDefaultSize, 0);
	m_static51->Wrap(-1);
	bSizer4_2->Add(m_static51, 0, wxALL, 5);


	gSizer4->Add(bSizer4_2, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_3;
	bSizer4_3 = new wxBoxSizer(wxVERTICAL);

	m_Keypad9 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("9"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad9->Wrap(-1);
	m_Keypad9->Enable(false);

	bSizer4_3->Add(m_Keypad9, 0, wxALL, 5);


	gSizer4->Add(bSizer4_3, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_4;
	bSizer4_4 = new wxBoxSizer(wxVERTICAL);

	m_KeypadMinus = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("-"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeypadMinus->Wrap(-1);
	bSizer4_4->Add(m_KeypadMinus, 0, wxALL, 5);

	m_static53 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Back"), wxDefaultPosition, wxDefaultSize, 0);
	m_static53->Wrap(-1);
	bSizer4_4->Add(m_static53, 0, wxALL, 5);


	gSizer4->Add(bSizer4_4, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_5;
	bSizer4_5 = new wxBoxSizer(wxVERTICAL);

	m_Keypad4 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("4"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad4->Wrap(-1);
	bSizer4_5->Add(m_Keypad4, 0, wxALL, 5);

	m_static54 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Turn Left"), wxDefaultPosition, wxDefaultSize, 0);
	m_static54->Wrap(-1);
	bSizer4_5->Add(m_static54, 0, wxALL, 5);


	gSizer4->Add(bSizer4_5, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_6;
	bSizer4_6 = new wxBoxSizer(wxVERTICAL);

	m_Keypad5 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("5"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad5->Wrap(-1);
	m_Keypad5->Enable(false);

	bSizer4_6->Add(m_Keypad5, 0, wxALL, 5);


	gSizer4->Add(bSizer4_6, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_7;
	bSizer4_7 = new wxBoxSizer(wxVERTICAL);

	m_Keypad6 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("6"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad6->Wrap(-1);
	bSizer4_7->Add(m_Keypad6, 0, wxALL, 5);

	m_static56 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Turn Right"), wxDefaultPosition, wxDefaultSize, 0);
	m_static56->Wrap(-1);
	bSizer4_7->Add(m_static56, 0, wxALL, 5);


	gSizer4->Add(bSizer4_7, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_8;
	bSizer4_8 = new wxBoxSizer(wxVERTICAL);

	m_KeypadPlus = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("+"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeypadPlus->Wrap(-1);
	bSizer4_8->Add(m_KeypadPlus, 0, wxALL, 5);

	m_static57 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Forw"), wxDefaultPosition, wxDefaultSize, 0);
	m_static57->Wrap(-1);
	bSizer4_8->Add(m_static57, 0, wxALL, 5);


	gSizer4->Add(bSizer4_8, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_9;
	bSizer4_9 = new wxBoxSizer(wxVERTICAL);

	m_Keypad1 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("1"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad1->Wrap(-1);
	bSizer4_9->Add(m_Keypad1, 0, wxALL, 5);

	m_static58 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Yaw CW"), wxDefaultPosition, wxDefaultSize, 0);
	m_static58->Wrap(-1);
	bSizer4_9->Add(m_static58, 0, wxALL, 5);


	gSizer4->Add(bSizer4_9, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_10;
	bSizer4_10 = new wxBoxSizer(wxVERTICAL);

	m_Keypad2 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("2"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad2->Wrap(-1);
	bSizer4_10->Add(m_Keypad2, 0, wxALL, 5);

	m_static59 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Turn Down"), wxDefaultPosition, wxDefaultSize, 0);
	m_static59->Wrap(-1);
	bSizer4_10->Add(m_static59, 0, wxALL, 5);


	gSizer4->Add(bSizer4_10, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_11;
	bSizer4_11 = new wxBoxSizer(wxVERTICAL);

	m_Keypad3 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("3"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_Keypad3->Wrap(-1);
	bSizer4_11->Add(m_Keypad3, 0, wxALL, 5);

	m_static60 = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Yaw CCW"), wxDefaultPosition, wxDefaultSize, 0);
	m_static60->Wrap(-1);
	bSizer4_11->Add(m_static60, 0, wxALL, 5);


	gSizer4->Add(bSizer4_11, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer4_12;
	bSizer4_12 = new wxBoxSizer(wxVERTICAL);

	m_KeypadEnter = new wxStaticText(sbSizerKeypad->GetStaticBox(), wxID_ANY, wxT("Enter"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeypadEnter->Wrap(-1);
	m_KeypadEnter->Enable(false);

	bSizer4_12->Add(m_KeypadEnter, 0, wxALL, 5);


	gSizer4->Add(bSizer4_12, 1, wxEXPAND, 5);


	sbSizerKeypad->Add(gSizer4, 1, wxEXPAND, 5);


	fgKeyPad->Add(sbSizerKeypad, 1, wxEXPAND | wxLEFT, 5);


	fgSizer1->Add(fgKeyPad, 1, wxEXPAND | wxLEFT, 5);

	wxStaticBoxSizer* sbSizerSpecialKeys;
	sbSizerSpecialKeys = new wxStaticBoxSizer(new wxStaticBox(this, wxID_ANY, wxT("Special keys")), wxVERTICAL);

	wxGridSizer* gSizer3;
	gSizer3 = new wxGridSizer(0, 2, 0, 0);

	m_KeyShift = new wxStaticText(sbSizerSpecialKeys->GetStaticBox(), wxID_ANY, wxT("Shift"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyShift->Wrap(-1);
	gSizer3->Add(m_KeyShift, 0, wxALL, 5);

	m_static40 = new wxStaticText(sbSizerSpecialKeys->GetStaticBox(), wxID_ANY, wxT("Fast speed"), wxDefaultPosition, wxDefaultSize, 0);
	m_static40->Wrap(-1);
	gSizer3->Add(m_static40, 0, wxALL, 5);

	m_KeyCtrl = new wxStaticText(sbSizerSpecialKeys->GetStaticBox(), wxID_ANY, wxT("Ctrl"), wxDefaultPosition, wxSize(50, -1), wxALIGN_CENTRE | wxRAISED_BORDER);
	m_KeyCtrl->Wrap(-1);
	gSizer3->Add(m_KeyCtrl, 0, wxALL, 5);

	m_static41 = new wxStaticText(sbSizerSpecialKeys->GetStaticBox(), wxID_ANY, wxT("Slow speed"), wxDefaultPosition, wxDefaultSize, 0);
	m_static41->Wrap(-1);
	gSizer3->Add(m_static41, 0, wxALL, 5);


	sbSizerSpecialKeys->Add(gSizer3, 1, wxEXPAND, 5);


	sbSizerSpecialKeys->Add(0, 0, 1, wxEXPAND, 5);


	fgSizer1->Add(sbSizerSpecialKeys, 1, wxEXPAND | wxLEFT, 5);


	this->SetSizer(fgSizer1);
	this->Layout();
	fgSizer1->Fit(this);

	this->Centre(wxBOTH);
}

HelpMenu::~HelpMenu()
{
}

About::About(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxDialog(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);

	wxFlexGridSizer* fgSizer5;
	fgSizer5 = new wxFlexGridSizer(0, 2, 0, 0);
	fgSizer5->SetFlexibleDirection(wxBOTH);
	fgSizer5->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	m_scrolledWindow1 = new wxScrolledWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxDOUBLE_BORDER | wxHSCROLL | wxVSCROLL);
	m_scrolledWindow1->SetScrollRate(5, 5);
	m_scrolledWindow1->SetMinSize(wxSize(800, 500));

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer4;
	bSizer4 = new wxBoxSizer(wxVERTICAL);

	m_staticText6 = new wxStaticText(m_scrolledWindow1, wxID_ANY, wxT("Xtrk3DViewer "+Xtrk3DViewerVersion+"\n"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText6->Wrap(-1);
	auto F=m_staticText6->GetFont();
	F.MakeLarger();
	F.MakeBold();
	m_staticText6->SetFont(F);
	bSizer4->Add(m_staticText6, 0, wxALL, 5);
	bSizer1->Add(bSizer4, 1, wxEXPAND, 5);

	m_staticText53 = new wxStaticText(m_scrolledWindow1, wxID_ANY, wxT("Copyright (c) Mats Wikstrom 2018-2023\nFreeware for both personal and commercial use (Boost Software License)"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText53->Wrap(-1);
	bSizer1->Add(m_staticText53, 0, wxALL, 5);

	m_hyperlink1 = new wxHyperlinkCtrl(m_scrolledWindow1, wxID_ANY, wxT("http://www.boost.org/LICENSE_1_0.txt"), wxT("http://www.boost.org/LICENSE_1_0.txt"), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
	bSizer1->Add(m_hyperlink1, 0, wxALL, 5);

	m_staticText1 = new wxStaticText(m_scrolledWindow1, wxID_ANY, wxT("Boost Software License - Version 1.0 - August 17th, 2003\n\nPermission is hereby granted, free of charge, to any person or organization\nobtaining a copy of the software and accompanying documentation covered by\nthis license (the \"Software\") to use, reproduce, display, distribute,\nexecute, and transmit the Software, and to prepare derivative works of the\nSoftware, and to permit third-parties to whom the Software is furnished to\ndo so, all subject to the following:\n\nThe copyright notices in the Software and this entire statement, including\nthe above license grant, this restriction and the following disclaimer,\nmust be included in all copies of the Software, in whole or in part, and\nall derivative works of the Software, unless such copies or derivative\nworks are solely in the form of machine-executable object code generated by\na source language processor.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\nIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\nFITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT\nSHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE\nFOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,\nARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER\nDEALINGS IN THE SOFTWARE."), wxDefaultPosition, wxDefaultSize, 0 | wxRAISED_BORDER);
	m_staticText1->Wrap(-1);
	m_staticText1->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_ACTIVEBORDER));

	bSizer1->Add(m_staticText1, 0, wxALL, 5);

	m_staticline1 = new wxStaticLine(m_scrolledWindow1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
	bSizer1->Add(m_staticline1, 0, wxEXPAND | wxALL, 5);

	m_staticText2 = new wxStaticText(m_scrolledWindow1, wxID_ANY, wxT("Clipper library:  (Boost Software License) Copyright © 2010-2014 Angus Johnson)"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText2->Wrap(-1);
	bSizer1->Add(m_staticText2, 0, wxALL, 5);

	m_hyperlink11 = new wxHyperlinkCtrl(m_scrolledWindow1, wxID_ANY, wxT("http://www.angusj.com/delphi/clipper.php"), wxT("http://www.angusj.com/delphi/clipper.php"), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
	bSizer1->Add(m_hyperlink11, 0, wxALL, 5);

	m_staticline2 = new wxStaticLine(m_scrolledWindow1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
	bSizer1->Add(m_staticline2, 0, wxEXPAND | wxALL, 5);

	m_staticText3 = new wxStaticText(m_scrolledWindow1, wxID_ANY, wxT("Credit to Lee Thomason (www.grinninglizard.com) for tinyxml library\nThis software is provided as-is without any express or implied warranty. In no event will the authors\nbe held liable for any damages arising from the use of this software.\n"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText3->Wrap(-1);
	bSizer1->Add(m_staticText3, 0, wxALL, 5);

	m_staticline3 = new wxStaticLine(m_scrolledWindow1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
	bSizer1->Add(m_staticline3, 0, wxEXPAND | wxALL, 5);

	m_staticText4 = new wxStaticText(m_scrolledWindow1, wxID_ANY, wxT("mapbox/earcut: ISC License\nCopyright (c) 2015, Mapbox Permission to use, copy, modify, and/or distribute this software for any purpose\n with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.\nTHE SOFTWARE IS PROVIDED \"AS IS\" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH\nREGARD TO THIS SOFTWARE\nINCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR \nANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF\nUSE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF \nOR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE."), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText4->Wrap(-1);
	bSizer1->Add(m_staticText4, 0, wxALL, 5);

	m_hyperlink2 = new wxHyperlinkCtrl(m_scrolledWindow1, wxID_ANY, wxT("https://github.com/mapbox/earcut.hpp"), wxT("https://github.com/mapbox/earcut.hpp"), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE);
	bSizer1->Add(m_hyperlink2, 0, wxALL, 5);

	m_staticline4 = new wxStaticLine(m_scrolledWindow1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL);
	bSizer1->Add(m_staticline4, 0, wxEXPAND | wxALL, 5);

	m_staticText5 = new wxStaticText(m_scrolledWindow1, wxID_ANY, wxT("OpenGL Mathematics (GLM)  is licensed under The Happy Bunny License  (Modified MIT) and MIT License.\n\nThe OpenGL Extension Wrangler Library is originally derived from the EXTGL project by Lev Povalahev. \nThe source code is licensed under the Modified BSD License, the Mesa 3-D License (MIT) and the Khronos License (MIT)."), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText5->Wrap(-1);
	bSizer1->Add(m_staticText5, 0, wxALL, 5);


	m_scrolledWindow1->SetSizer(bSizer1);
	m_scrolledWindow1->Layout();
	bSizer1->Fit(m_scrolledWindow1);
	fgSizer5->Add(m_scrolledWindow1, 1, wxEXPAND | wxALL, 5);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	m_OK = new wxButton(this, wxID_OK, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer2->Add(m_OK, 0, wxALL, 5);


	fgSizer5->Add(bSizer2, 1, wxEXPAND, 5);


	this->SetSizer(fgSizer5);
	this->Layout();
	fgSizer5->Fit(this);

	this->Centre(wxBOTH);
}

About::~About()
{
}
