#include "OpenGLWindow.h"

BEGIN_EVENT_TABLE(OpenGLWindow, wxGLCanvas)

EVT_MOTION(OpenGLWindow::OnMouseMove)
EVT_LEFT_DOWN(OpenGLWindow::OnLButtonDown)
EVT_LEFT_DCLICK(OpenGLWindow::OnLButtonDblClick)
EVT_RIGHT_DOWN(OpenGLWindow::OnRButtonDown)
EVT_RIGHT_DCLICK(OpenGLWindow::OnRButtonDblClick)
EVT_MIDDLE_DOWN(OpenGLWindow::OnMButtonDown)
EVT_LEAVE_WINDOW(OpenGLWindow::mouseLeftWindow)
EVT_ENTER_WINDOW(OpenGLWindow::mouseEnterWindow)
EVT_SIZE(OpenGLWindow::OnSize)
EVT_KEY_DOWN(OpenGLWindow::keyPressed)
EVT_KEY_UP(OpenGLWindow::keyReleased)
EVT_MOUSEWHEEL(OpenGLWindow::OnMouseHWheel)
EVT_PAINT(OpenGLWindow::OnDraw)

END_EVENT_TABLE()

#define TimerValue 20 // refresh rate 20ms

enum KeyEvents {
	WXK_EVT_SHIFT = 0,
	WXK_EVT_CTRL = 1,
	WXK_EVT_UP = 4,
	WXK_EVT_DOWN = 5,
	WXK_EVT_LEFT = 6,
	WXK_EVT_RIGHT = 7,
	WXK_EVT_Z = 8,
	WXK_EVT_C = 9,
	WXK_EVT_A = 10,
	WXK_EVT_D = 11,
	WXK_EVT_S = 12,
	WXK_EVT_W = 13,
	WXK_EVT_Q = 14,
	WXK_EVT_E = 15,
	WXK_EVT_PLUS = 16,
	WXK_EVT_MINUS = 17,
	WXK_EVT_2=18,
	WXK_EVT_4 = 19,
	WXK_EVT_6 = 20,
	WXK_EVT_8 = 21,
	WXK_EVT_T = 22,
	WXK_EVT_F = 23,
	WXK_EVT_R = 24
};
//remember to change bit size of UserCMDs to match largest used keyEvent

std::string OpenGLLastError;

void GLAPIENTRY
MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* msg,
	const void* data)
{
	char* _source;
	char* _type;
	char* _severity;

	switch (source) {
	case GL_DEBUG_SOURCE_API:
		_source = "API";
		break;

	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		_source = "WINDOW SYSTEM";
		break;

	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		_source = "SHADER COMPILER";
		break;

	case GL_DEBUG_SOURCE_THIRD_PARTY:
		_source = "THIRD PARTY";
		break;

	case GL_DEBUG_SOURCE_APPLICATION:
		_source = "APPLICATION";
		break;

	case GL_DEBUG_SOURCE_OTHER:
		_source = "OTHER";
		break;

	default:
		_source = "UNKNOWN";
		break;
	}

	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		_type = "ERROR";
		break;

	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		_type = "DEPRECATED BEHAVIOR";
		break;

	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		_type = "UDEFINED BEHAVIOR";
		break;

	case GL_DEBUG_TYPE_PORTABILITY:
		_type = "PORTABILITY";
		break;

	case GL_DEBUG_TYPE_PERFORMANCE:
		_type = "PERFORMANCE";
		break;

	case GL_DEBUG_TYPE_OTHER:
		_type = "OTHER";
		break;

	case GL_DEBUG_TYPE_MARKER:
		_type = "MARKER";
		break;

	default:
		_type = "UNKNOWN";
		break;
	}

	switch (severity) {
	case GL_DEBUG_SEVERITY_HIGH:
		_severity = "HIGH";
		break;

	case GL_DEBUG_SEVERITY_MEDIUM:
		_severity = "MEDIUM";
		break;

	case GL_DEBUG_SEVERITY_LOW:
		_severity = "LOW";
		break;

	case GL_DEBUG_SEVERITY_NOTIFICATION:
		_severity = "NOTIFICATION";
		break;

	default:
		_severity = "UNKNOWN";
		break;
	}
	if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) {
		FILE* fp = fopen("Xtrk3DViewer_logfile.txt", "a");
		fprintf(fp, "id:%d, type:%s, severity: %s , raised from %s: [%s]\n", id, _type, _severity, _source, msg);
		fflush(fp);
		fclose(fp);
		OpenGLLastError=msg;
	}
}

int OpenGLWindow::MapKey(const int button)
{
	switch (button)
	{
	case WXK_SHIFT:
		return WXK_EVT_SHIFT;
	case WXK_CONTROL:
		return WXK_EVT_CTRL;
	case WXK_UP:
		return WXK_EVT_UP;
	case WXK_DOWN:
		return WXK_EVT_DOWN;
	case WXK_LEFT:
		return WXK_EVT_LEFT;
	case WXK_RIGHT:
		return WXK_EVT_RIGHT;
	case 'A':
		return WXK_EVT_A;
	case 'D':
		return WXK_EVT_D;
	case 'S':
		return WXK_EVT_S;
	case 'W':
		return WXK_EVT_W;
	case 'Q':
		return WXK_EVT_Q;
	case 'E':
		return WXK_EVT_E;
	case 'Z':
	case WXK_NUMPAD1:
		return WXK_EVT_Z;
	case 'C':
	case WXK_NUMPAD3:
		return WXK_EVT_C;
	case WXK_NUMPAD2:
		return WXK_EVT_2;
	case WXK_NUMPAD4:
		return WXK_EVT_4;
	case WXK_NUMPAD6:
		return WXK_EVT_6;
	case WXK_NUMPAD8:
		return WXK_EVT_8;
	case WXK_NUMPAD_ADD:
		return WXK_EVT_PLUS;
	case WXK_NUMPAD_SUBTRACT:
		return WXK_EVT_MINUS;
	case 'T':
		return WXK_EVT_T;
	case 'F':
		return WXK_EVT_F;
	case 'R':
		return WXK_EVT_R;
	default:
		return -1;
	}
}

// some useful events to use
void OpenGLWindow::OnMouseMove(wxMouseEvent& event)
{
	float delta = 0.1f;
	if (UserCMDs[WXK_EVT_SHIFT])
		delta /= 10.0f;
	else if (UserCMDs[WXK_EVT_CTRL])
		delta *= 10.0f;
	wxPoint p = event.GetPosition();

	if (event.LeftIsDown()) {
		float rx = (float)(p.x - m_downPoint.x)*delta;
		float ry = (float)(p.y - m_downPoint.y)*delta;
		CameraRotate(rx, ry,0);
		m_downPoint = p;
	}
	else if (event.RightIsDown()) {
		float rx = (float)(p.x - m_downPoint.x)*delta;
		float ry = (float)(p.y - m_downPoint.y)*delta;
		CameraMove(0, rx, ry);
		m_downPoint = p;
	}
	else if (event.MiddleIsDown()) {
		float rz = (float)(p.x - m_downPoint.x)*delta;
		float ry = (float)(p.y - m_downPoint.y)*delta;
		CameraRotate(0,0,rz);
		CameraMove(-10*ry,0,0);
		m_downPoint = p;
	}
}

void OpenGLWindow::OnLButtonDown(wxMouseEvent& event) {
	m_downPoint = event.GetPosition();
	SetFocus();
	focus=Xtrk3D::getObjInfoAtScreenCoordinates(m_downPoint.x, m_downPoint.y);
}

void OpenGLWindow::OnMouseHWheel(wxMouseEvent& event)
{
	if (!event.MiddleIsDown()) {
		float WheelSpeed((float)(event.GetWheelRotation()));
		if (UserCMDs[WXK_EVT_SHIFT])
			WheelSpeed /= 10.0f;
		else if (UserCMDs[WXK_EVT_CTRL])
			WheelSpeed *= 10.0f;
		CameraMove(WheelSpeed,0, 0);
	}
}

void OpenGLWindow::OnRButtonDown(wxMouseEvent& event)
{
	m_downPoint = event.GetPosition();
}

void OpenGLWindow::OnMButtonDown(wxMouseEvent& event)
{
	m_downPoint = event.GetPosition();
}

void OpenGLWindow::OnLButtonDblClick(wxMouseEvent& event)
{
	timer->Stop();
	wxPoint p = event.GetPosition();
	Xtrk3D::setCameraAt(p.x,p.y);
	timer->Start();
}

void OpenGLWindow::OnRButtonDblClick(wxMouseEvent& event)
{
	if (!IsShown())
		return;
	timer->Stop();
	wxPoint p = event.GetPosition();
	wxGLCanvas::SetCurrent(*m_context);

	std::vector<SELECT_TYPE> properties;
	auto obj = Xtrk3D::getPropertiesAtScreenCoordinates(p.x, p.y, properties);
	if (obj != nullptr) {
		bool status;
		Select Menu(properties, Parent->options.metric, status);
		if (status) {
			Xtrk3D::updatePropeties(obj);
		}
	}
	timer->Start();
}

void OpenGLWindow::mouseLeftWindow(wxMouseEvent& event)
{
	UserCMDs.reset(); 
}

void OpenGLWindow::mouseEnterWindow(wxMouseEvent& event)
{
	UserCMDs.reset();
	SetFocus();
}

void OpenGLWindow::keyPressed(wxKeyEvent& event) {
	int key(MapKey(event.GetKeyCode()));
	if (key>=0)
		UserCMDs.set(key);
}

void OpenGLWindow::keyReleased(wxKeyEvent& event) {
	int key = MapKey( event.GetKeyCode());
	if (key >= 0)
		UserCMDs.reset(key);
}

OpenGLWindow::OpenGLWindow(MyFrame* parent, int* args) :
	wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	,Parent(parent), countdown(-1), verbose(false)

{
	m_context = new wxGLContext(this);
	// Initialize GLEW
	wxGLCanvas::SetCurrent(*m_context);

	glewExperimental = GL_TRUE;
	if (GLEW_OK != glewInit())
	{
		// glewInit failed, something is seriously wrong.
		throw std::runtime_error("OpenGLWindow::glewInit failed");
		exit(-1);
	}

	//2023-09-27 -- check OpenGL Version
	GLint major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	if ((major < 4) || (major == 4 && minor < 4)) {
		throw std::runtime_error(std::string(" Minimum OpenGL Version required: 4.4, Your Hardware supports ") + std::to_string(major) + "." + std::to_string(minor));
	}

#ifdef _DEBUG
	FILE* fp1 = fopen("Xtrk3DViewer_verbose.txt", "w");
	fprintf(fp1, "OpenGL %d.%d, %s\n",major,minor, glGetString(GL_VERSION));
	fclose(fp1);
#endif
	FILE* fp = fopen("Xtrk3DViewer_logfile.txt", "w");
	fprintf(fp, "OpenGL %d.%d, %s\n", major, minor, glGetString(GL_VERSION));
	fclose(fp);
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, nullptr);

	//2023-09-27 end

	// To avoid flashing on MSW
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);

	timer = new wxTimer(this, 1);
	Connect(wxEVT_TIMER, wxCommandEventHandler(OpenGLWindow::OnTimer));
	timer->Start(TimerValue);
}

OpenGLWindow::~OpenGLWindow()
{
#ifdef _DEBUG
	FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
	fprintf(fp, "~OpenGLWindow()\n");
	fclose(fp);
#endif

	delete timer;
	delete m_context;
}

void OpenGLWindow::OnSize(wxSizeEvent& evt)
{
	Xtrk3D::prepareViewport(GetSize().x, GetSize().y);
	Refresh();
	SetFocus();
}

void OpenGLWindow::OnDraw(wxPaintEvent& evt)
{
	if (!IsShown()) return;

	auto T2 = Clock::now();
	float FPS = (1e9 / (T2 - T1).count());

#ifndef _DEBUG
	if (--countdown < 0) {
		countdown = (int)FPS;
		FrameRate = "|"+std::to_string(countdown) + " FPS| ";
		countdown /= 4;
	}
	Parent->SetStatusText(FrameRate+focus+OpenGLLastError);
#endif

	T1 = Clock::now();
	wxGLCanvas::SetCurrent(*m_context);
	int nodeCount = Xtrk3D::drawScene();

#ifdef _DEBUG
	char buffer[40];
	int n;
	n = sprintf(buffer, "%d scene obj. %.2f FPS|%s", nodeCount,FPS,OpenGLLastError.c_str());
	Parent->SetStatusText(std::string(buffer));
#endif

	SwapBuffers();
}

void OpenGLWindow::CameraMove(float dx, float dy, float dz)
{
	Xtrk3D::moveCamera(dx*0.05f, dy*0.05f, dz*0.05f);
}

void OpenGLWindow::CameraRotate(float dx, float dy, float dz)
{
	Xtrk3D::rotateCamera(dy*0.005f, dx*0.005f, dz*0.005);
}

void OpenGLWindow::OnTimer(wxCommandEvent& event)
{
	float WheelSpeed(2.0f);
	if (UserCMDs[WXK_EVT_SHIFT])
		WheelSpeed = 0.2f;
	else if (UserCMDs[WXK_EVT_CTRL])
		WheelSpeed = 20.0f;
	if (UserCMDs[WXK_EVT_UP] || UserCMDs[WXK_EVT_W])
		CameraMove(0, 0, 2 * WheelSpeed);
	if (UserCMDs[WXK_EVT_DOWN] || UserCMDs[WXK_EVT_S])
		CameraMove(0, 0, -2 * WheelSpeed);
	if (UserCMDs[WXK_EVT_LEFT]|| UserCMDs[WXK_EVT_A])
		CameraMove(0, 2 * WheelSpeed, 0);
	if (UserCMDs[WXK_EVT_RIGHT]|| UserCMDs[WXK_EVT_D])
		CameraMove(0, -2 * WheelSpeed, 0);
	if (UserCMDs[WXK_EVT_PLUS] || UserCMDs[WXK_EVT_Q])
		CameraMove(2 * WheelSpeed, 0, 0);
	if (UserCMDs[WXK_EVT_MINUS] || UserCMDs[WXK_EVT_E])
		CameraMove(-2 * WheelSpeed, 0, 0);
	if (UserCMDs[WXK_EVT_Z])
		CameraRotate(0,0,-WheelSpeed);
	if (UserCMDs[WXK_EVT_C])
		CameraRotate(0,0,WheelSpeed);

	if (UserCMDs[WXK_EVT_2])
		CameraRotate(0,-WheelSpeed,0);
	if (UserCMDs[WXK_EVT_4])
		CameraRotate(-WheelSpeed,0,0);
	if (UserCMDs[WXK_EVT_6])
		CameraRotate(WheelSpeed,0,0);
	if (UserCMDs[WXK_EVT_8])
		CameraRotate(0,WheelSpeed,0);

	if (UserCMDs[WXK_EVT_T]) {
		Xtrk3D::setTrainMode(true, m_downPoint.x, m_downPoint.y);
		focus = Xtrk3D::getTrainPos();
	}
	else 
		Xtrk3D::setTrainMode(false,0,0);

	if (UserCMDs[WXK_EVT_F]) {
		Xtrk3D::goTrainForward(WheelSpeed / 16, verbose);
		focus = Xtrk3D::getTrainPos();
	}

	Xtrk3D::setTrainReverse(UserCMDs[WXK_EVT_R]);

	Refresh();
}
