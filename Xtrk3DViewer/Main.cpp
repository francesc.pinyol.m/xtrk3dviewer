// wxWidgets port of Xtrk3DViewer
// For compilers that support precompilation, includes "wx/wx.h".
#include "Main.h"

//#include "misc.h"
#include "OpenGLWindow.h"

#include "DimDialog.h"
#include <wx/stdpaths.h>
#include <wx/stdstream.h> //
#include <wx/filename.h>
#include <wx/wfstream.h> //
#include <wx/zipstrm.h> //
#include <niohman_json/json.hpp>
using json = nlohmann::json; // for convenience

#include "ObjInfoDialog.h"
#include "HelpMenu.h"
#include "Select.h"
#include "BasePlaneDialog.h"
#include "xtrk3dview16.xpm"
#include "xtrk3dview32.xpm"
#include "xtrk3dview48.xpm"
#include <sstream>
#include <iomanip>
#include "tinyxml2.h"

#include "Mydoc.h"
#include "../Xtrk3D/TrackPlan.h"
#include "../Xtrk3D/misc.h"
#include "../Xtrk3D/ElevationGrid.h"


enum IDs
{
	ID_Hello = 1100
	,ID_ManageLayers=1101
	,ID_ScaleDialog=1102
	,ID_OptionDialog=1103
	,ID_ResetView=1104
	,ID_ViewHelp =1105
	,ID_BenchWorkDialog =1106
	,ID_BasePlaneDialog =1107
	,ID_Dump=1108
	,ID_Import=1109
};

// Add the event handlers for the menu items
// to the event table.
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_CLOSE(MyFrame::OnClose)
EVT_MENU(ID_Hello, MyFrame::OnHello)
EVT_MENU(wxID_EXIT, MyFrame::OnExit)
EVT_MENU(wxID_OPEN, MyFrame::OnOpen)
EVT_MENU(wxID_ABOUT, MyFrame::OnAbout)
EVT_MENU(ID_ViewHelp, MyFrame::OnHelp)
EVT_MENU(ID_ManageLayers, MyFrame::OnLayers)
EVT_MENU(ID_ResetView, MyFrame::OnReset)
EVT_MENU(ID_ScaleDialog, MyFrame::OnScaleDialog)
EVT_MENU(ID_OptionDialog, MyFrame::OnOptionDialog)
EVT_MENU(ID_BenchWorkDialog, MyFrame::OnBenchworkDialog)
EVT_MENU(ID_BasePlaneDialog, MyFrame::OnBasePlaneDialog)
EVT_MENU(ID_Dump, MyFrame::OnDump)
EVT_MENU(ID_Import, MyFrame::OnImport)
EVT_MENU(wxID_ANY, MyFrame::OnMenu) //catch wxID_FILE1 -- wxID_FILE9 (recent files)
END_EVENT_TABLE()
wxIMPLEMENT_APP(MyApp);

bool MyApp::OnInit()
{

	frame = new MyFrame("Xtrk3DViewer "+ Xtrk3DViewerVersion, wxPoint(50, 50), wxSize(600, 400));

	// Set the frame's icons
	wxBitmap bmp(xtrk3dview16_xpm);
	wxIcon icon;
	icon.CopyFromBitmap(bmp);
	wxIconBundle iconBundle(icon);
	bmp = wxBitmap(xtrk3dview32_xpm);
	icon.CopyFromBitmap(bmp);
	iconBundle.AddIcon(icon);
	bmp = wxBitmap(xtrk3dview48_xpm);
	icon.CopyFromBitmap(bmp);
	iconBundle.AddIcon(icon);
	frame->SetIcons(iconBundle);
	frame->Show(true);
	return true;
}

MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
	: wxFrame(NULL, wxID_ANY, title, pos, size)
{
	try {
		wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
		int args[] = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0 };
		glPane = new OpenGLWindow(this, args);
		sizer->Add(glPane, 1, wxEXPAND);
		SetSizer(sizer);
		SetAutoLayout(true);
		Xtrk3D::init();
		ReadIniFile();

		wxMenu *menuFile = new wxMenu;
		menuFile->AppendSeparator();
		menuFile->Append(wxID_OPEN, "&Open", "Open File");
		menuFile->Append(ID_Import, "&Import Overlay", "Import a json file");

		menuFile->Append(wxID_EXIT);



		wxMenu *menuView = new wxMenu;
		menuView->Append(ID_ResetView, "reset\tCtrl-R", "reset view");

		wxMenu *menuManage = new wxMenu;
		menuManage->Append(ID_ManageLayers, "&Layers...\tCtrl-L", "manage Layers");
		menuManage->Append(ID_ScaleDialog, "Parameters...", "Scale Parameters");
		menuManage->Append(ID_BenchWorkDialog, "BenchWork...", "Benchwork properties");
		menuManage->Append(ID_BasePlaneDialog, "Baseplan...\tCtrl-B", "Baseplan properties");

		wxMenu *menuPreferences = new wxMenu;
		menuPreferences->Append(ID_OptionDialog, "Display...\tCtrl-D", "Display options");

		wxMenu *menuHelp = new wxMenu;
		menuHelp->Append(ID_ViewHelp, "View Commands\tCtrl-C", "View Commands");
		menuHelp->Append(wxID_ABOUT);

		wxMenuBar *menuBar = new wxMenuBar;
		menuBar->Append(menuFile, "&File");
		menuBar->Append(menuView, "&View");
		menuBar->Append(menuManage, "&Manage");
		menuBar->Append(menuPreferences, "&Options");
		menuBar->Append(menuHelp, "&Help");

#ifdef _DEBUG
		wxMenu* menuHello = new wxMenu;
		menuHello->Append(ID_Hello, "&Hello...\tCtrl-H",
			"Help string shown in status bar for this menu item");
		menuHello->Append(ID_Dump, "&Dump", "Dump Trackplan to json file");
//		menuHello->Append(ID_Import, "&Import\tCtrl-H", "Import a json file");
		menuBar->Append(menuHello, "&Debug");
#endif

		SetMenuBar(menuBar);
		CreateStatusBar();

		m_docManager = new wxDocManager;
		m_docManager->FileHistoryUseMenu(menuFile);

		auto rit = fileHistory.rbegin();
		for (; rit != fileHistory.rend(); ++rit) {
			m_docManager->AddFileToHistory(*rit);
		}
		SetStatusText("Xtrk3DViewer");
	}
	catch (std::exception& e)
	{
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			e.what(), wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;
		exit(-1);
	}
}

using namespace tinyxml2;

void MyFrame::ReadIniFile()
{
	wxStandardPaths* paths = &wxStandardPaths::Get();
	//	wxString cnfg = paths->GetUserConfigDir();
	std::string userDataDir = paths->GetUserDataDir();
	//std::string exeDir = paths->GetExecutablePath();
	std::string resourceDir = paths->GetResourcesDir();

	wxFileName s(userDataDir, "Xtrk3DViewer", "xml");
	std::string inifile;
	inifile = s.GetFullPath();
	if (!wxFileExists(inifile))
	{
		wxFileName s(resourceDir, "Xtrk3DViewer", "xml");
		inifile = s.GetFullPath();
		if (!wxFileExists(inifile))
			return;
	}

	tinyxml2::XMLDocument doc;
	doc.LoadFile(inifile.c_str());
	if (!doc.Error())
	{
		auto t = doc.FirstChild();
		XMLNode* body = doc.FirstChildElement("X3dViewer");
		if (body) {
			XMLNode* preferences = body->FirstChildElement("preferences");
			if (preferences)
			{
				preferences->ToElement()->QueryBoolAttribute("metric", &options.metric);
				preferences->ToElement()->QueryBoolAttribute("DrawStruct", &options.drawStruct);
				preferences->ToElement()->QueryBoolAttribute("ColorByLayer", &options.colorByLayer);
				preferences->ToElement()->QueryBoolAttribute("Elevations", &options.drawElevations);
				if (preferences->ToElement()->Attribute("tunnelStyle", "full")) options.tunnelMode = 2;
				else if (preferences->ToElement()->Attribute("tunnelStyle", "sides")) options.tunnelMode = 1;
				else if (preferences->ToElement()->Attribute("tunnelStyle", "off")) options.tunnelMode = 0;
				if (preferences->ToElement()->Attribute("resolution")) {
					preferences->ToElement()->QueryFloatAttribute("resolution", &options.resolution);
				}
				Xtrk3D::setOptions(options.colorByLayer, options.tunnelMode, options.metric, options.drawStruct, options.drawElevations, options.resolution);
			}
			XMLNode* dimensions = body->FirstChildElement("dimensions");
			if (dimensions)
			{
				XMLNode* dimension = dimensions->FirstChildElement("dimension");
				while (dimension)
				{
					std::string scale(dimension->ToElement()->Attribute("scale"));
					XMLNode* rail = dimension->FirstChildElement("rail");
					XMLNode* embankment = dimension->FirstChildElement("embankment");
					XMLNode* sleeper = dimension->FirstChildElement("sleeper");
					XMLNode* clearance = dimension->FirstChildElement("clearance");

					//Gauge	ratio	RailH	RailW	TieL	TieH	TieW	TieCC	embTop	embBot	embH	B		H		E	L
					std::vector<float> param;
					float value;
					rail->ToElement()->QueryFloatAttribute("gauge", &value);
					param.push_back(value);
					dimension->ToElement()->QueryFloatAttribute("ratio", &value);
					param.push_back(value);//ratio -- not used
					rail->ToElement()->QueryFloatAttribute("height", &value);
					param.push_back(value);
					rail->ToElement()->QueryFloatAttribute("width", &value);
					param.push_back(value);

					sleeper->ToElement()->QueryFloatAttribute("length", &value);
					param.push_back(value);
					sleeper->ToElement()->QueryFloatAttribute("height", &value);
					param.push_back(value);
					sleeper->ToElement()->QueryFloatAttribute("width", &value);
					param.push_back(value);
					sleeper->ToElement()->QueryFloatAttribute("cc", &value);
					param.push_back(value);

					embankment->ToElement()->QueryFloatAttribute("topWidth", &value);
					param.push_back(value);
					embankment->ToElement()->QueryFloatAttribute("bottomWidth", &value);
					param.push_back(value);
					embankment->ToElement()->QueryFloatAttribute("height", &value);
					param.push_back(value);

					clearance->ToElement()->QueryFloatAttribute("width", &value);
					param.push_back(value);
					clearance->ToElement()->QueryFloatAttribute("height", &value);
					param.push_back(value);
					clearance->ToElement()->QueryFloatAttribute("extendedWidth", &value);
					param.push_back(value);
					clearance->ToElement()->QueryFloatAttribute("extendedLength", &value);
					param.push_back(value);

					const char* strp = scale.c_str();
					Xtrk3D::defineScaleParameters(strp, param.data());
					scales[scale] = param;

					dimension = dimension->ToElement()->NextSibling();
				}
			}
			XMLNode* history = body->FirstChildElement("filelist");
			if (history) {
				XMLNode* file = history->FirstChildElement("file");
				while (file) {
					fileHistory.push_back(file->FirstChild()->Value());
					file = file->ToElement()->NextSibling();
				}
			}
		}
	}
}


/////////////////////////////////////
// Event handlers for the menu items.
/////////////////////////////////////
void MyFrame::OnClose(wxCloseEvent& event)
{
	Xtrk3D::clearScene();
	if (m_docManager->GetFileHistory()) {
		fileHistory.clear();
		unsigned int last(m_docManager->GetHistoryFilesCount());
		for (unsigned int i = 0; i < last; ++i) {
			std::string str = m_docManager->GetFileHistory()->GetHistoryFile(i);
			fileHistory.push_back(str);
		}
		delete m_docManager;
	}
	wxStandardPaths* paths = &wxStandardPaths::Get();
	wxString resource = paths->GetUserDataDir();
	if (!wxDirExists(resource)) {
		bool stat = wxMkdir(resource);
	}
	wxFileName s(resource, "Xtrk3DViewer", "xml");
	std::string inifile = s.GetFullPath();
	SaveIniFile(inifile);
	Xtrk3D::close();
	Destroy();

#ifdef _DEBUG
	FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
	fprintf(fp, "MyFrame::OnClose()\n");
	fclose(fp);
#endif

}

void MyFrame::OnExit(wxCommandEvent& event)
{
	Close(true);
}

void MyFrame::OnAbout(wxCommandEvent& event)
{
	glPane->timer->Stop();
	About ViewAbout(this,-1,"About Xtrk3DViewer");
	ViewAbout.ShowModal();
	glPane->timer->Start();
}

void MyFrame::OnHelp(wxCommandEvent& event)
{
	glPane->timer->Stop();
	HelpMenu ViewHelp(this,-1,"Commands");
	ViewHelp.ShowModal();
	glPane->timer->Start();
}

void MyFrame::OnDump(wxCommandEvent& event)
{
	dump();
}

#ifdef _DEBUG
#include "../Xtrk3D/PolygonSet.h"
void MyFrame::OnHello(wxCommandEvent& event)
{
	try {
		wxFileDialog openFileDialog(this, _("Open file"), "", "",
			"STL/Obj file (*.stl,*.obj)|*.stl;*.obj;*.zip|All files (*.*)|*.*", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
		if (openFileDialog.ShowModal() == wxID_OK) {
			std::string Rf(openFileDialog.GetPath());
			Xtrk3D::PolygonSet* poly = new Xtrk3D::PolygonSet;
			wxBusyCursor Busy;
			poly->readFile(Rf, glm::mat4(),(1.0f/25.4f));
			Xtrk3D::XTrkObj* Shape = new  Xtrk3D::Draw(0, 0, glm::vec3(), 0);
			Shape->addSegment(poly);
			Xtrk3D::getTrackplan()->Objects.push_back(Shape);
			Xtrk3D::init();
		}
	}
	catch (const std::exception& e) {
		std::string msg(e.what());
		wxMessageDialog* dial = new wxMessageDialog(NULL,
			wxT("OnImport: " + msg), wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;
	}
	catch (...) {
		wxMessageDialog* dial = new wxMessageDialog(NULL,
			wxT("Failed to parse file"), wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;
	}
	return;
	//*/
	/*
	ObjInfoDialog info("test",true);
	glm::vec3 p0(1, 2, 3);
	glm::vec3 p1(10, 20, 30);
	glm::vec3 p2(4, 4, 0);
	std::vector<glm::vec3>points;
	points.push_back(p0);
	points.push_back(p1);
	points.push_back(p2);

	info.insert("Layer", 1);
	info.insert("width", 2.0f);
	info.insert("point 1", p0,true);
	info.insert("point 2", p1,false);
	info.insert("Label", "this is a small text for one line but a gigant line for beeing a label");
//	info.insert("points", points,true);
	info.ShowModal();//*/
	/*
	wxStandardPaths path= wxStandardPaths::Get();
	wxString str = "Exe: " + path.GetExecutablePath() + "\nData: " + path.GetDataDir() + "\nConfig; " + path.GetConfigDir() + "\nDocs: " + path.GetAppDocumentsDir();
	wxLogMessage(str); //*/
}
#else
void MyFrame::OnHello(wxCommandEvent& event) {}
#endif

void MyFrame::OnOpen(wxCommandEvent& event)
{
	glPane->timer->Stop();
	// Create a new wxFileDialog dialog
	try {
		wxFileDialog openFileDialog(this, _("Open XTrackCad file"), "", "",
			"XTrackCad Trackplan (*.xtc, *.xtce)|*.xtc;*.xtce|All files (*.*)|*.*", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
		if (openFileDialog.ShowModal() == wxID_OK) {
			ReadFile(openFileDialog.GetPath());
		}
	}
	catch (...) {
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			wxT("Fail to read file"), wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;

	}
	glPane->timer->Start();
}

void MyFrame::OnImport(wxCommandEvent& event)
{
	glPane->timer->Stop();
	try {
		wxFileDialog openFileDialog(this, _("Open json file"), "", "",
			"Track elements (*.json|*.json|All files (*.*)|*.*", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
		if (openFileDialog.ShowModal() == wxID_OK) {
			std::ifstream ifs;
			wxBusyCursor Busy;
			std::string fdir(openFileDialog.GetDirectory());
			ifs.open(openFileDialog.GetPath().mb_str(), std::ifstream::in|std::ifstream::binary);

			std::stringstream ss;
			while (ifs.good()) {
				char x(ifs.get());
				if (x == '\\') {
					ss << x;
				}
				ss << x;
			}

			json j;
			ss >> j;
			ifs.close();
			double scaleFactor(1.0);
			if (j.contains("Units"))
			{
				auto units = j["Units"].get<std::string>();
				if (units == "inch")
					scaleFactor = 1.0;
				else if (units == "feet")
					scaleFactor = 12;
				else if (units == "mm")
					scaleFactor = 1 / 25.4;
				else if (units == "cm")
					scaleFactor = 1 / 2.54;
				else if (units == "dm")
					scaleFactor = 10 / 2.54;
				else if (units == "m")
					scaleFactor = 100 / 2.54;
				else {
					wxMessageDialog *dial = new wxMessageDialog(NULL,
						wxT("unrecognized unit: "+ units), wxT("Overlay Error"), wxOK | wxICON_ERROR);
					dial->ShowModal();
					delete dial;
					glPane->timer->Start();
					return;
				}
			}

			Xtrk3D::Overlay O(scaleFactor, Xtrk3D::getTrackplan());
			O.setDefaultDir(fdir);
			//std::map<std::string, Xtrk3D::XTrkObj*>DefMap;
			if (j.contains("Define"))
			{
				O.ParseDefine(j["Define"]);
			}

			if (j.contains("Group"))
			{
				j = j["Group"];
			}

			if (j.contains("Overlay"))
			{
				j = j["Overlay"];
			}

			O.parseOverlay(j);
			Xtrk3D::init();
		}
	}
	catch (const std::exception& e) {
		std::string msg(e.what());
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			wxT("OnImport: "+msg), wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;
	}
	catch (...) {
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			wxT("Failed to parse file"), wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;
	}
	glPane->timer->Start();
}

void MyFrame::OnReset(wxCommandEvent& event)
{
	Xtrk3D::resetScene();
}

void MyFrame::OnLayers(wxCommandEvent& event)
{
	glPane->timer->Stop();
	Xtrk3D::TrackPlan* myLayout = Xtrk3D::getTrackplan();
	wxArrayString choices;
	wxArrayInt selections;
	std::vector<bool> visible(myLayout->Layers.size(),false);

	int i = 0;
	for (auto &ii : myLayout->Layers) {
		choices.Add(ii.second.Name);
		if(ii.second.Visible)
			selections.Add(i);
		++i;
	}
	wxMultiChoiceDialog dialog(this, wxT(""), wxT("Layers"), choices);
	dialog.SetSelections(selections);
	if (dialog.ShowModal() == wxID_OK) {
		selections = dialog.GetSelections();
		for (auto &ii : selections)
			visible[ii] = true;
		i = 0;
		for (auto &ii : myLayout->Layers)
			ii.second.Visible = visible[i++];
		int count = selections.GetCount();
	}
	glPane->timer->Start();
}


void MyFrame::OnScaleDialog(wxCommandEvent& event)
{
	glPane->timer->Stop();
	Xtrk3D::TrackPlan* myLayout = Xtrk3D::getTrackplan();
	DimDialog *Dlg = new DimDialog(myLayout,defaultScale,options.metric);
	if (Dlg->status == wxID_OK)
	{
		myLayout->reCalculateTrackObjects();
		Xtrk3D::init();
	}
	glPane->timer->Start();
}

void MyFrame::OnOptionDialog(wxCommandEvent& event)
{
	glPane->timer->Stop();
	OptionDialog *Dlg = new OptionDialog(options);
	if (Dlg->status == wxID_OK)
	{
		Xtrk3D::init();
	}
	glPane->timer->Start();
}

void MyFrame::OnBenchworkDialog(wxCommandEvent& event)
{
	glPane->timer->Stop();

	int pz;
	bool status(false);
	Select Menu(-1, "Benchwork properties", options.metric);
	pz = Menu.insert("benchwork zero level", Xtrk3D::getBenchworkHeight(), true);
	status = Menu.showModal();
	if (status) {
		Xtrk3D::setBenchworkHeight((float)(Menu.getValue(pz) / (options.metric ? 2.54 : 1)));
	}
	glPane->timer->Start();
}

void MyFrame::OnBasePlaneDialog(wxCommandEvent& event)
{
	glPane->timer->Stop();
	BasePlaneDialog Menu(Xtrk3D::getBasePlane(),options.metric);
	glPane->timer->Start();
	if (Menu.status== wxID_OK) {
		Xtrk3D::updateBasePlane();
	}
}

void MyFrame::OnMenu(wxCommandEvent& event)
{
	int winId = event.GetId();
	if ((winId >= wxID_FILE1) && (winId <= wxID_FILE9)) {
		glPane->timer->Stop();
		wxString fileName = m_docManager->GetFileHistory()->GetHistoryFile(winId - wxID_FILE1);
		if (!wxFileExists(fileName)) {
			wxMessageDialog *dial = new wxMessageDialog(NULL,
				wxT("file not found"), wxT("Error"), wxOK|wxCANCEL | wxICON_ERROR);
			auto ret= dial->ShowModal();
			delete dial;
			if(ret== wxID_OK)
				m_docManager->GetFileHistory()->RemoveFileFromHistory(winId - wxID_FILE1);
			glPane->timer->Start();
			return;
		}
		ReadFile(fileName);
		glPane->timer->Start();
	}
}

void MyFrame::ReadFile(wxString lpszPathName)
{
#ifdef _DEBUG
	FILE* fp = fopen("Xtrk3DViewer_logfile.txt", "a");
	std::string str(lpszPathName.c_str());
	fprintf(fp, "Read %s\n",str.c_str());
	fclose(fp);
#endif

	if (lpszPathName.find(".xtce") != std::string::npos || lpszPathName.find(".zip") != std::string::npos)
	{
		std::auto_ptr<wxZipEntry> entry;
		wxFFileInputStream Zin(lpszPathName);
		wxZipInputStream zip(Zin);
		std::map<std::string, std::auto_ptr<wxZipEntry>>filelist;

		while (entry.reset(zip.GetNextEntry()), entry.get() != NULL)
		{
			// access meta-data
			std::string name = entry->GetName();
			filelist[name] = entry;
		}
		if (filelist.count("manifest.json") > 0) {
			zip.OpenEntry(*filelist["manifest.json"]);
			// read 'zip' to access the entry's data
			wxStdInputStream sIn(zip);
			json j;
			sIn >> j;

#ifdef _DEBUG
			//*test code -- find background image -----
			std::string background;
			auto d = j["dependencies"];
			if (d.is_array()) {
				for (auto &a : d) {
					if (a["name"].is_string()) {
						if (a["name"].get<std::string>() == "background") {
							auto path = a["arch-path"].get<std::string>();
							auto fname = a["filename"].get<std::string>();
							double size = a["size"].get<double>();
							double pos_x = a["pos-x"].get<double>();
							double pos_y = a["pos-y"].get<double>();
							double screen = a["screen"].get<double>();
							double angle = a["angle"].get<double>();
							background = path + "\\" + fname;
							zip.OpenEntry(*filelist[background]);
							if (zip.CanRead()) {
								//save as temporary file and open with Bitmap::bitmapFromFile(filename)
								wxStandardPaths* paths = &wxStandardPaths::Get();
								wxString tempfile = paths->GetTempDir() + "\\" + fname;
								wxFileOutputStream fos(tempfile);
								if (fos.IsOk()) {
									zip.Read(fos);
									fos.Close();
								}
								//myLayout.LoadBkgImage(tempfile);
								int i = 0;
							}
							zip.CloseEntry();

						}
					}
				}
			}
			//end test code -- find background image ----//*/
#endif

			std::string layout(j["layout"]["name"]);
			zip.OpenEntry(*filelist[layout]);
			wxStdInputStream in(zip);
			if (in.good()) {
				SetStatusText(std::string("Reading " + lpszPathName));
				MyDoc Doc(in);
				zip.CloseEntry();
				defaultScale = Doc.scale;
				SetStatusText(std::string(ConvertToStr(Doc.size()) + " objects"));
				this->SetTitle(lpszPathName+ " - Xtrk3DViewer "+ Xtrk3DViewerVersion);
				Xtrk3D::init();
				Xtrk3D::resetScene();
				m_docManager->AddFileToHistory(lpszPathName);
				return;
			}
		}
		wxMessageDialog *dial = new wxMessageDialog(NULL,
		wxT("Error opening Extended Trackplan ") + lpszPathName, wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;
		return;
	}

	std::ifstream in;
	in.open(lpszPathName.mb_str());
	if (in.good()) {
		SetStatusText(std::string("Reading "+lpszPathName));
		MyDoc Doc(in);
		in.close();
		defaultScale = Doc.scale;
		SetStatusText(std::string(ConvertToStr(Doc.size())+" objects"));
		this->SetTitle(lpszPathName + " - Xtrk3DViewer " + Xtrk3DViewerVersion);
		Xtrk3D::init();
		Xtrk3D::resetScene();
		m_docManager->AddFileToHistory(lpszPathName);
	}
	else {
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			wxT("Error opening Trackplan")+ lpszPathName, wxT("Error"), wxOK | wxICON_ERROR);
		dial->ShowModal();
		delete dial;
	}
}


void MyFrame::dump()
{
#ifdef _DEBUG
	std::ofstream ofs;
	ofs.open("Xtrk3DViewer_verbose.txt", std::ofstream::out);// | std::ofstream::app);
	json j;
	j["Header"]["File"]="Xtrk3DViewer_verbose.txt";
	Xtrk3D::TrackPlan* P=Xtrk3D::getTrackplan();
	P->dump(j);
	ofs << j.dump(2)<<std::endl;
	ofs.close();
#endif
}

std::string MyFrame::convert(float val, int precision)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(precision) << val;
	return stream.str();
}

void MyFrame::SaveIniFile(std::string inifile)
{

	FILE* fp = fopen(inifile.c_str(), "w");
	XMLPrinter printer(fp);
	printer.PushHeader(false, true);
	printer.OpenElement("X3dViewer");
	printer.PushAttribute("version", "1.1");

	///description
	printer.OpenElement("description");
	printer.PushText("init file for X3dViewer");
	printer.CloseElement();

	///preferences
	printer.OpenElement("preferences");
	printer.PushAttribute("metric", options.metric);
	printer.PushAttribute("DrawStruct", options.drawStruct);
	printer.PushAttribute("ColorByLayer", options.colorByLayer);
	printer.PushAttribute("Elevations", options.drawElevations);
	printer.PushAttribute("resolution", options.resolution);
	switch (options.tunnelMode)
	{
	case 0:
		printer.PushAttribute("tunnelStyle", "off");
		break;
	case 1:
		printer.PushAttribute("tunnelStyle", "sides");
		break;
	case 2:
		printer.PushAttribute("tunnelStyle", "full");
		break;
	}
	printer.CloseElement();

	///dimensions
	printer.OpenElement("dimensions");

	for (auto it = Xtrk3D::getTrackplan()->Dimensions.begin(); it != Xtrk3D::getTrackplan()->Dimensions.end(); ++it)
	{
		printer.OpenElement("dimension");
		printer.PushAttribute("scale", it->first.c_str());
		printer.PushAttribute("ratio", convert(it->second.ratio()).c_str());
		printer.OpenElement("rail");
		printer.PushAttribute("gauge", convert(it->second.gauge()*25.4).c_str());
		printer.PushAttribute("height", convert(it->second.RailH()*25.4).c_str());
		printer.PushAttribute("width", convert(it->second.RailW()*25.4).c_str());
		printer.CloseElement();
		printer.OpenElement("sleeper");
		printer.PushAttribute("length", convert(it->second.TieL()*25.4).c_str());
		printer.PushAttribute("height", convert(it->second.TieH()*25.4).c_str());
		printer.PushAttribute("width", convert(it->second.TieW()*25.4).c_str());
		printer.PushAttribute("cc", convert(it->second.TieCC()*25.4).c_str());
		printer.CloseElement();
		printer.OpenElement("embankment");
		printer.PushAttribute("topWidth", convert(it->second.embTopW()*25.4).c_str());
		printer.PushAttribute("bottomWidth", convert(it->second.embBotW()*25.4).c_str());
		printer.PushAttribute("height", convert(it->second.embH()*25.4).c_str());
		printer.CloseElement();
		printer.OpenElement("clearance");
		printer.PushAttribute("width", convert(it->second.MaxW()*25.4).c_str());
		printer.PushAttribute("height", convert(it->second.MaxH()*25.4).c_str());
		printer.PushAttribute("extendedWidth", convert(it->second.ExtendW()*25.4).c_str());
		printer.PushAttribute("extendedLength", convert(it->second.ExtendL()*25.4).c_str());
		printer.CloseElement();
		printer.CloseElement();
	}
	printer.CloseElement();

	///file List
	printer.OpenElement("filelist");
	for (auto &ii : fileHistory) {
		printer.OpenElement("file");
		printer.PushText(ii.c_str());
		printer.CloseElement();
	}
	printer.CloseElement();
	printer.CloseElement();
	fclose(fp);
}

