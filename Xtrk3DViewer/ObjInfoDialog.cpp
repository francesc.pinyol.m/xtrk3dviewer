
#include "ObjInfoDialog.h"
#include <sstream>
#include <iomanip>
#include <wx/valnum.h>
#include "../Xtrk3D/misc.h"

void ObjInfoDialog::OnSelectBaseZ(wxCommandEvent& event)
{
	BaseZisChecked = BaseZCheckedCtrl->IsChecked();
	BaseZvalueCtrl->Enable(BaseZisChecked);
}

ObjInfoDialog::ObjInfoDialog(const std::string title) : wxDialog(NULL, -1, title, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE), m_metric(true), BaseZtarget(0), BaseZvalueCtrl(nullptr), BaseZCheckedCtrl(nullptr), BaseZisChecked(false)
{
	mainPanel = new wxPanel(this);
	mainSizer = new wxFlexGridSizer(0, 2, 0, 0);
	mainSizer->SetFlexibleDirection(wxBOTH);
	mainSizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	leftPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	leftSizer = new wxFlexGridSizer(0, 2, 0, 0);
	leftSizer->SetFlexibleDirection(wxBOTH);
	leftSizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	rightPanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxBoxSizer* rightSizer = new wxBoxSizer(wxVERTICAL);
	rightSizer->Add(new wxButton(rightPanel, wxID_OK, "Done", wxDefaultPosition, wxDefaultSize, 0), 0, wxALL, 5);
	rightSizer->Add(new wxButton(rightPanel, wxID_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0), 0, wxALL, 5);
	rightPanel->SetSizer(rightSizer);
	rightPanel->Layout();
	rightSizer->Fit(rightPanel);
}

ObjInfoDialog::ObjInfoDialog(const std::string title, const bool metric, Zbase* baseInfo) : ObjInfoDialog(title)
{
	BaseZtarget = baseInfo;
	m_metric = metric;

	if (baseInfo != nullptr) {
		wxBoxSizer* bSizer2;
		bSizer2 = new wxBoxSizer(wxHORIZONTAL);
		BaseZCheckedCtrl = new wxCheckBox(leftPanel, wxID_ANY, wxT("Set base z value"), wxDefaultPosition, wxDefaultSize, 0);
		BaseZvalueCtrl = new wxTextCtrl(leftPanel, wxID_ANY, ConvertToStr(baseInfo->baseZValue, m_metric), wxDefaultPosition, wxDefaultSize, 0, wxFloatingPointValidator<float>(4));
		BaseZisChecked = baseInfo->baseZset;
		BaseZCheckedCtrl->SetValue(BaseZisChecked);
		BaseZvalueCtrl->Enable(BaseZisChecked);
		bSizer2->Add(BaseZCheckedCtrl, 0, wxALL, 5);
		bSizer2->Add(BaseZvalueCtrl, 0, wxALL, 5);
		Bind(wxEVT_CHECKBOX, &ObjInfoDialog::OnSelectBaseZ, this);
		leftSizer->Add(bSizer2, 1, wxEXPAND, 5);
	}
	else {
		leftSizer->Add(0, 0, 1, wxEXPAND, 5);
	}

	wxStaticText* unitText = new wxStaticText(leftPanel, wxID_ANY, (m_metric ? wxT("[cm]") : wxT("[inch]")), wxDefaultPosition, wxDefaultSize, 0);
	unitText->Wrap(-1);
	leftSizer->Add(unitText, 0, wxALL | wxALIGN_RIGHT, 5);
}

int ObjInfoDialog::ShowModal()
{
	leftPanel->SetSizer(leftSizer);
	leftPanel->Layout();
	leftSizer->Fit(leftPanel);
	mainSizer->Add(leftPanel, 1, wxEXPAND | wxALL, 5);

	mainSizer->Add(rightPanel, 1, wxEXPAND | wxALL, 5);


	this->SetSizer(mainSizer);
	this->Layout();
	mainSizer->Fit(this);

	this->Centre(wxBOTH);

	auto stat = wxDialog::ShowModal();
	if (BaseZtarget != nullptr && stat == wxID_OK)
	{
		wxString ret = BaseZvalueCtrl->GetValue();
		double value;
		ret.ToDouble(&value);
		BaseZtarget->set(BaseZisChecked, (float)(m_metric ? value / 2.54f : value));
	}
	return stat;
}

ObjInfoDialog::~ObjInfoDialog()
{
}

void ObjInfoDialog::insertLegend(const std::string Legend, int flag)
{
	wxStaticText* Label = new wxStaticText(leftPanel, wxID_ANY, Legend, wxDefaultPosition, wxDefaultSize, 0);
	Label->Wrap(-1);
	leftSizer->Add(Label, 0, wxALL | flag, 5);
}

wxTextCtrl* ObjInfoDialog::insertFloat(const float value, wxPanel* m_vec3Panel, wxFlexGridSizer* vec3Sizer, bool z_editable)
{
	wxTextCtrl* m_textCtrl5 = new wxTextCtrl(m_vec3Panel, wxID_ANY, ConvertToStr(value, m_metric), wxDefaultPosition, wxDefaultSize, 0, wxFloatingPointValidator<float>(4));
	m_textCtrl5->Enable(z_editable);
	m_textCtrl5->SetMaxSize(wxSize(80, -1));
	vec3Sizer->Add(m_textCtrl5, 0, wxLEFT | wxRIGHT, 5);
	return m_textCtrl5;
}

wxTextCtrl* ObjInfoDialog::insertVec3(const glm::vec3 point, wxPanel* m_vec3Panel, wxFlexGridSizer* vec3Sizer, bool z_editable)
{
	wxStaticText* m_staticText4 = new wxStaticText(m_vec3Panel, wxID_ANY, wxT("x:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText4->Wrap(-1);
	vec3Sizer->Add(m_staticText4, 0, wxLEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	wxTextCtrl* m_textCtrl3 = new wxTextCtrl(m_vec3Panel, wxID_ANY, ConvertToStr(point.x, m_metric), wxDefaultPosition, wxDefaultSize, 0);
	m_textCtrl3->Enable(false);
	m_textCtrl3->SetMaxSize(wxSize(80, -1));
	vec3Sizer->Add(m_textCtrl3, 0, wxLEFT | wxRIGHT, 5);

	wxStaticText* m_staticText5 = new wxStaticText(m_vec3Panel, wxID_ANY, wxT("y:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText5->Wrap(-1);
	vec3Sizer->Add(m_staticText5, 0, wxLEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	wxTextCtrl* m_textCtrl4 = new wxTextCtrl(m_vec3Panel, wxID_ANY, ConvertToStr(point.y, m_metric), wxDefaultPosition, wxDefaultSize, 0);
	m_textCtrl4->Enable(false);
	m_textCtrl4->SetMaxSize(wxSize(80, -1));
	vec3Sizer->Add(m_textCtrl4, 0, wxLEFT | wxRIGHT, 5);

	wxStaticText* m_staticText6 = new wxStaticText(m_vec3Panel, wxID_ANY, wxT("z:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText6->Wrap(-1);
	vec3Sizer->Add(m_staticText6, 0, wxLEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	wxTextCtrl* m_textCtrl5 = new wxTextCtrl(m_vec3Panel, wxID_ANY, ConvertToStr(point.z, m_metric), wxDefaultPosition, wxDefaultSize, 0, wxFloatingPointValidator<float>(4));
	m_textCtrl5->Enable(z_editable);
	m_textCtrl5->SetMaxSize(wxSize(80, -1));
	vec3Sizer->Add(m_textCtrl5, 0, wxLEFT | wxRIGHT, 5);
	return m_textCtrl5;
}

void ObjInfoDialog::insert(const std::string Legend, const int value)
{
	insertLegend(Legend);
	wxStaticText* intLabel = new wxStaticText(leftPanel, wxID_ANY, std::to_string(value), wxDefaultPosition, wxDefaultSize, 0);
	intLabel->Wrap(-1);
	leftSizer->Add(intLabel, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);
}

void ObjInfoDialog::insert(const std::string Legend, const glm::vec2 value)
{
	insertLegend(Legend);
	wxPanel* vec3Panel = new wxPanel(leftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxFlexGridSizer* vec3Sizer;
	vec3Sizer = new wxFlexGridSizer(0, 6, 0, 0);
	vec3Sizer->SetFlexibleDirection(wxBOTH);
	vec3Sizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	wxStaticText* m_staticText4 = new wxStaticText(vec3Panel, wxID_ANY, wxT("x:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText4->Wrap(-1);
	vec3Sizer->Add(m_staticText4, 0, wxLEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	wxTextCtrl* m_textCtrl3 = new wxTextCtrl(vec3Panel, wxID_ANY, ConvertToStr(value.x, m_metric), wxDefaultPosition, wxDefaultSize, 0);
	m_textCtrl3->Enable(false);
	m_textCtrl3->SetMaxSize(wxSize(80, -1));
	vec3Sizer->Add(m_textCtrl3, 0, wxLEFT | wxRIGHT, 5);

	wxStaticText* m_staticText5 = new wxStaticText(vec3Panel, wxID_ANY, wxT("y:"), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText5->Wrap(-1);
	vec3Sizer->Add(m_staticText5, 0, wxLEFT | wxRIGHT | wxALIGN_CENTER_VERTICAL, 5);
	wxTextCtrl* m_textCtrl4 = new wxTextCtrl(vec3Panel, wxID_ANY, ConvertToStr(value.y, m_metric), wxDefaultPosition, wxDefaultSize, 0);
	m_textCtrl4->Enable(false);
	m_textCtrl4->SetMaxSize(wxSize(80, -1));
	vec3Sizer->Add(m_textCtrl4, 0, wxLEFT | wxRIGHT, 5);

	vec3Panel->SetSizer(vec3Sizer);
	vec3Panel->Layout();
	vec3Sizer->Fit(vec3Panel);
	leftSizer->Add(vec3Panel, 1, wxEXPAND | wxLEFT | wxRIGHT, 5);
}

wxTextCtrl* ObjInfoDialog::insert(const std::string Legend, const double value, bool z_editable)
{
	insertLegend(Legend);
	wxPanel* vec3Panel = new wxPanel(leftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxFlexGridSizer* vec3Sizer;
	vec3Sizer = new wxFlexGridSizer(0, 6, 0, 0);
	vec3Sizer->SetFlexibleDirection(wxBOTH);
	vec3Sizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);
	wxTextCtrl* zval = insertFloat(value, vec3Panel, vec3Sizer, z_editable);
	vec3Panel->SetSizer(vec3Sizer);
	vec3Panel->Layout();
	vec3Sizer->Fit(vec3Panel);
	leftSizer->Add(vec3Panel, 1, wxEXPAND | wxLEFT | wxRIGHT, 5);
	return zval;
}

wxTextCtrl* ObjInfoDialog::insert(const std::string Legend, const glm::vec3 value, bool z_editable)
{
	insertLegend(Legend);
	wxPanel* vec3Panel = new wxPanel(leftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
	wxFlexGridSizer* vec3Sizer;
	vec3Sizer = new wxFlexGridSizer(0, 6, 0, 0);
	vec3Sizer->SetFlexibleDirection(wxBOTH);
	vec3Sizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

	wxTextCtrl* zval = insertVec3(value, vec3Panel, vec3Sizer, z_editable);

	vec3Panel->SetSizer(vec3Sizer);
	vec3Panel->Layout();
	vec3Sizer->Fit(vec3Panel);
	leftSizer->Add(vec3Panel, 1, wxEXPAND | wxLEFT | wxRIGHT, 5);
	return zval;
}

void ObjInfoDialog::insert(const std::string Legend, const std::string value)
{
	insertLegend(Legend);
	wxStaticText* floatLabel = new wxStaticText(leftPanel, wxID_ANY, value, wxDefaultPosition, wxDefaultSize, 0);
	floatLabel->Wrap(-1);
	leftSizer->Add(floatLabel, 0, wxALL | wxALIGN_CENTER_VERTICAL, 5);
}

std::string ObjInfoDialog::getValue(wxTextCtrl* w)
{
	return std::string(w->GetValue());
}

float ObjInfoDialog::getValuef(wxTextCtrl* w)
{
	double Value;
	w->GetValue().ToDouble(&Value);
	return (float)Value / (m_metric ? 2.54f : 1.0f);
}

