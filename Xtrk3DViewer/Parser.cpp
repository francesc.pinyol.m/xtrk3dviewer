#include "Parser.h"
#include <iostream>     // std::cout, std::ios
#include <sstream>      // std::stringstream

std::map <std::string, enum xtrkType> Parser::XtrkToken =
{ {"VERSION",XtrkVersion}
,{"TITLE1",XtrkTitle1}
,{"TITLE2",XtrkTitle2}
,{"MAPSCALE",XtrkMapScale}
,{"ROOMSIZE",XtrkRoomSize}
,{"SCALE",XtrkScale}
,{"LAYERS",XtrkLayers}
,{"TURNOUT",XtrkTurnout}
,{"JOINT",XtrkJoint}
,{"CURVE",XtrkCurve}
,{"STRAIGHT",XtrkStraight}
,{"TURNTABLE",XtrkTurntable}
,{"STRUCTURE",XtrkStructure}
,{"DRAW",XtrkDraw}
,{"END",XtrkEnd}
,{"END$SEGS",XtrkEnd}
,{"END$TRACKS",XtrkEnd}
,{"BEZIER",XtrkBezier}
,{"BZRLIN",XtrkBzrlin}
,{"CORNU",XtrkCornu}
,{"NOTE",XtrkNote}
,{"BLOCK",XtrkBlock}
,{"CONTROL",XtrkControl}
,{"SENSOR",XtrkSensor}
,{"SIGNAL",XtrkSignal}
,{"CURRENT",XtrkCurrentLayer}

};



Parser::Parser(std::istream* in)
	: instream(in)
{
	parseNextRow();
}

Parser::~Parser()
{
}

bool Parser::parseNextRow()
{
	if (instream != nullptr)
	{
		if (instream->good()) {
			std::getline(*instream, m_str);
			tokenize();
			return true;
		}
	}
	return false;
}

void Parser::tokenize()
{
	m_tknv.clear();
	m_curpos = 0;
	m_curtkn = 0;
	std::string tkn;
	while (parseNextToken(tkn, " \t\r")) {
		m_tknv.push_back(tkn);
	}

}

std::string Parser::getNextToken()
{
//	if (m_tknv.size()>0 && m_tknv.size() > m_curtkn) //m_curtkn always >=0
	if (m_tknv.size() > m_curtkn)
		return m_tknv[m_curtkn++];
	else
		return std::string();
}

xtrkType Parser::getNextToken(std::map <std::string, enum xtrkType>& TokenList)
{
	if (m_tknv.size() > m_curtkn)
	{
		std::string str = m_tknv[m_curtkn++];
		if (TokenList.count(str) > 0) {
			return TokenList[str];
		}
	}
	return xtrkUndefined;
}

std::string Parser::GetToken(unsigned int nr)
{
	if (m_tknv.size()> nr)
		return m_tknv[nr];
	else
		return std::string();
}

xtrkType Parser::getToken(unsigned int nr, std::map <std::string, enum xtrkType>& TokenList)
{
	if (m_tknv.size() > nr)
	{
		std::string str = m_tknv[nr];
		if (TokenList.count(str) > 0) {
			return TokenList[str];
		}
	}
	return xtrkUndefined;
}

bool Parser::parseNextToken(std::string& tkn, const std::string& delimiters)
{
	m_curpos = m_str.find_first_not_of(delimiters, m_curpos);
	if (m_curpos > m_str.length())
		return false;
	size_t endpos;
	if (m_str[m_curpos] == '#') {
		return false;
	}
	if (m_str[m_curpos] == '\"')
	{
		auto s = ++m_curpos;
		for (;;s+=2) {
			endpos = m_str.find_first_of("\"", s);
			if (m_str[endpos + 1] != '\"')
				break;
		}
	} else
		endpos = m_str.find_first_of(delimiters, m_curpos + 1);
	tkn = m_str.substr(m_curpos, endpos - m_curpos);
	m_curpos = endpos;
	return true;
}

bool Parser::isInt(const std::string& tkn)
{
	try {
		std::stoi(tkn);
	}
	catch (...) {
		return false;
	}
	return true;
}

bool Parser::isDouble(const std::string& tkn)
{
	try {
		std::stol(tkn);
	}
	catch (...) {
		return false;
	}
	return true;
}
