#pragma once

#include <glm/glm/glm.hpp>
#include <vector>

class Plane
{
public:
	Plane();
	~Plane();
	Plane(float t_a, float t_b, float t_c, float t_d) : a(t_a), b(t_b), c(t_c), d(t_d) {};
	Plane(const Plane &P) :a(P.a), b(P.b), c(P.c), d(P.d) {};
	Plane(const glm::vec3 &Pt1, const glm::vec3 &Pt2, const glm::vec3 &Pt3);	//create plane from 3 points on the surface
	Plane(const glm::vec3 &Pt, const glm::vec3 &Normal);						//create plane from a point on the surface and a normal vector
	Plane(const std::vector<glm::vec3> Points);

	void test();
																											//
	// Math functions
	//
	float unsignedDistance(const glm::vec3 &Pt) const;
	float signedDistance(const glm::vec3 &Pt) const;
	glm::vec3 closestPoint(const glm::vec3 &Pt);
//	bool fitToPoints(const std::vector<glm::vec3> &Points, float &ResidualError);
//	bool fitToPoints(const std::vector< glm::vec3> &Points, glm::vec3 &Basis1, glm::vec3 &Basis2, float &NormalEigenvalue, float &ResidualError);
	bool FindLLSQPlane(std::vector<glm::vec3> points, glm::vec3 *destCenter, glm::vec3 *destNormal);

	//
	// Line intersection
	//
	glm::vec3 intersectLine(const glm::vec3 &Pt1, const glm::vec3 &Pt2, bool &Hit) const;	//determines the intersect of the line defined by the points Pt1 and Pt2 with the plane.
	float intersectLineRatio(const glm::vec3 &Pt1, const glm::vec3 &Pt2, bool &Hit);	//Paramaterize the line with the variable t such that t = 0 is Pt1 and t = 1 is Pt2.
																		//returns the t for this line that lies on this plane.

	static float dotCoord(const Plane &P, const glm::vec3 &Pt);        //dot product of a plane and a 3D coordinate
	static float dotNormal(const Plane &P, const glm::vec3 &V);    //dot product of a plane and a 3D normal

	Plane normalize();
	glm::vec3 getNormal() const { return glm::vec3(a, b, c); };
	float getZ(const float x, const float y, bool &Hit) const;
	bool setZValue(glm::vec3& Pt);

protected:
	float a, b, c, d;        //the (a, b, c, d) in a*x + b*y + c*z + d = 0.

};

