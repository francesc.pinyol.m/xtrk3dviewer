#include "OptionDialog.h"
#include <sstream> 
#include <wx/valnum.h>
#include "../Xtrk3D/track.h"
#include "../Xtrk3D/misc.h"

OptionDialog::OptionDialog(Options& options) : wxDialog(NULL, -1, "Display Options", wxDefaultPosition, wxDefaultSize)
{
	wxPanel *panel = new wxPanel(this);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

	wxSizer *LeftPanelSizer = new wxFlexGridSizer(2);

	///////////////////////////////////
	wxPanel *CL11 = new wxPanel(panel);
	wxSizer *CLsizer11 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer11->Add(new wxStaticText(CL11, -1, "Track color by Layer"));
	CLsizer11->AddSpacer(10);
	CL11->SetSizer(CLsizer11);
	CLsizer11->SetSizeHints(CL11);
	LeftPanelSizer->Add(CL11);
	wxPanel *CL12 = new wxPanel(panel);
	wxSizer *CLsizer12 = new wxBoxSizer(wxHORIZONTAL);
	wxRadioButton *CLRadio1 = new wxRadioButton(CL12, -1, "Off");
	wxRadioButton *CLRadio2 = new wxRadioButton(CL12, -1, "On");
	options.colorByLayer ? CLRadio2->SetValue(true) : CLRadio1->SetValue(true);
	CLsizer12->Add(CLRadio1);
	CLsizer12->Add(CLRadio2);
	CL12->SetSizer(CLsizer12);
	CLsizer12->SetSizeHints(CL12);
	LeftPanelSizer->Add(CL12);
	///////////////////////////////////
	wxPanel *CL21 = new wxPanel(panel);
	wxSizer *CLsizer21 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer21->Add(new wxStaticText(CL21, -1, "Tunnel Draw mode"));
	CLsizer21->AddSpacer(10);
	CL21->SetSizer(CLsizer21);
	CLsizer21->SetSizeHints(CL21);
	LeftPanelSizer->Add(CL21);
	wxPanel *CL22 = new wxPanel(panel);
	wxSizer *CLsizer22 = new wxBoxSizer(wxHORIZONTAL);
	wxRadioButton *CLRadio3 = new wxRadioButton(CL22, -1, "Off");
	wxRadioButton *CLRadio4 = new wxRadioButton(CL22, -1, "Sides");
	wxRadioButton *CLRadio5 = new wxRadioButton(CL22, -1, "Sides and Top");
	switch (options.tunnelMode)
	{
	case 0:
		CLRadio3->SetValue(true);
		break;
	case 1:
		CLRadio4->SetValue(true);
		break;
	default:
		CLRadio5->SetValue(true);
	}
	CLsizer22->Add(CLRadio3);
	CLsizer22->Add(CLRadio4);
	CLsizer22->Add(CLRadio5);
	CL22->SetSizer(CLsizer22);
	CLsizer22->SetSizeHints(CL22);
	LeftPanelSizer->Add(CL22);
	///////////////////////////////////
	wxPanel *CL31 = new wxPanel(panel);
	wxSizer *CLsizer31 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer31->Add(new wxStaticText(CL31, -1, "Draw structures"));
	CLsizer31->AddSpacer(10);
	CL31->SetSizer(CLsizer31);
	CLsizer31->SetSizeHints(CL31);
	LeftPanelSizer->Add(CL31);
	wxPanel *CL32 = new wxPanel(panel);
	wxSizer *CLsizer32 = new wxBoxSizer(wxHORIZONTAL);
	wxRadioButton *CLRadio6 = new wxRadioButton(CL32, -1, "Off");
	wxRadioButton *CLRadio7 = new wxRadioButton(CL32, -1, "On");
	options.drawStruct ? CLRadio7->SetValue(true) : CLRadio6->SetValue(true);
//	CLRadio6->SetValue(true);
	CLsizer32->Add(CLRadio6);
	CLsizer32->Add(CLRadio7);
	CL32->SetSizer(CLsizer32);
	CLsizer32->SetSizeHints(CL32);
	LeftPanelSizer->Add(CL32);
	///////////////////////////////////
	wxPanel *CL41 = new wxPanel(panel);
	wxSizer *CLsizer41 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer41->Add(new wxStaticText(CL41, -1, "Units"));
	CLsizer41->AddSpacer(10);
	CL41->SetSizer(CLsizer41);
	CLsizer41->SetSizeHints(CL41);
	LeftPanelSizer->Add(CL41);
	wxPanel *CL42 = new wxPanel(panel);
	wxSizer *CLsizer42 = new wxBoxSizer(wxHORIZONTAL);
	wxRadioButton *CLRadio8 = new wxRadioButton(CL42, -1, "English");
	wxRadioButton *CLRadio9 = new wxRadioButton(CL42, -1, "Metric");
	options.metric ? CLRadio9->SetValue(true) : CLRadio8->SetValue(true);
//	CLRadio9->SetValue(true);
	CLsizer42->Add(CLRadio8);
	CLsizer42->Add(CLRadio9);
	CL42->SetSizer(CLsizer42);
	CLsizer42->SetSizeHints(CL42);
	LeftPanelSizer->Add(CL42);
	///////////////////////////////////
	wxPanel *CL51 = new wxPanel(panel);
	wxSizer *CLsizer51 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer51->Add(new wxStaticText(CL51, -1, "Elevations"));
	CLsizer51->AddSpacer(10);
	CL51->SetSizer(CLsizer51);
	CLsizer51->SetSizeHints(CL51);
	LeftPanelSizer->Add(CL51);
	wxPanel *CL52 = new wxPanel(panel);
	wxSizer *CLsizer52 = new wxBoxSizer(wxHORIZONTAL);
	wxRadioButton *CLRadio10 = new wxRadioButton(CL52, -1, "Off");
	wxRadioButton *CLRadio11 = new wxRadioButton(CL52, -1, "On");
	options.drawElevations ? CLRadio11->SetValue(true) : CLRadio10->SetValue(true);
//	CLRadio11->SetValue(true);
	CLsizer52->Add(CLRadio10);
	CLsizer52->Add(CLRadio11);
	CL52->SetSizer(CLsizer52);
	CLsizer52->SetSizeHints(CL52);
	LeftPanelSizer->Add(CL52);
	/*//////////////////////////////////
	wxPanel *CL61 = new wxPanel(panel);
	wxSizer *CLsizer61 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer61->Add(new wxStaticText(CL61, -1, "Resolution"));
	CLsizer61->AddSpacer(10);
	CL51->SetSizer(CLsizer61);
	CLsizer61->SetSizeHints(CL61);
	LeftPanelSizer->Add(CL61);
	wxPanel *CL62 = new wxPanel(panel);
	wxSizer *CLsizer62 = new wxBoxSizer(wxHORIZONTAL);
	wxTextCtrl* Value = new wxTextCtrl(CL62, -1, ConvertToStr(Xtrk3D::Segment::resolution), wxPoint(0, 0), wxSize(60, 20), 0, wxFloatingPointValidator<float>());
	CLsizer62->Add(Value);
	CLsizer62->Add(new wxStaticText(CL62, -1, " degrees"));

	CL62->SetSizer(CLsizer62);
	CLsizer62->SetSizeHints(CL62);
	LeftPanelSizer->Add(CL62);//*/

	///////////////////////////////////
	//	wxTextCtrl *tp= Insert(panel, LeftPanelSizer, std::string("test"));
	///////////////////////////////////
	panel->SetSizer(LeftPanelSizer);
	LeftPanelSizer->SetSizeHints(panel);

	wxButton *okButton = new wxButton(this, wxID_OK, wxT("Ok"),wxDefaultPosition, wxSize(70, 30));
	wxButton *closeButton = new wxButton(this, wxID_CANCEL, wxT("Cancel"),wxDefaultPosition, wxSize(70, 30));
	hbox->Add(okButton, 1, wxLEFT, 5);
	hbox->Add(closeButton, 1, wxLEFT, 5);
	vbox->AddSpacer(20);
	vbox->Add(panel, 1);
//	vbox->Add(hbox, 0, wxALIGN_TOP | wxTOP | wxBOTTOM, 10);
	vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

	SetSizer(vbox);

	Centre();
	status = ShowModal();
	if (status == wxID_OK) {
		options.colorByLayer = CLRadio2->GetValue();
		options.drawStruct = CLRadio7->GetValue();
		options.metric = CLRadio9->GetValue();
		options.drawElevations = CLRadio11->GetValue();

		if (CLRadio3->GetValue()) options.tunnelMode = 0;
		else if (CLRadio4->GetValue()) options.tunnelMode = 1;
		else options.tunnelMode = 2;

		//TODO:
/*		double new_resolution;
		Value->GetValue().ToDouble(&new_resolution);
		if (new_resolution != Segment::resolution) {
			Segment::resolution=fmax(new_resolution,0.1);
			Xtrk3D::getTrackplan->reCalculateTrackObjects();
		}//*/
		Xtrk3D::setOptions(options.colorByLayer, options.tunnelMode, options.metric, options.drawStruct, options.drawElevations, options.resolution);

	}
	Destroy();
}


OptionDialog::~OptionDialog()
{
}


wxTextCtrl* OptionDialog::Insert(wxPanel* panel, wxSizer* Sizer, std::string legend)
{
	wxPanel *CL11 = new wxPanel(panel);
	wxSizer *CLsizer11 = new wxBoxSizer(wxHORIZONTAL);
	CLsizer11->Add(new wxStaticText(CL11, -1, legend), wxALIGN_BOTTOM);
	CLsizer11->AddSpacer(10);
	CL11->SetSizer(CLsizer11);
	CLsizer11->SetSizeHints(CL11);
	Sizer->Add(CL11);

	wxPanel *CL12 = new wxPanel(panel);
	wxSizer *CLsizer12 = new wxBoxSizer(wxHORIZONTAL);
	wxTextCtrl* Value = new wxTextCtrl(CL12, -1, wxT(""), wxPoint(0, 0), wxSize(60, 20));
	CLsizer12->Add(Value);
	CL12->SetSizer(CLsizer12);
	CLsizer12->SetSizeHints(CL12);
	Sizer->Add(CL12);
	return Value;
}
