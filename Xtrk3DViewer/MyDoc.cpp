#include "MyDoc.h"

#ifdef _DEBUG
//-----------------------------------------------------------------------------------------------------------
void Dump(std::string str,int i)
{
	FILE* fp = fopen("Xtrk3DViewer_verbose.txt", "a");
	fprintf(fp, "%d %s",i, str.c_str());
	fflush(fp);
	fclose(fp);
}
#endif


MyDoc::MyDoc(std::istream& in) :objListHead(nullptr), objListTail(nullptr),_size(0)
{
	Xtrk3D::newTrack();
	int errcount= 0;

	while (in.good()) {
		Parser CS(&in);
		switch (CS.getNextToken(Parser::XtrkToken)) {
		case XtrkRoomSize:
			if (CS.GetToken(2)[0] == 'x')
				Xtrk3D::setRoomsize(std::stof(CS.GetToken(1)), stof(CS.GetToken(3)));
			break;
		case XtrkScale:
			scale = CS.GetToken(1);
			break;
		case XtrkLayers:
			if (CS.count() > 3) {
				int id = std::stoi(CS.GetToken(1));
				bool visible = std::stoi(CS.GetToken(2)) == 1;
				char* name = new char[CS.GetToken(10).length() + 8];
				sprintf(name, "%d - %s", id + 1, CS.GetToken(10).c_str());
				int color = std::stoi(CS.GetToken(5));
				Xtrk3D::defineLayer(id, name, color, visible);
				delete [] name;
			}

			break;
		case XtrkTurnout:
		{
			//TURNOUT<sp>index<sp>layer<sp>options<sp>postion<sp>0<sp>scale <sp>visible&bridge&no_ties <sp>origx<sp>origy<sp>elev<sp>angle<sp>"Manufacturer<tab>Description <tab>Part<tab>"
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::turnoutObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			turnout->width = std::stoi(CS.GetToken(3));
			strcpy(turnout->scale, CS.GetToken(6).c_str());
			turnout->visibility = std::stoi(CS.GetToken(7));
			turnout->point.x = std::stod(CS.GetToken(8));
			turnout->point.y = std::stod(CS.GetToken(9));
			turnout->point.z = 0;
			turnout->rotation = std::stof(CS.GetToken(11));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			char* lbl = new char[CS.GetToken(12).length() + 1];
			turnout->label = lbl;
			strcpy(lbl, CS.GetToken(12).c_str());
			parseObject(in, turnout);
			break;
		}
		case XtrkStraight:
		{
			//STRAIGHT<sp>index<sp>layer<sp>line-width<sp>0<sp>0<sp>scale<sp>visibility&bridge&no_ties
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::straightObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			turnout->width = std::stoi(CS.GetToken(3));
			strcpy(turnout->scale, CS.GetToken(6).c_str());
			turnout->visibility = std::stoi(CS.GetToken(7));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			turnout->label = nullptr;
			parseObject(in, turnout);
			break;
		}
		case XtrkCurve:
		{
			//CURVE<sp>index<sp>layer<sp>line-width<sp>0<sp>0<sp>scale<sp>visibility&bridge&no_ties<sp>center-X<sp>center-Y<sp>0<sp>radius<sp>helix-turns<sp>desc-X<sp>desc-Y
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::curveObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			turnout->width = std::stoi(CS.GetToken(3));
			strcpy(turnout->scale, CS.GetToken(6).c_str());
			turnout->visibility = std::stoi(CS.GetToken(7));
			turnout->point.x = std::stod(CS.GetToken(8));
			turnout->point.y = std::stod(CS.GetToken(9));
			turnout->point.z = 0;
			turnout->radius = stof(CS.GetToken(11));
			turnout->turns = stoi(CS.GetToken(12));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			turnout->label = nullptr;
			parseObject(in, turnout);
			break;
		}
		case XtrkDraw:
		{
			//DRAW<sp>index<sp>layer<sp>lineType<sp>0<sp>0<sp>start-x<sp>start-y<sp>start angle
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::drawObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			turnout->point.x = std::stod(CS.GetToken(6));
			turnout->point.y = std::stod(CS.GetToken(7));
			turnout->point.z = 0;
			turnout->rotation = std::stof(CS.GetToken(9));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			turnout->label = nullptr;
			parseObject(in, turnout);
			break;
		}
		case XtrkStructure:
		{
			//STRUCTURE<sp>index<sp>layer<sp>lineType<sp>0<sp>0<sp>scale <sp>visible<sp>origx<sp>origy<sp>elev<sp>angle <sp>"textfield1 <tab> textfield2 <tab> textfield3"
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::structureObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			strcpy(turnout->scale, CS.GetToken(6).c_str());
			turnout->visibility = std::stoi(CS.GetToken(7));
			turnout->point.x = std::stod(CS.GetToken(8));
			turnout->point.y = std::stod(CS.GetToken(9));
			turnout->point.z = 0;
			turnout->rotation = std::stof(CS.GetToken(11));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			//			turnout->label = nullptr;
			char* lbl = new char[CS.GetToken(12).length() + 1];
			turnout->label = lbl;
			strcpy(lbl, CS.GetToken(12).c_str());
			parseObject(in, turnout);
			break;
		}
		case XtrkBezier:
		case XtrkCornu:
		{
			//BEZIER<sp>index<sp>layer<sp>track-width<sp>color<sp>0.000000<sp>scale<sp>visible&bridge&no_ties<sp>X1<sp>Y1<sp>X2<sp>Y2<sp>X3<sp>Y3<sp>X4<sp>Y4<sp>0<sp>desc-X<sp>desc-Y
			//CORNU<sp>index<sp>layer<sp>width<sp>0<sp>0<sp>scale<sp>visible&bridge&no_ties<sp>pos1x<sp>pos1y<sp>m_angle<sp>radius1<sp>center1x<sp>center1y<sp>pos2x<sp>pos2y<sp>angle2<sp>radius2<sp>center2x<sp>center2y
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::cornuObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			strcpy(turnout->scale, CS.GetToken(6).c_str());
			turnout->visibility = std::stoi(CS.GetToken(7));
			turnout->point.x = std::stod(CS.GetToken(8));
			turnout->point.y = std::stod(CS.GetToken(9));
			turnout->point.z = 0;
			turnout->rotation = std::stof(CS.GetToken(11));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			turnout->label = nullptr;
			parseObject(in, turnout);
			break;
		}
		case XtrkJoint:
		{
			//JOINT<sp>index<sp>layer<sp>width<sp>0<sp>0<sp>scale<sp>visible&bridge&no_ties<sp>l0<sp>l1<sp>R<sp><sp>flip<sp>negate<sp>S-curve<sp>x<sp>y<sp>0<sp>angle
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::joinObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			strcpy(turnout->scale, CS.GetToken(6).c_str());
			turnout->visibility = std::stoi(CS.GetToken(7));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			turnout->label = nullptr;
			parseObject(in, turnout);
			break;
		}

		case XtrkBzrlin:
		{
			//BZRLIN<sp>index<sp>layer<sp>0<sp>0<sp>line-width<sp>scale<sp>visible<sp>X1<sp>Y1<sp>X2<sp>Y2<sp>X3<sp>Y3<sp>X4<sp>Y4 < sp>0 < sp > desc - X<sp>desc - Y
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::bzrlinObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			turnout->label = nullptr;
			parseObject(in, turnout);
			break;
		}

		case XtrkTurntable:
		{
			//TURNTABLE<sp>index<sp>layer<sp>0<sp>0<sp>0<sp>scale<sp>visible<sp>x<sp>y<sp>0<sp>radius<sp>current-ep	
			Xtrk3D::OBJ_TYPE* turnout = new Xtrk3D::OBJ_TYPE;
			insertObjList(turnout);
			turnout->type = Xtrk3D::turntableObj;
			turnout->index = std::stoi(CS.GetToken(1));
			turnout->layer = std::stoi(CS.GetToken(2));
			strcpy(turnout->scale, CS.GetToken(6).c_str());
			turnout->visibility = std::stoi(CS.GetToken(7));
			turnout->point.x = std::stod(CS.GetToken(8));
			turnout->point.y = std::stod(CS.GetToken(9));
			turnout->point.z = 0;
			turnout->radius = std::stof(CS.GetToken(11));
			turnout->segments = nullptr;
			turnout->endpoints = nullptr;
			turnout->label = nullptr;
			parseObject(in, turnout);
			break;
		}
		case XtrkCar:
			//skipObject(in);
			break;
		case xtrkUndefined:
		default:
			errcount++;
			break;
		}
	}
	_size=Xtrk3D::addTracks(objListHead);
}

MyDoc::~MyDoc()
{
	while (objListHead != nullptr)
	{
		objListHead = deleteXtrk3DObj(objListHead);
	}
}

void MyDoc::insertObjList(Xtrk3D::OBJ_TYPE* obj)
{
	obj->next = nullptr;
	if (objListHead == nullptr) {
		objListHead = objListTail = obj;
		return;
	}
	objListTail->next = obj;
	objListTail = obj;
}

void MyDoc::insertEndPointList(Xtrk3D::OBJ_TYPE* obj, Xtrk3D::ENDPOINT_TYPE* endp)
{
	endp->next = nullptr;
	Xtrk3D::ENDPOINT_TYPE* p = obj->endpoints;
	if (p == nullptr) {
		obj->endpoints = endp;
		return;
	}
	while (p->next != nullptr) {
		p = p->next;
	}
	p->next = endp;
}

void MyDoc::insertSegmentList(Xtrk3D::OBJ_TYPE* obj, Xtrk3D::SEGMENT_TYPE* segmp)
{
	segmp->next = nullptr;
	Xtrk3D::SEGMENT_TYPE* p = obj->segments;
	if (p == nullptr) {
		obj->segments = segmp;
		return;
	}
	while (p->next != nullptr) {
		p = p->next;
	}
	p->next = segmp;
}

Xtrk3D::OBJ_TYPE* MyDoc::deleteXtrk3DObj(Xtrk3D::OBJ_TYPE* obj)
{
	Xtrk3D::ENDPOINT_TYPE* endp = obj->endpoints;
	Xtrk3D::SEGMENT_TYPE* segmp = obj->segments;
	while (endp != nullptr) {
		Xtrk3D::ENDPOINT_TYPE* p = endp;
		endp = endp->next;
		delete p;
	}
	while (segmp != nullptr) {
		Xtrk3D::SEGMENT_TYPE* p = segmp;
		segmp = segmp->next;
		switch (p->type) {
		case 'Y':
		case 'F':
			//delete point array
			free(p->data_.pointArray.points);
			break;
		case 'Z':
			//delete string
			free(p->data_.textString.str);
			break;
		}
		delete p;
	}
	if (obj->label != nullptr) {
		delete obj->label;
	}
	Xtrk3D::OBJ_TYPE* nextObj = obj->next;
	delete obj;
	return nextObj;
}

void MyDoc::parseObject(std::istream& in, Xtrk3D::OBJ_TYPE* obj)
{
	std::map <std::string, enum xtrkType> Token =
	{
		{"END",XtrkEnd}
		,{"END$SEGS",XtrkEnd}
		,{"END$TRACKS",XtrkEnd}
	};

	while (in.good()) {
		Parser CS(&in);
		if (CS.getNextToken(Token) == XtrkEnd)
		{
			break;
		}
		switch (CS.GetToken(0)[0]) {
		case 'E':
		case 'T':
		{
			Xtrk3D::ENDPOINT_TYPE* endp = new Xtrk3D::ENDPOINT_TYPE;
			int index = 0;
			endp->position.z = 0;
			endp->index = -1;
			std::string RecordType = CS.GetToken(index++);
			if (RecordType[0] == 'T') {
				endp->index = std::stoi(CS.GetToken(index++));
			}
			endp->position.x = std::stof(CS.GetToken(index++));
			endp->position.y = std::stof(CS.GetToken(index++));
			endp->angle = std::stof(CS.GetToken(index++));
			endp->options = 0;
			if (CS.count() > index) {
				int Z_def = std::stoi(CS.GetToken(index++));
				if ((Z_def & 0x7) < 3) {
					index++;
					index++;
					if (CS.count() > index)
						endp->position.z = std::stof(CS.GetToken(index));
				}
				endp->options = Z_def;
			}
			insertEndPointList(obj, endp);
			break;
		}
		case 'S':
		{
			if (CS.GetToken(0)[1] == 'U') // SUBSEGS or SUBSEND
			{
				break;
			}
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'S';
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			segp->data_.two_points.p1.x = std::stof(CS.GetToken(3));
			segp->data_.two_points.p1.y = std::stof(CS.GetToken(4));
			segp->data_.two_points.p1.z = 0;
			segp->data_.two_points.p2.x = std::stof(CS.GetToken(5));
			segp->data_.two_points.p2.y = std::stof(CS.GetToken(6));
			segp->data_.two_points.p2.z = 0;
			insertSegmentList(obj, segp);
			break;
		}
		case 'C':
		{
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'C';
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			segp->data_.curve.radius = std::stof(CS.GetToken(3));
			segp->data_.curve.center.x = std::stof(CS.GetToken(4));
			segp->data_.curve.center.y = std::stof(CS.GetToken(5));
			segp->data_.curve.center.z = 0;
			segp->data_.curve.a0 = std::stof(CS.GetToken(6));
			segp->data_.curve.a1 = std::stof(CS.GetToken(7));
			insertSegmentList(obj, segp);
			break;
		}
		case 'L': //Straight line segment L3
		{
			//L3<sp>color<sp>width<sp>x1<sp>y1<sp>0<sp>x2<sp>y2<sp>0
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'L';
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			segp->data_.two_points.p1.x = std::stof(CS.GetToken(3));
			segp->data_.two_points.p1.y = std::stof(CS.GetToken(4));
			segp->data_.two_points.p1.z = 0;
			segp->data_.two_points.p2.x = std::stof(CS.GetToken(6));
			segp->data_.two_points.p2.y = std::stof(CS.GetToken(7));
			segp->data_.two_points.p2.z = 0;
			insertSegmentList(obj, segp);
			break;
		}
		case 'A': //Curved line segment A3
		{
			//documentation incorrect, extra arg 6 is 0
			//A3<sp>color<sp>line-width<sp>radius<sp>center-X<sp>center-Y<sp>0<sp>angle<sp>degrees of swing.
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'A';
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			segp->data_.curve.radius = std::stof(CS.GetToken(3));
			segp->data_.curve.center.x = std::stof(CS.GetToken(4));
			segp->data_.curve.center.y = std::stof(CS.GetToken(5));
			segp->data_.curve.center.z = 0;
			segp->data_.curve.a0 = std::stof(CS.GetToken(7));
			segp->data_.curve.a1 = std::stof(CS.GetToken(8));
			insertSegmentList(obj, segp);
			break;
		}
		case 'G': //Filled circle segment G3
		{
			//G3<sp>color<sp>width<sp>radius<sp>center-X<sp>center-Y<sp>0
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'G';
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			segp->data_.curve.radius = std::stof(CS.GetToken(3));
			segp->data_.curve.center.x = std::stof(CS.GetToken(4));
			segp->data_.curve.center.y = std::stof(CS.GetToken(5));
			segp->data_.curve.center.z = 0;
			insertSegmentList(obj, segp);
			break;
		}
		case 'Y': //Poly segment Y/Y3/Y4
		case 'F': //Filled poly segment F/F3/F4
		{
			//Y3<sp>color<sp>width<sp>end-points
			//Y4<sp>color<sp>width<sp>end-points<sp>polygon-type
			//F3<sp>color<sp>width<sp>end-points
			//F4<sp>color<sp>width<sp>#end-points<sp>polygon-type
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = CS.GetToken(0)[0];
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			int size = std::stoi(CS.GetToken(3));
			segp->data_.pointArray.mode = CS.count() > 4 ? std::stoi(CS.GetToken(4)) : 0;
			segp->data_.pointArray.count = size;
			Xtrk3D::VEC3* p = (Xtrk3D::VEC3*) malloc(size * sizeof(Xtrk3D::VEC3)); //new Xtrk3D::VEC3[size];
			segp->data_.pointArray.points = p;
			while (size-- > 0) {
				if (CS.parseNextRow()) {
					p->x = std::stof(CS.GetToken(0));
					p->y = std::stof(CS.GetToken(1));
					p->z = 0;
					p++;
				}
			} //*/
			insertSegmentList(obj, segp);
			break;
		}
		case 'Z': //Text segment Z
		{
			//Z<sp>color<sp>X-Offset<sp>Y-Offset<sp>angle<sp>0<sp>height<sp>"Text"
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'Z';
			segp->color = std::stoi(CS.GetToken(1));
			segp->data_.textString.pos.x = std::stof(CS.GetToken(2));
			segp->data_.textString.pos.y = std::stof(CS.GetToken(3));
			segp->data_.textString.pos.z = 0;
			segp->data_.textString.angle = std::stof(CS.GetToken(4));
			segp->data_.textString.height = std::stof(CS.GetToken(6));
			std::string s(CS.GetToken(7));
			char* strbuf = (char*)malloc((s.size() + 1) * sizeof(char));
			strcpy(strbuf, s.c_str());
			segp->data_.textString.str = strbuf;
			insertSegmentList(obj, segp);
			break;
		}
		case 'Q': //table-edge line Q3
		{
			//Q3<sp>0<sp>0.187500<sp>startX<sp>startY<sp>0<sp>endX<sp>endY<sp>0
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'Q';
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			segp->data_.two_points.p1.x = std::stof(CS.GetToken(3));
			segp->data_.two_points.p1.y = std::stof(CS.GetToken(4));
			segp->data_.two_points.p1.z = 0;
			segp->data_.two_points.p2.x = std::stof(CS.GetToken(6));
			segp->data_.two_points.p2.y = std::stof(CS.GetToken(7));
			segp->data_.two_points.p2.z = 0;
			insertSegmentList(obj, segp);
			break;
		}
		case 'B': //benchwork line B3
		{
			//B3<sp>color<sp>width<sp>startX<sp>startY<sp>0<sp>endX<sp>endY<sp>0<sp>lumbersize
			Xtrk3D::SEGMENT_TYPE* segp = new Xtrk3D::SEGMENT_TYPE;
			segp->type = 'B';
			segp->color = std::stoi(CS.GetToken(1));
			segp->width = std::stof(CS.GetToken(2));
			segp->data_.two_points.p1.x = std::stof(CS.GetToken(3));
			segp->data_.two_points.p1.y = std::stof(CS.GetToken(4));
			segp->data_.two_points.p1.z = 0;
			segp->data_.two_points.p2.x = std::stof(CS.GetToken(6));
			segp->data_.two_points.p2.y = std::stof(CS.GetToken(7));
			segp->data_.two_points.p2.z = 0;
			segp->data_.two_points.mode = std::stoi(CS.GetToken(9));
			insertSegmentList(obj, segp);
			break;
		}
		case 'X': //special segment
			break;
		}
	}
}

