///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __NONAME_H__
#define __NONAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>
#include <wx/clrpicker.h>
#include <wx/slider.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class BasePlaneMenu
///////////////////////////////////////////////////////////////////////////////
class BasePlaneMenu : public wxDialog 
{
	private:
	
	protected:
		wxStaticText* m_staticText53;
		wxTextCtrl* m_textCtrl1;
		wxStaticText* m_staticText54;
		wxCheckBox* m_checkBox1;
		wxStaticText* m_staticText6;
		wxColourPickerCtrl* m_colourPicker1;
		wxStaticText* m_staticText4;
		wxSlider* m_slider2;
		wxStaticText* m_staticText5;
	
	public:
		
		BasePlaneMenu( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Benchwork properties"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE ); 
		~BasePlaneMenu();
	
};

#endif //__NONAME_H__
